package me.Lorinth.RPGE.Dungeons;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Items.Gear;
import me.Lorinth.RPGE.Managers.DungeonManager;
import me.Lorinth.RPGE.Parties.CharacterParty;
import net.elseland.xikage.MythicMobs.API.Mobs;
import net.elseland.xikage.MythicMobs.API.MythicMobDeathEvent;
import net.elseland.xikage.MythicMobs.API.MythicMobSpawnEvent;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import com.sk89q.worldedit.bukkit.selections.Selection;

@SuppressWarnings("deprecation")
public class Dungeon implements Listener{

	private DungeonManager manager;
	public String name;
	private Integer id;
	public Location spawn;
	private CharacterParty currentParty = null;
	private ArrayList<CharacterParty> queuedParties = new ArrayList<CharacterParty>();
	private boolean inProgress = false;
	
	private Integer expReward = 0;
	private Integer goldReward = 0;
	private Random random = new Random();
	
	private String finalBoss = "";
	
	private boolean ready = true;
	public Location lobbyMin;
	public Location lobbyMax;
	public Integer level = 20;
	private Integer minPartySize = 1;
	private Integer maxPartySize = 5;
	
	//private HashMap<Integer, Location> locationIndex = new HashMap<Integer, Location>();
	private HashMap<Location, EntityType> monsterSpawns = new HashMap<Location, EntityType>();
	private ArrayList<Entity> spawnedMobs = new ArrayList<Entity>();
	private HashMap<Location, String> bossnames = new HashMap<Location, String>();
	private HashMap<String, Block> bossLoot = new HashMap<String, Block>();
	private HashMap<Block, HashMap<Player, Inventory>> chestStorage = new HashMap<Block, HashMap<Player, Inventory>>();
	
	public Location exit;
	private ArrayList<Gear> gear = new ArrayList<Gear>();
	private ArrayList<Chunk> chunks = new ArrayList<Chunk>();
	
	private ArrayList<String> completed = new ArrayList<String>();
	
	private BukkitTask checkTask = null;
	
	public Dungeon(DungeonManager manage, Integer id){
		this.id = id;
		manager = manage;
		
		Bukkit.getPluginManager().registerEvents(this, manager.getMain());
	}
	
	public void addGear(Gear gea){
		gear.add(gea);
	}
	
	public boolean isInProgress(){
		return inProgress;
	}
	
	public boolean isReady(){
		return ready;
	}
	
	public void create(String name, Location start){
		this.name = ChatColor.DARK_RED + name + ChatColor.RESET;
		this.spawn = start;
		save();
	}
	
	public void queueParty(CharacterParty party){
		if(party.getSize() >= minPartySize){
			if(party.getSize() <= maxPartySize){
				Player p = checkPartyPlayers(party);
				try{
					if(p != null){
						party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]" + ChatColor.GOLD + " " + p.getName() + " has completed this dungeon too recently.");
						return;
					}
				}
				catch(NullPointerException e){
					
				}
				if(queuedParties.contains(party)){
					return;
					//party.leader.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: You are already queued...");
				}
				party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]" + ChatColor.GOLD + " Your party has been queued for " + name);
				queuedParties.add(party);
				checkTask = Bukkit.getScheduler().runTaskLaterAsynchronously(manager.getMain(), new Runnable(){
		
					@Override
					public void run() {
						if(checkReady()){
							checkTask.cancel();
						}
					}
					
					
					
				}, 100);
			}
			else{
				manager.sendMessage(party.leader.owner, ChatColor.GOLD + "Your party isn't large enough! Need less than " + this.maxPartySize + " players to queue!");
			}
		}
		else{
			manager.sendMessage(party.leader.owner, ChatColor.GOLD + "Your party isn't large enough! Need at least " + this.minPartySize + " players to queue!");
		}
	}
	
	public boolean checkReady(){
		if(!inProgress){
			ready = true;
			
			ArrayList<CharacterParty> copied = (ArrayList<CharacterParty>) queuedParties.clone();
			
			for(CharacterParty p : copied){
				if(p.isEmpty()){
					queuedParties.remove(p);
				}
				else{
					p.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]" + " " + name + ChatColor.GOLD + " is ready to enter");
					p.leader.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] Use " + ChatColor.GOLD + "/dungeon join" + ChatColor.DARK_PURPLE + " to send you and your party.");
					manager.joinDungeons.put(p.leader.owner, this);
					return true;
				}
			}
			return false;
		}
		else{
			ready = false;
			return false;
		}
	}

	public Player checkPartyPlayers(CharacterParty party){
		for(Player p : party.getPlayers()){
			if(this.completed.contains(p.getName())){
				return p;
			}
		}
		return null;
	}
	
	public void setExit(Player player){
		exit = player.getLocation();
		manager.sendMessage(player, ChatColor.GOLD + "You have set the exit for " + name);
		save();
	}
	
	public void removeCurrentParty(){
		currentParty.teleportToLocationMessage(exit, ChatColor.DARK_PURPLE + "[Dungeon]" + ChatColor.GOLD + " You have been removed from " + name);
		currentParty.unSync();
		currentParty = null;
		inProgress = false;
	}
	
	public boolean isBetween(Location loc, Location c1, Location c2){
		if(c1.getBlockX() <= loc.getBlockX() && loc.getBlockX() <= c2.getBlockX()){
			if(c1.getBlockZ() <= loc.getBlockZ() && loc.getBlockZ() <= c2.getBlockZ()){
				if(c1.getBlockY() <= loc.getBlockY() && loc.getBlockY() <= c2.getBlockY()){
					return true;
				}
			}
		}
		return false;
	}
	
	public void unregister(){
		HandlerList.unregisterAll(this);
	}
	
	public void checkPlayersInLobby(){
		if(!isLoaded()){
			return;
		}
		
		Integer x1 = lobbyMin.getChunk().getX();
		Integer x2 = lobbyMax.getChunk().getX();
		Integer z1 = lobbyMin.getChunk().getZ();
		Integer z2 = lobbyMax.getChunk().getZ();
		
		
		for(int x = x1; x <= x2; x++){
			for(int z = z1; z <= z2; z++){
				Chunk c = lobbyMin.getWorld().getChunkAt(x, z);
				for (Entity e :c.getEntities()){
					if(e instanceof Player){
						if(isBetween(e.getLocation(), lobbyMin, lobbyMax)){
							Player player = (Player) e;
							CharacterParty party = manager.getMain().getCharacterManager().getCharacter(player).currentParty;
							if(party == null){
								player.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] " + ChatColor.GOLD + "You need to be in a party to join dungeons!");
							}
							else{
								if(party.isLeader(player)){
									queueParty(manager.getMain().getCharacterManager().getCharacter(player).currentParty);
								}
								else{
									manager.sendMessage(player, "You must be the leader to queue your party.");
								}
							}
						}
					}
				}
			}
		}
	}
	
	public boolean isLoaded(){
		try{
			if(lobbyMin.getChunk().isLoaded()){
				return true;
			}
			else{
				return false;
			}
		}
		catch(NullPointerException e){
			return false;
		}
		
	}
	
	public void setLobby(Player player){
		Selection sel = manager.getMain().wep.getSelection(player);
		if(sel == null){
			manager.sendMessage(player, ChatColor.RED + "not region selected");
		}
		else{
			lobbyMin = sel.getMinimumPoint();
			lobbyMax = sel.getMaximumPoint();
			manager.sendMessage(player, ChatColor.GOLD + "You have set the lobby for " + name);
		}
		
		save();
	}
	
	public void sendQueuedParty(){
		final Dungeon dung = this;
		
		final CharacterParty party = queuedParties.get(0);
		if(checkPartyPlayers(party) == null){
		
			party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] Entering " + name + ChatColor.DARK_PURPLE + " in 10...");
			party.setDungeon(dung);
			this.removeCreatures();
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(manager.getMain(), new Runnable(){
	
				@Override
				public void run() {
					party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] 5...");
					
				}
				
			}, 100);
			Bukkit.getScheduler().scheduleSyncDelayedTask(manager.getMain(), new Runnable(){
	
				@Override
				public void run() {
					party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] 4...");
					
				}
				
			}, 120);
			Bukkit.getScheduler().scheduleSyncDelayedTask(manager.getMain(), new Runnable(){
	
				@Override
				public void run() {
					party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] 3...");
					
				}
				
			}, 140);
			Bukkit.getScheduler().scheduleSyncDelayedTask(manager.getMain(), new Runnable(){
	
				@Override
				public void run() {
					party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] 2...");
					
				}
				
			}, 160);
			Bukkit.getScheduler().scheduleSyncDelayedTask(manager.getMain(), new Runnable(){
	
				@Override
				public void run() {
					party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] 1...");
					
				}
				
			}, 180);
			Bukkit.getScheduler().scheduleSyncDelayedTask(manager.getMain(), new Runnable(){
	
				@Override
				public void run() {
					currentParty = party;
					currentParty.teleportToLocationMessage(spawn.clone().add(0, 1, 0), ChatColor.DARK_PURPLE + "[Dungeon]" + ChatColor.GOLD + " Welcome to " + name);
					currentParty.syncParty(dung.level);
					queuedParties.remove(0);
					inProgress = true;
					
					currentParty.setDungeon(dung);
					
					for(Player p : currentParty.getPlayers()){
						completed.add(p.getName());
					}				
					start();
				}
				
			}, 200);
		}
		else{
			party.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]" + ChatColor.GOLD + " " + checkPartyPlayers(party) + " has completed this dungeon too recently.");
			return;
		}
	}
	
	public void start(){
		loadChunks();
		clearMobsInChunks();
		
		spawnCreatures();
		
		spawnBosses();
		
		
	}
	
	private void loadChunks(){
		Chunk chunk;
		for(Entity ent : this.spawnedMobs){
			chunk = ent.getLocation().getChunk();
			if(!chunk.isLoaded()){
				if(!chunks.contains(chunk)){
					chunk.load();
					chunks.add(chunk);
				}
				
			}
		}
	}
	
	private void unloadChunks(){
		for(Chunk c : chunks){
			for(Entity e : c.getEntities()){
				e.remove();
			}
			c.unload();
		}
	}
	
	private void clearMobsInChunks(){
		for(Chunk c : chunks){
			for(Entity e : c.getEntities()){
				e.remove();
			}
		}
	}
	
	public void end(){
		removeCreatures();
		unloadChunks();
		
		currentParty.removeDungeon();
		inProgress = false;
		
		checkReady();
	}
	
	private void removeCreatures(){
		for(Entity ent : this.spawnedMobs){
			ent.remove();
		}
	}
	
	@EventHandler
	public void onEntityBurn(EntityCombustEvent event){
		Entity ent = event.getEntity();
		if(this.spawnedMobs.contains(ent)){
			event.setCancelled(true);
			event.setDuration(0);
		}
	}
	
	@EventHandler
	public void onChestOpen(PlayerInteractEvent event){
		Player player = event.getPlayer();
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(event.getClickedBlock().getType() == Material.CHEST){
				Block chest = event.getClickedBlock();
				if(chestStorage.containsKey(chest)){
					if(chestStorage.get(chest).containsKey(player)){
						player.openInventory(chestStorage.get(chest).get(player));
						event.setCancelled(true);
						//player.openInventory(chestStorage.get(chest).get(player));
					}
					else{
						player.sendMessage(ChatColor.RED + "That chest is locked! Kill the boss!");
						event.setCancelled(true);
					}
				}
			}
		}
	}
	
	public void onFinish(){
		final CharacterParty current = currentParty;
		for(Entity ent : this.spawnedMobs){
			ent.remove();
		}
		try{
			for(Character cha : currentParty.getCharacters()){
				cha.sendMessage("");
				cha.sendMessage("");
				cha.sendMessage(ChatColor.GREEN + "You have finished, " + name + "!");
				cha.sendMessage(ChatColor.GOLD + "========== Rewards ==========");
				cha.giveExp(expReward);
				cha.giveGold(goldReward, false);
				cha.sendMessage(ChatColor.GOLD + "=============================");
			}
		}
		catch(NullPointerException e2){
			//Meh, no clue
		}
		currentParty.leader.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: please use "+ ChatColor.GOLD + "/dungeon leave" + ChatColor.DARK_PURPLE + ", to exit when you are done.");
		currentParty.leader.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: You will be removed in 1 minute.");
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(manager.getMain(), new Runnable(){

			@Override
			public void run() {
				try{
					if(currentParty.equals(current)){
						kickParty();
					}
				}
				catch(NullPointerException e){
					//Empty
				}
			}
			
		}, 6000);
	}
	
	//on boss death, check if it was a boss here
	@EventHandler
	public void onBossDeath(MythicMobDeathEvent event){
		//For players in party, set inventory loot for chest for that player
		String mob = event.getMobType().getInternalName();
		try{
			if(this.bossLoot.containsKey(mob)){
				Block chest = bossLoot.get(mob);
				HashMap<Player, Inventory> storage = new HashMap<Player, Inventory>();
				Inventory inv;
				for(Player player : currentParty.getPlayers()){
					inv = generateLoot(event.getLivingEntity().getCustomName());
					storage.put(player, inv);
					player.sendMessage(ChatColor.GOLD + "[Loot] Chest unlocked!");
				}
				
				chestStorage.put(chest, storage);
				
			}
		}
		catch(NullPointerException e){
			//System.out.println("Failed to spawn loot for... " + mob);
			e.printStackTrace();
		}
		if(finalBoss.toLowerCase().equals(mob.toLowerCase())){
			onFinish();
		}
	}
	
	private Inventory generateLoot(String name) {
		Inventory inv = Bukkit.createInventory(null, 9, ChatColor.GOLD + "Loot of " + name);
		
		int Amount = 2;
		
		int amtRoll = random.nextInt(100);
		if(amtRoll > 50){
			Amount = 3;
		}
		
		int choice = 0;
		for(int i = 0; i<Amount; i++){
			try{
				choice = random.nextInt(gear.size());
			}
			catch(IllegalArgumentException e){
				//no gear
				break;
			}
			Gear gear = this.gear.get(choice);
			
			String rarity = "";
			rarity = manager.calculateRarity(this.level);
			ItemStack item = gear.spawnItem(rarity, this.manager.getTitles(gear, rarity));
			try{
				inv.addItem(item);
			}
			catch(NullPointerException e){
				
			}
		}
		
		return inv;
	}

	public void setLevel(Integer Level){
		level = Level;
	}
	
	public void spawnCreatures(){
		Entity ent;
		for(Location loc : monsterSpawns.keySet()){
			ent = MobDifficulty.getPlugin().spawnDungeonMob(loc, monsterSpawns.get(loc));
			if(ent instanceof Skeleton){
				if(((Skeleton)ent).getSkeletonType() == SkeletonType.NORMAL){
					((Skeleton)ent).getEquipment().setItemInHand(new ItemStack(Material.BOW));
				}
			}
			spawnedMobs.add(ent);
			((Creature)ent).setRemoveWhenFarAway(false);
		}
	}
	
	public void spawnBosses(){
		for(Location loc : bossnames.keySet()){
			String name = bossnames.get(loc);
			if(!Mobs.spawnMythicMob(name, loc)){
				System.out.println("[ERROR] " + name + " is spawning fake boss, " + name);
			}
		}
	}
	
	@EventHandler
	public void onBossSpawn(MythicMobSpawnEvent event){
		if(this.bossnames.values().contains(event.getMobType().getInternalName())){
			spawnedMobs.add(event.getLivingEntity());
		}
		//System.out.println(event.getMobType().getInternalName());
	}
	
	public void save(){
		String directory = manager.getMain().getDataFolder() + File.separator + "Dungeons";
		File file = new File(directory + File.separator + id.toString() + ".yml");
		YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);
		
		
		yml.set("Name", ChatColor.stripColor(name));
		yml.set("Level", level);
		yml.set("Exp", this.expReward);
		yml.set("Gold", this.goldReward);
		yml.set("FinalBoss", this.finalBoss);
		yml.set("MinSize", this.minPartySize);
		yml.set("MaxSize", this.maxPartySize);
		yml.set("Start.X", (double) spawn.getX());
		yml.set("Start.Y", (double) spawn.getY());
		yml.set("Start.Z", (double) spawn.getZ());
		yml.set("Start.World", spawn.getWorld().getName());
		
		try{
			yml.set("Exit.X", (double) exit.getX());
			yml.set("Exit.Y", (double) exit.getY());
			yml.set("Exit.Z", (double) exit.getZ());
			yml.set("Exit.World", exit.getWorld().getName());
		}
		catch(NullPointerException e){
			yml.set("Exit.X", (double) spawn.getX());
			yml.set("Exit.Y", (double) spawn.getY());
			yml.set("Exit.Z", (double) spawn.getZ());
			yml.set("Exit.World", "world2");
		}
		
		try{
			yml.set("Lobby.min.X", (double) lobbyMin.getX());
			yml.set("Lobby.min.Y", (double) lobbyMin.getY());
			yml.set("Lobby.min.Z", (double) lobbyMin.getZ());
			yml.set("Lobby.World", lobbyMin.getWorld().getName());
		}
		catch(NullPointerException e){
			yml.set("Lobby.min.X", 0.0);
			yml.set("Lobby.min.Y", 0.0);
			yml.set("Lobby.min.Z", 0.0);
			yml.set("Lobby.World", "world2");
		}
		
		try{
			yml.set("Lobby.max.X", (double) lobbyMax.getX());
			yml.set("Lobby.max.Y", (double) lobbyMax.getY());
			yml.set("Lobby.max.Z", (double) lobbyMax.getZ());
		}
		catch(NullPointerException e){
			yml.set("Lobby.max.X", 0.0);
			yml.set("Lobby.max.Y", 0.0);
			yml.set("Lobby.max.Z", 0.0);
		}
		
		Integer count = 0;
		for(Location loc : this.monsterSpawns.keySet()){
			try{
				count += 1;
				EntityType type = monsterSpawns.get(loc);
				yml.set("MonsterSpawns." + count + ".Type", type.name());
				yml.set("MonsterSpawns." + count + ".X", loc.getX());
				yml.set("MonsterSpawns." + count + ".Y", loc.getY());
				yml.set("MonsterSpawns." + count + ".Z", loc.getZ());
				yml.set("MonsterSpawns." + count + ".World", loc.getWorld().getName());
			}
			catch(NullPointerException e){
				//System.out.println(loc.toString());
			}
		}
		
		count = 0;
		for(Location loc : this.bossnames.keySet()){
			try{
				count += 1;
				String type = bossnames.get(loc);
				yml.set("BossSpawns." + count + ".Type", type);
				yml.set("BossSpawns." + count + ".X", loc.getX());
				yml.set("BossSpawns." + count + ".Y", loc.getY());
				yml.set("BossSpawns." + count + ".Z", loc.getZ());
				yml.set("BossSpawns." + count + ".World", loc.getWorld().getName());
				
				Location locC = bossLoot.get(type).getLocation();
				
				yml.set("BossSpawns." + count + ".Chest.X", locC.getBlockX()); 
	        	yml.set("BossSpawns." + count + ".Chest.Y", locC.getBlockY()); 
	        	yml.set("BossSpawns." + count + ".Chest.Z", locC.getBlockZ()); 
				
			}
			catch(NullPointerException e){
				System.out.println(loc.toString());
			}
		}
		
		
		try {
			yml.save(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addSpawn(Location loc, EntityType type, boolean save) {
		monsterSpawns.put(loc, type);
		if(save){
			this.save();
		}
		//player.sendMessage("Added spawn point for, " + type.name() + ", with id");
	}

	public void addBossSpawn(Location location, String Name, Block block, boolean save) {
		bossnames.put(location, Name);
		bossLoot.put(Name, block);
		if(save){
			this.save();
		}
	}

	public void setRewards(int gold, int exp) {
		this.goldReward = gold;
		this.expReward = exp;
	}

	public void setFinalBoss(String type) {
		finalBoss = type;
	}
	
	public void kickParty() {
		currentParty.teleportToLocation(exit);
		//currentParty.teleportToLocation(exit);
		currentParty.setDungeon(null);
		currentParty.unSync();
		this.inProgress = false;
		currentParty = null;
		
		end();
	}

	public void setSize(Integer min, Integer max) {
		this.minPartySize = min;
		this.maxPartySize = max;
		
	}
}
