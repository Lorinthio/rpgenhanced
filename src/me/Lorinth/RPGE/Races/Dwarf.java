package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.DwarfMining;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Dwarf extends Race{

	public Dwarf(){
		super("Dwarf", new ItemStack(Material.IRON_PICKAXE, 1));
		
		this.setDescription("You were born deep underground, and have a natural ability for mining.");
		
		this.addPassive(new DwarfMining());
		
		this.addAttribute(StatType.STRENGTH, 2);
		this.addAttribute(StatType.CONSTITUTION, 4);
		
		this.addAttribute(StatType.AGILITY, -2);
		this.addAttribute(StatType.WISDOM, -2);
		this.addAttribute(StatType.INTELLIGENCE, -2);
	}
	
}
