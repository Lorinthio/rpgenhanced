package me.Lorinth.RPGE.Races;

import java.util.ArrayList;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public abstract class Race {

	private String name;
	private String description = "";
	
	private Integer Strength = 10;
	private Integer Constitution = 10;
	private Integer Dexterity = 10;
	private Integer Agility = 10;
	private Integer Intelligence = 10;
	private Integer Wisdom = 10;
	
	private ItemStack icon;
	
	private ArrayList<ActiveSkill> actives = new ArrayList<ActiveSkill>();
	private ArrayList<PassiveSkill> passives = new ArrayList<PassiveSkill>();
	
	public Race(String name, ItemStack item){
		this.name = name;
		icon = item;
		ItemMeta im = icon.getItemMeta();
		im.setDisplayName(ChatColor.GRAY + name);
		icon.setItemMeta(im);
	}
	
	public void apply(Character cha){
		cha.active_skills.addAll(actives);
		cha.passive_skills.addAll(passives);
		for(PassiveSkill skill : passives){
			skill.apply(cha);
		}
	}
	
	public void remove(Character cha){
		cha.active_skills.removeAll(actives);
		cha.passive_skills.removeAll(passives);
		for(PassiveSkill skill : passives){
			skill.remove(cha);
		}
	}
	
	public void setDescription(String descrip){
		description = descrip;
	}
	
	public void create(Character cha){
		cha.Strength = Strength;
		cha.Constitution = Constitution;
		cha.Dexterity = Dexterity;
		cha.Agility = Agility;
		cha.Intelligence = Intelligence;
		cha.Wisdom = Wisdom;
	}
	
	public void addActive(ActiveSkill active){
		actives.add(active);
	}
	
	public void addPassive(PassiveSkill passive){
		passives.add(passive);
	}
	
	public ItemStack getIcon(){
		return icon;
	}
	
	public void setAttribute(StatType type, Integer value){
		switch(type){
		case AGILITY:
			this.Agility = value;
			break;
		case CONSTITUTION:
			this.Constitution = value;
			break;
		case DEXTERITY:
			this.Dexterity = value;
			break;
		case INTELLIGENCE:
			Intelligence = value;
			break;
		case STRENGTH:
			Strength = value;
			break;
		case WISDOM:
			Wisdom = value;
			break;
		}
	}
	
	public void addAttribute(StatType type, Integer value){
		switch(type){
		case AGILITY:
			this.Agility += value;
			break;
		case CONSTITUTION:
			this.Constitution += value;
			break;
		case DEXTERITY:
			this.Dexterity += value;
			break;
		case INTELLIGENCE:
			Intelligence += value;
			break;
		case STRENGTH:
			Strength += value;
			break;
		case WISDOM:
			Wisdom += value;
			break;
		}
	}
	
	public Integer getAttribute(StatType type){
		switch(type){
		case AGILITY:
			return Agility;
		case CONSTITUTION:
			return Constitution;
		case DEXTERITY:
			return Dexterity;
		case INTELLIGENCE:
			return Intelligence;
		case STRENGTH:
			return Strength;
		case WISDOM:
			return Wisdom;
		}
		
		return 10;
	}
	
	public String getName(){
		return name;
	}
	
	public String getDescription(){
		return description;
	}

	public ArrayList<ActiveSkill> getActives(){
		return actives;
	}
	
	public ArrayList<PassiveSkill> getPassives(){
		return passives;
	}
	
	public void printStats(Player player) {
		player.sendMessage(ChatColor.RED + "STR" + ChatColor.GRAY + " : " + Strength );
		player.sendMessage(ChatColor.GOLD + "CON" + ChatColor.GRAY + " : " + Constitution );
		player.sendMessage(ChatColor.DARK_GREEN + "DEX" + ChatColor.GRAY + " : " + Dexterity );
		player.sendMessage(ChatColor.GREEN + "AGI" + ChatColor.GRAY + " : " + Agility );
		player.sendMessage(ChatColor.BLUE + "WIS" + ChatColor.GRAY + " : " + Wisdom );
		player.sendMessage(ChatColor.DARK_PURPLE + "INT" + ChatColor.GRAY + " : " + Intelligence );
		
		if(!actives.isEmpty()){
			player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Actives");
			for(ActiveSkill skill : actives){
				player.sendMessage(skill.getName() + skill.getDescription());
			}
		}
		if(!passives.isEmpty()){
			player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Passives");
			for(PassiveSkill skill : passives){
				player.sendMessage(skill.getName() + skill.getDescription());
			}
		}
	}
}
