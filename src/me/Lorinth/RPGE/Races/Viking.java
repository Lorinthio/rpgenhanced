package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.BloodOfTheNorth;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Viking extends Race {

	public Viking(){
		super("Viking", new ItemStack(Material.IRON_AXE, 1));
		
		this.setDescription("Vikings were born to keep watch of the tundras");
		
		this.addAttribute(StatType.STRENGTH, 4);
		this.addAttribute(StatType.CONSTITUTION, 3);
		
		this.addAttribute(StatType.AGILITY, -1);
		this.addAttribute(StatType.WISDOM, -3);
		this.addAttribute(StatType.INTELLIGENCE, -3);
		
		this.addPassive(new BloodOfTheNorth());
	}
	
}
