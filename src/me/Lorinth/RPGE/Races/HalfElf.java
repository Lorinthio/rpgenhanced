package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class HalfElf extends Race{

	public HalfElf(){
		super("Half Elf", new ItemStack(Material.LEATHER_BOOTS, 1));
		
		this.setDescription("Born of a human and an elf, these beings have an open mind and are more nimble.");
		
		this.addAttribute(StatType.DEXTERITY, 1);
		this.addAttribute(StatType.WISDOM, 1);
		
		this.addAttribute(StatType.STRENGTH, -1);
		this.addAttribute(StatType.CONSTITUTION, -1);
	}
	
}
