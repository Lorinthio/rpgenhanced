package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.EntientRegeneration;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Entient extends Race{

	public Entient(){
		super("Entient", new ItemStack(Material.SAPLING, 1));
		
		this.setDescription("You grew from the ground, and gained insight to fight evil");
		
		this.addAttribute(StatType.CONSTITUTION, 3);
		this.addAttribute(StatType.AGILITY, -4);
		this.addAttribute(StatType.DEXTERITY, -2);
		this.addAttribute(StatType.WISDOM, 3);
		
		this.addPassive(new EntientRegeneration());
	}
	
}
