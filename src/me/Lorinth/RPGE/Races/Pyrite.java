package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.PyriteMoltenSkin;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Pyrite extends Race{

	//Higher intelligence
	//Burn on hit
	
	public Pyrite(){
		super("Pyrite", new ItemStack(Material.LAVA_BUCKET, 1));
		
		this.setDescription("You are a being born of fire. Your magic abilities are higher than other races. But your reflexes are slower.");
		
		this.addAttribute(StatType.DEXTERITY, -1);
		this.addAttribute(StatType.AGILITY, -3);
		this.addAttribute(StatType.INTELLIGENCE, 4);
		
		this.addPassive(new PyriteMoltenSkin());
	}
	
	
}
