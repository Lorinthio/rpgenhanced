package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.AvianFlight;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Avian extends Race{

	public Avian(){
		super("Avian", new ItemStack(Material.FEATHER, 1));
		
		this.setDescription("You were created to fly, and protect the skies.");
		
		this.addAttribute(StatType.STRENGTH, -2);
		this.addAttribute(StatType.CONSTITUTION, -1);
		this.addAttribute(StatType.AGILITY, 2);
		this.addAttribute(StatType.WISDOM, 1);
		
		this.addPassive(new AvianFlight());
	}
	
}
