package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.WoodElfMastery;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class WoodElf extends Race{

	public WoodElf(){
		super("Wood Elf", new ItemStack(Material.BOW, 1));
		
		this.setDescription("Born from the southern forests. You have been trained in the ways of the bow and of survival.");
		
		this.addPassive(new WoodElfMastery());
		
		this.addAttribute(StatType.DEXTERITY, 3);
		this.addAttribute(StatType.AGILITY, 1);
		
		this.addAttribute(StatType.STRENGTH, -2);
		this.addAttribute(StatType.CONSTITUTION, -1);
		this.addAttribute(StatType.INTELLIGENCE, -1);
	}
	
}
