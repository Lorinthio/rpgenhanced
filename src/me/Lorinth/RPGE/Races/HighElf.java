package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.HighElfPassive;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class HighElf extends Race{

	public HighElf(){
		super("High Elf", new ItemStack(Material.STICK, 1));
		
		setDescription("High elves are the nobles of their breed. They tend more to books than physical activity.");
		
		this.addPassive(new HighElfPassive());
		
		this.addAttribute(StatType.STRENGTH, -2);
		this.addAttribute(StatType.CONSTITUTION, -2);
		this.addAttribute(StatType.DEXTERITY, -2);
		
		this.addAttribute(StatType.WISDOM, 3);
		this.addAttribute(StatType.INTELLIGENCE, 3);
	}
	
}
