package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.HalfOrcMovementSpeed;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Halforc extends Race{
	
	public Halforc(){
		
		super("Half Orc", new ItemStack(Material.IRON_SPADE, 1));
		
		this.setDescription("Half Orcs have a higher physical rating, but are also clumsy and unintelligent");
		
		this.addAttribute(StatType.STRENGTH, 7);
		this.addAttribute(StatType.CONSTITUTION, 5);
		
		this.addAttribute(StatType.AGILITY, -1);
		this.addAttribute(StatType.WISDOM, -3);
		this.addAttribute(StatType.INTELLIGENCE, -3);
		
		this.addPassive(new HalfOrcMovementSpeed());
	}
	
}
