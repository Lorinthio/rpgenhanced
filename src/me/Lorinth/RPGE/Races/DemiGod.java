package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.AquosUnderwater;
import me.Lorinth.RPGE.Abilities.RacePassives.AvianFlight;
import me.Lorinth.RPGE.Abilities.RacePassives.PyriteMoltenSkin;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class DemiGod extends Race{

	public DemiGod(){
		super("Demi God", new ItemStack(Material.NETHER_STAR, 1));
		
		this.setDescription("You were created in the beginning of the Voxelverse.");
		
		this.addAttribute(StatType.STRENGTH, 5);
		this.addAttribute(StatType.CONSTITUTION, 5);
		this.addAttribute(StatType.DEXTERITY, 5);
		this.addAttribute(StatType.AGILITY, 5);
		this.addAttribute(StatType.WISDOM, 5);
		this.addAttribute(StatType.INTELLIGENCE, 5);
		
		PassiveSkill flight = new AvianFlight();
		flight.setName("Celestial Wings");
		flight.setDescription("You can double tap jump to flap your wings! (Max 3)");
		
		PassiveSkill lava = new PyriteMoltenSkin();
		lava.setName("Demi God Skin");
		
		PassiveSkill breathing = new AquosUnderwater();
		breathing.setName("Gilden Lungs");
		breathing.setDescription("You have no need for air");
		
		this.addPassive(flight);
		this.addPassive(lava);
		this.addPassive(breathing);
	}
	
}
