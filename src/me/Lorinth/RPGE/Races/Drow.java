package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.DrowNightMastery;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Drow extends Race{

	public Drow(){
		super("Drow", new ItemStack(Material.IRON_SWORD, 1));
		
		this.setDescription("You are an elf born of the night, and are stronger during the night");
		
		this.addAttribute(StatType.AGILITY, 1);
		this.addAttribute(StatType.INTELLIGENCE, 2);
		
		this.addAttribute(StatType.STRENGTH, -2);
		this.addAttribute(StatType.CONSTITUTION, -1);
		
		this.addPassive(new DrowNightMastery());
		
	}
	
}
