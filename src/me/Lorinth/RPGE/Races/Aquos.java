package me.Lorinth.RPGE.Races;

import me.Lorinth.RPGE.Abilities.RacePassives.AquosUnderwater;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Aquos extends Race{

	public Aquos(){
		super("Aquos", new ItemStack(Material.RAW_FISH, 1));
		
		this.setDescription("You are a being born of water. Your healing abilities are higher than other races.");
		
		this.addAttribute(StatType.DEXTERITY, -2);
		this.addAttribute(StatType.AGILITY, -2);
		this.addAttribute(StatType.WISDOM, 4);
		
		this.addPassive(new AquosUnderwater());
	}
	
}
