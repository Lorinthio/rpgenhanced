package me.Lorinth.RPGE.SkillAPI;

import java.util.ArrayList;
import java.util.List;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Events.SpellCastEvent;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public abstract class Skill {

	private Integer STR = 1;
	private Integer CON = 1;
	private Integer DEX = 1;
	private Integer AGI = 1;
	private Integer WIS = 1;
	private Integer INT = 1;
	
	private Integer healthCost = 0;
	private Integer manaCost = 0;
	private Integer staminaCost = 0;
	private Integer cooldown = 10;
	
	private String SkillName = "";
	private String description = "";
	
	private MagicType magicType = null;
	public boolean isMagic = false;
	
	private RpgEnhancedMain main = (RpgEnhancedMain)Bukkit.getPluginManager().getPlugin("RpgEnhanced");
	
	private ArrayList<Action> actions = new ArrayList<Action>();
	
	public void addAction(Action action){
		actions.add(action);
	}
	
	public String getName(){
		return SkillName;
	}
	
	public Skill(String name){
		SkillName = name;
	}

	public void setName(String name){
		this.SkillName = name;
	}
	
	public StatType getMainStat(){
		StatType main = null;
		Integer value = 0;
		
		if(STR > value){
			main = StatType.STRENGTH;
			value = STR;
		}
		if(CON > value){
			main = StatType.CONSTITUTION;
			value = CON;
		}
		if(DEX > value){
			main = StatType.DEXTERITY;
			value = DEX;
		}
		if(AGI > value){
			main = StatType.AGILITY;
			value = AGI;
		}
		if(WIS > value){
			main = StatType.WISDOM;
			value = WIS;
		}
		if(INT > value){
			main = StatType.INTELLIGENCE;
			value = INT;
		}
		
		return main;
	}
	
	public RpgEnhancedMain getMain(){
		return main;
	}
	
	public void setDescription(String descript){
		description = " : " + descript;
	}
	
	public String getDescription(){
		return description;
	}
	
	public void setStatRequirement(StatType stat, Integer value){
		switch(stat){
			case STRENGTH:
				this.STR = value + 10;
				break;
			case CONSTITUTION:
				this.CON = value + 10;
				break;
			case DEXTERITY:
				this.DEX = value + 10;
				break;
			case AGILITY:
				this.AGI = value + 10;
				break;
			case WISDOM:
				this.WIS = value + 10;
				break;
			case INTELLIGENCE:
				this.INT = value + 10;
				break;
		}
	}
	
	public void setVitalCost(Vital vital, Integer value){
		switch(vital){
			case HEALTH:
				this.healthCost = value;
				break;
			case MANA:
				this.manaCost = value;
				break;
			case STAMINA:
				this.staminaCost = value;
				break;
		}
	}
		
	public void setMagicType(MagicType mt){
		this.magicType = mt;
		this.isMagic  = true;
	}
	
	public MagicType getMagicType(){
		return magicType;
	}
	
	public void cast(final Player player){
		final Character cha = main.getCharacterManager().getCharacter(player);
		
		cha.removeCloak();
		
		//Check Health
		if(cha.Health - healthCost < 0){
			cha.sendMessage(ChatColor.RED + "[Spell] : Not enough " + ChatColor.GREEN + "Health" + ChatColor.RED + " to cast " + this.SkillName);
			return;
		}
		
		//Check Mana
		else if(cha.Mana - manaCost < 0){
			cha.sendMessage(ChatColor.RED + "[Spell] : Not enough " + ChatColor.BLUE + "Mana" + ChatColor.RED + " to cast " + this.SkillName);
			return;
		}
		
		//Check Stamina
		else if(cha.Stamina - staminaCost < 0){
			cha.sendMessage(ChatColor.RED + "[Spell] : Not enough " + ChatColor.YELLOW + "Stamina" + ChatColor.RED + " to cast " + this.SkillName);
			return;
		}
		
		
		
		List<Entity> entities = player.getNearbyEntities(20, 20, 20);
		for(Entity ent : entities){
			if(ent instanceof Player){
				((Player) ent).sendMessage(player.getDisplayName() + " has cast " + getName());
			}
		}
		
		final ArrayList<Entity> targets = new ArrayList<Entity>();
		
		final ArrayList<Integer> tasks = new ArrayList<Integer>();
		
		ActionResult success = ActionResult.Success;
		long totalDelay = 0;
		
		for(final Action action : actions){
			if(action.getDelay() > 0){
				totalDelay += action.getDelay();
			}

			
			if(totalDelay > 0){
				int task = Bukkit.getScheduler().scheduleSyncDelayedTask(getMain(), new Runnable(){
	
					@Override
					public void run() {
						
						ActionResult success = action.Act(player, main);
						if(success == ActionResult.Success){
							targets.addAll(action.getTargets());
						}
						else{
							for(Integer task : tasks){
								Bukkit.getScheduler().cancelTask(task);
							}
							if(success == ActionResult.NoVital){
								if(cha.Health <= healthCost){
									cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.GREEN + "Stamina" + ChatColor.RED + " to cast " + SkillName);
								}
								if(cha.Mana <= manaCost){
									cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.BLUE + "Mana" + ChatColor.RED + " to cast " + SkillName);
								}
								if(cha.Stamina <= staminaCost){
									cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.GOLD + "Stamina" + ChatColor.RED + " to cast " + SkillName);
								}
							}
							else if(success == ActionResult.NoTarget){
								cha.sendMessage(ChatColor.RED + "No target found for " + SkillName);
							}
							else if(success == ActionResult.NoResources){
								cha.sendMessage(ChatColor.RED + "You don't have enough consumables for that spell");
							}
						}
					}
					
				}, totalDelay);
				
				tasks.add(task);
			}
			else{
				success = action.Act(player, main);
				if(success == ActionResult.Success){
					targets.addAll(action.getTargets());
				}
				else{
					if(success == ActionResult.NoVital){
						if(cha.Health <= healthCost){
							cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.GREEN + "Stamina" + ChatColor.RED + " to cast " + SkillName);
						}
						if(cha.Mana <= manaCost){
							cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.BLUE + "Mana" + ChatColor.RED + " to cast " + SkillName);
						}
						if(cha.Stamina <= staminaCost){
							cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.GOLD + "Stamina" + ChatColor.RED + " to cast " + SkillName);
						}
					}
					else if(success == ActionResult.NoTarget){
						cha.sendMessage(ChatColor.RED + "No target found for " + SkillName);
					}
					else if(success == ActionResult.NoResources){
						cha.sendMessage(ChatColor.RED + "You don't have enough consumables for that spell");
					}
					break;
				}
			}
			
			
		}
		
		if(success == ActionResult.Success){
			String names = "";
			for(Entity ent : targets){
				if(ent instanceof Player){
					if(ent != player){
						if(!names.contains(((Player) ent).getDisplayName())){
							names += ChatColor.GOLD + ((Player) ent).getDisplayName() + ChatColor.GREEN + ", ";
						}
					}
				}
				if(ent instanceof Creature){
					if(!names.contains(ent.getType().name().toLowerCase())){
						names += ChatColor.GOLD + ent.getType().name().toLowerCase() + ", ";
					}
				}
			}
			
			if(names != ""){
				names = names.trim();
				names = names.substring(0, names.length() - 1);
				names = names + ChatColor.GREEN;
				cha.sendMessage(ChatColor.GREEN + "[Spell] : You have cast " + SkillName + ChatColor.GREEN + " on " + names);
			}
			else{
				cha.sendMessage(ChatColor.GREEN + "[Spell] : You have cast " + SkillName);
			}
			
			cha.addCooldown(this);
			cha.Health -= healthCost;
			cha.Mana -= manaCost;
			cha.Stamina -= staminaCost;
		}
		else{
			if(success == ActionResult.NoVital){
				if(cha.Health <= healthCost){
					cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.GREEN + "Stamina" + ChatColor.RED + " to cast " + SkillName);
				}
				if(cha.Mana <= manaCost){
					cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.BLUE + "Mana" + ChatColor.RED + " to cast " + SkillName);
				}
				if(cha.Stamina <= staminaCost){
					cha.sendMessage(ChatColor.RED + "You don't have enough " + ChatColor.GOLD + "Stamina" + ChatColor.RED + " to cast " + SkillName);
				}
			}
			else if(success == ActionResult.NoTarget){
				cha.sendMessage(ChatColor.RED + "No target found for " + SkillName);
			}
			else if(success == ActionResult.NoResources){
				cha.sendMessage(ChatColor.RED + "You don't have enough consumables for that spell");
			}
		}
		
		SpellCastEvent event = new SpellCastEvent(cha, this, targets);
		Bukkit.getPluginManager().callEvent(event);
		
	}
	
	public void setCooldown(Integer cd){
		cooldown = cd;
	}
	
	public Integer getCooldown(){
		return cooldown;
	}
	
	public boolean learnable(Character cha){
		if(cha.Strength < STR){
			//cha.sendMessage(ChatColor.RED + "" + cha.Strength + " " + STR);
			return false;
		}
		else if(cha.Constitution < CON){
			//cha.sendMessage(ChatColor.GOLD + "" + cha.Constitution + " " + CON);
			return false;
		}
		else if(cha.Dexterity < DEX){
			//cha.sendMessage(ChatColor.DARK_GREEN + "" + cha.Dexterity + " " + DEX);
			return false;
		}
		else if(cha.Agility < AGI){
			//cha.sendMessage(ChatColor.GREEN + "" + cha.Agility + " " + AGI);
			return false;
		}
		else if(cha.Wisdom < WIS){
			//cha.sendMessage(ChatColor.BLUE + "" + cha.Wisdom + " " + WIS);
			return false;
		}
		else if(cha.Intelligence < INT){
			//cha.sendMessage(ChatColor.DARK_PURPLE + "" + cha.Intelligence + " " + INT);
			return false;
		}
		else{
			//cha.sendMessage("You have learned " + this.SkillName);
			return true;
			
		}
	}

	public void remove(Character character) {
		// TODO Auto-generated method stub
		
	}

	public int getStatRequirement(StatType mainStat) {
		switch(mainStat){
			case STRENGTH:
				return STR;
			case CONSTITUTION:
				return CON;
			case DEXTERITY:
				return DEX;
			case AGILITY:
				return AGI;
			case WISDOM:
				return WIS;
			case INTELLIGENCE:
				return INT;
			default:
				return 0;
		}
	}
}
