package me.Lorinth.RPGE.SkillAPI;

public enum ActionResult {

	NoTarget, NoVital, NoResources, Success
	
}
