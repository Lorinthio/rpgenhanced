package me.Lorinth.RPGE.SkillAPI;

public class Delay {

	private long delay;
	
	public Delay(long ticks){
		delay = ticks;
	}
	
	public long getDelay(){
		return delay;
	}
	
}
