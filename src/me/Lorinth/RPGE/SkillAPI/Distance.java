package me.Lorinth.RPGE.SkillAPI;

public class Distance {

	public Integer X = 0;
	public Integer Y = 0;
	public Integer Z = 0;
	
	public boolean area = false;
	
	public Integer dist = 0;
	
	public Distance(Integer distanceX, Integer distanceY, Integer distanceZ){
		X = distanceX;
		Y = distanceY;
		Z = distanceZ;
		
		area = true;
	}
	
	public Distance(Integer dis){
		dist = dis;
	}
	
	public Distance(Integer radius, Integer directional){
		dist = directional;
		X = radius;
		Y = radius;
		Z = radius;
	}
	
}
