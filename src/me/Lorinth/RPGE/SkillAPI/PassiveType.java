package me.Lorinth.RPGE.SkillAPI;

public enum PassiveType {

	MeleeDamage, RangedDamage, MagicDamage, PureDamage,
	Attack, Defense, 
	MagicAttack, MagicDefense, 
	Dodge, Critical, 
	Lifesteal, CooldownReduction,  
	FireEnhant, LightningEnchant, IceEnchant, 
	Speed, Jump, 
	HealthRegen, ManaRegen, StaminaRegen, 
	HealthMax, ManaMax, StaminaMax, 
	Shadows,
	HealingPotency;
	
}
