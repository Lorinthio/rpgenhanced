package me.Lorinth.RPGE.SkillAPI;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;

public class Target {
	
	//public String skillName;
	private Skill skill; 
	
	public Target(Skill skill){
		this.skill = skill;
	}
	
	//SELF, SINGLE_FRIENDLY, SINGLE_HOSTILE, AOE, AOE_HOSTILE, AOE_FRIENDLY, TARGET_AOE, TARGET_AOE_FRIENDLY, TARGET_AOE_HOSTILE;
	
	public Entity TargetSelf(Player player){
		return player;
	}
	
	public Entity SingleTarget(Player player, Integer distance){
		List<Entity> nearbyE = player.getNearbyEntities(distance, distance, distance);
		Entity target = null;
		BlockIterator bItr = new BlockIterator(player, distance);
		Block block;
		Location loc;
		int bx, by, bz;
		double ex, ey, ez;
		while (bItr.hasNext()) {

			block = bItr.next();
			bx = block.getX();
			by = block.getY();
			bz = block.getZ();
			for (Entity e : nearbyE) {
				loc = e.getLocation();
				ex = loc.getX();
				ey = loc.getY();
				ez = loc.getZ();
				if ((bx - .75 <= ex && ex <= bx + 1.75) && (bz - .75 <= ez && ez <= bz + 1.75) && (by - 1 <= ey && ey <= by + 2.5)) {
					if(e instanceof Player || e instanceof Creature){
						target = e;
						break;
					}
				}
			}

		}
		
		try{
			if(target.equals(null)){
				//Blank get target
			}
		}
		catch(NullPointerException e){
			//No Target
		}
		return target;

	}

	public Entity SingleFriendly(Player player, Integer distance){
		List<Entity> nearbyE = player.getNearbyEntities(distance, distance, distance);
		Entity target = player;
		BlockIterator bItr = new BlockIterator(player, distance);
		Block block;
		Location loc;
		int bx, by, bz;
		double ex, ey, ez;
		while (bItr.hasNext()) {

			block = bItr.next();
			bx = block.getX();
			by = block.getY();
			bz = block.getZ();
			for (Entity e : nearbyE) {
				loc = e.getLocation();
				ex = loc.getX();
				ey = loc.getY();
				ez = loc.getZ();
				if ((bx - .75 <= ex && ex <= bx + 1.75) && (bz - .75 <= ez && ez <= bz + 1.75) && (by - 1 <= ey && ey <= by + 2.5)) {
					if(e instanceof Player){
						if(skill.getMain().cm.getCharacter(player).playerInParty((Player) e)){
							target = e;
							break;
						}
					}
				}
			}
		}
		return target;

	}
	
	public Entity SingleHostile(Player player, Integer distance){
		List<Entity> nearbyE = player.getNearbyEntities(distance, distance, distance);
		Entity target = null;
		BlockIterator bItr = new BlockIterator(player, distance);
		Block block;
		Location loc;
		int bx, by, bz;
		double ex, ey, ez;
		while (bItr.hasNext()) {

			block = bItr.next();
			bx = block.getX();
			by = block.getY();
			bz = block.getZ();
			for (Entity e : nearbyE) {
				loc = e.getLocation();
				ex = loc.getX();
				ey = loc.getY();
				ez = loc.getZ();
				if ((bx - .75 <= ex && ex <= bx + 1.75) && (bz - .75 <= ez && ez <= bz + 1.75) && (by - 1 <= ey && ey <= by + 2.5)) {
					if(e instanceof Player){
						if(e == player){
							//pass;
						}
						else if(!skill.getMain().cm.getCharacter(player).playerInParty((Player) e)){
							double X = e.getLocation().getX();
							double Z = e.getLocation().getZ();
							if(e.getLocation().getWorld().getName().equals("world")){
								if(-3000 <= X && X <= 3000 && -3000 <= Z && Z <= 3000){
									//pass;
								}
							}
							else{
								target = e;
								break;
							}
						}
						
					}
					else if(e instanceof Creature){
						target = e;
						break;
					}
				}
			}

		}
		
		try{
			if(target.equals(null)){
				//Blank get target
			}
		}
		catch(NullPointerException e){
			//No Target
		}
		return target;

	}
	
	public static Entity TargetPlayer(Player player, Integer distance){
		List<Entity> nearbyE = player.getNearbyEntities(distance, distance, distance);
		Entity target = null;
		BlockIterator bItr = new BlockIterator(player, distance);
		Block block;
		Location loc;
		int bx, by, bz;
		double ex, ey, ez;
		while (bItr.hasNext()) {

			block = bItr.next();
			bx = block.getX();
			by = block.getY();
			bz = block.getZ();
			for (Entity e : nearbyE) {
				loc = e.getLocation();
				ex = loc.getX();
				ey = loc.getY();
				ez = loc.getZ();
				if ((bx - .75 <= ex && ex <= bx + 1.75) && (bz - .75 <= ez && ez <= bz + 1.75) && (by - 1 <= ey && ey <= by + 2.5)) {
					if(e instanceof Player){
						target = e;
						break;
					}
				}
			}

		}
		if(target == null){
			target = player;
		}
		return target;
	}
		
	public List<Entity> Aoe(Player player, Integer Xdistance, Integer Ydistance, Integer Zdistance){
		List<Entity> entities = null;
		entities = player.getNearbyEntities(Xdistance, Ydistance, Zdistance);
		return entities;
	}
	
	public List<Entity> Aoe_Friendly(Player player, Integer Xdistance, Integer Ydistance, Integer Zdistance){
		List<Entity> entities = null;
		ArrayList<Entity> finish = new ArrayList<Entity>();
		entities = player.getNearbyEntities(Xdistance, Ydistance, Zdistance);
		for(Entity ent : entities){
			if(ent instanceof Player){
				if(skill.getMain().cm.getCharacter(player).playerInParty((Player) ent)){
					finish.add(ent);
				}
			}
		}
		finish.add(player);
		
		return finish;
	}
	
	public List<Entity> TargetAoe(Player player, Integer distance, Integer radiusX, Integer radiusY, Integer radiusZ){
		Entity entity = SingleTarget(player, distance);
		List<Entity> entities = entity.getNearbyEntities(radiusX, radiusY, radiusZ);
		return entities;
	}
	
	public List<Entity> TargetAoeHostile(Player player, Integer distance, Integer radiusX, Integer radiusY, Integer radiusZ){
		Entity entity = SingleHostile(player, distance);
		if(entity == null){
			return null;
		}
		List<Entity> entities = entity.getNearbyEntities(radiusX, radiusY, radiusZ);
		ArrayList<Entity> targets = new ArrayList<Entity>();
		targets.add(entity);
		
		for(Entity e : entities){
			if(e instanceof Player){
				if(e == player){
					//pass
				}
				else if(!skill.getMain().cm.getCharacter(player).playerInParty((Player) e)){
					double X = e.getLocation().getX();
					double Z = e.getLocation().getZ();
					if(-3000 <= X && X <= 3000 && -3000 <= Z && Z <= 3000){
						//pass;
					}
					else{
						targets.add(e);
						break;
					}
				}
			}
			else if(e instanceof Creature){
				targets.add(e);
			}
		}
		return targets;
	}

	public List<Entity> Aoe_Hostile(Player player, Integer Xdistance, Integer Ydistance, Integer Zdistance){
		List<Entity> entities = Aoe(player, Xdistance, Ydistance, Zdistance);
		ArrayList<Entity> finish = new ArrayList<Entity>();
		
		for(Entity e : entities){
			if(e instanceof Player){
				if(e == player){
					//pass;
				}
				else if(!skill.getMain().cm.getCharacter(player).playerInParty((Player) e)){
					double X = e.getLocation().getX();
					double Z = e.getLocation().getZ();
					if(-3000 <= X && X <= 3000 && -3000 <= Z && Z <= 3000){
						//pass;
					}
					else{
						finish.add(e);
					}
				}
				
			}
			else if(e instanceof Creature){
				finish.add(e);
			}
		}
		
		return finish;
	}
	
}
