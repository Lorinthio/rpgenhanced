package me.Lorinth.RPGE.SkillAPI;

public enum ProjectileEffect {

	Damage, Heal, Drain, Buff, Debuff, Taunt, Teleport, Launch, Message
	
}
