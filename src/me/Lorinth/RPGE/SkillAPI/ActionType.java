package me.Lorinth.RPGE.SkillAPI;

public enum ActionType {

	Damage, Heal, Drain, Buff, Debuff, Taunt, Teleport, Launch, Projectile, Message, ProduceItem, ConsumeItem, VisualEffect, Stun, Stance, Cloak;
	
}
