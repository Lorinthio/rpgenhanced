package me.Lorinth.RPGE.SkillAPI;

public enum KnockbackDirection {

	Away_from_Caster, Caster_Direction, Specific_Direction
	
}
