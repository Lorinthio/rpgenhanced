package me.Lorinth.RPGE.SkillAPI;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;

import org.bukkit.potion.PotionEffect;

public class Buff {
	
	protected Integer duration;
	protected PassiveType passiveType;
	protected Integer buffValue;
	private Boolean customFormula = false;
	private String CustomFormula = "";
	private Skill skill;
	private String name;
	
	private Boolean isPotion = false;
	private PotionEffect pe = null;
	
	public Buff(PassiveType stat, Integer value, Integer duration, Skill skill){
		this.duration = duration;
		this.passiveType = stat;
		this.buffValue = value;
		this.skill = skill;
		name = skill.getName();
	}
	
	public Buff(PassiveType stat, Integer value, Integer duration, String name){
		this.duration = duration;
		this.passiveType = stat;
		this.buffValue = value;
		this.name = name;
	}
	
	public Integer getDuration(){
		return duration;
	}
	
	public Buff(PotionEffect potion){
		isPotion = true;
		pe = potion;
	}
	
	public void setCustomFormula(String custom){
		customFormula = true;
		CustomFormula = custom;
	}
	
	public boolean isPotion(){
		return isPotion;
	}
	
	public PotionEffect getPotionEffect(){
		return pe;
	}
	
	public String getName(){
		if(skill != null){
			return skill.getName();
		}
		return name;
	}
	
	public Integer getValue(Character character){
		if(customFormula){
			String newFormula = CustomFormula;
			newFormula = newFormula.replaceAll("<str>", character.Strength.toString());
			newFormula = newFormula.replaceAll("<con>", character.Constitution.toString());
			newFormula = newFormula.replaceAll("<dex>", character.Dexterity.toString());
			newFormula = newFormula.replaceAll("<agi>", character.Agility.toString());
			newFormula = newFormula.replaceAll("<wis>", character.Wisdom.toString());
			newFormula = newFormula.replaceAll("<int>", character.Intelligence.toString());
			newFormula = newFormula.replaceAll("<melee>", ((int)character.getAttackDamage() + ""));
			newFormula = newFormula.replaceAll("<ranged>", ((int)character.GetRangedAttackDamage() + ""));
			newFormula = newFormula.replaceAll("<magic>", ((int)character.getMagicDamage() + ""));
			newFormula = newFormula.replaceAll("<Attack>", character.getAttack() + "");
			newFormula = newFormula.replaceAll("<MAttack>", character.getMagicAttack() + "");
			newFormula = newFormula.replaceAll("<PDefense>", character.getPhysicalDefense() + "");
			newFormula = newFormula.replaceAll("<MDefense>", character.getMagicDefense() + "");
			
			return (int) skill.getMain().evaluateFormula(newFormula, skill.getName());
		}
		else{
			return buffValue;
		}
	}
	
	public PassiveType getType(){
		return passiveType;
	}
	
	public void apply(final Character cha){
		if(!isPotion){
			if(cha.hasBuff(this)){
				cha.removeBuff(this);
			}
			
			HashMap<PassiveType, Integer> passives = cha.getPassives();
			Integer value = passives.get(passiveType);
			Integer buffValue = getValue(cha);
			
			//cha.sendMessage(value + " + " + buffValue);
			value += buffValue;
			//cha.sendMessage("=" + value);
			
			cha.addBuff(this, buffValue);
			
			passives.put(passiveType, value);
			
			cha.setPassives(passives);
			
			
		}
		else{
			if(this.customFormula){
				PotionEffect pe2 = new PotionEffect(pe.getType(), pe.getAmplifier(), buffValue);
			}
			else{
				cha.owner.addPotionEffect(pe);
			}
		}
	}
	
	public void removed(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer value = passives.get(passiveType);
		//cha.sendMessage(value + " - " + getValue(cha));
		value -= getValue(cha);
		//cha.sendMessage("=" + value);
		passives.put(passiveType, value);
		
		cha.setPassives(passives);
	}

}
