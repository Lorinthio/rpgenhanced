package me.Lorinth.RPGE.SkillAPI;

import java.util.ArrayList;
import java.util.List;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Events.HealingEvent;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import net.elseland.xikage.MythicMobs.API.Mobs;
import net.elseland.xikage.MythicMobs.API.ThreatTables;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class Action {

	private TargetType targetType;
	private Object statType;
	private Boolean customFormula = false;
	private String CustomFormula = "";
	private Integer value;
	private String message;
	private Distance distance = new Distance(5);
	private ActionType actionType;
	private Position position;
	private Class projectile;
	private ItemStack item;
	private ProjectileEffect PEffect;
	private Target target;
	private Buff buff;
	private Stance stance;
	private Skill skill;
	private Vector direction;
	private ArrayList<Entity> targets;
	private Integer visualRepeat = 1;
	public Delay delay = null;
	private AttackType at = AttackType.Physical;
	
	//Visual Effect
	private Effect visual;
	private Integer visualParams;
	private boolean lightningEffect;
	
	public long getDelay(){
		if(delay == null){
			return 0;
		}
		else{
			return delay.getDelay();
		}
	}
	
	public Action(Skill skill, ActionType action, ArrayList<Object> params){
		//Positive
		target = new Target(skill);
		this.skill = skill;
		actionType = action;
		
		switch(action){
		
		case Buff:
			for(Object param: params){
				if(param instanceof Buff){
					buff = (Buff) param;
				}
				else if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
		case Heal:
			for(Object param : params){
				if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Vital){
					statType = (Vital) param;
				}
				else if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
				else if(param instanceof Integer){
					value = (Integer) param;
				}
			}
			break;
		
		case Damage:
			for(Object param : params){
				if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Vital){
					statType = (Vital) param;
				}
				else if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
			
		case Debuff:
			for(Object param: params){
				if(param instanceof Buff){
					buff = (Buff) param;
				}
				else if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
		case Drain:
			
			break;
			
		case Taunt:
			for(Object param : params){
				if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof Integer){
					value = (Integer) param;
				}
				else if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
			
		case Teleport:
			for(Object param : params){
				if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Position){
					position = (Position) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
			
		case Launch:
			for(Object param : params){
				if(param instanceof Vector){
					direction = (Vector) param;
				}
				else if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
		case Message:
			for(Object param : params){
				if(param instanceof String){
					message = (String) param;
				}
				else if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Distance){
					distance = (Distance) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
		case Projectile:
			targetType = TargetType.MANUAL;
			for(Object param : params){
				if(param instanceof Class<?>){
					projectile = (Class<?>) param;
				}
				else if(param instanceof Vector){
					direction = (Vector) param;
				}
				else if(param instanceof ProjectileEffect){
					PEffect = (ProjectileEffect) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
				
			}
			break;
		case ConsumeItem:
			targetType = TargetType.SELF;
			for(Object param : params){
				if(param instanceof ItemStack){
					item = (ItemStack) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
		case ProduceItem:
			for(Object param : params){
				if(param instanceof ItemStack){
					item = (ItemStack) param;
				}
				else if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Delay){
					delay = (Delay) param;
				}
			}
			break;
		case VisualEffect:
			for(Object param : params){
				if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
			}
			break;
		case Stance:
			for(Object param : params){
				if(param instanceof Stance){
					stance = (Stance) param;
				}
			}
			break;
		case Cloak:
			for(Object param : params){
				if(param instanceof TargetType){
					targetType = (TargetType) param;
				}
				else if(param instanceof Integer){
					value = (Integer) param;
				}
				else if(param instanceof Distance){
					distance = (Distance) param;
				}
			}
		default:
			break;
		}

	}
	
	public void setCustomFormula(String custom){
		customFormula = true;
		CustomFormula = custom;
	}
	
	public ArrayList<Entity> getTargets(){
		return targets;
	}
	
	public void addVisualEffect(Effect visual, Integer params){
		this.visual = visual;
		this.visualParams = params;
	}
	
	public void addVisualEffect(Effect visual, Integer params, Integer repeat){
		this.visual = visual;
		this.visualParams = params;
		this.visualRepeat = repeat;
	}
	
	public void setAttackType(AttackType type){
		at = type;
	}
	
	public void playVisualEffect(Entity ent){
		try{
			if(visual == null){
				return;
			}
			for(int i=0;i<this.visualRepeat;i++){
				ent.getWorld().playEffect(ent.getLocation().add(0, 2, 0), visual, 0);
			}
		}
		catch(IllegalArgumentException error){
			//pass
		}
	}
	
	@SuppressWarnings("deprecation")
	public ActionResult Act(Player player, RpgEnhancedMain main){
		if(delay != null){
			
		}
		//List<Entity> targets = null;
		Character character = main.getCharacterManager().getCharacter(player);
		ArrayList<Entity> target = new ArrayList<Entity>();
		switch(targetType){	
			case SELF:
				target.add(this.target.TargetSelf(player));
				break;
			case SINGLE_FRIENDLY:
				target.add(this.target.SingleFriendly(player, distance.dist));
				break;
			case SINGLE_HOSTILE:
				target.add(this.target.SingleHostile(player, distance.dist));
				break;
			case PARTY:
				target.addAll(character.getPartyPlayers());
				break;
			case AOE:
				if(!distance.area){
					target.addAll(this.target.Aoe(player, distance.dist, distance.dist, distance.dist));
				}
				else{
					target.addAll(this.target.Aoe(player, distance.X, distance.Y, distance.Z));
				}
				break;
			case AOE_FRIENDLY:
				if(!distance.area){
					target.addAll(this.target.Aoe_Friendly(player, distance.dist, distance.dist, distance.dist));
				}
				else{
					target.addAll(this.target.Aoe_Friendly(player, distance.X, distance.Y, distance.Z));
				}
				break;
			case AOE_HOSTILE:
				if(!distance.area){
					target.addAll(this.target.Aoe_Hostile(player, distance.dist, distance.dist, distance.dist));
				}
				else{
					target.addAll(this.target.Aoe_Hostile(player, distance.X, distance.Y, distance.Z));
				}
				target.addAll(target);
				break;
			case TARGET_AOE_HOSTILE:
				List<Entity> targets = this.target.TargetAoeHostile(player, distance.dist, distance.X, distance.Y, distance.Z);
				if(targets == null){
					return ActionResult.NoTarget;
				}
				else if(targets.isEmpty()){
					return ActionResult.NoTarget;
				}
				target.addAll(targets);
				break;
			case MANUAL:
				break;
		default:
			break;
		}
		
		if(targetType.equals(TargetType.MANUAL)){
			//Pass this check
		}
		else if(target.isEmpty()){
			//player.sendMessage(ChatColor.RED + "There is no target for that spell");
			return ActionResult.NoTarget;
		}
		else if(target.get(0) == null){
			//player.sendMessage(ChatColor.RED + "There is no target for that spell");
			return ActionResult.NoTarget;
		}
		
		targets = target;
		
		if(customFormula){
			String newFormula = CustomFormula;
			newFormula = newFormula.replaceAll("<str>", character.Strength.toString());
			newFormula = newFormula.replaceAll("<con>", character.Constitution.toString());
			newFormula = newFormula.replaceAll("<dex>", character.Dexterity.toString());
			newFormula = newFormula.replaceAll("<agi>", character.Agility.toString());
			newFormula = newFormula.replaceAll("<wis>", character.Wisdom.toString());
			newFormula = newFormula.replaceAll("<int>", character.Intelligence.toString());
			newFormula = newFormula.replaceAll("<melee>", ((int)character.getSpellMeleeDamage() + ""));
			newFormula = newFormula.replaceAll("<ranged>", ((int)character.getSpellRangedDamage() + ""));
			newFormula = newFormula.replaceAll("<magic>", ((int)character.getSpellMagicDamage() + ""));
			newFormula = newFormula.replaceAll("<Attack>", character.getAttack() + "");
			newFormula = newFormula.replaceAll("<MAttack>", character.getMagicAttack() + "");
			newFormula = newFormula.replaceAll("<PDefense>", character.getPhysicalDefense() + "");
			newFormula = newFormula.replaceAll("<MDefense>", character.getMagicDefense() + "");
			
			value = (int) main.evaluateFormula(newFormula, skill.getName());
		}
		
		switch(actionType){
			case Damage:
				if(statType.equals(Vital.HEALTH)){
					for(Entity ent : target){
						if(ent instanceof LivingEntity){
							if(!(((LivingEntity) ent).getNoDamageTicks() > 10)){
								float FullAttack = 0;
								if(this.at == AttackType.Physical){
									FullAttack = character.getAttack();
								}
								else{
									FullAttack = character.getMagicAttack();
								}
								
								float DamageReduction = 0;
								double resultdamage = value;

								if(ent instanceof Player){
								    Player Tplayer = (Player) ent;
									Character targ = main.getCharacterManager().getCharacter(Tplayer);
									
									double normal = targ.Health;
									targ.damage(at, value, player, false);
									double health = targ.Health;
									
									if((health - 0.01) <= 0){
										player.teleport(new Location(Bukkit.getWorld("world"), -0.5, 65, -0.5));
										player.sendMessage(ChatColor.GOLD + "[Respawn] " + ChatColor.GRAY + ": You have been protected by the gods and have been brought to this world...");
									}
									else{
										Tplayer.setHealth(Math.min(health + 0.1, ((Damageable)Tplayer).getMaxHealth()));
										Tplayer.damage(0.1);			
									}
									
									Integer difference = (int) (normal - health);
									if(difference > 0){
										targ.sendMessage(ChatColor.RED + "You were damaged  by " + player.getDisplayName() + ChatColor.GREEN + " for " + difference + "HP");
										player.sendMessage(ChatColor.GOLD + "You damaged " + targ.owner.getDisplayName() + " for " + difference + ChatColor.GREEN + " hp");
									}
									
								}
								else if(ent instanceof Creature){
									if(ent instanceof Monster){
										Integer mobLevel = main.getLevel(ent);
										if(MobDifficulty.getPlugin().isDungeonMob(ent)){
											DamageReduction = (float)(mobLevel) + (mobLevel / 2) + (mobLevel / 3) + 30;
										}
										else{
											DamageReduction = (float)(mobLevel) + (mobLevel / 3) + (mobLevel / 5) + 20;
										}
										
									}else{
										Integer mobLevel = main.getLevel(ent);
										DamageReduction = (float)(mobLevel/6) + 15;
									}
									
									if(Mobs.isMythicMob(ent.getUniqueId())){
										DamageReduction *= 2;
									}
									
									float DamageMod = (FullAttack + 10) / (DamageReduction + 10);
									if(DamageMod > 2.0){
										DamageMod = (float) 2.0;
									}
									resultdamage = (int) (DamageMod * resultdamage);
									resultdamage += -3 + main.getRandom().nextInt(4);
									double health = ((Damageable) ent).getHealth() - resultdamage;
									if(health <= 0){
										health = 0;
									}
									if(health > ((Damageable) ent).getMaxHealth()){
										health = ((Damageable) ent).getMaxHealth();
									}
									((LivingEntity) ent).setHealth(health);
									
									//player.sendMessage("(" + DamageMod + ") * " + value + " = " + resultdamage);
									
									EntityDamageByEntityEvent e = new EntityDamageByEntityEvent(player, ent, DamageCause.ENTITY_ATTACK, value);
									ent.setLastDamageCause(e);
									
									((Creature) ent).damage(0.1);
									((Creature) ent).setHealth(Math.max(health, 0));
									player.sendMessage(ChatColor.GOLD + "You dealt " + ChatColor.RED + value + ChatColor.GOLD + " damage to " + ent.getType().name().toLowerCase());
									
									if(((Damageable) ent).getHealth() == 0){
										ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
										drops.add(new ItemStack(Material.AIR));
										EntityDeathEvent e2 = new EntityDeathEvent((LivingEntity) ent, drops);
										Bukkit.getPluginManager().callEvent(e2);
									}
								}
							}
						}
						playVisualEffect(ent);
					}
				}
				else if(statType.equals(Vital.MANA)){
					for(Entity ent : target){
						if(ent instanceof Player){
							Character cha = main.cm.getCharacter((Player) ent);
							Integer normal = cha.Mana;
							cha.Mana -= value;
							cha.normalizeVitals();
							Integer difference = normal - cha.Mana;
							if(difference > 0){
								cha.sendMessage(ChatColor.RED + "You were " + ChatColor.BLUE + " refreshed " + ChatColor.GREEN + " by " + player.getDisplayName() + ChatColor.GREEN + " for " + difference + "MP");
								player.sendMessage(ChatColor.GOLD + "You damaged " + cha.owner.getDisplayName() + " for " + difference + ChatColor.BLUE + " mp");
							}
						}
						playVisualEffect(ent);
					}
				}
				else if(statType.equals(Vital.STAMINA)){
					for(Entity ent : target){
						if(ent instanceof Player){
							Character cha = main.cm.getCharacter((Player) ent);
							Integer normal = cha.Stamina;
							cha.Stamina -= value;
							cha.normalizeVitals();
							Integer difference = normal - cha.Stamina;
							if(difference > 0){
								cha.sendMessage(ChatColor.RED + "You were " + ChatColor.YELLOW + " encouraged " + ChatColor.GREEN + " by " + player.getDisplayName() + ChatColor.GREEN + " for " + difference + "SP");
								player.sendMessage(ChatColor.GOLD + "You damaged " + cha.owner.getDisplayName() + " for " + difference + ChatColor.GOLD + " sp");
							}
						}
						playVisualEffect(ent);
					}
				}
				break;
			case Heal:
				for(Entity ent : target){
					if(ent instanceof Player){
						Character cha = main.cm.getCharacter((Player) ent);
						value += cha.getPassive(PassiveType.HealingPotency);
						HealingEvent event = new HealingEvent(character, cha, ((Vital)statType), value, skill);
						Bukkit.getPluginManager().callEvent(event);
						playVisualEffect(ent);
					}
				}
				break;
			case Stance:
				stance.apply(character);
				break;
			case Drain:
			
				break;
			case Buff: 
				for(Entity ent : target){
					if(ent instanceof Player){
						
						//NEED CHECK TO SEE IF PLAYER IS IN PARTY
						
						Character cha = main.cm.getCharacter((Player) ent);
						this.buff.apply(cha);
						
						playVisualEffect(ent);
					}
					if(ent instanceof Creature){
						if(buff.isPotion()){
							((Creature) ent).addPotionEffect(buff.getPotionEffect());
						}
					}
				}
			
				break;
			case VisualEffect:
				for(Entity ent : target){
					if(ent instanceof LivingEntity){
						
					}
				}
				break;
			case Debuff:
				for(Entity ent : target){
					if(ent instanceof Player){
						
						//NEED CHECK TO SEE IF PLAYER IS IN PARTY
						
						Character cha = main.cm.getCharacter((Player) ent);
						this.buff.apply(cha);
						
						playVisualEffect(ent);
					}
				}
				
				break;
			case Launch:
				for(Entity ent : target){
					if(ent instanceof LivingEntity){
						ent.setVelocity(direction);
					}
				}
				break;
			case Taunt:
				for(Entity ent : target){
					if(ent instanceof Creature){
						if(Mobs.isMythicMob((LivingEntity) ent)){
							ThreatTables.addThreat(ent, player, value);
						}
						else{
							main.tm.addThreat(ent, value, player);
						}
						
					}
					
				}
				break;
			case Teleport:
				for(Entity ent : target){
					if(ent instanceof LivingEntity){
						ent.teleport(position.getLocation(player, this));
					}
				}
				break;
			case Projectile:
				@SuppressWarnings("unchecked")
				Projectile project = player.launchProjectile(projectile);
				if(project instanceof Fireball){
					((Fireball) project).setIsIncendiary(false);
					((Fireball) project).setYield(0);
				}
				project.setVelocity(player.getLocation().getDirection().multiply(5));
				project.setMetadata("Skill", new FixedMetadataValue(main, skill.getName()));
				project.setMetadata("Damage", new FixedMetadataValue(main, value));
				project.setMetadata("Player", new FixedMetadataValue(main, player.getName()));
				break;
			case ConsumeItem:
				if(player.getInventory().contains(item.getType())){
					player.getInventory().remove(item);
					return ActionResult.Success;
				}
				else{
					return ActionResult.NoResources;
				}
			case Message:
				for(Entity ent : target){
					if(ent instanceof Player){
						((Player) ent).sendMessage(message);
					}
				}
				break;
			case ProduceItem:
				for(Entity ent : target){
					if(ent instanceof Player){
						((HumanEntity) ent).getInventory().addItem(item.clone());
					}
				}
				break;
			case Cloak:
				for(Entity ent : target){
					if(ent instanceof Player){
						skill.getMain().cm.getCharacter((Player) ent).cloak(value);
					}
				}
				break;
		default:
			break;
		}
		
		for(Entity ent : target){
			main.cm.addLastSpellEvent(ent, player);
			if(lightningEffect){
				ent.getWorld().strikeLightningEffect(ent.getLocation());
			}
		}
		
		return ActionResult.Success;
	}

	public void addLightningEffect() {
		lightningEffect = true;
	}
}
