package me.Lorinth.RPGE.SkillAPI;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class Position {

	private Location loc;
	private PositionType type;
	
	public Position(Location loc, PositionType type){
		this.loc = loc;
		this.type = type;
	}
	
	public Position(PositionType type){
		this.type = type;
	}
	
	public Location getLocation(Player player, Action action){
		switch(type){
		case Caster:
			return player.getLocation();
		case Location:
			return loc;
		case Target:
			return action.getTargets().get(0).getLocation();
		default:
			break;
		
		}
		return loc;
	}
	
}
