package me.Lorinth.RPGE.SkillAPI;

import java.util.ArrayList;
import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Managers.SkillManager;

public class Stance {

	ArrayList<PassiveType> types;
	ArrayList<String> formulas;
	SkillManager sm;
	String name;
	
	public Stance(String name, SkillManager sm){
		this.types = new ArrayList<PassiveType>();
		this.name = name;
		this.sm = sm;
	}
	
	public void addType(PassiveType t, String formula){
		types.add(t);
	}
	
	public void apply(Character character){
		Stance current = character.stance;
		HashMap<PassiveType, Integer> passives = character.getPassives();
		HashMap<PassiveType, Integer> pStance = new HashMap<PassiveType, Integer>();
		
		try{
			if(current != null){
				for(PassiveType t : pStance.keySet()){
					passives.put(t, passives.get(t) - pStance.get(t));
				}
			}
		}
		catch(NullPointerException e){
			
		}
		
		character.stance = this;
		pStance = new HashMap<PassiveType, Integer>();
		
		for(int i=0; i <= types.size(); i++){
			Integer value = getValue(formulas.get(i), character);
			pStance.put(types.get(0), value);
			
			value += passives.get(types.get(i));
			
			passives.put(types.get(i), value);
		}
		
		character.setPassives(passives);
	}
	
	public String getName(){
		return name;
	}
	
	public Integer getValue(String formula, Character character){
		String newFormula = formula;
		newFormula = newFormula.replaceAll("<str>", character.Strength.toString());
		newFormula = newFormula.replaceAll("<con>", character.Constitution.toString());
		newFormula = newFormula.replaceAll("<dex>", character.Dexterity.toString());
		newFormula = newFormula.replaceAll("<agi>", character.Agility.toString());
		newFormula = newFormula.replaceAll("<wis>", character.Wisdom.toString());
		newFormula = newFormula.replaceAll("<int>", character.Intelligence.toString());
		newFormula = newFormula.replaceAll("<melee>", ((int)character.getAttackDamage() + ""));
		newFormula = newFormula.replaceAll("<ranged>", ((int)character.GetRangedAttackDamage() + ""));
		newFormula = newFormula.replaceAll("<magic>", ((int)character.getMagicDamage() + ""));
		newFormula = newFormula.replaceAll("<Attack>", character.getAttack() + "");
		newFormula = newFormula.replaceAll("<MAttack>", character.getMagicAttack() + "");
		newFormula = newFormula.replaceAll("<PDefense>", character.getPhysicalDefense() + "");
		newFormula = newFormula.replaceAll("<MDefense>", character.getMagicDefense() + "");
		
		return (int) sm.evaluateFormula(newFormula);
	}
	
}
