package me.Lorinth.RPGE.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Characters.ExperienceListeners;
import me.Lorinth.RPGE.Characters.PlayerMenuPage;
import me.Lorinth.RPGE.Characters.SkillsProfile;
import me.Lorinth.RPGE.EventHandler.RandomEventManager;
import me.Lorinth.RPGE.Events.DayTime;
import me.Lorinth.RPGE.Events.TimeChangeEvent;
import me.Lorinth.RPGE.Items.ItemGenerator;
import me.Lorinth.RPGE.Managers.AbilityResultManager;
import me.Lorinth.RPGE.Managers.CharacterManager;
import me.Lorinth.RPGE.Managers.CombatManager;
import me.Lorinth.RPGE.Managers.DungeonManager;
import me.Lorinth.RPGE.Managers.PartyManager;
import me.Lorinth.RPGE.Managers.SkillManager;
import me.Lorinth.RPGE.Managers.ThreatManager;
import me.Lorinth.RPGE.SkillAPI.StatType;
import net.elseland.xikage.MythicMobs.API.Mobs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

public class RpgEnhancedMain extends JavaPlugin implements Listener{

	public ItemGenerator IG;
	public MobDifficulty md;
	public ConsoleCommandSender console;
	public ExperienceListeners EL;

	public CharacterManager cm;
	public CombatManager ch;
	public PartyManager pm;
	public DungeonManager dm;
	public SkillManager sm;
	public ThreatManager tm;
	public AbilityResultManager arm;
	public RandomEventManager rem;
	
	public Integer levelCap = 50;
	public ItemStack book;
	public HashMap<Player, PlayerMenuPage> pinvs = new HashMap<Player, PlayerMenuPage>();
	
	private HashMap<World, DayTime> daytime  = new HashMap<World, DayTime>();
	
	private Random random = new Random();
	
	private ArrayList<String> weapons = new ArrayList<String>();
	private ArrayList<String> armor = new ArrayList<String>();
	
	
	private ScriptEngineManager factory = new ScriptEngineManager();
	private ScriptEngine engine = factory.getEngineByName("JavaScript");
	
	public Economy eco;
	
	public WorldEditPlugin wep;
	
	@Override
	public void onEnable(){
		makeTutorialBook();
		
		IG = new ItemGenerator(this);
		md = MobDifficulty.getPlugin();
		console = Bukkit.getConsoleSender();
		
		EL = new ExperienceListeners(this);
		
		//Skills
		sm = new SkillManager(this);
		
		//Character handler
		cm = new CharacterManager(this);
		
		//COMBAT
		ch = new CombatManager(this);
		
		//PARTIES
		pm = new PartyManager(this);
		
		//Dungeons
		dm = new DungeonManager(this);
		
		//Ability Listeners
		arm = new AbilityResultManager(this);
		
		//Threat Manager
		tm = new ThreatManager(this);
		
		
		rem = new RandomEventManager(this);
		
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(arm, this);
		Bukkit.getPluginManager().registerEvents(EL, this);
		Bukkit.getPluginManager().registerEvents(cm, this);
		Bukkit.getPluginManager().registerEvents(ch, this);
		Bukkit.getPluginManager().registerEvents(pm, this);
		
		for(Plugin plug : Bukkit.getPluginManager().getPlugins()){
			if(plug instanceof WorldEditPlugin){
				wep = (WorldEditPlugin) plug;
			}
		}
		
		weapons.add("sword");
		weapons.add("mace");
		weapons.add("axe");
		weapons.add("bow");
		weapons.add("staff");
		
		armor.add("chest");
		armor.add("legs");
		armor.add("boots");
		armor.add("helm");
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){

			@Override
			public void run() {
				checkTime();
				
			}
			
		}, 0, 40);
	}
	
	//PlayerEvent event;
	
	@Override
	public void onDisable(){
		EL.saveAll();
		cm.autoSaveCharacters();
	}
	
	public ItemGenerator getItemGenerator(){
		return this.IG;
	}
	
	@EventHandler
	public void dropRpgItem(EntityDeathEvent event){
		if(event.getEntity().getWorld().getName() == "CODWarfare"){
			return;
		}
		
		Entity entity = event.getEntity();
		try{
			if(event.getDrops().contains(new ItemStack(Material.BOW))){
				event.getDrops().remove(new ItemStack(Material.BOW));
			}
			IG.checkEntityDeath(entity);
		}
		catch(NullPointerException e){
			
		}
	}
	
	/////////////////////
	//UTILITY FUNCTIONS//
	/////////////////////
	
	//Utility for finding information outside of Generator
	public final Integer getLevel(Entity entity){
		Integer Level = 1;
		if(Mobs.isMythicMob(entity.getUniqueId())){
			if(entity.isDead()){
				if(entity instanceof LivingEntity){
					Player p = ((LivingEntity) entity).getKiller();
					try{
						Character c = cm.getCharacter(p);
						
						if(c.Synced || c.forceSynced){
							Level = c.syncedLevel;
						}
						else{
							Level = c.Level;
						}
					}
					catch(NullPointerException e){
						//Character doesn't exist??
					}
				}
			}
		}
		else{
			try{
				try{
					Level = entity.getMetadata("Level").get(0).asInt();
				}catch (IndexOutOfBoundsException e){
					if(entity instanceof Creature){
						String name = ((Creature) entity).getCustomName();
						name = ChatColor.stripColor(name);
						String level = "";
						for(String letter : name.split("")){
							if(isInteger(letter)){
								level = level + letter;
							}
						}
						try{
							Level = Integer.parseInt(level);
						}
						catch(NumberFormatException e2){
							
						}
					}
				}
			}
			catch(NullPointerException e){
				//Mob doesn't have level, return result will be 1
			}
		}
		return Level;
	}
	
	public void makeTutorialBook(){
		book = new ItemStack(Material.WRITTEN_BOOK, 1);
		BookMeta bm = (BookMeta) book.getItemMeta();
		bm.setAuthor(ChatColor.GOLD + "Lorinthio");
		bm.setTitle(ChatColor.GREEN + "RpgEnhanced Tutorial");
		bm.addPage("\n\n\n RPG Enhanced\n   Tutorial Book ");
		bm.addPage(ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Command List" + ChatColor.RESET + 
					"\n\n" +
					"/battlestats - shows detailed info\n" +
					"/races - displays current race options \n" +
					"/repair - repairs the item in your hand \n" +
					"/skills - displays your crafting and fighting skills\n"  +
					"/spells - displays your ability list\n"
					
		);
		bm.addPage("/stats - shows your core stats \n" +
					"/setrace <race> - lets you choose a race (You can change once initially)\n" +
					"/sell - opens a sell window, to gain gold!\n" +
					"/trash - open a window to trash items");
		
		bm.addPage(ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Skills Description" + ChatColor.RESET + 
					"\n\n" + ChatColor.AQUA + "Agility" + ChatColor.RESET + " : \nReduces damage you take from falling. \n" +
					ChatColor.AQUA + "Bludgeoning" + ChatColor.RESET + " : Increases damage with maces, and gives give you a chance to block with maces. \n" +
					ChatColor.AQUA + "Building" + ChatColor.RESET + " : \nGives a chance to duplicate a block on placement. \n");
					
		bm.addPage(
					ChatColor.AQUA + "Digging" + ChatColor.RESET + " : \nGives a chance to recieve haste while digging. \n" +
					ChatColor.AQUA + "Fencing" + ChatColor.RESET + " : \nIncreases damage with swords, and gives give you a chance to counter attack.\n" +
					ChatColor.AQUA + "Marauding" + ChatColor.RESET + " : \nIncreases damage with axes, and gives a chance to deal true damage. \n"
					);
		bm.addPage(
					ChatColor.AQUA + "Marksmanship" + ChatColor.RESET + " : Increases damage with bows. \n"+
					ChatColor.AQUA + "Mining" + ChatColor.RESET + " : \nGives a chance to recieve multiple ores. \n" +
					ChatColor.AQUA + "Staves" + ChatColor.RESET + " : \nIncreases damage with wands. \n" +
					ChatColor.AQUA + "Unarmed" + ChatColor.RESET + " : \nIncreases damage with fists. \n"
					//ChatColor.AQUA + "Woodcutting" + ChatColor.RESET + " : \nNo specific bonus. \n"
					);
		
		bm.addPage(ChatColor.BOLD + "" + ChatColor.UNDERLINE + "Core Stats" + ChatColor.RESET + 
				"\n\n" + ChatColor.RED + "Strength" + ChatColor.RESET + " :\n Increases your melee damage, and stamina. \n" +
				ChatColor.GOLD + "Constitution" + ChatColor.RESET + " : Increases defense and health. \n" +
				ChatColor.DARK_GREEN + "Dexterity" + ChatColor.RESET + " :\n Increases your ranged damage, and critical chance. \n"
				);
		bm.addPage(ChatColor.GREEN + "Agility" + ChatColor.RESET + " :\n Gives a chance to dodge, and critical. \n" +
				ChatColor.DARK_PURPLE + "Intelligence" + ChatColor.RESET + " : Increases magic damage.\n" +
				ChatColor.BLUE + "Wisdom" + ChatColor.RESET + " :\n Increases magic defense and mana. \n"
				);
		bm.addPage(ChatColor.BLUE + "How To Cast A Spell\n" + ChatColor.RESET +
						"* To get your first spell upgrade any stat to 15.\n" +
						"* When you get a stat to 15 you will see you have unlocked a spell \n" +
						"* To cast this spell, pick a hotbar slot 1-9 \n" +
						" Then type the following line... \n" +
						ChatColor.GOLD + "/bind <spellname>"
				);
		bm.addPage(ChatColor.BLUE + "How To Cast A Spell (cont)\n" + ChatColor.RESET +
						"* After binding the skill you can use mouse right click to cast the spell.\n" +
						"* Note you can only cast with an item in that slot");
		bm.addPage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Beginning Tips\n" + ChatColor.RESET +
						"Its recommended you use /gearup to get your beginning gear\n" +
						"Also take some time to level around spawn at night."
				
						);
		//STAVES, FENCING, MARAUDING, BLUDGEONING,  MARKSMANSHIP, DIGGING, MINING, WOODCUTTING, BUILDING, AGILITY, UNARMED;
		book.setItemMeta(bm);
	}
	
	public boolean inDungeon(Entity entity){
		return false;
	}
	
	public boolean isInteger( String input ) {
	    try {
	        Integer.parseInt( input );
	        return true;
	    }
	    catch( NumberFormatException e ) {
	        return false;
	    }
	}
	
	public void addMoney(Player player, double amount){
		try {
			Economy.add(player.getName(), amount);
		} catch (NoLoanPermittedException | UserDoesNotExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ExperienceListeners getExperienceListener(){
		return EL;
	}
	
	public Random getRandom(){
		return random;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if(sender instanceof Player){
			Player player = (Player) sender;
			Character cha = null;
			SkillsProfile profile = null;
			switch(commandLabel){
			case "skills":
				profile = EL.getPlayerProfile(player);
				profile.sendStats();
				break;
			case "spells":
				cha = cm.getCharacter(player);
				cha.sendSpells();
				break;
			case "spellbook":
				cha = cm.getCharacter(player);
				cha.sendSpellBook();
				break;
			case "stats":
				if(args.length == 1){
					cha = cm.getCharacter(Bukkit.getPlayer(args[0]));
					cha.sendStats(player);
				}
				else{
					cm.getCharacter(player).sendStats(player);
				}
				break;
			case "chatty":
				SkillsProfile sp = EL.getPlayerProfile(player);
				if(sp.chatty){
					sp.chatty = false;
				}
				else{
					sp.chatty = true;
				}
				break;
			case "check":
				check(Bukkit.getPlayer(args[0]), player);
				break;
			case "resetpoints":
				cha = cm.getCharacter(player);
				cha.resetStats();
				break;
			case "races":
				cm.getRaceManager().openRaceWindow(player);
				break;
			case "rpgod":
				if(cm.getCharacter(player).isGod){
					cm.getCharacter(player).ungod("");
				}
				else{
					cm.getCharacter(player).god();
				}
				break;
			case "raceinfo":
				String racename = "";
				for(String arg : args){
					racename += arg + " ";
				}
				racename = racename.trim();
				try{
					cm.getRaceManager().raceInfo(player, racename);
				}
				catch(NullPointerException e){
					player.sendMessage(ChatColor.RED + "Race doesn't exist...");
				}
				break;
			case "addstat":
				if(args.length < 1){
					player.sendMessage(ChatColor.RED + "You need to enter a stat");
					player.sendMessage(ChatColor.BLUE + "example : /addstat str");
					return false;
				}
				StatType ST = null;
				String stat = args[0].toLowerCase();
				if ("strength".contains(stat)){
					ST = StatType.STRENGTH;
				}
				else if ("constitution".contains(stat)){
					ST = StatType.CONSTITUTION;
				}
				else if ("dexterity".contains(stat)){
					ST = StatType.DEXTERITY;
				}
				else if ("agility".contains(stat)){
					ST = StatType.AGILITY;
				}
				else if ("wisdom".contains(stat)){
					ST = StatType.WISDOM;
				}
				else if ("intelligence".contains(stat)){
					ST = StatType.INTELLIGENCE;
				}else{
					player.sendMessage("Invalid stat type, '" + stat + "'");
					return false;
				}
				cm.getCharacter(player).addStat(ST);
				break;
			case "battlestats":
				cm.getCharacter(player).sendBattleStats(player);
				break;
			case "viewbinds":
				cm.getCharacter(player).showBinds();
				break;
			case "pc":
				String line = ChatColor.DARK_AQUA + "[Party]" + player.getName() + " : ";
				for(String word : args){
					line += word + " ";
				}
				if(cm.getCharacter(player).currentParty != null){
					cm.getCharacter(player).currentParty.sendMessage(line);
				}
				break;
			case "gearup":
				cha = cm.getCharacter(player);
				long currentTime = System.currentTimeMillis() / 1000;
				if(currentTime > cha.lastGearClaim + 1200){
					try{
						String set = args[0].toLowerCase();
						if("fighter".contains(set)){
							IG.spawnWeapon((cha.Level - 8), cha.owner.getLocation(), "Common", "sword", cha.owner);
							IG.spawnWeapon((cha.Level - 8), cha.owner.getLocation(), "Common", "axe", cha.owner);
							giveArmorSet(cha);
							cha.owner.getInventory().addItem(new ItemStack(Material.POTION, 3));
							cha.sendMessage(ChatColor.AQUA + "Successfully spawned " + ChatColor.GOLD + "Fighter" + ChatColor.AQUA + " set!");
						}
						else if("defender".contains(set)){
							IG.spawnWeapon((cha.Level - 8), cha.owner.getLocation(), "Common", "sword", cha.owner);
							IG.spawnWeapon((cha.Level - 8), cha.owner.getLocation(), "Common", "mace", cha.owner);
							giveArmorSet(cha);
							cha.owner.getInventory().addItem(new ItemStack(Material.POTION, 3));
							cha.sendMessage(ChatColor.AQUA + "Successfully spawned " + ChatColor.GOLD + "Defender" + ChatColor.AQUA + " set!");
						}
						else if("ranger".contains(set)){
							IG.spawnWeapon((cha.Level - 8), cha.owner.getLocation(), "Common", "bow", cha.owner);
							player.getInventory().addItem(new ItemStack(Material.ARROW, 64));
							giveArmorSet(cha);
							cha.owner.getInventory().addItem(new ItemStack(Material.POTION, 3));
							cha.sendMessage(ChatColor.AQUA + "Successfully spawned " + ChatColor.GOLD + "Ranger" + ChatColor.AQUA + " set!");
						}
						else if("mage".contains(set)){
							IG.spawnWeapon((cha.Level - 8), cha.owner.getLocation(), "Common", "staff", cha.owner);
							giveArmorSet(cha);
							cha.owner.getInventory().addItem(new ItemStack(Material.POTION, 3));
							cha.sendMessage(ChatColor.AQUA + "Successfully spawned " + ChatColor.GOLD + "Mage" + ChatColor.AQUA + " set!");
						}
						else{
							cha.sendMessage(ChatColor.AQUA + "You need to select from, " + 
									ChatColor.GOLD + "Fighter" + ChatColor.AQUA + ", " + 
									ChatColor.GOLD + "Defender" + ChatColor.AQUA + ", " + 
									ChatColor.GOLD + "Ranger" + ChatColor.AQUA + ", or " + 
									ChatColor.GOLD + "Mage"
									);
							cha.sendMessage(ChatColor.AQUA + "use /gearup <type>");
						}
						cha.lastGearClaim = currentTime;
					}
					catch(IndexOutOfBoundsException e){
						cha.sendMessage(ChatColor.AQUA + "You need to select from, " + 
								ChatColor.GOLD + "Fighter" + ChatColor.AQUA + ", " + 
								ChatColor.GOLD + "Defender" + ChatColor.AQUA + ", " + 
								ChatColor.GOLD + "Ranger" + ChatColor.AQUA + ", or " + 
								ChatColor.GOLD + "Mage"
								);
						cha.sendMessage(ChatColor.AQUA + "use /gearup <type>");
					}
				}
				break;
			case "save":
				cm.autoSaveCharacters();
				dm.saveDungeons();
				EL.saveAll();
				player.sendMessage(ChatColor.GOLD + "Saved RPGEnhanced");
				break;
			case "genitem":
				String item = args[0];
				String rarity = args[1];
				Integer level = Integer.parseInt(args[2]);
				
				if(weapons.contains(item)){
					IG.spawnWeapon(level, player.getLocation(), rarity, item, player);
				}
				else if(armor.contains(item)){
					IG.spawnArmor(level, player.getLocation(), rarity, item, player);
				}
				break;
			case "sell":
				openSellWindow(player);
				break;
			case "repair":
				repairItem(player);
				break;
//			case "chatty":
//				profile = EL.getPlayerProfile(player);
//				if(profile.chatty){
//					profile.chatty = false;
//					player.sendMessage(ChatColor.AQUA + "You have turned off experience gain messages");
//				}
//				else{
//					profile.chatty = true;
//					player.sendMessage(ChatColor.AQUA + "You are now recieving experience gain messages");
//				}
//				break;
			case "tutorial":
				player.getInventory().addItem(book);
				break;
			case "char":
				PlayerMenuPage pmp = new PlayerMenuPage(cm.getCharacter(player));
				pmp.show(player);
				pinvs.put(player, pmp);
				break;
			case "party":
				pm.handleCommand(player, args);
				break;
			case "dungeon":
				dm.handleCommand(player, args);
				break;
			case "cast":
				cha = cm.getCharacter(player);
				cha.cast(args);
				break;
			case "equip":
				cha = cm.getCharacter(player);
				cha.equipItem(player.getItemInHand());
				break;
			case "viewequip":
				cha = cm.getCharacter(player);
				player.openInventory(cha.equipment);
				break;
			case "bind":
				cha = cm.getCharacter(player);
				cha.addBind(player.getInventory().getHeldItemSlot(), args, false);
				break;
			case "unbind":
				cha = cm.getCharacter(player);
				cha.removeBind(player.getInventory().getHeldItemSlot());
				break;
			case "backpack":
				if(player.hasPermission("RPG.backpack")){
					cha = cm.getCharacter(player);
					player.openInventory(cha.backpack);
				}
				else{
					player.sendMessage(ChatColor.RED + "You do not have permission to use that!");
				}
				break;
			case "addtoken":
				String type = args[0].trim();
				cha = cm.getCharacter(player);
				if(type.equalsIgnoreCase("race")){
					try {
						Integer money = (int) Economy.getMoney(player.getName());
						if(money >= 10000){
							Economy.subtract(player.getName(), 10000);
							player.sendMessage(ChatColor.GOLD + "You have received a token to change your race! (/setrace <race>)" );
							cha.RaceToken += 1;
						}
						else{
							player.sendMessage(ChatColor.RED + "Not enough money!" );
						}
						
					} catch (UserDoesNotExistException | NoLoanPermittedException e) {
						player.sendMessage(ChatColor.DARK_RED + "An issue has occured..." );
					}
					
				}
				else if(type.equalsIgnoreCase("reset")){
					
					try {
						Integer money = (int) Economy.getMoney(player.getName());
						if(money >= 1000){
							Economy.subtract(player.getName(), 1000);
							player.sendMessage(ChatColor.GOLD + "You have completely reset your character to level 1!" );
							cha.getRace().create(cha);
							cha.TotalSkillPoints = 5;
							cha.CurrentSkillPoints = 5;
						}
						
					} catch (UserDoesNotExistException | NoLoanPermittedException e) {
						
					}
					
				}
				else if(type.equalsIgnoreCase("stat")){
					try {
						Integer money = (int) Economy.getMoney(player.getName());
						if(money >= 5000){
							Economy.subtract(player.getName(), 5000);
							player.sendMessage(ChatColor.GOLD + "You have regained all your stats!" );
							cha.getRace().create(cha);
							cha.CurrentSkillPoints = cha.TotalSkillPoints;
						}
						
					} catch (UserDoesNotExistException | NoLoanPermittedException e) {
						
					}	
				}
				break;
			}
		}
		else{
			if(commandLabel.equals("rankup")){
				Player player = Bukkit.getPlayer(args[0]);
				Rankup(player);
			}
		}
		
		return true;
	}
	
	private void check(Player player, Player player2) {
		ItemStack[] armor = player.getInventory().getArmorContents();
		Inventory inv = Bukkit.createInventory(null, 9, player.getDisplayName().substring(0, Math.min(10, player.getDisplayName().length())) + "'s Equipped Armor");
		inv.setContents(armor);
		player2.openInventory(inv);
	}

	public void giveArmorSet(Character cha){
		IG.spawnArmor((cha.Level - 8), cha.owner.getLocation(), "Common", "chest", cha.owner);
		IG.spawnArmor((cha.Level - 8), cha.owner.getLocation(), "Common", "legs", cha.owner);
		IG.spawnArmor((cha.Level - 8), cha.owner.getLocation(), "Common", "boots", cha.owner);
		IG.spawnArmor((cha.Level - 8), cha.owner.getLocation(), "Common", "helm", cha.owner);
	}
	
	public void checkTime(){
		for(World world : Bukkit.getServer().getWorlds()){
			long time = world.getTime();
			boolean change = false;
			
			if(time > 18000){
				if(daytime.get(world) != DayTime.Midnight){
					daytime.put(world, DayTime.Midnight);
					change = true;
				}
			}
			else if(time > 14000){
				if(daytime.get(world) != DayTime.Night){
					daytime.put(world, DayTime.Night);
					change = true;
				}
			}
			else if(time > 12000){
				if(daytime.get(world) != DayTime.Dusk){
					daytime.put(world, DayTime.Dusk);
					change = true;
				}
			}
			else if(time > 6000){
				if(daytime.get(world) != DayTime.Midday){
					daytime.put(world, DayTime.Midday);
					change = true;
				}
			}
			else if(time > 1000){
				if(daytime.get(world) != DayTime.Day){
					daytime.put(world, DayTime.Day);
					change = true;
				}
			}
			else if(time > 0){
				if(daytime.get(world) != DayTime.Dawn){
					daytime.put(world, DayTime.Dawn);
					change = true;
				}
			}
			
			if(change){
				TimeChangeEvent event = new TimeChangeEvent(world, daytime.get(world));
				Bukkit.getPluginManager().callEvent(event);
			}
		}
	}
	
	@EventHandler
	public void onTimeChange(TimeChangeEvent event){
		if(event.getWorld().getName() == "CODWarfare"){
			return;
		}
		
		DayTime time = event.getDayTime();
		World world = event.getWorld();
		String timeTag = ChatColor.DARK_AQUA + "[Time] ";
		switch(time){
		case Night:
			for(Player player : world.getPlayers()){
				player.sendMessage(timeTag + ChatColor.GRAY + "Night has fallen...");
			}
			this.daytime.put(world, DayTime.Night);
			break;
		case Dawn:
			for(Player player : world.getPlayers()){
				player.sendMessage(timeTag + ChatColor.YELLOW + "It is now dawn");
			}
			this.daytime.put(world, DayTime.Dawn);
			break;
		case Day:
			for(Player player : world.getPlayers()){
				player.sendMessage(timeTag + ChatColor.GREEN + "It is now day");
			}
			this.daytime.put(world, DayTime.Day);
			break;
		case Dusk:
			for(Player player : world.getPlayers()){
				player.sendMessage(timeTag + ChatColor.DARK_AQUA + "It is now dusk..");
			}
			this.daytime.put(world, DayTime.Dusk);
			break;
		case Midday:
			for(Player player : world.getPlayers()){
				player.sendMessage(timeTag + ChatColor.AQUA + "It is now midday");
			};
			this.daytime.put(world, DayTime.Midday);
			break;
		case Midnight:
			for(Player player : world.getPlayers()){
				player.sendMessage(timeTag + ChatColor.DARK_GRAY + "It is now midnight");
			};
			this.daytime.put(world, DayTime.Midnight);
			break;
		}
		
	}

	public DayTime getDayTime(World world){
		return daytime.get(world);
	}
	
	@EventHandler
	public void onNightTime(TimeChangeEvent event){
		DayTime time = event.getDayTime();
		if(time == DayTime.Night){
			World world = event.getWorld();
			if(!world.getName().toLowerCase().equals("world2")){
				for(Entity ent : world.getLivingEntities()){
					if(ent instanceof Monster){
						if(ent.getCustomName().contains("[Lvl.")){
							ent.remove();
						}
					}
				}
			}
		}
		else if(time == DayTime.Midnight){
			final World world = event.getWorld();
			world.setGameRuleValue("doDaylightCycle", "false");
			Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){

				@Override
				public void run() {
					world.setGameRuleValue("doDaylightCycle", "true");
					
				}
				
				
			}, 12000); //Extends night by 10 minutes
		}
	}
	
	@EventHandler
	public void onPlayerMenuClick(InventoryClickEvent e){
		
		if(e.getInventory().getName().contains("Core Stats")){
			Player player = (Player) e.getWhoClicked();
			if(pinvs.containsKey(player)){
				PlayerMenuPage pmp = pinvs.get(player);
				pmp.handleEvent(e.getCurrentItem(), this);
				e.setCancelled(true);
			}
			
		}
		
	}
	
	@EventHandler
	public void onDayTime(TimeChangeEvent event){
		DayTime time = event.getDayTime();
		if(time == DayTime.Midday){
			final World world = event.getWorld();
			world.setGameRuleValue("doDaylightCycle", "false");
			Bukkit.getScheduler().scheduleSyncDelayedTask(this, new Runnable(){

				@Override
				public void run() {
					world.setGameRuleValue("doDaylightCycle", "true");
					
				}
				
				
			}, 12000); // extends day by 10 minutes
		}
	}
	
	private void openSellWindow(Player player){
		Inventory inv = Bukkit.createInventory(player, 27, "Sell Window");
		player.openInventory(inv);
	}
	
	@EventHandler
	private void onInventoryClose(InventoryCloseEvent event){
		if(event.getPlayer().getWorld().getName() == "CODWarfare"){
			return;
		}
		
		Inventory inv = event.getInventory();
		Player player = (Player) event.getPlayer();
		if(inv.getName() == "Sell Window"){
			Integer total = 0;
			Integer value = 0;
			for(ItemStack item : inv.getContents()){
				if(item != null){
					if(item.getType() != Material.AIR){
						value = sellItem(item);
						if(value == 0){
							player.getInventory().addItem(item);
							player.sendMessage(ChatColor.RED + "Couldn't sell item, " + item.getType().name());
						}
						else{
							total += sellItem(item);
						}
					}
				}
			}
			try {
				Economy.add(player.getName(), total);
			} catch (NoLoanPermittedException
					| UserDoesNotExistException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//player.getInventory().setItemInHand(new ItemStack(Material.AIR));
			
			if(total > 0){
				player.sendMessage(ChatColor.GOLD + "You made " + total + " total!");
			}
			else{
				player.sendMessage(ChatColor.GOLD + "You didn't make anything from that sale.");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	private Integer sellItem(ItemStack item) {
	
		Integer result = 0;
		
		if(item.hasItemMeta()){
			double cur = 0;
			double max = 0;
			Integer value = 0;
			for(String line : item.getItemMeta().getLore()){
				line = ChatColor.stripColor(line);
				if(line.contains("Value")){
					line = line.replace("Value : ", "").trim();
					value = Integer.parseInt(line);
				}
				else if(line.contains("Durability")){
	    			String dura = ChatColor.stripColor(line).replace("Durability : ", "");
	    			String[] duras = dura.trim().split("/");
	    			cur = Integer.parseInt(duras[0]);
	    			max = Integer.parseInt(duras[1]);
				}
			}
			result = (int) ((cur / max) * value);
		}
		
		return result;
		
		
	}

	@SuppressWarnings("deprecation")
	private void repairItem(Player player) {
		ItemStack item = player.getItemInHand();
		if(item.hasItemMeta()){
			double cur = 0;
			double max = 0;
			Integer value = 0;
			
			Integer index = 0;
			Integer duraLine = 0;
			
			ArrayList<String> lore = new ArrayList<String>();
			lore.addAll(item.getItemMeta().getLore());
			
			
			
			for(String line : lore){
				line = ChatColor.stripColor(line);
				if(line.contains("Value")){
					line = line.replace("Value : ", "").trim();
					value = Integer.parseInt(line);
				}
				else if(line.contains("Durability")){
					duraLine = index;
	    			String dura = ChatColor.stripColor(line).replace("Durability : ", "");
	    			String[] duras = dura.trim().split("/");
	    			cur = Integer.parseInt(duras[0]);
	    			max = Integer.parseInt(duras[1]);
				}
				index += 1;
			}
			
			Integer result = (int) (((max - cur) / max) * value) / 2;
			if(result <= 0){
				result = 1;
			}
			
			try {
				Economy.subtract(player.getName(), result);
			} catch (NoLoanPermittedException
					| UserDoesNotExistException e) {
				// TODO Auto-generated catch block
				return;
			}
			
			lore.set(duraLine, ChatColor.GRAY + "Durability : " + (int)max + "/" + (int)max);
			
			ItemStack newItem = new ItemStack(item.getType());
			ItemMeta im = newItem.getItemMeta();
			im.setLore(lore);
			im.setDisplayName(item.getItemMeta().getDisplayName());
			newItem.setItemMeta(im);
			
			player.getInventory().setItemInHand(newItem);
			
			player.sendMessage(ChatColor.GOLD + "You paid " + result + " to repair that piece of equipment.");
			
			
		}
		else{
			return;
		}
		
		
	}
	
	private void compareItem(Player player){
		ItemStack held = player.getItemInHand();
		ItemStack compare;
		if(held.getType().name().toLowerCase().contains("helm")){
			compare = player.getInventory().getHelmet();
		}
		else if(held.getType().name().toLowerCase().contains("chest")){
			compare = player.getInventory().getChestplate();
		}
		else if(held.getType().name().toLowerCase().contains("leg")){
			compare = player.getInventory().getLeggings();
		}
		else if(held.getType().name().toLowerCase().contains("boots")){
			compare = player.getInventory().getBoots();
		}
	}
	
	public double evaluateFormula(String formula, String source){
		double result = 0;
		try {
			result = (int) Math.round((double) engine.eval(formula));
		} catch (ScriptException e) {
			//TODO Auto-generated catch block
			console.sendMessage(ChatColor.DARK_RED + "[RpgEnhanced]: ERROR with " + source + "'s formula");
		}
		return result;
	}
	
	public boolean isInSafeZone(Location loc){
		if(loc.getWorld() == Bukkit.getWorld("world")){
			if(loc.getBlockX() > 3000 | loc.getBlockX() < -3000){
				return false;
			}
			if(loc.getBlockZ() > 3000 | loc.getBlockZ() < -3000){
				return false;
			}
		}
		return true;
	}
	
	public CharacterManager getCharacterManager() {
		// TODO Auto-generated method stub
		return this.cm;
	}
	
	@EventHandler
	public void setSurvival(PlayerChangedWorldEvent event){
		if(!event.getPlayer().hasPermission("worldguard.*")){
			event.getPlayer().setGameMode(GameMode.SURVIVAL);
		}
	}
	
	public void Rankup(Player player){
		PermissionUser user = PermissionsEx.getUser(player);
		String[] groups = user.getGroupsNames();
		String group = groups[0];
		String prefix = "";
		String rankupTo = "";
		Integer value = 0;
		
		if(group == "mod"){
			group = "member";
			prefix = "mod";
		}
		else if(group == "admin"){
			group = "member";
			prefix = "admin";
		}
		else if(group == "dev"){
			group = "member";
			prefix = "dev";
		}
		
		if(group.contains("mod")){
			prefix = "mod_";
			group = group.replace("mod_", "");
		}
		else if(group.contains("admin")){
			prefix = "admin_";
			group = group.replace("admin_", "");
		}
		else if(group.contains("dev")){
			prefix = "dev_";
			group = group.replace("dev_", "");
		}
		
		
		switch(group){
		case "member":
			rankupTo = "serf";
			break;
		case "serf":
			rankupTo = "commoner";
			break;
		case "commoner":
			rankupTo = "thane";
			break;
		case "thane":
			rankupTo = "vassal";
			break;
		case "vassal":
			rankupTo = "knight";
			break;
		case "knight":
			rankupTo = "baron";
			break;
		case "baron":
			rankupTo = "count";
			break;
		case "count":
			rankupTo = "duke";
			break;
		case "duke":
			rankupTo = "viceroy";
			break;
		case "viceroy":
			rankupTo = "regent";
			break;
		case "regent":
			rankupTo = "prince";
			break;
		case "prince":
			rankupTo = "king";
			break;
		case "king":
			rankupTo = "hero";
			break;
		case "hero":
			rankupTo = "god";
			break;
		case "god":
			return;
			//break;
		}
		
		switch(rankupTo){
		case "serf":
			value = 1000;
			break;
		case "commoner":
			value = 3000;
			break;
		case "thane":
			value = 5000;
			break;
		case "vassal":
			value = 10000;
			break;
		case "knight":
			value = 25000;
			break;
		case "baron":
			value = 50000;
			break;
		case "count":
			value = 75000;
			break;
		case "duke":
			value = 100000;
			break;
		case "viceroy":
			value = 130000;
			break;
		case "regent":
			value = 160000;
			break;
		case "prince":
			value = 200000;
			break;
		case "king":
			value = 250000;
			break;
		case "hero":
			value = 325000;
			break;
		case "god":
			value = 400000;
			break;
		}
		
		Integer bank = 0;
		
		try {
			bank = (int) Economy.getMoney(player.getName());
		} catch (UserDoesNotExistException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(bank < value){
			player.sendMessage(ChatColor.RED + "You don't have enough funds to purchase " + ChatColor.GOLD + rankupTo);
		}
		else{
			bank -= value;
			try {
				Economy.setMoney(player.getName(), bank);
			} catch (NoLoanPermittedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UserDoesNotExistException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Bukkit.broadcastMessage(ChatColor.GOLD + player.getName() + ChatColor.GREEN + " has ranked up to rank " + ChatColor.GOLD + rankupTo + ChatColor.GREEN + "!");
			
			String rankname = prefix + rankupTo;
			
			player.sendMessage(ChatColor.GREEN + "You have spent " + ChatColor.GOLD + value + ChatColor.GREEN + " to buy the " + ChatColor.GOLD + rankname + ChatColor.GREEN + " rank");
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex user " + player.getName() + " group set " + rankname);
		}
		
	}
	
}
