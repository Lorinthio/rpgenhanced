package me.Lorinth.RPGE.Parties;

import java.util.ArrayList;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Dungeons.Dungeon;
import me.Lorinth.RPGE.Managers.PartyManager;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class CharacterParty {

	private PartyManager manager;
	private ArrayList<Character> characters = new ArrayList<Character>();
	public Character leader;
	private boolean Synced = false;
	private Character syncTarget = null;
	private Integer syncLevel = 0;
	private Integer partySize = 1;
	private Integer partyLimit = 5;
	private boolean forceSynced = false;
	private boolean inDungeon = false;
	private Dungeon currentDungeon;
	
	private Team team;
	
	private Scoreboard board;
	
	public CharacterParty(Character cha, PartyManager mana){
		manager = mana;
		characters.add(cha);
		cha.currentParty = this;
		this.leader = cha;
		
		createScoreboard();
		
		team = board.registerNewTeam(cha.toString().substring(0, 15));
		team.addPlayer(cha.owner);
		team.setCanSeeFriendlyInvisibles(true);
		team.setAllowFriendlyFire(false);
	}
	
	public void giveMoneyToParty(double value, boolean tripleDrop, Character chara){
		Integer count = 1;
		ArrayList<Character> recievers = new ArrayList<Character>();
		recievers.add(chara);
		
		for(Character cha : characters){
			if(cha != chara){
				if(cha.owner.getWorld() == chara.owner.getWorld()){
					if(chara.owner.getLocation().distance(cha.owner.getLocation()) <= 50){
						count += 1;
						recievers.add(cha);
					}
				}
			}
		}
		
		Integer money = (int)(value / count);
		for(Character cha : recievers){
			cha.giveGold(money, tripleDrop);
		}
	}
	
	public void forceSync(Integer level){
		forceSynced = true;
		syncLevel = level;
		this.Synced = true;
		this.syncParty(level);
		
	}
	
	public ArrayList<Player> getPlayers(){
		ArrayList<Player> players = new ArrayList<Player>();
		for(Character cha : characters){
			players.add(cha.owner);
		}
		return players;
	}
	
	private Integer calculateAverageLevel(){
		Integer avg = 0;
		Integer total = 0;
		for(Character cha : characters){
			if(cha.Synced){
				total += cha.syncedLevel;
			}
			else {
				total += cha.Level;
			}
		}
		avg = total / characters.size();
		return avg;
	}
	
	public boolean isEmpty(){
		if(partySize == 0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public ArrayList<Character> getCharacters(){
		return characters;
	}
	
	public void sendMessage(String message){
		for(Character cha : characters){
			cha.sendMessage(message);
		}
	}
	
	public void sendMessageToLeader(String message){
		leader.sendMessage(message);
	}
	
	public void unSync(){
		for(Character cha : characters){
			cha.unsync("PARTY UNSYNC");
		}
		this.Synced = false;
		this.syncTarget = null;
		this.syncLevel = 0;
	}
	
	public boolean isSynced(){
		return Synced;
	}
	
	public void teleportToLocation(Location loc){
		for(Character cha : characters){
			cha.owner.teleport(loc);
		}
	}
	
	public void teleportToLocationMessage(Location loc, String message){
		for(Character cha : characters){
			cha.sendMessage(message);
			cha.owner.teleport(loc);
		}
	}
	
	public Integer getSyncLevel(){
		return syncLevel;
	}
	
	public Character getSyncTarget(){
		return syncTarget;
	}
	
	public void checkPartySync(Character chara){
		if(syncTarget == chara){
			syncParty(chara);
		}
	}
	
	public void giveExpToParty(float level, Character chara, Entity ent){
		Integer count = 1;
		ArrayList<Character> recievers = new ArrayList<Character>();
		recievers.add(chara);
		
		for(Character cha : characters){
			if(cha != chara){
				if(chara.owner.getWorld() == cha.owner.getWorld()){
					if(chara.owner.getLocation().distance(cha.owner.getLocation()) <= 50){
						count += 1;
						recievers.add(cha);
					}
				}
			}
		}
		
		Integer size = count;
		double flatExp = 75;
		Integer expMax = 125;
		float ratio = (float) (5 - (calculateAverageLevel() - level)) / 5;
		
		if(size == 2){
			flatExp = 140; // 70 per
			expMax = 260; // 130 max per
		}
		else if(size == 3){
			flatExp = 180; // 60 per
			expMax = 420; // 140 max per
		}
		else if(size == 4){
			flatExp = 200; // 50 per
			expMax = 600; // 150 max per
		}
		else if(size == 5){
			flatExp = 225; // 45 per
			expMax = 875; // 175 max per
		}
		
		Integer exp = (int) (ratio * flatExp);
		
		if(exp > expMax){
			exp = expMax;
		}
		
		exp = exp / size;
		
		if(exp < 5){
			exp = 5;
		}
		
		if(MobDifficulty.getPlugin().isDungeonMob(ent)){
			exp /= 2;
		}
		
		for(Character cha : recievers){
			cha.giveExp(exp);
		}
	}
	
	public void syncParty(Character chara){
		this.syncTarget = chara;
		this.Synced = true;
		this.syncLevel = chara.Level;
		for(Character cha : characters){
			if(cha.Synced){
				cha.unsync("PARTY RESYNC");
			}
			if(cha.Level > chara.Level){
				cha.sync(chara.Level, true);
				cha.forceSynced = forceSynced;
			}
		}
	}
	
	public void syncParty(Integer level){
		this.Synced = true;
		this.syncLevel = level;
		for(Character cha : characters){
			if(cha.Synced){
				cha.unsync("PARTY SYNC");
			}
			if(cha.Level > level){
				cha.sync(level, true);
				cha.forceSynced = forceSynced;
			}
		}
	}
	
	public void addCharacter(Character chara){
		if(partySize == partyLimit){
			leader.sendMessage(ChatColor.DARK_AQUA + "[Party] Cannot add anymore players!");
			return;
		}
		partySize += 1;
		
		for(Character cha : characters){
			cha.sendMessage(ChatColor.DARK_AQUA + "[Party] " + chara.owner.getDisplayName() + " has joined the party!");
		}
		
		characters.add(chara);
		
		chara.currentParty = this;
		chara.sendMessage(ChatColor.DARK_AQUA + "[Party] You have joined " + leader.owner.getDisplayName() + "'s party!");
		if(Synced){
			chara.sync((float) syncLevel, true);
		}
		
		team.addPlayer(chara.owner);
		
	}
	
	public void removePlayer(Player player, PartyReason reason){
		Character cha = manager.plugin.cm.getCharacter(player);
		removeCharacter(cha, reason);
		
		team.removePlayer(player);
		
	}
	
	public void removeCharacter(Character cha, PartyReason reason){
		if(leader.equals(cha)){
			leader = characters.get(0);
		}
		if(Synced){
			cha.unsync("PARTY LEAVE UNSYNC");
		}
		if(reason.equals(PartyReason.KICKED)){
			cha.sendMessage(ChatColor.RED + "You were kicked from the party");
			for(Character chara : characters){
				chara.sendMessage(ChatColor.RED + cha.owner.getDisplayName() + " was kicked from the party.");
			}
		}
		if(reason.equals(PartyReason.LEFT)){
			cha.sendMessage(ChatColor.RED + "You left the party");
			for(Character chara : characters){
				chara.sendMessage(ChatColor.RED + cha.owner.getDisplayName() + " has left the party.");
			}
		}
		if(reason.equals(PartyReason.DISBANDED)){
			cha.sendMessage(ChatColor.RED + "The party was disbanded.");
		}
		cha.currentParty = null;
		partySize -= 1;
		characters.remove(cha);
		
		Scoreboard clear = Bukkit.getScoreboardManager().getNewScoreboard();
		
		cha.owner.setScoreboard(clear);
	}
	
	public void changeLeader(Character cha){
		leader = cha;
		cha.sendMessage(ChatColor.GOLD + "You were promoted to party leader");
	}
	
	public void disbandParty(){
		@SuppressWarnings("unchecked")
		ArrayList<Character> kickedPlayers = (ArrayList<Character>) characters.clone();
		for(Character cha : kickedPlayers){
			removeCharacter(cha, PartyReason.DISBANDED);
		}
	}

	public int getSize() {
		return partySize;
	}
	
	public boolean isLeader(Player player){
		Character cha = manager.getCharacter(player);
		if(cha.equals(leader)){
			return true;
		}
		else{
			return false;
		}
	}

	public void healthUpdate(Character character) {
		//createScoreboard();
		board.clearSlot(DisplaySlot.SIDEBAR);
		
		Objective object1 = board.getObjective("Party");
		object1.unregister();
		
		Objective object = board.registerNewObjective("Party", "dummy");
		
		object.setDisplayName(ChatColor.DARK_AQUA + "Party");
		object.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		//Objective nametag = board.registerNewObjective("showHealth", "health");
		//nametag.setDisplaySlot(DisplaySlot.BELOW_NAME);
		
		Integer count = 1;
		
		for(Character cha : characters){
			Score score = object.getScore(ChatColor.BLUE + "(" + cha.Mana + " / " + cha.maxMana + ") " + ChatColor.YELLOW + "(" + cha.Stamina + " / " + cha.maxStamina + ")");
			
			String name = cha.owner.getDisplayName();
			Integer length = Math.min(name.length(), 7);
			
			Score score2 = object.getScore(cha.owner.getDisplayName().substring(0, length) + " "
					 + ChatColor.GREEN + " (" + cha.Health + " / " + cha.maxHealth + ")");
			score.setScore(count);
			score2.setScore(count);
			count += 1;
		}
		
		for(Character cha : characters){
			cha.updateScoreBoard(board);
		}
	}
	
	public void setDungeon(Dungeon dung){
		this.currentDungeon = dung;
		this.inDungeon = true;
	}
	
	public void removeDungeon(){
		this.currentDungeon = null;
		this.inDungeon = false;
	}
	
	public boolean inDungeon(){
		return inDungeon;
	}
	
	public Dungeon getDungeon(){
		return currentDungeon;
	}
	
	public void createScoreboard(){
		board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective object = board.registerNewObjective("Party", "dummy");
		object.setDisplayName(ChatColor.DARK_AQUA + "Party");
		object.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Objective nameTag = board.registerNewObjective("Name", "health");
		nameTag.setDisplaySlot(DisplaySlot.BELOW_NAME);
		
		Integer count = 1;
		
		for(Character cha : characters){
			
			Score score = object.getScore(ChatColor.BLUE + "(" + cha.Mana + " / " + cha.maxMana + ") " + ChatColor.YELLOW + "(" + cha.Stamina + " / " + cha.maxStamina + ")");
			
			String name = cha.owner.getDisplayName();
			Integer length = Math.min(name.length(), 7);
			
			Score score2 = object.getScore(cha.owner.getDisplayName().substring(0, length) + " "
					 + ChatColor.GREEN + " (" + cha.Health + " / " + cha.maxHealth + ")");
			score.setScore(count);
			score2.setScore(count);
			count += 1;
			
		}
		
		for(Character cha : characters){
			cha.owner.setScoreboard(board);
		}
	}
}
