package me.Lorinth.RPGE.EventHandler.Bosses;

import java.util.ArrayList;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.AttackType;
import me.Lorinth.RPGE.SkillAPI.Skill;

import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Boss {

	//General
	private EntityType type;
	private Entity ent;
	private Integer id;
	private String name;
	
	//Spawning Range
	private Integer levelmin;
	private Integer levelmax;
	
	
	//Combat stats
	private Integer Level = 1;
	
	private double healthMax;
	private Integer Str;
	private Integer Con;
	private Integer Dex;
	private Integer Agi;
	private Integer Wis;
	private Integer Int;
	private BossManager bm;
	
	//Skills
	private ArrayList<Skill> bossSkills;
	
	public Boss(BossManager bm, String name, EntityType type, int levelmin, int levelmax, double healthMax, int Str, int Con, int Dex, int Agi, int Wis, int Int){
		this.bm = bm;
		
		this.name = name;
		this.type = type;
		this.levelmin = levelmin;
		this.levelmax = levelmax;
		
		this.healthMax = healthMax;
		
		this.Str = Str;
		this.Con = Con;
		this.Dex = Dex;
		this.Agi = Agi;
		this.Wis = Wis;
		this.Int = Int;
	}
	
	public void setEntity(Entity e){
		this.ent = e;
	}
	
	public Entity getEntity(){
		return ent;
	}
	
	public Integer getLevel(){
		return Level;
	}
	
	public void setLevel(Integer level){
		Level = level;
	}
	
	public int getId(){
		return ent.getEntityId();
	}
	
	public boolean isEntity(Entity e){
		if(this.ent.equals(e)){
			return true;
		}
		return false;
	}
	
	public String getName(){
		return name;
	}
	
	public void AssignSkills(ArrayList<Skill> skills){
		this.bossSkills = skills;
	}
	
	public Boss Spawn(Location loc){
		//Create the normal Minecraft Entity
		Integer level = MobDifficulty.getPlugin().getLevelAtLocation(loc);
	
		Entity ent = loc.getWorld().spawnEntity(loc, type);
		ent.setCustomName(name);
		ent.setCustomNameVisible(true);
		
		this.ent = ent;
		this.id = ent.getEntityId();
		
		//Recalculate stats based on spawn location
		
		double ratio = getRatio(level);
		
		Integer Str = (int) ((this.Str - 10) * ratio) + 10;
		Integer Con = (int) ((this.Con - 10) * ratio) + 10;
		Integer Dex = (int) ((this.Dex - 10) * ratio) + 10;
		Integer Agi = (int) ((this.Agi - 10) * ratio) + 10;
		Integer Wis = (int) ((this.Wis - 10) * ratio) + 10;
		Integer Int = (int) ((this.Int - 10) * ratio) + 10;
		
		System.out.println("Name : " + name);
		
		System.out.println("Level : " + this.levelmin + "->" +level);
		
		System.out.println("Health : " + (healthMax + (this.Con * 5)) + "->" +(healthMax + (Con * 5)));
		System.out.println("Str : " + this.Str + " -> " + Str);
		System.out.println("Con : " + this.Con + " -> " + Con);
		System.out.println("Dex : " + this.Dex + " -> " + Dex);
		System.out.println("Agi : " + this.Agi + " -> " + Agi);
		System.out.println("Wis : " + this.Wis + " -> " + Wis);
		System.out.println("Int : " + this.Int + " -> " + Int);
		
		//Clone the boss into a new object with the new stats
		Boss newB = new Boss(bm, name, type, levelmin, levelmax, healthMax, Str, Con, Dex, Agi, Wis, Int);
		newB.setEntity(ent);
		newB.setLevel(level);
		
		
		//Set creatures health based on new stats...
		
		((Creature)ent).setMaxHealth(healthMax + (Con * 5));
		((Creature)ent).setHealth(healthMax + (Con * 5));
		
		return newB;
		
	}
	
	private double getRatio(Integer level){
    	double Character1 = level;
    	double Character2 = levelmin;
    	
    	double sp1 = 5;
    	
    	for(Integer Level=2; Level<=Character1; Level++){
			if(Level % 10 == 0){
				sp1 += 5;
			}else if (Level % 5 == 0){
				sp1 += 3;
			}else{
				sp1 += 2;
			}
    	}
    	
    	double sp2 = 5;
    	
    	for(Integer Level=2; Level<=Character2; Level++){
			if(Level % 10 == 0){
				sp2 += 5;
			}else if (Level % 5 == 0){
				sp2 += 3;
			}else{
				sp2 += 2;
			}
    	}
    	
    	//System.out.println("Character 1 Skill Points : " + sp);
    	
    	return sp1 / sp2;
	}

	public void damage(AttackType at, double d, Player p, boolean pureDamage){
		Character attacker = bm.getMain().cm.getCharacter(p);
		
		float attack = 0;
		float defense = 0;
		
		if(at == AttackType.Physical){
			attack = attacker.getAttack();
			defense = getDefense(AttackType.Physical);
		}
		else if(at == AttackType.Ranged){
			attack = attacker.getRangedAttack();
			defense = getDefense(AttackType.Ranged);
		}
		else {
			attack = attacker.getMagicAttack();
			defense = getDefense(AttackType.Magic);
		}
		
		float DamageMod = (attack + 10) / (defense + 10);
		if(DamageMod >= 2.5){
			DamageMod = (float) 2.5;
		}
		
		double damage = (double) (DamageMod * d) + 1;
		((Damageable) this.ent).setHealth(((Damageable) this.ent).getHealth() - damage);
		
		((LivingEntity) this.ent).setNoDamageTicks(15);
	}
	
	public int getDamage(AttackType at){
		if(at == AttackType.Physical){ // Touch based
			return 1 + (Str / 5);
		}
		else if(at == AttackType.Magic){ // Magic Touch based
			return 1 + (Int / 5);
		}
		else{ // Projectile based
			return 1 + (Dex / 5);
		}
	}
	
	public int getDefense(AttackType at){
		if(at == AttackType.Magic){ // Magic Touch based
			return 40 + Wis * 2;
		}
		else{
			return 40 + Con * 2;
		}
	}
	
	public boolean inLevelRange(int spawnLevel) {
		if(levelmin <= spawnLevel && spawnLevel <= levelmax){
			return true;
		}
		return false;
	}
	
}
