package me.Lorinth.RPGE.EventHandler.Bosses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class BossManager {

	private HashMap<String, Boss> bossBase = new HashMap<String,Boss>(); //Used for spawning new bosses
	private HashMap<Integer, Boss> curBosses = new HashMap<Integer, Boss>(); //Used for tracking alive bosses
	private RpgEnhancedMain main;
	private Random random = new Random();
	
	public BossManager(RpgEnhancedMain main){
		this.main = main;
		
		//Integer minTime = 36000; //30 minutes
		//Integer maxTime = 360000; //5 hours
		
		registerBosses();
		
		Integer minTime = 5 * 20;
		Integer maxTime = 10 * 20;
		
//		Integer spawnTime = (random.nextInt(maxTime - minTime) + minTime);
//		System.out.println("Spawn Time : " + spawnTime);
//		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){
//
//			@Override
//			public void run() {
//				spawnBosses();
//			}
//			
//		}, spawnTime);
	}
	
	public RpgEnhancedMain getMain(){
		return main;
	}
	
	public void registerBosses(){
		//Name, Type, min, max, health, STR, CON, DEX, AGI, WIS, INT
		register(new Boss(this, "Slardar", EntityType.ZOMBIE, 20, 50, 500, 30, 50, 10, 25, 30, 20));
		register(new Boss(this, "Haordi", EntityType.SKELETON, 30, 60, 500, 25, 40, 30, 30, 40, 30));
		//register(new Boss(""))
		
	}
	
	public void spawnBosses(){
		//System.out.println("Spawning Bosses");
		int size = Bukkit.getOnlinePlayers().length;
		int count = 1 + size / 10;
		
		for(int i=0;i<count;i++){
			final Location loc = randomLocation();
			
			//System.out.println("(" + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ() + ")");
			//System.out.println("Spawn location light = " + loc.getBlock().getLightLevel());
			
			int Level = MobDifficulty.getPlugin().getLevelAtLocation(loc);
			final ArrayList<Boss> available = new ArrayList<Boss>();
			
			//ArrayList<>
			for(Boss b : bossBase.values()){
				if(b.inLevelRange(Level)){
					available.add(b);
				}
			}
			
			if(available.size() == 0){
				i--;
			}
			else{
				int choice = random.nextInt(available.size());
				Boss b = available.get(choice).Spawn(loc);
				curBosses.put(b.getId(), b);
						
				Bukkit.broadcastMessage(ChatColor.DARK_PURPLE + "[Lvl." + b.getLevel() + "]" + b.getName() + " spawned at... " +ChatColor.GOLD + loc.getBlockX() + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + " (" + loc.getBlock().getLightLevel() + ")");
						

			}
		}
	}
	
	public Location randomLocation(){
		
		ArrayList<String> worlds = new ArrayList<String>();
		worlds.add("world");
		//Add more worlds here for more locations for random boss spawning
		
		World w = Bukkit.getWorld(worlds.get(random.nextInt(worlds.size())));
		
		int X = random.nextInt(12000) - 5999;
		int Z = random.nextInt(12000) - 5999;
		
		Location l = new Location(w, X, 0, Z);
		
		while((l.distanceSquared(new Location(w, 0, l.getY(), 0))) > 36000000){
			X = random.nextInt(12000) - 5999;
			Z = random.nextInt(12000) - 5999; // from -5999 to 5999
			
			l = new Location(w, X, 0, Z);
			l = getTopBlock(l);
			
			if(l == null){
				//Will break the while loop everytime, so need recursive step
				l = randomLocation();
			}
			
		}
		
		l = getTopBlock(l);
		while(l == null){
			l = randomLocation();
		}
		return l;
	}
	
	//Gets the surface block between 60 and 80
	public Location getTopBlock(Location l){
		for(int i=60; i<=80;i++){
			l.setY(i);
			if(l.getBlock().getType() == Material.DIRT || l.getBlock().getType() == Material.GRASS || l.getBlock().getType() == Material.STONE){
				//air1
				l.setY(i+1);
				if(l.getBlock().getType() == Material.AIR){
					l.setY(i+2);
					if(l.getBlock().getType() == Material.AIR){
						l.setY(i+3);
						if(l.getBlock().getType() == Material.AIR){
							//Offset to be in center of block instead of vertex of 4 blocks
							l.add(0.5, 0, 0.5);
							
							return l;
						}
					}
				}
			}
			
		}
		return null;
	}
	
	public void bossTracking(Player p){
		double dist = 0;
		Boss target = null;
		if(curBosses.values().isEmpty()){
			p.sendMessage(ChatColor.DARK_PURPLE + "No bosses to track");
			return;
		}
		for(Boss b : curBosses.values()){
			if(dist == 0){
				dist = p.getLocation().distanceSquared(b.getEntity().getLocation());
				target = b;
			}
			else{
				double check = p.getLocation().distanceSquared(b.getEntity().getLocation());
				if(check < dist){
					dist = check;
					target = b;
				}
			}
		}
		
		p.setCompassTarget(target.getEntity().getLocation());
	}
	
	private void register(Boss b){
		bossBase.put(b.getName(), b);
	}
	
	public boolean isBoss(Entity ent){
		return curBosses.containsKey(ent.getEntityId());
	}
	
	public Boss getBoss(Integer i){
		return curBosses.get(i);
	}
	
	public Boss getBoss(Entity e){
		return curBosses.get(e.getEntityId());
	}
	
}
