package me.Lorinth.RPGE.EventHandler;

public abstract class Task {

	public String id;
	public Integer delay;
	
	public Task(String name){
		id = name;
	}
	
	public String getId(){
		return id;
	}
	
	public void execute(){
		
	}
	
}
