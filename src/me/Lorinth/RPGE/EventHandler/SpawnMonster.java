package me.Lorinth.RPGE.EventHandler;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

public class SpawnMonster extends Task{

	private EntityType type;
	private Integer count;
	private Location loc;
	private Integer radius = 0;
	
	private Random random;
	
	public SpawnMonster(EntityType entType, Integer amount, Location loc){
		super("SpawnMonster");
		type = entType;
		count = amount;
		this.loc = loc;
	}
	
	public SpawnMonster(EntityType entType, Integer amount, Location loc, Integer radius){
		super("SpawnMonster");
		type = entType;
		count = amount;
		this.loc = loc;
		
		random = new Random();
	}
	
	@Override
	public void execute(){
		Location spawn;
		
		for(int x=0; x < count; x++){
			
			if(radius > 0){
				spawn = loc.add(random.nextInt(radius), random.nextInt(radius), random.nextInt(radius));
			}
			else{
				spawn = loc;
			}
				
			loc.getWorld().spawnEntity(spawn, type);
		}
	}
	
}
