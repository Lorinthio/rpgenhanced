package me.Lorinth.RPGE.EventHandler;

import me.Lorinth.RPGE.EventHandler.Bosses.BossManager;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;

public class RandomEventManager {

	private RpgEnhancedMain main;
	private BossManager bm;
	
	public RandomEventManager(RpgEnhancedMain m){
		main = m;
		bm = new BossManager(m);
	}
	
	public BossManager getBossManager(){
		return bm;
	}
	
}
