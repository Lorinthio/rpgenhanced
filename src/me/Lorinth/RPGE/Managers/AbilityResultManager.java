package me.Lorinth.RPGE.Managers;

import java.util.ArrayList;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Events.HealingEvent;
import me.Lorinth.RPGE.Events.SpellCastEvent;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.SkillAPI.Skill;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class AbilityResultManager implements Listener{

	private RpgEnhancedMain main;
	private ArrayList<Entity> stunnedMobs = new ArrayList<Entity>();
	
	
	public AbilityResultManager(RpgEnhancedMain main){
		this.main = main;
	}
	
	
	
	@EventHandler
	public void onSpellCast(SpellCastEvent event){
		Character cha = event.getCharacter();
		ArrayList<Entity> targets = event.getTargets();
		Skill skill = event.getSkill();
		String skillname = ChatColor.stripColor(skill.getName());
		
		switch(skillname){
		case "Shield Bash":
			for(final Entity et : targets){
				((LivingEntity) et).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20, 100));
				final Location loc = et.getLocation();
				if(et instanceof Player){
					stunnedMobs.add(et);
					((Player) et).sendMessage(ChatColor.YELLOW + "You have been stunned!");
					Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){

						@Override
						public void run() {
							stunnedMobs.remove(et);
							if(et instanceof Player){
								((Player) et).sendMessage(ChatColor.YELLOW + "You are no longer stunned...");
							}
						}
						
						
						
					}, 20);
				}
				else{
					for(int i=0; i<12; i++){
						Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){
							
							@Override
							public void run() {
								((Creature)et).teleport(loc);
							}
						
						}, i*5);
					}
				}
				
			}
		
		
		}
	}
	
	@EventHandler
	public void onEntityMove(PlayerMoveEvent event){
		if(stunnedMobs.contains(event.getPlayer())){
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerHeal(HealingEvent event){
		Vital statType = event.getVitalType();
		Character cha = event.getTarget();
		Integer value = event.getAmount();
		Character caster = event.getCaster();
		
		
		if(statType == Vital.HEALTH){
			Integer normal = cha.Health;
			cha.Health += value;
			cha.normalizeVitals();
			Integer difference = cha.Health - normal;
			if(difference > 0){
				cha.sendMessage(ChatColor.GREEN + "You were " + ChatColor.DARK_GREEN + " healed " + ChatColor.GREEN + " by " + caster.owner.getDisplayName() + ChatColor.GREEN + " for " + difference + "HP");
			}
		}
		else if(statType == Vital.MANA){
			Integer normal = cha.Mana;
			cha.Mana += value;
			cha.normalizeVitals();
			Integer difference = cha.Mana - normal;
			if(difference > 0){
				cha.sendMessage(ChatColor.GREEN + "You were " + ChatColor.BLUE + " refreshed " + ChatColor.GREEN + " by " + caster.owner.getDisplayName() + ChatColor.GREEN + " for " + difference + "MP");
			}
		}
		else if(statType == Vital.STAMINA){
			Integer normal = cha.Stamina;
			cha.Stamina += value;
			cha.normalizeVitals();
			Integer difference = cha.Stamina - normal;
			if(difference > 0){
				cha.sendMessage(ChatColor.GREEN + "You were " + ChatColor.YELLOW + " encouraged " + ChatColor.GREEN + " by " + caster.owner.getDisplayName() + ChatColor.GREEN + " for " + difference + "SP");
			}
		}
	}
	
}
