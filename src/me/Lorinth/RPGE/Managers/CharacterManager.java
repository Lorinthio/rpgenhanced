package me.Lorinth.RPGE.Managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Characters.SkillsProfile;
import me.Lorinth.RPGE.Characters.ThirstState;
import me.Lorinth.RPGE.Items.Title;
import me.Lorinth.RPGE.Items.socket;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.Parties.PartyReason;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

public class CharacterManager implements Listener{

	private RpgEnhancedMain main;
	private String directory;
	private Integer levelCap;
	private Integer goldDropChance = 40;
	private Random random = new Random();
	private ArrayList<Material> armors = new ArrayList<Material>();
	public HashMap<Player, Character> Characters = new HashMap<Player, Character>();
	private ArrayList<Character> logCharacters = new ArrayList<Character>();
	
	private HashMap<ItemStack, Integer> playerSlots = new HashMap<ItemStack, Integer>();
	private HashMap<ItemStack, Integer> socketPosition = new HashMap<ItemStack, Integer>();
	private HashMap<Player, ItemStack> socketting = new HashMap<Player, ItemStack>();
	
	private RaceManager rm;
	
	private HashMap<Entity, Player> lastSpell = new HashMap<Entity, Player>();
	
	//TODO
	//Add the combat toggle for characters
	//create backpack(for extra space, possible for water containers)

	//In the character manager
	//Ran every 10 seconds
	public void updateThirst(){
		Integer thirstRate = 0;
		for(Character cha : Characters.values()){
			Player player = cha.owner;
			
			if(player.getGameMode() == GameMode.SURVIVAL){
				if(player.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
					return;
				}
				double temp = player.getLocation().getBlock().getTemperature();
				thirstRate = (int) (temp / 0.3);
				
				if(thirstRate < 1){
					thirstRate = 1;
				}
	
				if(player.isSprinting()){// | cha.inCombat()){
					thirstRate += 2;
				}
	
				cha.hydration -= thirstRate;
				if(cha.hydration < 0){
					cha.hydration = -1;
				}
	
				checkThirstState(cha);
			}
		
		}

	}
	
	@EventHandler
	public void onSocket(PlayerInteractEvent e){
		if(e.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) || e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			ItemStack i = e.getItem();
			try{
				if(i.getType() == Material.SLIME_BALL || i.getType() == Material.MAGMA_CREAM){
					if(i.hasItemMeta()){
						ItemMeta im = i.getItemMeta();
						List<String> lore = im.getLore();
						if(lore.get(0).contains("Weapon Socket")){
							Player p = e.getPlayer();
							this.socketting.put(p, i);
							openSocketableEquipment(p, socket.Weapon);
						}
						else if(lore.get(0).contains("Armor Socket")){
							Player p = e.getPlayer();
							this.socketting.put(p, i);
							openSocketableEquipment(p, socket.Armor);
						}
						e.setCancelled(true);
					}
				}
			}
			catch(NullPointerException e2){
				
			}
		}
	}
	
	public void openSocketableEquipment(Player p, socket type){
		Inventory socketable = Bukkit.createInventory(null, 27, ChatColor.GREEN + "Socketable Equipment");
		for(int i = 0; i <= 35; i++) {
			ItemStack item = p.getInventory().getItem(i);
			try{
				if(type == socket.Weapon){
					if(main.getItemGenerator().isSocketableWeapon(item)){
						socketable.addItem(item);
						this.playerSlots.put(item, i);
					}
				}
				else if(type == socket.Armor){
					if(main.getItemGenerator().isSocketableArmor(item)){
						socketable.addItem(item);
						this.playerSlots.put(item, i);
					}
				}
				if(item.equals(socketting.get(p))){
					this.socketPosition.put(item, i);
				}
			}
			catch(NullPointerException e){
				//Checking Air
			}
		}
		p.openInventory(socketable);
	}
	
	@EventHandler
	public void onSocketInventoryClick(InventoryClickEvent e){
		if(e.getWhoClicked().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		try{
			if(e.getInventory().getName().contains("Socketable Equipment")){
				e.setCancelled(true);
			}
			if(e.getClickedInventory().getName().contains("Socketable Equipment")){
				ItemStack item = e.getInventory().getItem(e.getSlot());
				Integer replace = 0;
				if(playerSlots.containsKey(item)){
					replace = playerSlots.get(item);
				}
				if(item != null){
					ItemMeta itemMeta = item.getItemMeta();
					List<String> itemLore = itemMeta.getLore();
					Player p = (Player) e.getWhoClicked();
					if(this.socketting.containsKey(p)){
						ItemStack socket = socketting.get(p);
						String rarityColor = "";
						if(socket.getItemMeta().getLore().get(0).contains("Armor")){
							rarityColor = ChatColor.getLastColors(itemLore.get(3));
						}
						else if(socket.getItemMeta().getLore().get(0).contains("Weapon")){
							rarityColor = ChatColor.getLastColors(itemLore.get(4));
						}
						
						List<String> lore = socket.getItemMeta().getLore();
						for(int j=4; j < lore.size(); j++){
							//for bonus in socket...
							String[] words = lore.get(j).split(" ");
							String key = words[0];
							Integer keyValue = 0;
							if(words[2].equals("+")){
								keyValue = Integer.parseInt(words[3]);
								key += " " + words[1];
							}
							else{
								keyValue = Integer.parseInt(words[2]);
							}
							 
							//If it was found in the socketed item
							boolean found = false;
							for(int f=0; f < itemLore.size(); f++){
								//for line in the item clicked
								String line = itemLore.get(f);
								//If the stat bonus is in this line, then add bonus
								if(line.contains(key)){
									try{
										String lineColor = ChatColor.getLastColors(line);
										String strippedLine = ChatColor.stripColor(line);
										Integer value = Integer.parseInt(strippedLine.replace(key + " : ", ""));
										value += keyValue;
										//the stat was found
										found = true;
										
										String newline = lineColor + key + " : " + value;
										itemLore.set(f, newline);
									}
									catch(NumberFormatException e2){
										//pass
									}
								}
								//If its the empty socket slot, put name and closed socket symbol
								else if(line.contains("○ - <empty>")){
									//If we have reached socket points we need to add the stat
									if(!found){
										//Add bonus
										itemLore.add(f, rarityColor + key + " : " + keyValue);
										found = true;
										f += 1;
									}
									//Then fill socket
									itemLore.set(f, ChatColor.GOLD + "● - " + socket.getItemMeta().getDisplayName()); 
									itemMeta.setLore(itemLore);
									item.setItemMeta(itemMeta);
									
									e.setCancelled(true);
									
									p.getInventory().setItem(replace, item);
									ItemStack newSocket = p.getInventory().getItem(socketPosition.get(socket));
									newSocket.setAmount(newSocket.getAmount() - 1);
									p.getInventory().setItem(socketPosition.get(socket), newSocket);
									
									p.closeInventory();
									p.updateInventory();
									return;
								}
							}
						}
					}
				}
			}
		}
		catch(NullPointerException e2){
			//e2.printStackTrace();
		}
	}
	
	@EventHandler
	public void onWaterDrink(PlayerItemConsumeEvent event){
		if(event.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		
		Player p = event.getPlayer();
		ItemStack item = event.getItem();

		if(item.getTypeId() == 373){
			//Is  water bottle?
			ItemMeta meta = item.getItemMeta();
			List<String> lore = meta.getLore();
			Integer hydration = 500;
//			for(String line : lore){
//				if(line.contains("Hydration")){
//					hydration += Integer.parseInt(ChatColor.stripColor(line).replace("Hydration : ", ""));
//				}
//			}
			Character c = getCharacter(p);
			c.hydration += hydration;
			if(c.hydration > 2000){
				c.hydration = 2000;
			}
			checkThirstState(c);

		}

	}

	public void onWaterObtain(PlayerInteractEvent event){
		//Get the name of the water bottle....
		//if bottle hydration = 125;
		//if canteen hydration = 250;
		//if iron jug hydration = 500;
		//if diamond canteen = 1000;
	}

	public void checkThirstState(Character cha){
		Player player = cha.owner;
		if(cha.hydration > 1500){
			if(cha.thirstState != ThirstState.Hydrated){
				player.sendMessage(ChatColor.AQUA + "[Thirst] " + ChatColor.DARK_AQUA + ": You are well hydrated");
				cha.thirstState = ThirstState.Hydrated;
			}
		}
		if(cha.hydration <= 1500 & cha.hydration > 1000){
			if(cha.thirstState != ThirstState.Sweating){
				player.sendMessage(ChatColor.AQUA + "[Thirst] " + ChatColor.DARK_AQUA + ": You begin to break a sweat");
				cha.thirstState = ThirstState.Sweating;
			}
		}
		else if(cha.hydration <= 1000 & cha.hydration > 500){
			if(cha.thirstState != ThirstState.DryMouth){
				player.sendMessage(ChatColor.AQUA + "[Thirst] " + ChatColor.DARK_AQUA + ": Your mouth becomes dry");
				cha.thirstState = ThirstState.DryMouth;
			}
		}
		else if(cha.hydration <= 500 && cha.hydration > 200){
			if(cha.thirstState != ThirstState.LightHeaded){
				player.sendMessage(ChatColor.AQUA + "[Thirst] " + ChatColor.DARK_AQUA + ": You begin to get light headed");
				cha.thirstState = ThirstState.LightHeaded;
			}
		}
		else if(cha.hydration <= 200){
			if(cha.thirstState != ThirstState.Dizzy){
				player.sendMessage(ChatColor.AQUA + "[Thirst] " + ChatColor.DARK_AQUA + ": You are delusional"); //Add dizzy effect;
				cha.thirstState = ThirstState.Dizzy;
			}
			player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 120, 10));
		}
		
		if(cha.hydration <= 0){
			player.sendMessage(ChatColor.AQUA + "[Thirst] " + ChatColor.DARK_AQUA + ": You cant go on like this, your body begins to deteriorate");
			cha.thirstState = ThirstState.Dying;
			
			cha.Health -= 6;
		}
	}
	
	public void addLastSpellEvent(Entity entity, Player player){
		lastSpell.put(entity, player);
	}
	
	public CharacterManager(RpgEnhancedMain plugin){
		main = plugin;
		rm = new RaceManager(this);
		directory = main.getDataFolder() + File.separator + "Characters";
		levelCap = main.levelCap;
		storeArmors();
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){

			@Override
			public void run() {
				checkPlayersOnline();
				
			}
			
		}, 2);
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
			public void run(){
				RegenTick();
			}
		}, 0, 60);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable() {
			public void run(){
				autoSaveCharacters();
			}
		}, 0, 6000);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(main, new Runnable(){

			@Override
			public void run() {
				updateThirst();
			}
			
			
		}, 40, 200);
	}
	
	public RaceManager getRaceManager(){
		return rm;
	}
	
	public void storeArmors(){
		armors.add(Material.LEATHER_HELMET);
		armors.add(Material.LEATHER_CHESTPLATE);
		armors.add(Material.LEATHER_LEGGINGS);
		armors.add(Material.LEATHER_BOOTS);
		
		armors.add(Material.CHAINMAIL_HELMET);
		armors.add(Material.CHAINMAIL_CHESTPLATE);
		armors.add(Material.CHAINMAIL_LEGGINGS);
		armors.add(Material.CHAINMAIL_BOOTS);
		
		armors.add(Material.IRON_HELMET);
		armors.add(Material.IRON_CHESTPLATE);
		armors.add(Material.IRON_LEGGINGS);
		armors.add(Material.IRON_BOOTS);
		
		armors.add(Material.GOLD_HELMET);
		armors.add(Material.GOLD_CHESTPLATE);
		armors.add(Material.GOLD_LEGGINGS);
		armors.add(Material.GOLD_BOOTS);
		
		armors.add(Material.DIAMOND_HELMET);
		armors.add(Material.DIAMOND_CHESTPLATE);
		armors.add(Material.DIAMOND_LEGGINGS);
		armors.add(Material.DIAMOND_BOOTS);
	}
	
	public SkillsProfile getSkills(Player player){
		SkillsProfile profile = main.EL.getPlayerProfile(player);
		return profile;
	}
	
	public Integer getLevelCap(){
		return levelCap;
	}
	
	@EventHandler
	public void onPlayerEquip(InventoryClickEvent event){
		if(event.getWhoClicked().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		final Player player = (Player) event.getWhoClicked();
		if(event.getClick().equals(ClickType.SHIFT_LEFT)){
			if(event.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)){
				ItemStack item = event.getCurrentItem();
				if(item.hasItemMeta()){
					if(armors.contains(item.getType())){
						Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){

							@Override
							public void run() {
								getCharacter(player).update(false);
								
							}

						}, 10);
					}
				}
			}
		}
		if(event.getInventory().getName().contains("'s Equipment")){
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		loadCharacter(player);
		
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event){
		Player player = event.getPlayer();
		try{
			saveCharacter(player, true);
		}
		catch(NullPointerException e){
			//Player was kicked
		}
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerKickEvent event){
		Player player = event.getPlayer();
		try{
			saveCharacter(player, true);
		}
		catch(NullPointerException e){
			//Player was kicked
		}
	}
	
	public final Character getCharacter(Player player){
		return Characters.get(player);
	}
	
	public void RegenTick(){
		for(Character c : Characters.values()){
			c.regen();
		}
	}
	
	public void checkPlayersOnline(){
		for(Player player : Bukkit.getOnlinePlayers()){
			if(!Characters.containsKey(player)){
				loadCharacter(player);
			}
		}
	}
	
	public final Integer getLevel(Entity entity){
		Integer Level = 1;
		try{
			if(entity instanceof Creature){
				String name = ((Creature) entity).getCustomName();
				name = ChatColor.stripColor(name);
				String level = "";
				for(String letter : name.split("")){
					if(isInteger(letter)){
						level = level + letter;
					}
				}
				Level = Integer.parseInt(level);
			}
		}
		catch(NullPointerException | NumberFormatException e){
			//Mob doesn't have level, return result will be 1
		}
		return Level;
	}
	
	public boolean isInteger( String input ) {
	    try {
	        Integer.parseInt( input );
	        return true;
	    }
	    catch( NumberFormatException e ) {
	        return false;
	    }
	}
	
	public void loadCharacter(Player player){
		Character c = new Character(this, player);
		boolean newC = false;
		File file = new File(directory, player.getUniqueId().toString() + ".yml");
		if(file.exists()){
			YamlConfiguration fc = YamlConfiguration.loadConfiguration(file);
			
			c.Level = fc.getInt("Level");
			c.Experience = fc.getInt("Experience");
			c.ToNextLevel = fc.getInt("ToNextLevel");
			
			c.CurrentSkillPoints = fc.getInt("CurrentSkillPoints");
			c.TotalSkillPoints = fc.getInt("TotalSkillPoints");
			
			c.Strength = fc.getInt("Strength");
			c.Constitution = fc.getInt("Constitution");
			c.Dexterity = fc.getInt("Dexterity");
			c.Agility = fc.getInt("Agility");
			c.Wisdom = fc.getInt("Wisdom");
			c.Intelligence = fc.getInt("Intelligence");
			
			
			c.RaceToken = fc.getInt("RaceToken");
			c.ResetToken = fc.getInt("ResetToken");
			String racename = fc.getString("Race");
			if(racename == null){
				racename = "Human";
				newC = true;
			}
			else if(racename == ""){
				racename = "Human";
				newC = true;
			}
			
			c.hydration = fc.getInt("Hydration");
			if(c.hydration == 0){
				c.hydration = 1000;
			}
			
			if(newC){
				c.resetStats();
			}
			else{
				c.setRace(rm.getRaceByName(racename), false);
			}
			
			c.update(false);
			
			HashMap<Integer, String> bindings = new HashMap<Integer, String>();
			bindings.put(0, fc.getString("Bind.0"));
			bindings.put(1, fc.getString("Bind.1"));
			bindings.put(2, fc.getString("Bind.2"));
			bindings.put(3, fc.getString("Bind.3"));
			bindings.put(4, fc.getString("Bind.4"));
			bindings.put(5, fc.getString("Bind.5"));
			bindings.put(6, fc.getString("Bind.6"));
			bindings.put(7, fc.getString("Bind.7"));
			bindings.put(8, fc.getString("Bind.8"));
			c.putBindings(bindings);
			
			c.loadBackpack(fc);
			
			Integer hp = fc.getInt("Health");
			Integer mp = fc.getInt("Mana");
			Integer sp = fc.getInt("Stamina");
			if(hp <= 0){
				hp = 1;
			}
			if(mp <= 0){
				mp = 1;
			}
			if(sp <= 0){
				sp = 1;
			}
			
			c.Health = hp;
			c.Mana = mp;
			c.Stamina = sp;
			
			c.updateHealth();
			
			Characters.put(player, c);
		}
		else{
			c.update(false);
			Characters.put(player, c);
			saveCharacter(player, false);
			//player.sendMessage("Your character has been created!");
		}
		c.profile = main.EL.getPlayerProfile(player);
		
	}
	
	//Cast and wand
	@EventHandler
	public void onCast(PlayerInteractEvent event){
		if(event.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		if(event.getAction().equals(Action.LEFT_CLICK_AIR)){
			Player player = event.getPlayer();
			ItemStack item = player.getItemInHand();
			if(item.hasItemMeta()){
				try{
					if(item.getItemMeta().getLore().get(0) != ""){
						if(item.getType() == Material.STICK){
							getCharacter(player).ShootMagicMissle();
						}
						if(item.getType() == Material.BLAZE_ROD){
							getCharacter(player).ShootMagicMissle();
						}
					}
				}
				catch(NullPointerException e){
					//pass empty error
				}
			}
		}
		else if(event.getAction().equals(Action.RIGHT_CLICK_AIR)){
			Player player = event.getPlayer();
			getCharacter(player).checkCast();
		}
	}
	
	public void autoSaveCharacters(){
		for(Player p : Characters.keySet()){
			saveCharacter(p, false);
		}
	}
	
	public void saveCharacter(Player player, boolean remove){
		Character c = Characters.get(player);
		
		File file = new File(directory, player.getUniqueId() + ".yml");
		YamlConfiguration fc = YamlConfiguration.loadConfiguration(file);
		
		if(c.Synced){
			c.unsync("SAVING");
		}
		if(c.isGod){
			c.ungod("SAVING");
		}
		
		fc.set("LastDisplayName", player.getName());
		fc.set("Race", c.getRace().getName());
		fc.set("Level", c.Level);
		fc.set("Experience", c.Experience);
		fc.set("ToNextLevel", c.ToNextLevel);
		
		fc.set("Health", c.Health);
		fc.set("Mana", c.Mana);
		fc.set("Stamina", c.Stamina);
		
		fc.set("RaceToken", c.RaceToken);
		fc.set("ResetToken", c.ResetToken);
		fc.set("CurrentSkillPoints", c.CurrentSkillPoints);
		fc.set("TotalSkillPoints", c.TotalSkillPoints);
		
		fc.set("Strength", c.Strength);
		fc.set("Constitution", c.Constitution);
		fc.set("Dexterity", c.Dexterity);
		fc.set("Agility", c.Agility);
		fc.set("Wisdom", c.Wisdom);
		fc.set("Intelligence", c.Intelligence);
		
		fc.set("Bind", null);
		
		c.saveBackpack(fc);
		
		HashMap<Integer, String> bindings = c.getBindings();
		if(bindings.get(0) != null){
			fc.set("Bind.0", ChatColor.stripColor(bindings.get(0)));
		}
		if(bindings.get(1) != null){
			fc.set("Bind.1", ChatColor.stripColor(bindings.get(1)));
		}
		if(bindings.get(2) != null){
			fc.set("Bind.2", ChatColor.stripColor(bindings.get(2)));
		}
		if(bindings.get(3) != null){
			fc.set("Bind.3", ChatColor.stripColor(bindings.get(3)));
		}
		if(bindings.get(4) != null){
			fc.set("Bind.4", ChatColor.stripColor(bindings.get(4)));
		}
		if(bindings.get(5) != null){
			fc.set("Bind.5", ChatColor.stripColor(bindings.get(5)));
		}
		if(bindings.get(6) != null){
			fc.set("Bind.6", ChatColor.stripColor(bindings.get(6)));
		}
		if(bindings.get(7) != null){
			fc.set("Bind.7", ChatColor.stripColor(bindings.get(7)));
		}
		if(bindings.get(8) != null){
			fc.set("Bind.8", ChatColor.stripColor(bindings.get(8)));
		}
		
		fc.set("Hydration", c.hydration);
		
		try{
			fc.save(file);
		}
		catch(IOException error){
			//error.printStackTrace();
			main.console.sendMessage(ChatColor.RED + "[PlayerDevelopment]: Failed to save character for... " + player.getDisplayName());
		}
		
		if(remove){
			try{
				if(c.currentParty != null){
					c.currentParty.removeCharacter(c, PartyReason.LEFT);
				}
			}
			catch(NullPointerException e){
				
			}
			Characters.remove(player);
		}
		else{
			try{
				if(c.currentParty.isSynced()){
					c.sync(c.syncedLevel, false);
				}
			}
			catch(NullPointerException e){
				//No party
			}
			if(c.isGod){
				c.god();
			}
		}
	}
	
	//GOLD AND EXP REWARD
	@EventHandler(priority = EventPriority.HIGH)
	public void KillReward(EntityDeathEvent event){
		
		Entity target = event.getEntity();
		if(target instanceof Player){
			return;
		}
		else if(target instanceof Creature){
			if(((Creature)target).isLeashed()){
				((Creature)target).setLeashHolder(null);
			}
		}
		Entity killer = null;
		
		EntityDamageEvent damageEvent = target.getLastDamageCause();
		
		Integer Level = 0;
		if ((damageEvent != null)) {
			if(damageEvent instanceof EntityDamageByEntityEvent){
				killer = ((EntityDamageByEntityEvent) damageEvent).getDamager();
			}
			else{
				killer = lastSpell.get(target);
				lastSpell.remove(target);
			}
			
			if(killer == null){
				return;
			}
			
			
			if ((killer instanceof Projectile)){
				Entity shooter = null;
				shooter = (Entity) ((Projectile) killer).getShooter();
				if(shooter instanceof Player){
					killer = shooter;
				}
				else{
					return;
				}
			}
			if(killer instanceof Player){
				if(killer.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
					return;
				}
				if(target instanceof Player){
					Level = getCharacter((Player) target).Level;
				}
				else{
					Level = getLevel(target);
				}
				if(target instanceof Monster){
					GiveExp(Level, (Player) killer, target);
					
					Integer roll = random.nextInt(100);
					if(roll < this.goldDropChance){
						GiveGold(Level, (Player) killer, false);
					}
					if(roll == this.goldDropChance){
						GiveGold(Level, (Player) killer, true);
					}
				}
			}	
		}
	}
	
	public RpgEnhancedMain getMain(){
		return main;
	}
	
	public void GiveExp(float level, Player player, Entity ent){
		Character character = getCharacter(player);
		if(character.currentParty != null){
			character.currentParty.giveExpToParty(level, character, ent);
		}
		else{
			Integer Level = character.Level;
			if(character.Synced){
				Level = character.syncedLevel; 
			}
			float levelmod = (float) (6 - (Level - level)) / 6;
			levelmod = (float) (levelmod * 75.00);
			Integer expgive = (int) levelmod;
			if (expgive >= 125){
				expgive = 125;
			}
			else if(expgive < 5){
				expgive = 5;
			}
			if(MobDifficulty.getPlugin().isDungeonMob(ent)){
				expgive /= 2;
			}
			if(MobDifficulty.getPlugin().isElite(ent)){
				expgive = expgive * 2;
			}
			character.giveExp(expgive);
		}
	}
	
	public void GiveGold(float level, Player player, boolean tripleDrop){
		Character character = getCharacter(player);
		
		Integer amountMin = 10 + (int)level / 3;
		Integer amountMax = 20 + (int)level / 2;
		Integer value = random.nextInt(amountMax - amountMin) + amountMin;
		
		if(tripleDrop){
			value *= 3;
		}
		
		if(character.currentParty != null){
			character.currentParty.giveMoneyToParty(value, tripleDrop, character);
		}
		else{
			character.giveGold(value, tripleDrop);
		}
	}
	
	public void giveRawExp(Integer exp, Player player){
		Character character = getCharacter(player);
		character.giveExp(exp);
	}

}
