package me.Lorinth.RPGE.Managers;

import java.util.ArrayList;
import java.util.HashMap;

import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import net.minecraft.server.v1_8_R1.EntityLiving;

import org.bukkit.craftbukkit.v1_8_R1.entity.CraftBat;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftBlaze;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftCaveSpider;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftChicken;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftCow;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftCreeper;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftEnderDragon;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftEnderman;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftEndermite;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftGhast;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftGiant;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftGuardian;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftHorse;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftIronGolem;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftMagmaCube;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPigZombie;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftSilverfish;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftSkeleton;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftSlime;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftSpider;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftWitch;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftWither;
import org.bukkit.craftbukkit.v1_8_R1.entity.CraftZombie;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;

public class ThreatManager {

	private RpgEnhancedMain main;
	private HashMap<Entity, HashMap<Player, Integer>> attackedMobs = new HashMap<Entity, HashMap<Player, Integer>>();
	private HashMap<Entity, Player> targets = new HashMap<Entity, Player>();
	private ArrayList<EntityType> controllableCheck = new ArrayList<EntityType>();


	public ThreatManager(RpgEnhancedMain main){
		this.main = main;
	}

	public void addThreat(Entity ent, Integer amount, Player player){
		if(attackedMobs.containsKey(ent)){
			HashMap<Player, Integer> threatTable = attackedMobs.get(ent);
			if(threatTable.containsKey(player)){
				threatTable.put(player, threatTable.get(player) + amount);
			}
			else{
				threatTable.put(player, amount);
			}
		}
		else{
			HashMap<Player, Integer> threatTable = new HashMap<Player, Integer>();
			threatTable.put(player, amount);
			attackedMobs.put(ent, threatTable);
		}

		checkTargetChange(ent);

	}

	public void checkTargetChange(Entity ent){
		HashMap<Player, Integer> threatTable = attackedMobs.get(ent);
		Integer value = 0;
		Player target = null;
		for(Player player : threatTable.keySet()){
			if(threatTable.get(player) > value){
				target = player;
			}
		}
		if(targets.get(ent) != target){
			targets.put(ent, target);
			changeTarget(ent, target);
		}
	}

	public void changeTarget(Entity ent, LivingEntity target){
		EntityLiving living;
		if(target instanceof Player){
			living = (EntityLiving) ((CraftPlayer) target).getHandle();
		}
		else{
			return;
		}
		EntityType type = ent.getType();
		switch(type){
		case BAT:
			CraftBat bat = (CraftBat) ent;
			bat.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case BLAZE:
			CraftBlaze blaze = (CraftBlaze) ent;
			blaze.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case CAVE_SPIDER:
			CraftCaveSpider ccs = (CraftCaveSpider) ent;
			ccs.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case CHICKEN:
			CraftChicken chick = (CraftChicken) ent;
			chick.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case COW:
			CraftCow cow = (CraftCow) ent;
			cow.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case CREEPER:
			CraftCreeper creeper = (CraftCreeper) ent;
			creeper.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case ENDER_DRAGON:
			CraftEnderDragon ed = (CraftEnderDragon) ent;
			ed.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case ENDERMAN:
			CraftEnderman em = (CraftEnderman) ent;
			em.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case ENDERMITE:
			CraftEndermite endermite = (CraftEndermite) ent;
			endermite.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case GIANT:
			CraftGiant giant = (CraftGiant) ent;
			giant.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case GHAST:
			CraftGhast ghast = (CraftGhast) ent;
			ghast.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case GUARDIAN:
			CraftGuardian guard = (CraftGuardian) ent;
			guard.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case HORSE:
			CraftHorse horse = (CraftHorse) ent;
			horse.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case IRON_GOLEM:
			CraftIronGolem golem = (CraftIronGolem) ent;
			golem.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case MAGMA_CUBE:
			CraftMagmaCube magma = (CraftMagmaCube) ent;
			magma.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case PIG_ZOMBIE:
			CraftPigZombie pz = (CraftPigZombie) ent;
			pz.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case SILVERFISH:
			CraftSilverfish sf = (CraftSilverfish) ent;
			sf.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case SLIME:
			CraftSlime slime = (CraftSlime) ent;
			slime.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case SKELETON:
			CraftSkeleton skele = (CraftSkeleton) ent;
			skele.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case SPIDER:
			CraftSpider spider = (CraftSpider) ent;
			spider.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case WITCH:
			CraftWitch witch = (CraftWitch) ent;
			witch.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case WITHER:
			CraftWither wither = (CraftWither) ent;
			wither.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		case ZOMBIE:
			CraftZombie zombie = (CraftZombie) ent;
			//zombie.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			zombie.getHandle().setGoalTarget(living, TargetReason.CUSTOM, true);
			break;
		default:
			break;
		}
	}

}
