package me.Lorinth.RPGE.Managers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import me.Lorinth.RPGE.Abilities.Agility.Cloak;
import me.Lorinth.RPGE.Abilities.Agility.DodgeBoostI;
import me.Lorinth.RPGE.Abilities.Agility.DodgeBoostII;
import me.Lorinth.RPGE.Abilities.Agility.DrainingBlade;
import me.Lorinth.RPGE.Abilities.Agility.Jump;
import me.Lorinth.RPGE.Abilities.Agility.ShadowsI;
import me.Lorinth.RPGE.Abilities.Agility.ShadowsII;
import me.Lorinth.RPGE.Abilities.Agility.Sprint;
import me.Lorinth.RPGE.Abilities.Agility.Sprinter;
import me.Lorinth.RPGE.Abilities.Agility.StabI;
import me.Lorinth.RPGE.Abilities.Agility.StabII;
import me.Lorinth.RPGE.Abilities.Agility.WitherBlade;
import me.Lorinth.RPGE.Abilities.Constitution.Defender;
import me.Lorinth.RPGE.Abilities.Constitution.DefenseBoostI;
import me.Lorinth.RPGE.Abilities.Constitution.DefenseBoostII;
import me.Lorinth.RPGE.Abilities.Constitution.EncouragingTaunt;
import me.Lorinth.RPGE.Abilities.Constitution.HealthRegenI;
import me.Lorinth.RPGE.Abilities.Constitution.Sentinel;
import me.Lorinth.RPGE.Abilities.Constitution.ShieldBash;
import me.Lorinth.RPGE.Abilities.Constitution.Stomp;
import me.Lorinth.RPGE.Abilities.Constitution.Taunt;
import me.Lorinth.RPGE.Abilities.Dexterity.CriticalBoostI;
import me.Lorinth.RPGE.Abilities.Dexterity.CriticalBoostII;
import me.Lorinth.RPGE.Abilities.Dexterity.DoubleShot;
import me.Lorinth.RPGE.Abilities.Dexterity.PoisonShot;
import me.Lorinth.RPGE.Abilities.Dexterity.PowerShotI;
import me.Lorinth.RPGE.Abilities.Dexterity.PowerShotII;
import me.Lorinth.RPGE.Abilities.Dexterity.RangedDamageBoostI;
import me.Lorinth.RPGE.Abilities.Dexterity.SummonArrows;
import me.Lorinth.RPGE.Abilities.Dexterity.TripleShot;
import me.Lorinth.RPGE.Abilities.Intelligence.FireballI;
import me.Lorinth.RPGE.Abilities.Intelligence.FireballII;
import me.Lorinth.RPGE.Abilities.Intelligence.IceBoltI;
import me.Lorinth.RPGE.Abilities.Intelligence.LightningStrikeI;
import me.Lorinth.RPGE.Abilities.Intelligence.MagicAttackBoostI;
import me.Lorinth.RPGE.Abilities.Intelligence.PartyTeleport;
import me.Lorinth.RPGE.Abilities.Intelligence.Poison;
import me.Lorinth.RPGE.Abilities.Intelligence.RecastMastery;
import me.Lorinth.RPGE.Abilities.Intelligence.Storm;
import me.Lorinth.RPGE.Abilities.Strength.AttackBoostI;
import me.Lorinth.RPGE.Abilities.Strength.BashI;
import me.Lorinth.RPGE.Abilities.Strength.BashII;
import me.Lorinth.RPGE.Abilities.Strength.Berserker;
import me.Lorinth.RPGE.Abilities.Strength.Cleave;
import me.Lorinth.RPGE.Abilities.Strength.Lift;
import me.Lorinth.RPGE.Abilities.Strength.StaminaPoolI;
import me.Lorinth.RPGE.Abilities.Strength.StaminaRegenI;
import me.Lorinth.RPGE.Abilities.Strength.Whirlwind;
import me.Lorinth.RPGE.Abilities.Wisdom.HealI;
import me.Lorinth.RPGE.Abilities.Wisdom.HealII;
import me.Lorinth.RPGE.Abilities.Wisdom.HealIII;
import me.Lorinth.RPGE.Abilities.Wisdom.HealingCircleI;
import me.Lorinth.RPGE.Abilities.Wisdom.HealingCircleII;
import me.Lorinth.RPGE.Abilities.Wisdom.HealingLight;
import me.Lorinth.RPGE.Abilities.Wisdom.ManaFountI;
import me.Lorinth.RPGE.Abilities.Wisdom.ManaFountII;
import me.Lorinth.RPGE.Abilities.Wisdom.ManaRegenerationI;
import me.Lorinth.RPGE.Abilities.Wisdom.NightVision;
import me.Lorinth.RPGE.Abilities.Wisdom.ProtectI;
import me.Lorinth.RPGE.Abilities.Wisdom.Regeneration;
import me.Lorinth.RPGE.Abilities.Wisdom.WardI;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.Skill;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public class SkillManager {

	private ArrayList<ActiveSkill> ActiveSkills = new ArrayList<ActiveSkill>();
	private HashMap<String, Skill> skills = new HashMap<String, Skill>();
	private ArrayList<PassiveSkill> PassiveSkills = new ArrayList<PassiveSkill>();
	
	private File f;
	private YamlConfiguration yc;
	
	private ScriptEngineManager factory = new ScriptEngineManager();
	private ScriptEngine engine = factory.getEngineByName("JavaScript");
	
	private RpgEnhancedMain plugin;
	
	public SkillManager(RpgEnhancedMain plug){
		plugin = plug;
		
		f = new File(plug.getDataFolder() + File.separator + "Skills", "SkillList.yml");
		yc = YamlConfiguration.loadConfiguration(f);
		
		registerSkills();
	}
	
	public double evaluateFormula(String formula){
		double result = 0;
		try {
			result = (int) Math.round((double) engine.eval(formula));
		} catch (ScriptException e) {
			//TODO Auto-generated catch block
			//console.sendMessage(ChatColor.DARK_RED + "[RpgEnhanced]: ERROR with " + source + "'s formula");
		}
		return result;
	}
	
	private void registerSkills(){
		//Strength
		register(new BashI()); //5
		register(new Lift()); //10
		register(new AttackBoostI());//10P
		register(new Cleave());//15
		register(new Berserker()); // 20
		register(new StaminaRegenI());//20P
		register(new Whirlwind()); //25
		register(new BashII());//30
		register(new StaminaPoolI());//30P
		//35
		//40
		//40P
		//45
		//50
		//50P
		
		//Constitution
		register(new Taunt()); //5
		register(new ShieldBash()); //10
		register(new DefenseBoostI()); // 10P
		register(new Sentinel()); // 15 
		register(new HealthRegenI()); // 20P
		register(new Stomp()); //20
		register(new Defender()); // 25
		register(new EncouragingTaunt()); //30
		register(new DefenseBoostII()); //30P
		//35
		//40
		//40P
		//45
		//50
		//50P
		
		
		//Dexterity
		register(new PowerShotI()); //5
		register(new SummonArrows()); //10
		register(new RangedDamageBoostI()); //10P
		register(new DoubleShot()); //15
		register(new CriticalBoostI()); //20P
		register(new PoisonShot()); //20
		register(new PowerShotII());//25
		//30
		//30P
		//35
		//40
		//40P
		register(new TripleShot());//45
		//50
		register(new CriticalBoostII());//50P
		
		//Agility
		register(new StabI()); //5
		register(new Sprinter()); //10P
		register(new Jump()); //10
		register(new WitherBlade()); //15
		register(new DodgeBoostI()); // 20P
		register(new ShadowsI());//25
		register(new Sprint());//30
		//30P
		register(new StabII());//35
		register(new DrainingBlade());//40
		//40P
		register(new ShadowsII());//45
		register(new Cloak());//50
		register(new DodgeBoostII());//50P
		
		//Wisdom
		register(new HealI()); //5
		register(new Regeneration()); //10
		register(new ManaRegenerationI());//10 P
		register(new ProtectI()); //15
		register(new ManaFountI()); //20 P
		register(new HealingCircleI()); // 20
		register(new HealII());//25
		register(new NightVision());//30
		register(new ManaFountII());//30p
		register(new WardI()); //35
		register(new HealIII()); //40
		register(new HealingLight());//40P
		register(new HealingCircleII());//45
		//50P
		//50
		
		
		//Intelligence
		register(new FireballI()); //5
		register(new Poison()); //10
		register(new MagicAttackBoostI()); //10P
		register(new IceBoltI()); //15
		register(new LightningStrikeI()); //20
		register(new RecastMastery()); //20P
		register(new FireballII());//25
		//30
		//30P
		//35
		register(new PartyTeleport()); //40
		//40P
		//45
		register(new Storm());//50
		//50P
		
		
		try {
			yc.save(f);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void register(Skill skill){
		if(skill instanceof ActiveSkill){
			ActiveSkills.add((ActiveSkill) skill);
			
			yc.set(skill.getMainStat() + ".Actives." + ChatColor.stripColor(skill.getName()) + ".Req", skill.getStatRequirement(skill.getMainStat()));
			yc.set(skill.getMainStat() + ".Actives." + ChatColor.stripColor(skill.getName()) + ".Description", skill.getDescription());
		}
		else if(skill instanceof PassiveSkill){
			PassiveSkills.add((PassiveSkill) skill);
			
			yc.set(skill.getMainStat() + ".Passives." + ChatColor.stripColor(skill.getName()) + ".Req", skill.getStatRequirement(skill.getMainStat()));
			yc.set(skill.getMainStat() + ".Passives." + ChatColor.stripColor(skill.getName()) + ".Description", skill.getDescription());
		}
		
		skills.put(ChatColor.stripColor(skill.getName().toLowerCase()), skill);
	}
	
	public Skill getSkillByName(String name){
		Skill skill = skills.get(name.toLowerCase());
		return skill;
	}
	
	public void updateSkills(Character cha, ArrayList<Skill> allskills, boolean notify){
		//cha.sendMessage("Updating Skills...");
//		cha.active_skills.clear();
//		cha.passive_skills.clear();
		for(Skill skill : ActiveSkills){
			if(skill.learnable(cha)){
				cha.active_skills.add(skill);
				
				if(!allskills.contains(skill) && notify){
					cha.sendMessage(ChatColor.GREEN + "You have learned the Active skill " + skill.getName());
				}
				//cha.sendMessage(ChatColor.GREEN + skill.getName());
			}
			else{
				//cha.sendMessage(ChatColor.RED + skill.getName());
			}
		}
		for(Skill skill : PassiveSkills){
			if(skill.learnable(cha)){
				cha.passive_skills.add(skill);
				((PassiveSkill) skill).apply(cha);
				//cha.sendMessage(ChatColor.GREEN + skill.getName());
				if(!allskills.contains(skill) && notify){
					cha.sendMessage(ChatColor.GREEN + "You have learned the Passive skill " + skill.getName());
				}
			}
			else{
				//cha.sendMessage(ChatColor.RED + skill.getName());
			}
		}
	}
	
}
