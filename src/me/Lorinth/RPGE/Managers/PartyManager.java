package me.Lorinth.RPGE.Managers;

import java.util.ArrayList;
import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.Parties.CharacterParty;
import me.Lorinth.RPGE.Parties.PartyReason;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PartyManager implements Listener{

	public RpgEnhancedMain plugin;
	private ArrayList<CharacterParty> parties = new ArrayList<CharacterParty>();
	private HashMap<Player, CharacterParty> pending = new HashMap<Player, CharacterParty>();
	private ArrayList<Player> partyChat = new ArrayList<Player>();
	
	public PartyManager(RpgEnhancedMain plugin){
		this.plugin = plugin;
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){

			@Override
			public void run() {
				cleanUpParties();
			}
			
			
		}, 0L, 300L);
	}
	
	public void createParty(Character cha){
		CharacterParty cp = new CharacterParty(cha, this);
		cha.sendMessage(ChatColor.DARK_AQUA + "You have created a party!");
		parties.add(cp);
	}
	
	private void cleanUpParties(){
		for(CharacterParty party : parties){
			if(party.isEmpty()){
				party = null;
			}
		}
	}
	
	public Character getCharacter(Player player){
		return plugin.cm.getCharacter(player);
	}
	
	public void sendPartyInfo(Player player){
		Character cha = getCharacter(player);
		if(cha.currentParty == null){
			cha.sendMessage(ChatColor.DARK_AQUA + "You are not in a party");
		}
		else{
			CharacterParty party = cha.currentParty;
			cha.sendMessage(ChatColor.DARK_AQUA + "Leader : " + party.leader.owner.getDisplayName());
			cha.sendMessage(ChatColor.DARK_AQUA + "Size : " + party.getSize());
			for(Character chara : party.getCharacters()){
				if(chara != party.leader){
					cha.sendMessage(ChatColor.DARK_AQUA + chara.owner.getDisplayName());
				}
			}
		}
	}
	
	public void sendHelp(Player player){
		player.sendMessage(ChatColor.DARK_AQUA + "Help page for party commands");
		player.sendMessage(ChatColor.DARK_AQUA + "/party ");
		player.sendMessage(ChatColor.DARK_AQUA + "/party create - creates a party if not in one");
		player.sendMessage(ChatColor.DARK_AQUA + "/party help - displays this page");
		player.sendMessage(ChatColor.DARK_AQUA + "/party join - joins a pending invite");
		player.sendMessage(ChatColor.DARK_AQUA + "/party deny - denies a pending invite");
		player.sendMessage(ChatColor.DARK_AQUA + "/party leave - leaves current party");
		player.sendMessage(ChatColor.DARK_AQUA + "/party lockchat - locks chat to party");
		try{
			if(getCharacter(player).currentParty.leader.owner.equals(player)){
				player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Party Leader Commands");
				player.sendMessage(ChatColor.GOLD + "/party invite <name> - invites player to your party");
				player.sendMessage(ChatColor.GOLD + "/party kick <name> - kicks player from party");
				player.sendMessage(ChatColor.GOLD + "/party sync <name> - sync players to a target player's level");
				player.sendMessage(ChatColor.GOLD + "/party promote <name> - promotes a player to be the new leader");
			}
		}
		catch(NullPointerException e){
			
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void handleCommand(Player player, String[] arg){
		if(player.getWorld().getName() == "CODWarfare"){
			return;
		}
		
		if(arg.length == 0){
			sendPartyInfo(player);
			return;
		}
		String command = arg[0];
		if(command.equalsIgnoreCase("help")){
			sendHelp(player);
		}
		Character cha = getCharacter(player);
		if(cha.currentParty == null){
			
		}
		else if(cha.currentParty.leader.equals(cha)){
			if (command.equalsIgnoreCase("invite")){
				if(cha.currentParty.inDungeon()){
					cha.sendMessage(ChatColor.DARK_AQUA + "[Party] Can't invite anyone while in a dungeon!");
				}
				Player target = Bukkit.getPlayer(arg[1]);
				if(target.equals(cha.owner)){
					target.sendMessage(ChatColor.DARK_AQUA + "[Party] You cant invite yourself!");
				}
				pending.put(target, cha.currentParty);
				player.sendMessage(ChatColor.DARK_AQUA + "[Party] You have invited " + ChatColor.GOLD + target.getDisplayName() + ChatColor.DARK_AQUA + " to your party!");
				target.sendMessage(ChatColor.DARK_AQUA + "[Party] You have been invited to " + ChatColor.GOLD + player.getDisplayName() + "'s" + ChatColor.DARK_AQUA + " party!");
				target.sendMessage(ChatColor.DARK_AQUA + "[Party] Type " + ChatColor.GOLD + "/party join " + ChatColor.DARK_AQUA + "to join!");
				return;
			}
			else if (command.equalsIgnoreCase("sync")){
				Player target = Bukkit.getPlayer(arg[1]);
				if(cha.currentParty.getPlayers().contains(target)){
					Character chara = getCharacter(target);
					cha.currentParty.syncParty(chara);
				}
				return;
			}
			else if(command.equalsIgnoreCase("unsync")){
				Character chara = getCharacter(player);
				chara.currentParty.unSync();
				return;
			}
			else if(command.equalsIgnoreCase("kick")){
				Player target = Bukkit.getPlayer(arg[1]);
				if(cha.currentParty.getPlayers().contains(target)){
					cha.currentParty.removePlayer(target, PartyReason.KICKED);
				}
				return;
			}
			else if(command.equalsIgnoreCase("disband")){
				CharacterParty party = cha.currentParty;
				parties.remove(party);
				party.disbandParty();
			}
			else if(command.equalsIgnoreCase("promote")){
				Player target = Bukkit.getPlayer(arg[1]);
				cha.currentParty.changeLeader(getCharacter(target));
			}
			
		}
		if(command.equalsIgnoreCase("create")){
			if(cha.currentParty == null){
				createParty(cha);
			}
			else{
				cha.sendMessage(ChatColor.DARK_AQUA + "[Party] You are already in a party");
			}
		}
		else if (command.equalsIgnoreCase("join")){
			if(pending.keySet().contains(player)){
				CharacterParty party = pending.get(player);
				party.addCharacter(getCharacter(player));
			}
			else{
				player.sendMessage(ChatColor.DARK_AQUA + "There is no pending invite");
			}
		}
		else if (command.equalsIgnoreCase("deny")){
			if(pending.keySet().contains(player)){
				CharacterParty party = pending.get(player);
				party.leader.sendMessage(ChatColor.DARK_AQUA + "[Party] " + player.getDisplayName() + " has denied your request");
			}
		}
		else if (command.equalsIgnoreCase("leave")){
			if(cha.currentParty != null){
				cha.currentParty.removeCharacter(cha, PartyReason.LEFT);
			}
		}
		else if(command.equalsIgnoreCase("lockchat")){
			if(partyChat.contains(player)){
				partyChat.remove(player);
				player.sendMessage(ChatColor.DARK_AQUA + "[Party] You have stopped talking to party");
			}
			else{
				if(cha.currentParty != null){
					partyChat.add(player);
					player.sendMessage(ChatColor.DARK_AQUA + "[Party] You are now talking to party");
				}
				else{
					player.sendMessage(ChatColor.DARK_AQUA + "[Party] No party to chat to");
				}
			}
			
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPartyChat(AsyncPlayerChatEvent event){
		try{
			Player player = event.getPlayer();
			if(partyChat.contains(player)){
				Character cha = this.getCharacter(player);
				cha.currentParty.sendMessage(ChatColor.DARK_AQUA + "[Party]" + player.getDisplayName() + " : " + event.getMessage());
				event.setCancelled(true);
			}
		}
		catch(NullPointerException e){
			Player player = event.getPlayer();
			partyChat.remove(player);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuit(PlayerQuitEvent event){
		Character cha = plugin.cm.getCharacter(event.getPlayer());
		try{
			cha.currentParty.removeCharacter(cha, PartyReason.LEFT);
		}
		catch(NullPointerException e){
			//pass
		}
	}
	
}
