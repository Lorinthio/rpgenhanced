package me.Lorinth.RPGE.Managers;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;

import me.Lorinth.RPGE.Dungeons.Dungeon;
import me.Lorinth.RPGE.Items.DungeonArmor;
import me.Lorinth.RPGE.Items.DungeonWeapon;
import me.Lorinth.RPGE.Items.Gear;
import me.Lorinth.RPGE.Items.Title;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.Parties.CharacterParty;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class DungeonManager implements Listener{

	private String directory;
	private RpgEnhancedMain plugin;
	private HashMap<Integer, Dungeon> dungeons = new HashMap<Integer, Dungeon>();
	public HashMap<Player, Dungeon> joinDungeons = new HashMap<Player, Dungeon>();
	private HashMap<Player, ArrayList<Object>> bossChestQueue = new HashMap<Player, ArrayList<Object>>();
	private boolean empty = false;
	private Random random = new Random();
	
	private int CheckTask = 0;
	
	public DungeonManager(final RpgEnhancedMain main){
		plugin = main;
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){

			@Override
			public void run() {
				start();
				
			}
			
		}, 5);
	}
	
	public void start(){
		Bukkit.getPluginManager().registerEvents(this, plugin);
		directory = plugin.getDataFolder() + File.separator + "Dungeons";
		loadDungeons();
		
		if(!empty){
			CheckTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){
	
				@Override
				public void run() {
					checkLobbies();
					
				}
				
			}, 20L, 200L);
		}
	}
	
	public Integer nextId(){
		Integer id = 0;
		Set<Integer> usedIds = dungeons.keySet();
		for(Integer nextId=1;nextId<=100;nextId++){
			if(!usedIds.contains(nextId)){
				id = nextId;
				break;
			}
		}
		if(id != 0){
			return id;
		}
		else{
			return null;
		}
	}
	
	public void handleCommand(Player player, String[] args){
		String command = "";
		try{
			command = args[0];
		}
		catch(ArrayIndexOutOfBoundsException e){
			if(player.hasPermission("RPG.Dungeon.mod")){
				sendMessage(player, "Available Commands...");
				sendMessage(player, "create <name>");
				sendMessage(player, "setLobby <id>");
				sendMessage(player, "setExit <id>");
				sendMessage(player, "setspawn <id> <mobtype>");
				sendMessage(player, "setboss <id> <type> <final>");
				sendMessage(player, "setrewards <id> <gold> <exp>");
				sendMessage(player, "start <id>");
				sendMessage(player, "tp <id>");
				sendMessage(player, "list");
				
				sendMessage(player, "reload");
				sendMessage(player, "save");
				return;
			}
		}
		Integer id = 0;
		try{
			id = Integer.parseInt(args[1]);
		}
		catch(NumberFormatException e){
			//pass
		}
		catch(ArrayIndexOutOfBoundsException e2){
			
		}
		if(player.hasPermission("RPG.Dungeon.mod")){
			if(command.equals("create")){
				String name = "";
				for(String n : args){
					if(!n.equals(command)){
						name += n + " ";
					}
				}
				name = name.trim();
				id = nextId();
				Dungeon dung = new Dungeon(this, id);
				dung.create(name, player.getLocation());
				dungeons.put(id, dung);
				sendMessage(player, "Created dungeon, " + dung.name + ChatColor.DARK_PURPLE + ", with id, " + id);
			}
			else if(command.equalsIgnoreCase("setExit")){
				Dungeon dung = dungeons.get(id);
				dung.setExit(player);
			}
			else if(command.equalsIgnoreCase("start")){
				Dungeon dung = dungeons.get(id);
				dung.start();
			}
			else if(command.equalsIgnoreCase("end")){
				Dungeon dung = dungeons.get(id);
				dung.end();
			}
			else if(command.equalsIgnoreCase("setLobby")){
				Dungeon dung = dungeons.get(id);
				dung.setLobby(player);
			}
			else if(command.equalsIgnoreCase("setspawn")){
				Dungeon dung = dungeons.get(id);
				EntityType type = EntityType.fromName(args[2]);
				dung.addSpawn(player.getLocation(), type, true);
				sendMessage(player, "Added spawn to " + dungeons.get(id).name);
			}
			else if(command.equalsIgnoreCase("setrewards")){
				Dungeon dung = dungeons.get(id);
				dung.setRewards(Integer.parseInt(args[2]), Integer.parseInt(args[3]));
			}
			else if(command.equalsIgnoreCase("setboss")){
				Dungeon dung = dungeons.get(id);
				String type = args[2];
				String finalBoss = args[3];
				if(finalBoss.toLowerCase().equals("true")){
					dung.setFinalBoss(type);
				}
				ArrayList<Object> bossSpawn = new ArrayList<Object>();
				bossSpawn.add(dung);
				bossSpawn.add(player.getLocation());
				bossSpawn.add(type);
				
				bossChestQueue.put(player, bossSpawn);
				
				sendMessage(player, ChatColor.DARK_PURPLE + "Now click on the boss chest");
			}
			else if(command.equalsIgnoreCase("list")){
				sendMessage(player, "Listed dungeons");
				for(Integer ids : dungeons.keySet()){
					sendMessage(player, "[" + ids + "] " + dungeons.get(ids).name);
				}
			}
			else if(command.equalsIgnoreCase("reload")){
				reload(player);
			}
			else if(command.equalsIgnoreCase("save")){
				save(player);
			}
			else if(command.equalsIgnoreCase("tp")){
				Dungeon dung = dungeons.get(id);
				player.teleport(dung.spawn);
				
			}
			else if(command.equalsIgnoreCase("join")){
				Dungeon dungeon = joinDungeons.get(player);
				dungeon.sendQueuedParty();
			}
			else if(command.equalsIgnoreCase("leave")){
				Character cha = plugin.cm.getCharacter(player);
				CharacterParty cp = cha.currentParty;
				if(cp.isLeader(player)){
					cp.getDungeon().kickParty();
				}
			}
		}
		else{
			if(command.equalsIgnoreCase("join")){
				Dungeon dungeon = joinDungeons.get(player);
				dungeon.sendQueuedParty();
			}
			else if(command.equalsIgnoreCase("leave")){
				Character cha = plugin.cm.getCharacter(player);
				CharacterParty cp = cha.currentParty;
				if(cp.isLeader(player)){
					cp.getDungeon().kickParty();
				}
			}
		}
	}
	
	public void reload(Player player){
		//saveDungeons();
		for(Dungeon d : dungeons.values()){
			d.unregister();
		}
		
		dungeons = new HashMap<Integer, Dungeon>();
		joinDungeons = new HashMap<Player, Dungeon>();
		
		loadDungeons();
		if(player != null){
			sendMessage(player, "Reset dungeon manager");
		}
	}
	
	public void save(Player player){
		saveDungeons();
		if(player != null){
			sendMessage(player, "Saved dungeons!");
		}
	}
	
	public void saveDungeons(){
		for(Dungeon dung : this.dungeons.values()){
			dung.save();
		}
	}
	
	@SuppressWarnings("deprecation")
	public void loadDungeons(){
		File file = new File(directory);
		if(!file.exists()){
			file.mkdir();
			System.out.println("Made Dungeon Directory");
		}
		YamlConfiguration yml = null;
		try{
			for (File fileEntry : file.listFiles()) {
		        yml = YamlConfiguration.loadConfiguration(fileEntry);
		        try{
		        	
		        	Integer id = Integer.parseInt(fileEntry.getName().replace(".yml", "").trim());
		        	Dungeon dungeon = new Dungeon(this, id);
			        
			        dungeon.name = ChatColor.DARK_RED + yml.getString("Name");
			        dungeon.setRewards(yml.getInt("Gold"), yml.getInt("Exp"));
			        dungeon.setFinalBoss(yml.getString("FinalBoss"));
			        
			        double X = (double) yml.get("Start.X");
			        double Y = (double) yml.get("Start.Y");
			        double Z = (double) yml.get("Start.Z");
			        String world = yml.getString("Start.World");
			        dungeon.spawn = new Location(Bukkit.getWorld(world), X, Y ,Z);
			        
			        try{
			        	dungeon.setSize(yml.getInt("MinSize"), yml.getInt("MaxSize"));
			        }
			        catch(NullPointerException e3){
			        	dungeon.setSize(3, 4);
			        }
			        
			        X = (double) yml.get("Exit.X");
			        Y = (double) yml.get("Exit.Y");
			        Z = (double) yml.get("Exit.Z");
			        world = yml.getString("Exit.World");
			        dungeon.exit = new Location(Bukkit.getWorld(world), X, Y, Z);
			        
			        world = yml.getString("Lobby.World");
			        
			        X = (double) yml.get("Lobby.min.X");
			        Y = (double) yml.get("Lobby.min.Y");
			        Z = (double) yml.get("Lobby.min.Z");
					dungeon.lobbyMin = new Location(Bukkit.getWorld(world), X, Y, Z);
			        
			        X = (double) yml.get("Lobby.max.X");
			        Y = (double) yml.get("Lobby.max.Y");
			        Z = (double) yml.get("Lobby.max.Z");
			        dungeon.lobbyMax = new Location(Bukkit.getWorld(world), X, Y, Z);
			        
			        try{
				        Set<String> ids = yml.getConfigurationSection("MonsterSpawns").getKeys(false);
				        if(ids != null){
				        	int Count = 0;
					        for(String idtag : ids){
					        	X = yml.getDouble("MonsterSpawns." + idtag + ".X");
					        	Y = yml.getDouble("MonsterSpawns." + idtag + ".Y");
					        	Z = yml.getDouble("MonsterSpawns." + idtag + ".Z");
					        	world = yml.getString("MonsterSpawns." + idtag + ".World");
					        	
					        	Location loc = new Location(Bukkit.getWorld(world), X, Y, Z);
					        	
					        	dungeon.addSpawn(loc, EntityType.fromName(yml.getString("MonsterSpawns." + idtag + ".Type")), false);
					        	Count += 1;
					        	//System.out.println(ChatColor.DARK_PURPLE + loc.getWorld().getName());
					        }
					        getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: Loaded in " + Count + " monsters for " + dungeon.name);
				        }
			        }
			        catch(NullPointerException e2){
			        	//e2.printStackTrace();
			        	 getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: No Mob Spawns for " + dungeon.name);
			        }
			        
			        try{
			        	Set<String> ids = yml.getConfigurationSection("BossSpawns").getKeys(false);
				        if(ids != null){
				        	int Count = 0;
					        for(String idtag : ids){
					        	X = yml.getDouble("BossSpawns." + idtag + ".X");
					        	Y = yml.getDouble("BossSpawns." + idtag + ".Y");
					        	Z = yml.getDouble("BossSpawns." + idtag + ".Z");
					        	world = yml.getString("BossSpawns." + idtag + ".World");
					        	
					        	double CX = yml.getDouble("BossSpawns." + idtag + ".Chest.X"); 
					        	double CY = yml.getDouble("BossSpawns." + idtag + ".Chest.Y"); 
					        	double CZ = yml.getDouble("BossSpawns." + idtag + ".Chest.Z"); 
					        	
					        	World Bworld = Bukkit.getWorld(world);
					        	
					        	Location chest = new Location(Bworld, CX, CY, CZ);
					        	
					        	dungeon.addBossSpawn(new Location(Bworld, X, Y, Z), yml.getString("BossSpawns." + idtag + ".Type"), Bworld.getBlockAt(chest), false);
					        	Count += 1;
					        }
					        getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: Loaded in " + Count + " bosses for " + dungeon.name);
				        }
			        }
			        catch(NullPointerException e2){
			        	 getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: No Boss Spawns");
			        }
			        
			        //Load loot
			        try{
			        	Set<String> ids = yml.getConfigurationSection("Loot.Weapons").getKeys(false);
				        if(ids != null){
				        	int Count = 0;
					        for(String idtag : ids){
					        	try{
						        	int value = yml.getInt("Loot.Weapons." + idtag + ".Damage");
						        	int level = yml.getInt("Loot.Weapons." + idtag + ".Level");
						        	int dura = yml.getInt("Loot.Weapons." + idtag + ".Durability");
						        	String MatID = yml.getString("Loot.Weapons." + idtag + ".Material");
						        	Material appearance = Material.getMaterial(MatID);
						        	if(appearance == null){
						        		//PASS
						        	}
						        	else{
						        		List<String> description = yml.getStringList("Loot.Weapons." + idtag + ".Description");
							        	
							        	DungeonWeapon weapon = new DungeonWeapon(idtag, level, value, appearance, dura);
							        	weapon.setDescription(description);
							        	
							        	dungeon.addGear(weapon);
							        	Count += 1;
						        	}
					        	}
					        	catch(NullPointerException e){
					        		
					        	}
					        	
					        	
					        }
					        getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: Loaded in " + Count + " weapons for " + dungeon.name);
				        }
			        }
			        catch(NullPointerException e){
			        	 getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: No weapons for " + dungeon.name);
			        	
			        }
			        
			        try{
			        	Set<String> ids = yml.getConfigurationSection("Loot.Armor").getKeys(false);
				        if(ids != null){
				        	int Count = 0;
					        for(String idtag : ids){
					        	try{
						        	int value = yml.getInt("Loot.Armor." + idtag + ".Armor");
						        	int level = yml.getInt("Loot.Armor." + idtag + ".Level");
						        	int dura = yml.getInt("Loot.Armor." + idtag + ".Durability");
						        	String MatID = yml.getString("Loot.Armor." + idtag + ".Material");
						        	String type = yml.getString("Loot.Armor." + idtag + ".Type");
						        	Material appearance = Material.getMaterial(MatID);
						        	if(appearance.equals(null)){
						        		//PASS
						        	}
						        	else{
						        	
							        	List<String> description = yml.getStringList("Loot.Armor." + idtag + ".Description");
							        	
							        	DungeonArmor armor = new DungeonArmor(idtag, level, value, appearance, dura, type);
							        	armor.setDescription(description);
							        	
							        	dungeon.addGear(armor);
							        	Count += 1;
						        	}
					        	}
					        	catch(NullPointerException e){
					        		
					        	}
					        }
					        getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: Loaded in " + Count + " armor for " + dungeon.name);
				        }
			        }
			        catch(NullPointerException e){
			        	 getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: No armor for " + dungeon.name);
			        	
			        }
			        
			        int Level = yml.getInt("Level");
			        
			        
			        dungeon.setLevel(Level);
			        getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon]: Level " + dungeon.level);
			        dungeons.put(id, dungeon);
			        
			        getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] Registered, " + dungeon.name);
		        }
		        catch(NumberFormatException e){
		        	getMain().console.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] Invalid id " + fileEntry.getName());
		        	//e.printStackTrace();
		        }
		        
		    }
		}
		catch(NullPointerException e){
			empty = true;
			e.printStackTrace();
		}
	}
	
	public void sendMessage(Player player, String message){
		player.sendMessage(ChatColor.DARK_PURPLE + "[Dungeon] " + message);
	}
	
	public RpgEnhancedMain getMain(){
		return plugin;
	}
	
	public ArrayList<Title> getTitles(Gear gear, String rarity){
		if(rarity != "Common"){
			Integer titleCount = 1;
			switch(rarity){
			case("Uncommon"):
				titleCount = 2;
			break;
			case("Rare"):
				titleCount = 2;
			break;
			case("Epic"):
				titleCount = 3;
			break;
			case("Legendary"):
				titleCount = 3;
			break;
			case("Godly"):
				titleCount = 4;
			break;
			}

			ArrayList<Title> titles = new ArrayList<Title>();
			ArrayList<Title> titleList;
			int choice;
			
			if(gear instanceof DungeonArmor){
				if(titleCount >= 1){
					DungeonArmor armor = (DungeonArmor) gear;
					titleList = plugin.getItemGenerator().getArmorTitles();
					ArrayList<Title> available = new ArrayList<Title>();
					for(Title title : titleList){
						if(title.level <= armor.level){
							available.add(title);
						}
					}
					for(int i=0;i<titleCount;i++){
						choice = random.nextInt(available.size());
						Title test = available.get(choice);
						if(!titles.contains(test)){
							titles.add(test);
						}
						else{
							i--;
						}
					}
				}
			}
			else if(gear instanceof DungeonWeapon){
				if(titleCount >= 1){
					DungeonWeapon armor = (DungeonWeapon) gear;
					titleList = plugin.getItemGenerator().getWeaponTitles();
					ArrayList<Title> available = new ArrayList<Title>();
					for(Title title : titleList){
						if(title.level <= armor.level){
							available.add(title);
						}
					}
					for(int i=0;i<titleCount;i++){
						choice = random.nextInt(available.size());
						Title test = available.get(choice);
						if(!titles.contains(test)){
							titles.add(test);
						}
						else{
							i--;
						}
					}
				}
			}
			return titles;
		}
		return null;
	}
	
	public String calculateRarity(Integer Level){
		Integer roll = random.nextInt(1000);
		
		int rarityoffset = 0;

		//RARE (Light Blue)
		//60% chance at Level 1
		//50% chance at Level 100
		//40% chance at Level 200
		rarityoffset += 600 - Level;
		if(roll <= rarityoffset){
			return "Rare";
		}

		//EPIC (Red)
		//25% chance at Level 1
		//30% chance at Level 100
		//35% chance at Level 200
		rarityoffset += 250 + Level / 2; 
		if(roll <= rarityoffset){
			return "Epic";
		}

		//LEGENDARY (Gold)
		//10% chance at Level 1
		//12.5% chance at Level 100
		//15% chance at Level 200
		rarityoffset += 100 + Level / 4;
		if(roll <= rarityoffset){
			return "Legendary";
		}

		//GODLY (Purple)
		//5% chance at Level 1
		//7.5% chance at Level 100
		//10% chance at Level 200
		rarityoffset += 50 + Level / 4;
		if(roll <= rarityoffset){
			return "Godly";
		}

		return "Common";
	}
	
	@EventHandler
	public void onPlayerPunchChest(PlayerInteractEvent event){
		Player p = event.getPlayer();
		Block b = event.getClickedBlock();
		if(this.bossChestQueue.keySet().contains(p)){
			if(event.getAction() == Action.LEFT_CLICK_BLOCK){
				if(b.getType() == Material.CHEST){
					ArrayList<Object> params = bossChestQueue.get(p);
					((Dungeon)params.get(0)).addBossSpawn((Location)params.get(1), (String)params.get(2), b, true);
					this.bossChestQueue.remove(p);
					p.sendMessage("You set the chest!");
					event.setCancelled(true);
				}
			}
		}
	}
	
	public void checkLobbies(){
		try{
			Player[] players = Bukkit.getServer().getOnlinePlayers();
			for(Dungeon dungeon : dungeons.values()){
				dungeon.checkPlayersInLobby();
			}
		}
		catch(NullPointerException e){
			
		}
	}
}
