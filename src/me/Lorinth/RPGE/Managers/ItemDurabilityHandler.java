package me.Lorinth.RPGE.Managers;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerItemBreakEvent;
import org.bukkit.inventory.ItemStack;

public class ItemDurabilityHandler {

	ArrayList<ChatColor> rarities = new ArrayList<ChatColor>();
	
	public ItemDurabilityHandler(){
		rarities.add(ChatColor.DARK_RED);
		rarities.add(ChatColor.GOLD);
		rarities.add(ChatColor.DARK_PURPLE);
	}
	
	// Items Indestructibles
	@EventHandler
	public void noWeaponBreakDamage( EntityDamageByEntityEvent event){
		if(event.getEntity().getWorld().getName() == "CODWarfare"){
			return;
		}
		
	    if( event.getDamager() instanceof Player )
	        ((Player) event.getDamager()).getItemInHand().setDurability((short) 1);
	    else
	    if ( event.getEntity() instanceof Player ){
	        ItemStack[] armor = ((Player) event.getEntity()).getInventory().getArmorContents();
	        for(ItemStack i:armor) {
	             i.setDurability((short) 0);
	        }
	        ((Player) event.getEntity()).getInventory().setArmorContents(armor);
	    }
	}
	@EventHandler
	public void noWeaponBreakDamage( EntityShootBowEvent event){
	    // Bow
		if(event.getEntity().getWorld().getName() == "CODWarfare"){
			return;
		}
		
	    if(event.getEntity() instanceof Player){
	    	event.setCancelled(true);
	    	event.setProjectile(null);
	    }
	}
	@EventHandler
	public void noWeaponBreakDamage( PlayerItemBreakEvent event){
		if(event.getPlayer().getWorld().getName() == "CODWarfare"){
			return;
		}
		
	    ItemStack item = event.getBrokenItem().clone();
	    if(this.rarities.contains(ChatColor.getLastColors(item.getItemMeta().getDisplayName()))){
	    	item.setDurability((short)0);
		    event.getPlayer().getInventory().addItem(item);
	    }
	}
}
