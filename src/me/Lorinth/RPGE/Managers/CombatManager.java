package me.Lorinth.RPGE.Managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Characters.DeathCause;
import me.Lorinth.RPGE.Items.WeaponType;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.SkillAPI.AttackType;
import me.Lorinth.RPGE.SkillAPI.Skill;
import net.elseland.xikage.MythicMobs.API.Mobs;
import net.elseland.xikage.MythicMobs.API.MythicMobSkillEvent;
import net.elseland.xikage.MythicMobs.Mobs.ActiveMob;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Slime;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CombatManager implements Listener{

	public ArrayList<Player> jumpPlayers = new ArrayList<Player>();
	public ArrayList<Player> dodgedPlayers = new ArrayList<Player>();
	public ArrayList<Player> invulnerablePlayers = new ArrayList<Player>();
	public HashMap<Player, Integer> protectedPlayers = new HashMap<Player, Integer>();
	//public HashMap<Player, HashMap<DamageCause, Integer>> playersProtection = new HashMap<Player, HashMap<DamageCause, Integer>>();
	public HashMap<Player, Integer> attackTime = new HashMap<Player, Integer>();
	public Random random = new Random();
	private RpgEnhancedMain main;
	
	public CombatManager(RpgEnhancedMain plugin){
		main = plugin;
	}
		
	@EventHandler
	public void onBossSkill(MythicMobSkillEvent e){
		String skill = e.getSkillName();
		System.out.println(skill);
		final ActiveMob boss = e.getActiveMob();
		final Entity target = e.getActiveMob().getThreatTable().getTopThreatHolder();
		
		switch(skill){
		case "SmashAttack":
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){

				@Override
				public void run() {
					for(Entity ent : boss.getLivingEntity().getNearbyEntities(10, 10, 10)){
						if(ent instanceof Player){
							Player p = (Player) ent;
							if(p.getGameMode() == GameMode.SURVIVAL){
								Character c = main.cm.getCharacter(p);
								c.damage(AttackType.Physical, 10, boss.getLivingEntity(), true);
							}
						}
					}
					
				}
				
				
			}, 10);
			
				
			break;
		case "ShootCorruption":
			if(target instanceof Player){
				Player p = (Player) target;
				Character c = main.cm.getCharacter(p);
				c.damage(AttackType.Physical, 20, e.getLivingEntity(), true);
			}
			break;
		case "SummonCorruption":
			Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){

				@Override
				public void run() {
					for(Entity ent : boss.getLivingEntity().getNearbyEntities(10, 10, 10)){
						if(ent instanceof Player){
							Player p = (Player) ent;
							if(p.getGameMode() == GameMode.SURVIVAL){
								Character c = main.cm.getCharacter(p);
								c.damage(AttackType.Physical, 10, boss.getLivingEntity(), true);
							}
						}
					}
					
				}
				
				
			}, 10);
			break;
		case "AwmbaLeap":
			if(target instanceof Player){
				Player p = (Player) target;
				Character c = main.cm.getCharacter(p);
				c.damage(AttackType.Physical, 10, e.getLivingEntity(), true);
			}
			break;
		default:
			break;
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void PlayerCombat(EntityDamageByEntityEvent event){
//		if(event.getDamager().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
//			return;
//		}
		
		if(event.isCancelled()){
			return;
		}
		Entity entity = event.getEntity();
		if(entity instanceof LivingEntity){
			if(((LivingEntity) entity).getNoDamageTicks() > 10){
				event.setCancelled(true);
				return;
			}
		}
		else{
			return;
		}
		boolean rangedAttack = false;
		float FullAttack = 0;
		float DamageReduction = 0;
		double resultdamage = event.getDamage();
		Entity target = event.getEntity();
		Entity attacker = event.getDamager();
		if(attacker instanceof Projectile){
			if(((Projectile)attacker).getShooter() instanceof Player){
				if(target instanceof Player){
					Player p1 = (Player) ((Projectile)attacker).getShooter();
					Player p2 = (Player) target;
					if(main.cm.getCharacter(p1).inParty(main.cm.getCharacter(p2))){
						Projectile p = (Projectile)attacker;
						event.setCancelled(true);
						p.teleport((p.getLocation().add(p.getVelocity().normalize().multiply(2))));
						return;
					}
				}
				try{
					String name = attacker.getMetadata("Skill").get(0).asString();
					onProjectileSpell(event, name);
					return;
					//event.setCancelled(true);
				}
				catch(IndexOutOfBoundsException error){
					//error.printStackTrace();
				}
			}
			attacker.remove();
			attacker = ((Projectile) attacker).getShooter();
			rangedAttack = true;

		}

		//Check attacker as player

		//Ranged attack by player
		if(attacker instanceof Player && rangedAttack){
			Player player = (Player) attacker;
			CharacterManager cm = main.getCharacterManager();
			Character cha = cm.getCharacter(player);
			resultdamage = cha.GetRangedAttackDamage();
			FullAttack = cha.getRangedAttack();
		}
		//Melee attack by player
		else if(attacker instanceof Player){
			//Check Attack Speed
			Integer curtime = (int) (System.currentTimeMillis() / 100);
			try{
				if(curtime < attackTime.get((Player) attacker)){
					event.setCancelled(true);
					return;
				}
				else{
					WeaponType wt = main.cm.getCharacter((Player) attacker).getWeaponType();
					int delay = 0;
					switch(wt){
					case Sword:
						delay = 4;
					case Axe:
						delay = 5;
					case Mace:
						delay = 6;
					default:
						delay = 10;
					}
					attackTime.put(((Player)attacker), (int) ((System.currentTimeMillis() / 100) + delay));
				}
			}
			catch(NullPointerException e){
				attackTime.put(((Player)attacker), (int) ((System.currentTimeMillis() / 100) + 5));
			}
			//Continue if good to attack

			Player player = (Player) attacker;
			CharacterManager cm = main.getCharacterManager();
			Character cha = cm.getCharacter(player);
			resultdamage = cha.getAttackDamage();
			FullAttack = cha.getAttack();
			
			if(cha.isCloaked()){
				float Tyaw = target.getLocation().getYaw();
				float Ayaw = player.getLocation().getYaw();
				
				if((360 - Math.abs(Tyaw - Ayaw)) % 360 <= 30 || Math.abs(Tyaw - Ayaw) % 360 <= 30){
					resultdamage *= 2;
					cha.sendMessage(ChatColor.GREEN + "Backstabbed!");
				}
				
				cha.removeCloak();
			}
		}


		if(target instanceof Player){
			if(target.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
				return;
			}
			
		    Player player = (Player) entity;
		    Damageable dplayer = (Damageable)player;
		    double health = dplayer.getHealth();
			CharacterManager cm = main.getCharacterManager();
			boolean death = cm.getCharacter(player).damage(AttackType.Physical, event.getDamage(), attacker, false);
			if(death){
				event.setCancelled(true);
				return;
			}
			else{
				if((health - 0.01) <= 0){
					player.teleport(new Location(Bukkit.getWorld("world"), -0.5, 65, -0.5));
					player.sendMessage(ChatColor.GOLD + "[Respawn] " + ChatColor.GRAY + ": You have been reborn...");
				}
				else{
					player.setHealth(Math.min(health + 0.1, dplayer.getMaxHealth()));
					event.setDamage(0.1);				
				}
			}
		}
		else if(target instanceof Creature){
			if(main.rem.getBossManager().isBoss(target)){
				if(rangedAttack){
					main.rem.getBossManager().getBoss(target).damage(AttackType.Ranged, resultdamage, (Player)attacker, false);
				}
				else{
					main.rem.getBossManager().getBoss(target).damage(AttackType.Physical, resultdamage, (Player)attacker, false);
				}
			}
			else{
				if(target instanceof Monster){
					Integer mobLevel = main.getLevel(target);
					if(Mobs.isMythicMob(target.getUniqueId())){
						if(attacker instanceof Player){
							mobLevel = main.cm.getCharacter((Player) attacker).getLevel();
						}
						
					}
					if(MobDifficulty.getPlugin().isDungeonMob(target)){
						DamageReduction = (float)(mobLevel) + (mobLevel / 2) + (mobLevel / 3) + 30;
					}
					else{
						if(mobLevel <= 10){
							DamageReduction = (float)(mobLevel) * 2 + 10;
						}
						else{
							DamageReduction = (float)(mobLevel) + (mobLevel / 3) + (mobLevel / 5) + 20;
						}
					}
					
				}else{
					Integer mobLevel = main.getLevel(target);
					DamageReduction = (float)(mobLevel/6) + 15;
				}
				
				if(Mobs.isMythicMob(target.getUniqueId())){
					DamageReduction *= 2;
				}
				
				float DamageMod = (FullAttack + 10) / (DamageReduction + 10);
				if(DamageMod > 2.0){
					DamageMod = (float) 2.0;
				}
				resultdamage = (int) (DamageMod * resultdamage);
				resultdamage += -3 + random.nextInt(4);
				if(resultdamage < 1){
					resultdamage = 1;
				}
				double health = ((Damageable) target).getHealth() - resultdamage;
				if(health <= 0){
					health = 0;
				}
				if(health > ((Damageable) target).getMaxHealth()){
					health = ((Damageable) target).getMaxHealth();
				}
				((LivingEntity) target).setHealth(health);
			}
			event.setDamage(0.1);
		}
		
		if(Mobs.isMythicMob(event.getEntity().getUniqueId())){
			((LivingEntity)event.getEntity()).setNoDamageTicks(15);
		}
	}
	
	@EventHandler
	public void onArrowHit(ProjectileHitEvent e){
		if(e.getEntity().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		if(e.getEntity() instanceof Arrow){
			e.getEntity().remove();
		}
	}
	
	public boolean isInteger( String input ) {
	    try {
	        Integer.parseInt( input );
	        return true;
	    }
	    catch( NumberFormatException e ) {
	        return false;
	    }
	}

	@EventHandler
	public void onDamageEvent(EntityDamageEvent event){
		if(event.getEntity().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		if(event.isCancelled()){
			return;
		}
		if(event.getEntity() instanceof Player){
			switch(event.getCause()){
			case CONTACT:
				break;
			case ENTITY_ATTACK:
				break;
			case ENTITY_EXPLOSION:
				break;
			case PROJECTILE:
				break;
			default:
				Character cha = main.cm.getCharacter((Player) event.getEntity());
				cha.Health = (int) Math.max(0, (cha.Health - event.getDamage()));
				if(cha.Health == 0){
					System.out.println(event.getEntity().getCustomName() + " died by " + event.getCause().name());
					cha.respawn(DeathCause.UNKNOWN, null);
				}
			}
		}
	}

//	public void onBlockBreak(BlockDamageEvent e){
//		e.getBlock().get
//	}
	
	public void onProjectileSpell(EntityDamageByEntityEvent event, String spell){
		try{
			if(event.isCancelled()){
		    	return;
		    }
			String spellname = ChatColor.stripColor(spell);
			Projectile projectile = (Projectile) event.getDamager();
			Entity shooter = (Entity) projectile.getShooter();
			final LivingEntity target = (LivingEntity) event.getEntity();
			if(shooter instanceof Player){
				Skill skill = main.sm.getSkillByName(spellname);
				
				Integer damage = projectile.getMetadata("Damage").get(0).asInt();
				Player player = (Player) shooter;
				Character cha = main.cm.getCharacter(player);
				
				float FullAttack = 0;
				
				if(skill == null){
					if(spellname == "Magic Missle"){
						FullAttack = cha.getMagicAttack();
					}
				}
				else{
					if(skill.isMagic){
						FullAttack = cha.getMagicAttack();
					}
					else{
						FullAttack = cha.getRangedAttack();
					}
				}
				
				float DamageReduction = 0;
				
				if(target instanceof Creature | target instanceof Slime){
					if(main.rem.getBossManager().isBoss(target)){
						if(projectile instanceof Arrow){
							main.rem.getBossManager().getBoss(target).damage(AttackType.Ranged, damage, player, false);
						}
						else{
							main.rem.getBossManager().getBoss(target).damage(AttackType.Magic, damage, player, false);
						}
					}
					else{
						if(target instanceof Monster){
							Integer mobLevel = main.getLevel(target);
							if(MobDifficulty.getPlugin().isDungeonMob(target)){
								DamageReduction = (float)(mobLevel) + (mobLevel / 2) + (mobLevel / 3) + 30;
							}
							else{
								DamageReduction = (float)(mobLevel) + (mobLevel / 3) + (mobLevel / 5) + 20;
							}
							
						}else{
							Integer mobLevel = main.getLevel(target);
							DamageReduction = (float)(mobLevel/6) + 15;
						}
						float DamageMod = (FullAttack + 10) / (DamageReduction + 10);
						if(DamageMod > 1.5){
							DamageMod = (float) 1.5;
						}
						//main.console.sendMessage("Spell Damage : " + DamageMod + " * " + damage );
						
						damage = (int) (DamageMod * damage);
						
						//main.console.sendMessage(" == " + damage);
						double health = ((Damageable) target).getHealth() - damage;
						if(health <= 0){
							health = 0;
						}
						
						if(spell == "Magic Missle"){
							//player.sendMessage(ChatColor.DARK_PURPLE + spell + ChatColor.GREEN + " dealt " + ChatColor.GOLD + damage + ChatColor.GREEN + " damage to the " + ChatColor.GOLD + ((Creature)target).getCustomName());
						}
						else{
							String name = ((Creature)target).getCustomName();
							if(name == null){
								name = ((Creature)target).getType().name();
							}
							player.sendMessage(spell + ChatColor.GREEN + " dealt " + ChatColor.GOLD + damage + ChatColor.GREEN + " damage to the " + ChatColor.GOLD + name);
						}
						((LivingEntity) target).setHealth(health);
					}
					event.setDamage(0.1);
				}
				else if(target instanceof Player){
				    Player t = (Player) target;
				    Damageable dplayer = (Damageable)t;
				    double health = dplayer.getHealth();
					CharacterManager cm = main.getCharacterManager();
					boolean death = false;
					if(skill.isMagic){
						death = cm.getCharacter(t).damage(AttackType.Magic, damage, shooter, false);
					}
					else{
						death = cm.getCharacter(t).damage(AttackType.Physical, damage, shooter, false);
					}
					
					if(death){
						event.setCancelled(true);
						return;
					}
					else{
						if((health - 0.01) <= 0){
							t.teleport(new Location(Bukkit.getWorld("world"), -0.5, 65, -0.5));
							t.sendMessage(ChatColor.GOLD + "[Respawn] " + ChatColor.GRAY + ": You have been protected by the gods and have been brought to this world...");
						}
						else{
							t.setHealth(Math.min(health + 0.1, dplayer.getMaxHealth()));
							event.setDamage(0.1);				
						}
					}
				}
				
				//Add special projectile effects
				switch(spellname){
				case "Ice Bolt I":
					if(target instanceof Player){
						if(!cha.playerInParty((Player) target)){
							if(!main.isInSafeZone(target.getLocation())){
								((Player) target).setWalkSpeed((float) (((Player) target).getWalkSpeed() - 0.7));
								Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable(){
	
									@Override
									public void run() {
										((Player) target).setWalkSpeed((float) (((Player) target).getWalkSpeed() + 0.7));
									}
									
									
									
								}, 200);
							}
						}
					}
					else{
						target.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 2));
					}
					break;
				case "Poison Shot":
					target.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 3));
					break;
				}
				
				
			}
			
			//projectile.remove();
		}
		catch(NullPointerException e){
			//pass
		}
	}
	
	// Items Indestructibles
	@EventHandler(priority = EventPriority.HIGH)
	public void noWeaponBreakDamage( final EntityDamageByEntityEvent event){
		if(event.getEntity().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
	    if(event.isCancelled()){
	    	return;
	    }
		
		if( event.getDamager() instanceof Player ){
			checkWeaponDurability((Player) event.getDamager());
	    	
	    }
	    if ( event.getEntity() instanceof Player ){
	    	checkArmorDurability((Player) event.getEntity());
	    }
	}
	
	public void checkWeaponDurability(Player player){
		ItemStack item = player.getItemInHand();
    	String rarity = "";
    	boolean broken = false;
    	if(item.hasItemMeta()){
    		try{
	    		ItemMeta im = item.getItemMeta();
	    		ArrayList<String> lore = new ArrayList<String>();
	    		lore.addAll(im.getLore());
		    	for(String line : lore){
		    		line = ChatColor.stripColor(line);
		    		if(line.contains("Rarity")){
		    			rarity = line.replace("Rarity : ", "");
		    			rarity = rarity.trim();
		    			if(rarity.contains("Godly")){
		    				item.setDurability((short) 0);
		    				break;
		    			}
		    		}
		    		else if(line.contains("Durability")){
		    			String dura = ChatColor.stripColor(line).replace("Durability : ", "");
		    			String[] duras = dura.trim().split("/");
		    			Integer cur = Integer.parseInt(duras[0]);
		    			Integer max = Integer.parseInt(duras[1]);
		    			cur -= 1;
		    			if(cur == 100){
		    				player.sendMessage(ChatColor.RED + "Your item " + im.getDisplayName() + ChatColor.RED + " has a durability of 100...");
		    			}
		    			else if(cur == 25){
		    				player.sendMessage(ChatColor.DARK_RED + "Your item " + im.getDisplayName() + ChatColor.RED + " has a durability of 25...");
		    			}
		    			else if(cur <= 0){
		    				broken = true;
		    				player.sendMessage(ChatColor.DARK_RED + "Your item " + im.getDisplayName() + ChatColor.RED + " has broken...");
		    			}
		    			lore.set(5, ChatColor.GRAY + "Durability : " + cur + "/" + max);
		    		}
		    	}
		    	if(rarity != "Godly"){
		    		im.setLore(lore);
			    	item.setItemMeta(im);
			    	item.setDurability((short) 0);
			    	
			    	player.setItemInHand(item);
		    	}
		    	if(broken){
		    		player.setItemInHand(new ItemStack(Material.AIR));
		    	}
    		}
    		catch(NullPointerException e){
    			
    		}
    	}
	}
	
	public void checkArmorDurability(Player player){
		ItemStack[] armor = player.getInventory().getArmorContents();
		ArrayList<ItemStack> newArmor = new ArrayList<ItemStack>();
		String broken = "";
        for(ItemStack i:armor) {
        	String rarity = "";
        	if(i != null){
        		if(i.hasItemMeta()){
        			ItemMeta im = i.getItemMeta();
    	    		ArrayList<String> lore = new ArrayList<String>();
    	    		lore.addAll(im.getLore());
			    	for(String line : i.getItemMeta().getLore()){
			    		if(ChatColor.stripColor(line).contains("Rarity")){
			    			rarity = ChatColor.stripColor(line).replace("Rarity : ", "");
			    			rarity = rarity.trim();
			    			if(rarity.contains("Godly")){
			    				i.setDurability((short) 0);
			    				break;
			    			}
			    		}
			    		else if(line.contains("Durability")){
			    			String dura = ChatColor.stripColor(line).replace("Durability : ", "");
			    			String[] duras = dura.trim().split("/");
			    			Integer cur = Integer.parseInt(duras[0]);
			    			Integer max = Integer.parseInt(duras[1]);
			    			cur -= 1;
			    			if(cur == 100){
			    				player.sendMessage(ChatColor.RED + "Your item " + im.getDisplayName() + ChatColor.RED + " has a durability of 100...");
			    			}
			    			else if(cur == 25){
			    				player.sendMessage(ChatColor.DARK_RED + "Your item " + im.getDisplayName() + ChatColor.RED + " has a durability of 25...");
			    			}
			    			else if(cur <= 0){
			    				if(i.getType().name().toLowerCase().contains("helm")){
			    					broken += "helm ";
			    				}
			    				else if(i.getType().name().toLowerCase().contains("chest")){
			    					broken += "chest ";  					
								}
			    				else if(i.getType().name().toLowerCase().contains("leg")){
			    					broken += "leg ";
								}
			    				else if(i.getType().name().toLowerCase().contains("boot")){
			    					broken += "boot ";
								}
			    				player.sendMessage(ChatColor.DARK_RED + "Your item " + im.getDisplayName() + ChatColor.RED + " has broken...");
			    			}
			    			lore.set(3, ChatColor.GRAY + "Durability : " + cur + "/" + max);
			    		}
			    	}
			    	if(rarity != "Godly"){
			    		if(i.getType() != Material.AIR){
			    			im.setLore(lore);
			    			i.setItemMeta(im);
			    		}
			    	}
			    	i.setDurability((short) 0);
		    	}
        	}
        	newArmor.add(i);
        }
        try{
        	 player.getInventory().setArmorContents((ItemStack[]) new ArrayList<ItemStack>().toArray());
        }
        catch(ClassCastException e){
        	//pass
        }
        
        if(broken != ""){
        	if(broken.contains("helm")){
        		player.getInventory().setHelmet(new ItemStack(Material.AIR));
        	}
        	if(broken.contains("chest")){
        		player.getInventory().setChestplate(new ItemStack(Material.AIR));
        	}
        	if(broken.contains("leg")){
        		player.getInventory().setLeggings(new ItemStack(Material.AIR));
        	}
        	if(broken.contains("boot")){
        		player.getInventory().setBoots(new ItemStack(Material.AIR));
        	}
        }
       
	}
	
	@EventHandler
	public void noWeaponBreakDamage( EntityShootBowEvent event){
		if(event.getEntity().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
	    // Bow
	    if( event.getEntity() instanceof Player ){
	    	checkWeaponDurability((Player) event.getEntity());
	    }
	}
	
	@EventHandler
	public void onBowAttack(PlayerInteractEvent e){
		if(e.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		if(e.getAction() == Action.LEFT_CLICK_AIR){
			if(e.getPlayer().getItemInHand().getType() == Material.BOW){
				Integer curtime = (int) (System.currentTimeMillis() / 100);
				try{
					if(curtime >= attackTime.get(e.getPlayer())){
						Player p = e.getPlayer();
						Arrow a = e.getPlayer().shootArrow();
						a.setVelocity(a.getVelocity().multiply(2));
						attackTime.put(p, (int) ((System.currentTimeMillis() / 100) + 10));
						checkWeaponDurability(p);
					}
				}
				catch(NullPointerException e2){
					//e2.printStackTrace();
					Player p = e.getPlayer();
					e.getPlayer().shootArrow();
					Arrow a = e.getPlayer().shootArrow();
					a.setVelocity(a.getVelocity().multiply(2));
					attackTime.put(p, (int) ((System.currentTimeMillis() / 100) + 10));
					checkWeaponDurability(p);
				}
				
			}
		}
	}
	
}
