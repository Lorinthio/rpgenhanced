package me.Lorinth.RPGE.Managers;

import java.util.ArrayList;
import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.Events.DayTime;
import me.Lorinth.RPGE.Events.TimeChangeEvent;
import me.Lorinth.RPGE.Races.Aquos;
import me.Lorinth.RPGE.Races.Avian;
import me.Lorinth.RPGE.Races.DemiGod;
import me.Lorinth.RPGE.Races.Drow;
import me.Lorinth.RPGE.Races.Dwarf;
import me.Lorinth.RPGE.Races.Entient;
import me.Lorinth.RPGE.Races.HalfElf;
import me.Lorinth.RPGE.Races.Halforc;
import me.Lorinth.RPGE.Races.HighElf;
import me.Lorinth.RPGE.Races.Human;
import me.Lorinth.RPGE.Races.Pyrite;
import me.Lorinth.RPGE.Races.Race;
import me.Lorinth.RPGE.Races.Viking;
import me.Lorinth.RPGE.Races.WoodElf;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Wool;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import ru.tehkode.permissions.bukkit.PermissionsEx;

public class RaceManager implements Listener{

	private CharacterManager cm;
	private HashMap<String, Race> races = new HashMap<String, Race>();

	private ArrayList<Character> drowpassive = new ArrayList<Character>();
	private ArrayList<Material> stoneblocks = new ArrayList<Material>();

	private HashMap<Player, Integer> avianjump = new HashMap<Player, Integer>();
	private ArrayList<Player> canjump = new ArrayList<Player>();
	private ArrayList<Character> entientRain = new ArrayList<Character>();
	private ArrayList<Character> vikingBlood = new ArrayList<Character>();
	
	private HashMap<Player, Race> raceChange = new HashMap<Player, Race>();
	
	private Inventory raceWindow;
	
	Race dwarf;
	Race drow;
	Race avian;
	Race woodElf;
	Race entient;
	Race viking;
	Race demigod;

	public RaceManager(CharacterManager cha){
		cm = cha;
		storeRaces();
		Bukkit.getPluginManager().registerEvents(this, cm.getMain());

		storeMiscData();
	}

	public void storeRaces(){
		races.put("aquos", new Aquos());
		avian = new Avian();
		races.put("avian", avian);
		drow = new Drow();
		races.put("drow", drow);

		dwarf = new Dwarf();
		races.put("dwarf", dwarf);
		
		entient = new Entient();
		races.put("entient", entient);
		races.put("half elf", new HalfElf());
		races.put("half orc", new Halforc());
		races.put("high elf", new HighElf());
		
		demigod = new DemiGod();
		races.put("demi god", demigod);
		
		ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
		races.put("human", new Human(skull));
		races.put("pyrite", new Pyrite());
		viking = new Viking();
		races.put("viking", viking);
		
		woodElf = new WoodElf();
		races.put("wood elf", woodElf);

		
		//13 Races
	}

	public void openRaceWindow(Player p){
		this.raceWindow = Bukkit.createInventory(null, 18, ChatColor.YELLOW + "Race List");
		raceWindow.setItem(0, races.get("aquos").getIcon());
		raceWindow.setItem(1, races.get("avian").getIcon());
		raceWindow.setItem(2, races.get("dwarf").getIcon());
		raceWindow.setItem(3, races.get("drow").getIcon());
		raceWindow.setItem(4, races.get("entient").getIcon());
		raceWindow.setItem(5, races.get("half elf").getIcon());
		raceWindow.setItem(6, races.get("half orc").getIcon());
		raceWindow.setItem(7, races.get("high elf").getIcon());
		raceWindow.setItem(8, races.get("human").getIcon());
		raceWindow.setItem(9, races.get("pyrite").getIcon());
		raceWindow.setItem(10, races.get("viking").getIcon());
		raceWindow.setItem(11, races.get("wood elf").getIcon());
		
		String[] groups = PermissionsEx.getUser(p).getGroupNames();
		for(String group : groups){
			if(group.contains("god")){
				raceWindow.setItem(17, races.get("demi god").getIcon());
			}
		}
		
		p.openInventory(raceWindow);
	}
	
	@EventHandler
	public void handleRaceWindowEvent(InventoryClickEvent event){
		Player p = (Player) event.getWhoClicked();
		
		if(p.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		Inventory inv = event.getClickedInventory();
		if(inv == null){
			return;
		}
		if(inv.getName().contains("Skill Binds")){
			event.setCancelled(true);
		}
		if(inv.getName().contains("Race List")){
			Integer slot = event.getSlot();
			switch(slot){
			case 0:
				this.showRaceWindow(races.get("aquos"), (Player) event.getWhoClicked());
				break;
			case 1:
				this.showRaceWindow(races.get("avian"), (Player) event.getWhoClicked());
				break;
			case 2:
				this.showRaceWindow(races.get("dwarf"), (Player) event.getWhoClicked());
				break;
			case 3:
				this.showRaceWindow(races.get("drow"), (Player) event.getWhoClicked());
				break;
			case 4:
				this.showRaceWindow(races.get("entient"), (Player) event.getWhoClicked());
				break;
			case 5:
				this.showRaceWindow(races.get("half elf"), (Player) event.getWhoClicked());
				break;
			case 6:
				this.showRaceWindow(races.get("half orc"), (Player) event.getWhoClicked());
				break;
			case 7:
				this.showRaceWindow(races.get("high elf"), (Player) event.getWhoClicked());
				break;
			case 8:
				this.showRaceWindow(races.get("human"), (Player) event.getWhoClicked());
				break;
			case 9:
				this.showRaceWindow(races.get("pyrite"), (Player) event.getWhoClicked());
				break;
			case 10:
				this.showRaceWindow(races.get("viking"), (Player) event.getWhoClicked());
				break;
			case 11:
				this.showRaceWindow(races.get("wood elf"), (Player) event.getWhoClicked());
				break;
			default:
				break;
			}
			
			if(event.getCurrentItem().getType() == Material.NETHER_STAR){
				this.showRaceWindow(races.get("demi god"), (Player) event.getWhoClicked());
			}
			
			event.setCancelled(true);
		}
		else if(inv.getName().contains("Race Info: ")){
			ItemStack item = event.getCurrentItem();
			if(item.getType() == Material.STAINED_GLASS_PANE){
				MaterialData md = item.getData();
				if(md.getData() == 13){
					Race race = races.get(inv.getName().replace("Race Info: ", "").toLowerCase().trim());
					if(cm.getCharacter(p).RaceToken >= 1){
						p.closeInventory();
						this.raceChange.put(p, race);
						p.sendMessage(ChatColor.DARK_AQUA + "Race Change : To complete type " + ChatColor.GOLD + " yes " + ChatColor.DARK_AQUA + "or " + ChatColor.GOLD + "no");
					}
				}
				else if(md.getData() == 14){
					openRaceWindow((Player) event.getWhoClicked());
				}
			}
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	private void onPlayerChat(AsyncPlayerChatEvent event){
		final Player p = event.getPlayer();
		String m = ChatColor.stripColor(event.getMessage());
		if(this.raceChange.containsKey(p)){
			if(m.toLowerCase().contains("yes")){
				Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){
					@Override
					public void run() {
						Character cha = cm.getCharacter(p);
						Race race = raceChange.get(p);
						if(cha.getRace().equals(race)){
							cha.sendMessage(ChatColor.RED + "You cannot change to the same race...");
							return;
						}
						else{
							cha.setRace(race, true);
							cha.update(false);
						}
						raceChange.remove(p);
					}
					
				}, 2);
			}
			else if(m.toLowerCase().contains("no")){
				raceChange.remove(p);
			}
			event.setCancelled(true);
		}
	}
	
	private void showRaceWindow(Race race, Player p){
		Inventory racewindow = Bukkit.createInventory(null, 27, "Race Info: " + race.getName());
		racewindow.setItem(0, createItem("Strength", race));
		racewindow.setItem(1, createItem("Constitution", race));
		racewindow.setItem(2, createItem("Dexterity", race));
		racewindow.setItem(3, createItem("Agility", race));
		racewindow.setItem(4, createItem("Wisdom", race));
		racewindow.setItem(5, createItem("Intelligence", race));
		
		ItemStack lore = new ItemStack(Material.BOOK_AND_QUILL, 1);
		ItemMeta loreMeta = lore.getItemMeta();
		loreMeta.setDisplayName(ChatColor.AQUA + "Description");
		lore.setItemMeta(getFormedDescrip(race.getDescription(), loreMeta));
		
		racewindow.setItem(8, lore);
		
		//Set middle line as actives
		Integer psCount = 0;
		for(ActiveSkill ps : race.getActives()){
			ItemStack passive = race.getIcon().clone();
			ItemMeta pm = passive.getItemMeta();
			pm.setDisplayName(ps.getName());
			passive.setItemMeta(getFormedDescrip(ps.getDescription(), pm));
			racewindow.setItem(9 + psCount, passive);
			psCount += 1;
		}
		
		//Set bottom line as passives
		psCount = 0;
		for(PassiveSkill ps : race.getPassives()){
			ItemStack passive = race.getIcon().clone();
			ItemMeta pm = passive.getItemMeta();
			pm.setDisplayName(ps.getName());
			passive.setItemMeta(getFormedDescrip(ps.getDescription(), pm));
			racewindow.setItem(18 + psCount, passive);
			psCount += 1;
		}
		
		//Make a set and back button
		if(cm.getCharacter(p).RaceToken == 0){
			ItemStack confirm = new ItemStack(Material.GLASS, 1, (short) 13);
			ItemMeta coIM = confirm.getItemMeta();
			coIM.setDisplayName(ChatColor.DARK_AQUA + "Unable to Change");
			confirm.setItemMeta(coIM);
			racewindow.setItem(25, confirm);
		}
		else{
			ItemStack confirm = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 13);
			ItemMeta coIM = confirm.getItemMeta();
			coIM.setDisplayName(ChatColor.GREEN + "Change Race!");
			confirm.setItemMeta(coIM);
			racewindow.setItem(25, confirm);
		}
		
		ItemStack back = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemMeta coBack = back.getItemMeta();
		coBack.setDisplayName(ChatColor.RED + "Back");
		back.setItemMeta(coBack);
		racewindow.setItem(26, back);
		
		p.openInventory(racewindow);
	}
	
	private ItemMeta getFormedDescrip(String FullLine, ItemMeta meta){
		String descrip = FullLine;
		String[] split = descrip.split(" ");
		String line = ChatColor.GRAY + "";
		ArrayList<String> lore = new ArrayList<String>();
		for(String word : split){
			if(line.length() + 1 + word.length() <= 20){
				line += word + " ";
			}
			else{
				lore.add(line.trim());
				line = ChatColor.GRAY + word + " ";
			}
		}
		lore.add(line.trim());
		meta.setLore(lore);
		return meta;
	}
	
	private ItemStack createItem(String name, Race cha)
	{
		ItemStack i = new ItemStack(Material.WOOL, 1);
		ItemMeta im = i.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();

		Wool wool;

		switch(name){
		case "Strength":
			wool = new Wool();
			wool.setColor(DyeColor.RED);
			i = wool.toItemStack(cha.getAttribute(StatType.STRENGTH));

			im.setDisplayName(ChatColor.RED + name + " : " + cha.getAttribute(StatType.STRENGTH));

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.RED + "Melee Bonus Damage : " + cha.getAttribute(StatType.STRENGTH) / 8);
			lore.add(ChatColor.RED + "Health Bonus : " + cha.getAttribute(StatType.STRENGTH).intValue() / 10);
			lore.add(ChatColor.RED + "Stamina Bonus : " + cha.getAttribute(StatType.STRENGTH));
			break;
		case "Constitution":
			wool = new Wool();
			wool.setColor(DyeColor.ORANGE);
			i = wool.toItemStack(cha.getAttribute(StatType.CONSTITUTION));

			im.setDisplayName(ChatColor.GOLD + name + " : " + cha.getAttribute(StatType.CONSTITUTION));

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			//Integer defense = Integer.valueOf(cha.getAttribute(StatType.CONSTITUTION).intValue() / 2 + cha.getAttribute(StatType.CONSTITUTION).intValue() / 5 + 10);
			lore.add(ChatColor.GOLD + "Defense : " + cha.getAttribute(StatType.CONSTITUTION));
			lore.add(ChatColor.GOLD + "Health Bonus : " + cha.getAttribute(StatType.CONSTITUTION).intValue() / 4);
			lore.add(ChatColor.GOLD + "Stamina Bonus : " + cha.getAttribute(StatType.CONSTITUTION).intValue() / 2);
			break;
		case "Dexterity":
			wool = new Wool();
			wool.setColor(DyeColor.GREEN);
			i = wool.toItemStack(cha.getAttribute(StatType.DEXTERITY));

			im.setDisplayName(ChatColor.DARK_GREEN + name + " : " + cha.getAttribute(StatType.DEXTERITY));

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.DARK_GREEN + "Ranged Bonus Damage : " + cha.getAttribute(StatType.DEXTERITY) / 8);
			lore.add(ChatColor.DARK_GREEN + "Critical : " + (cha.getAttribute(StatType.DEXTERITY).intValue() / 8 + 1));
			break;
		case "Agility":
			wool = new Wool();
			wool.setColor(DyeColor.LIME);
			i = wool.toItemStack(cha.getAttribute(StatType.AGILITY));

			im.setDisplayName(ChatColor.GREEN + name + " : " + cha.getAttribute(StatType.AGILITY));

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.GREEN + "Critical : " + cha.getAttribute(StatType.AGILITY).intValue() / 12);
			lore.add(ChatColor.GREEN + "Dodge : " + cha.getAttribute(StatType.AGILITY) / 10);
			break;
		case "Wisdom":
			wool = new Wool();
			wool.setColor(DyeColor.BLUE);
			i = wool.toItemStack(cha.getAttribute(StatType.WISDOM));

			im.setDisplayName(ChatColor.BLUE + name + " : " + cha.getAttribute(StatType.WISDOM));

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.BLUE + "Magic Defense : " + ((cha.getAttribute(StatType.WISDOM) / 2)+ (cha.getAttribute(StatType.WISDOM) / 5) + 14));
			lore.add(ChatColor.BLUE + "Mana Bonus : " + cha.getAttribute(StatType.WISDOM));
			break;
		case "Intelligence":
			wool = new Wool();
			wool.setColor(DyeColor.PURPLE);
			i = wool.toItemStack(cha.getAttribute(StatType.INTELLIGENCE));

			im.setDisplayName(ChatColor.DARK_PURPLE + name + " = " + cha.getAttribute(StatType.INTELLIGENCE));

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.DARK_PURPLE + "Magic Bonus Damage : " + cha.getAttribute(StatType.INTELLIGENCE) / 8);
			lore.add(ChatColor.DARK_PURPLE + "Mana : " + cha.getAttribute(StatType.INTELLIGENCE));
			break;
		}
		im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}
	
	public void storeMiscData(){
		stoneblocks.add(Material.STONE);
		stoneblocks.add(Material.COBBLESTONE);
		stoneblocks.add(Material.REDSTONE_ORE);
		stoneblocks.add(Material.COAL_ORE);
		stoneblocks.add(Material.IRON_ORE);
		stoneblocks.add(Material.GOLD_ORE);
		stoneblocks.add(Material.DIAMOND_ORE);
		stoneblocks.add(Material.SMOOTH_BRICK);
		stoneblocks.add(Material.MOSSY_COBBLESTONE);
		stoneblocks.add(Material.SMOOTH_STAIRS);
	}

	public void listRaces(Player player){
		player.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Current Races");
		for(Race race : races.values()){
			player.sendMessage(ChatColor.GRAY + race.getName());
		}
		player.sendMessage(ChatColor.GRAY + "For more info about a race type " + ChatColor.GOLD + "/raceinfo <race>");
	}

	public void raceInfo(Player player, String name){
		Race race = getRaceByName(name);
		player.sendMessage(ChatColor.GRAY + "Race : " + race.getName());
		player.sendMessage(ChatColor.GRAY + "Description : " + race.getDescription());
		race.printStats(player);
	}

	public Race getRaceByName(String name) throws NullPointerException{
		Race race = races.get(name.toLowerCase());

		if(race == null){
			throw new NullPointerException();
		}
		else{
			return race;
		}
	}

	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event){
		if(event.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		World world = event.getWorld();
		if(event.toWeatherState()){
			for(Player p : world.getPlayers()){
				Character cha = cm.getCharacter(p);
				if(cha.getRace() == entient){
					if(!entientRain.contains(cha)){
						entientRain.add(cha);
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						passives.put(PassiveType.HealthRegen, passives.get(PassiveType.HealthRegen) + 2);
						passives.put(PassiveType.ManaRegen, passives.get(PassiveType.ManaRegen) + 2);
						passives.put(PassiveType.StaminaRegen, passives.get(PassiveType.StaminaRegen) + 2);
					}
				}
			}
		}
		else{
			for(Player p : world.getPlayers()){
				Character cha = cm.getCharacter(p);
				if(cha.getRace() == entient){
					if(entientRain.contains(cha)){
						entientRain.remove(cha);
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						passives.put(PassiveType.HealthRegen, passives.get(PassiveType.HealthRegen) - 2);
						passives.put(PassiveType.ManaRegen, passives.get(PassiveType.ManaRegen) - 2);
						passives.put(PassiveType.StaminaRegen, passives.get(PassiveType.StaminaRegen) - 2);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onEntientChangeWorld(PlayerChangedWorldEvent event){
		if(event.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			event.getPlayer().setMaxHealth((double)20);
			event.getPlayer().setHealth((double)20);
			return;
		}
		
		Player p = event.getPlayer();
		Character cha = cm.getCharacter(p);
		if(cha.getRace() == entient){	
			World from = event.getFrom();
			World to = p.getWorld();
			
			if(from.hasStorm() != to.hasStorm()){
				if(from.hasStorm()){
					if(entientRain.contains(cha)){
						entientRain.remove(cha);
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						passives.put(PassiveType.HealthRegen, passives.get(PassiveType.HealthRegen) - 2);
						passives.put(PassiveType.ManaRegen, passives.get(PassiveType.ManaRegen) - 2);
						passives.put(PassiveType.StaminaRegen, passives.get(PassiveType.StaminaRegen) - 2);
					}
				}
				else{
					if(!entientRain.contains(cha)){
						entientRain.add(cha);
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						passives.put(PassiveType.HealthRegen, passives.get(PassiveType.HealthRegen) + 2);
						passives.put(PassiveType.ManaRegen, passives.get(PassiveType.ManaRegen) + 2);
						passives.put(PassiveType.StaminaRegen, passives.get(PassiveType.StaminaRegen) + 2);
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void EventPassives(EntityDamageEvent event){

		if(event.getEntity().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		if(event.getEntity() instanceof Player){
			Player player = (Player) event.getEntity();
			Character cha = cm.getCharacter(player);
			Race race = cha.getRace();
			
			if(event.getCause().equals(DamageCause.DROWNING)){
				if(race.equals(races.get("aquos")) | race.equals(races.get("demi god"))){
					event.setCancelled(true);
				}
			}


			else if(event.getCause() == DamageCause.LAVA){
				if(race.equals(races.get("pyrite")) | race.equals(races.get("demi god"))){
					event.setCancelled(true);
					player.setFireTicks(0);
				}
			}
			else if(event.getCause() == DamageCause.FIRE){
				if(race.equals(races.get("pyrite")) | race.equals(races.get("demi god"))){
					event.setCancelled(true);
					player.setFireTicks(0);
				}
			}
			else if(event.getCause() == DamageCause.FIRE_TICK){
				if(race.equals(races.get("pyrite")) | race.equals(races.get("demi god"))){
					event.setCancelled(true);
					player.setFireTicks(0);
				}
			}

		}


	}

	@EventHandler
	public void onAvianJoin(PlayerJoinEvent event)
	{
		final Player p = event.getPlayer();
		
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				Character c = cm.getCharacter(p);
				if(c.getRace().equals(avian) | c.getRace().equals(demigod)){
					p.setAllowFlight(true);
					canjump.add(p);
					avianjump.put(p, 0);
				}
				
			}
			
			
		}, 40);
		
	}

	@EventHandler
	public void onDoubleJump(PlayerToggleFlightEvent event){
		if(event.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		Player player = event.getPlayer();
		if(player.getGameMode() == GameMode.CREATIVE){
			return;
		}
		Character cha = cm.getCharacter(player);
		if(cha.getRace().equals(avian) | cha.getRace().equals(demigod)){
			if(canJump(player)){
				player.setVelocity(player.getVelocity().multiply(1.3).setY(0.5));
				
				player.setAllowFlight(false);
				event.setCancelled(true);
				Sound s = Sound.ENDERDRAGON_WINGS;
				player.getWorld().playSound(player.getLocation(), s, 2, 0);
				
				Integer jumps = avianjump.get(player) + 1;
				if(cha.getRace().equals(avian) && jumps >= 3){
					if(jumps == 3){
						player.setFoodLevel(player.getFoodLevel() - 1);
						avianjump.put(player, jumps);
						canjump.remove(player);
						canjump.add(player);
						player.setAllowFlight(true);
					}
					else if(jumps == 4){
						avianjump.put(player, jumps);
						canjump.remove(player);
						canjump.add(player);
						player.setAllowFlight(true);
					}
					else if(jumps >= 5){
						player.setFoodLevel(player.getFoodLevel() - 1);
						canjump.remove(player);
						avianjump.put(player, 0);
					}
				}
				else if(cha.getRace().equals(demigod) && jumps >= 3){
					canjump.remove(player);
					avianjump.put(player, 0);
					player.setFoodLevel(player.getFoodLevel() - 1);
				}
				else{
					avianjump.put(player, jumps);
					canjump.remove(player);
					canjump.add(player);
					player.setAllowFlight(true);
				}
			}
			else{
				event.setCancelled(true);
				return;
			}
		}
		
		
	}

	public boolean isInAir(Player player)
	{
		if (player.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.AIR) {
			return true;
		}
		return false;
		
	}

	public boolean canJump(Player player)
	{
		if (this.canjump.contains(player))
		{
			return true;
		}
		return false;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerLand(PlayerMoveEvent event)
	{
		if(event.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		try{
			Player play = event.getPlayer();
			Character cha = cm.getCharacter(event.getPlayer());
			if(cha.getRace().equals(avian) | cha.getRace().equals(demigod)){
				if(event.getPlayer().getGameMode() == GameMode.CREATIVE){
					return;
				}
				if (!isInAir(event.getPlayer()))
				{
					this.canjump.add(event.getPlayer());
					event.getPlayer().setAllowFlight(true);
					
					avianjump.put(event.getPlayer(), 0);
				}
			}
			else if(cha.getRace().equals(viking)){
				
				double temp = play.getLocation().getBlock().getTemperature();
				temp = (int) (temp / 0.3);
				if(temp < 1){
					if(!vikingBlood.contains(cha)){
						this.vikingBlood.add(cha);
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						int Hp = passives.get(PassiveType.HealthMax);
						Hp += 4;
						int Damage = passives.get(PassiveType.PureDamage);
						Damage += 3;
						passives.put(PassiveType.HealthMax, Hp);
						passives.put(PassiveType.PureDamage, Damage);
					}
					
				}
			}
		}
		catch(NullPointerException e){
			//PASS
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event){
		final Player player = event.getPlayer();
		final RaceManager rm = this;

		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				Character cha = cm.getCharacter(player);
				if(cha.getRace().equals(drow)){
					DayTime time = cm.getMain().getDayTime(player.getWorld());
					if(time == DayTime.Night){
						player.setWalkSpeed((float) (player.getWalkSpeed() + 0.035));
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						Integer damage = passives.get(PassiveType.PureDamage) + 2;
						passives.put(PassiveType.PureDamage, damage);
						rm.drowpassive.add(cha);
						player.sendMessage(ChatColor.GRAY + "[Drow] : You have been empowered by the night.");
					}
					else if(time == DayTime.Midnight){
						player.setWalkSpeed((float) (player.getWalkSpeed() + 0.035));
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						Integer damage = passives.get(PassiveType.PureDamage) + 2;
						passives.put(PassiveType.PureDamage, damage);
						rm.drowpassive.add(cha);
						player.sendMessage(ChatColor.GRAY + "[Drow] : You have been empowered by the night.");
					}
				}

			}

		}, 20);


	}

	@EventHandler
	public void drowPassive(TimeChangeEvent event){
		if(event.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		DayTime dt = event.getDayTime();
		if(dt == DayTime.Night){
			Character cha;
			for(Player player : event.getWorld().getPlayers()){
				cha = cm.getCharacter(player);
				if(cha.getRace().equals(drow)){
					player.setWalkSpeed((float) (player.getWalkSpeed() + 0.035));
					HashMap<PassiveType, Integer> passives = cha.getPassives();
					Integer damage = passives.get(PassiveType.PureDamage) + 2;
					passives.put(PassiveType.PureDamage, damage);
					this.drowpassive.add(cha);
					player.sendMessage(ChatColor.GRAY + "[Drow] : You have been empowered by the night.");
				}
			}
		}
		if(dt == DayTime.Dawn){
			Character cha;
			for(Player player : event.getWorld().getPlayers()){
				cha = cm.getCharacter(player);
				if(cha.getRace().equals(drow)){
					if(this.drowpassive.contains(cha)){
						player.setWalkSpeed((float) (player.getWalkSpeed() - 0.035));
						HashMap<PassiveType, Integer> passives = cha.getPassives();
						Integer damage = passives.get(PassiveType.PureDamage) + 2;
						passives.put(PassiveType.PureDamage, damage);
						this.drowpassive.remove(cha);
						player.sendMessage(ChatColor.GRAY + "[Drow] : You have returned to normal strength.");
					}
				}
			}
		}
	}

	@EventHandler
	public void dwarfPassive(BlockBreakEvent event){
		if(event.getPlayer().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		Block block = event.getBlock();
		if(stoneblocks.contains(block.getType())){
			Player player = event.getPlayer();
			if(cm.getCharacter(player).getRace().equals(dwarf)){
				if(!player.hasPotionEffect(PotionEffectType.FAST_DIGGING)){
					player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 2, 10));
				}
			}
		}
	}
	
	@EventHandler
	public void ElvenBow(EntityShootBowEvent event){
		if(event.getEntity().getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		Entity ent = event.getEntity();
		if(ent instanceof Player){
			Player p = (Player) ent;
			Character cha = cm.getCharacter(p);
			if(cha.getRace() == woodElf){
				event.getProjectile().setVelocity(event.getProjectile().getVelocity().multiply(1.5));
			}
		}
	}



}
