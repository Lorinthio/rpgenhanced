package me.Lorinth.RPGE.Managers;

import java.util.ArrayList;
import java.util.Random;

import me.Lorinth.RPGE.Characters.SkillsProfile;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.Resources.Ore;
import me.Lorinth.RPGE.Resources.Resource;
import me.Lorinth.RPGE.Resources.Wood;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class ResourceManager implements Listener{

	private ArrayList<Material> oreMaterials = new ArrayList<Material>();
	
	private ArrayList<Wood> wood = new ArrayList<Wood>();
	private ArrayList<Ore> ore = new ArrayList<Ore>();
	private RpgEnhancedMain main;
	
	private Integer dropchance = 25;
	
	private Random random = new Random();
	
	public ResourceManager(RpgEnhancedMain main){
		storeOres();
		this.main = main;
	}
	
	public void storeOres(){
		oreMaterials.add(Material.STONE);
		oreMaterials.add(Material.IRON_ORE);
		oreMaterials.add(Material.GOLD_ORE);
		oreMaterials.add(Material.DIAMOND_ORE);
	}
	
	public void createResources(){
		//Ores
		registerResource(new Ore("Gravel", Material.STONE, 1));
		registerResource(new Ore("Jagged Gravel", Material.STONE, 6));
		registerResource(new Ore("Dark Stone", Material.STONE, 11));
		registerResource(new Ore("Petrified Bone", Material.STONE, 16));
		registerResource(new Ore("Jagged Bone", Material.STONE, 21));
		registerResource(new Ore("Dark Bone", Material.STONE, 26));
		registerResource(new Ore("Rusted Iron", Material.IRON_ORE, 31));
		registerResource(new Ore("Steel", Material.IRON_ORE, 36));
		
		
		
		//Wood
	}
	
	public void registerResource(Resource res){
		if(res instanceof Ore){
			ore.add((Ore) res);
		}
		if(res instanceof Wood){
			wood.add((Wood) res);
		}
	}
	
	@EventHandler
	public void onResourceBroken(BlockBreakEvent event){
		Player player = event.getPlayer();
		SkillsProfile profile = main.EL.getPlayerProfile(player);
		Block block = event.getBlock();
		if(block.getType() == Material.WOOD){
			
		}
		else if(oreMaterials.contains(block.getType())){
			if(random.nextInt(100) >= dropchance){
				Integer mining = profile.Mining;
				ArrayList<Ore> possible = new ArrayList<Ore>();
				for(Ore res : ore){
					if(res.getType() == block.getType()){
						if(mining >= res.getLevel()){
							possible.add(res);
						}
					}
				}
				Ore drop = possible.get(random.nextInt(possible.size()));
				
				drop.spawnOre(block.getLocation());
				event.getBlock().setType(Material.AIR);
				event.setCancelled(true);
			}
		}
	}
	
}
