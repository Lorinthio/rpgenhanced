package me.Lorinth.RPGE.Abilities.Dexterity;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.inventory.ItemStack;

public class PoisonShot extends ActiveSkill{

	public PoisonShot(){
		super(ChatColor.DARK_GREEN + "Poison Shot");
		this.setDescription("Shoots an arrow for normal ranged damage, and poisons the target"
				+ "");
		this.setCooldown(12);
		this.setStatRequirement(StatType.DEXTERITY, 20);
		this.setVitalCost(Vital.STAMINA, 8);
		
		ArrayList<Object> params1 = new ArrayList<Object>();
		params1.add(new ItemStack(Material.ARROW, 1));
		Action consume = new Action(this, ActionType.ConsumeItem, params1);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(Arrow.class);
		Action act1 = new Action(this, ActionType.Projectile, params);
		act1.setCustomFormula("<ranged>");
		
		this.addAction(consume);
		this.addAction(act1);
	}
	
}
