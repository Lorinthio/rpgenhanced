package me.Lorinth.RPGE.Abilities.Dexterity;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class CriticalBoostII extends PassiveSkill{

	public CriticalBoostII() {
		super(ChatColor.DARK_GREEN + "Critical Boost II");
		this.setDescription("Adds 4 to passive critical chance");
		this.setStatRequirement(StatType.DEXTERITY, 50);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer crit = passives.get(PassiveType.Critical);
		crit += 4;
		passives.put(PassiveType.Critical, crit);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer crit = passives.get(PassiveType.Critical);
		crit -= 4;
		passives.put(PassiveType.Critical, crit);
		
		cha.setPassives(passives);
	}
	
}
