package me.Lorinth.RPGE.Abilities.Dexterity;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class RangedDamageBoostI extends PassiveSkill{

	public RangedDamageBoostI() {
		super(ChatColor.DARK_GREEN + "Ranged Damage Boost I");
		this.setDescription("Adds 1 pure damage to ranged attacks.");
		this.setStatRequirement(StatType.DEXTERITY, 10);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer rDam = passives.get(PassiveType.RangedDamage);
		rDam += 1;
		passives.put(PassiveType.RangedDamage, rDam);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer rDam = passives.get(PassiveType.RangedDamage);
		rDam -= 1;
		passives.put(PassiveType.RangedDamage, rDam);
		
		cha.setPassives(passives);
	}
}
