package me.Lorinth.RPGE.Abilities.Dexterity;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class SummonArrows extends ActiveSkill{

	public SummonArrows(){
		super(ChatColor.DARK_GREEN + "Summon Arrows");
		this.setDescription("Gives the caster 32 Arrows");
		this.setCooldown(120);
		this.setStatRequirement(StatType.DEXTERITY, 10);
		this.setVitalCost(Vital.MANA, 10);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(new ItemStack(Material.ARROW, 32));
		params.add(TargetType.SELF);
		
		Action act1 = new Action(this, ActionType.ProduceItem, params);
		
		this.addAction(act1);
	}
	
	
	
}
