package me.Lorinth.RPGE.Abilities.Dexterity;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Delay;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.inventory.ItemStack;

public class DoubleShot extends ActiveSkill{

	public DoubleShot() {
		super(ChatColor.DARK_GREEN + "Double Shot");
		this.setDescription("Shoots two arrows in rapid succession dealing 80% damage");
		this.setCooldown(14);
		this.setStatRequirement(StatType.DEXTERITY, 15);
		this.setVitalCost(Vital.STAMINA, 8);
		
		ArrayList<Object> params1 = new ArrayList<Object>();
		params1.add(new ItemStack(Material.ARROW, 1));
		Action consume = new Action(this, ActionType.ConsumeItem, params1);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(Arrow.class);
		Action act1 = new Action(this, ActionType.Projectile, params);
		act1.setCustomFormula("(<ranged> * 8) / 10");
		
		this.addAction(consume);
		this.addAction(act1);
		
		ArrayList<Object> params3 = new ArrayList<Object>();
		params3.add(new ItemStack(Material.ARROW, 1));
		Action consume2 = new Action(this, ActionType.ConsumeItem, params3);
		
		ArrayList<Object> params2 = new ArrayList<Object>();
		params2.add(Arrow.class);
		params2.add(new Delay(10));
		Action act2 = new Action(this, ActionType.Projectile, params2);
		act2.setCustomFormula("(<ranged> * 8) / 10");
		
		this.addAction(consume2);
		this.addAction(act2);
	}

}
