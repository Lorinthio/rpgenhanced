package me.Lorinth.RPGE.Abilities.Dexterity;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.inventory.ItemStack;

public class PowerShotII extends ActiveSkill{

	public PowerShotII(){
		super(ChatColor.DARK_GREEN + "Power Shot II");
		this.setDescription("Shoots an arrow stronger than normal for ranged damage + 3 + (DEX / 5)"
				+ "");
		this.setCooldown(10);
		this.setStatRequirement(StatType.DEXTERITY, 25);
		this.setVitalCost(Vital.STAMINA, 7);
		
		ArrayList<Object> params1 = new ArrayList<Object>();
		params1.add(new ItemStack(Material.ARROW, 1));
		Action consume = new Action(this, ActionType.ConsumeItem, params1);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(Arrow.class);
		Action act1 = new Action(this, ActionType.Projectile, params);
		act1.setCustomFormula("<ranged> + 3 + (<dex> / 5)");
		
		this.addAction(consume);
		this.addAction(act1);
	}
	
}
