package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.MagicType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.entity.Fireball;

public class FireballII extends ActiveSkill{

	public FireballII() {
		super(ChatColor.DARK_PURPLE + "Fireball II");
		this.setDescription("Deals damage to a non-friendly target for magic damage + 3 + (INT / 5)"
				+ "");
		this.setCooldown(6);
		this.setStatRequirement(StatType.INTELLIGENCE, 25);
		this.setVitalCost(Vital.MANA, 7);
		this.setMagicType(MagicType.Fire);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(Fireball.class);
		Action act1 = new Action(this, ActionType.Projectile, params);
		act1.setCustomFormula("<magic> + 5 + (<int> / 6)");
		this.addAction(act1);
	}

	
	
}
