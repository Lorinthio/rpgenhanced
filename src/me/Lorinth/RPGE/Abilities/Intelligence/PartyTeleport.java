package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.Position;
import me.Lorinth.RPGE.SkillAPI.PositionType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class PartyTeleport extends ActiveSkill{

	public PartyTeleport() {
		super(ChatColor.DARK_PURPLE + "Party Teleport");
		this.setDescription("Teleports your party to you");
		this.setCooldown(120);
		this.setStatRequirement(StatType.INTELLIGENCE, 40);
		this.setVitalCost(Vital.MANA, 20);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.PARTY);
		params.add(new Distance(4));
		params.add(new Position(PositionType.Caster));
		Action act1 = new Action(this, ActionType.Teleport, params);
		
		this.addAction(act1);
	}
	
}
