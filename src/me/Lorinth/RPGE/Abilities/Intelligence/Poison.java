package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Poison extends ActiveSkill{

	public Poison() {
		super(ChatColor.DARK_PURPLE + "Poison");
		this.setDescription("Poisons your target reducing their regeneration by 3 / 3 seconds");
		
		this.setStatRequirement(StatType.INTELLIGENCE, 10);
		this.setVitalCost(Vital.MANA, 6);
		this.setCooldown(8);
		
		ArrayList<Object> params = new ArrayList<Object>();
		PotionEffect pe = new PotionEffect(PotionEffectType.POISON, 200, 2);
		Buff poison = new Buff(pe);
		
		params.add(poison);
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(new Distance(20));
		
		Action act1 = new Action(this, ActionType.Buff, params);
		act1.addVisualEffect(Effect.POTION_BREAK, 4);
		this.addAction(act1);
		
	}
	
}
