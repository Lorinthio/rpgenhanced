package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class Storm extends ActiveSkill{

	public Storm(){
		super(ChatColor.DARK_PURPLE + "Storm");
		this.setDescription("Deals lightning damage to nearby enemies");
		this.setCooldown(180);
		this.setStatRequirement(StatType.INTELLIGENCE, 30);
		this.setVitalCost(Vital.MANA, 30);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.AOE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(10, 5, 10));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<magic>");
		
		act1.addLightningEffect();
		
		this.addAction(act1);
	}
	
}
