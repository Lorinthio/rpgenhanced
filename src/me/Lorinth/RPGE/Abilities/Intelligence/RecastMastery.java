package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class RecastMastery extends PassiveSkill{

	public RecastMastery() {
		super(ChatColor.DARK_PURPLE + "Recast Mastery");
		this.setDescription("Adds 5% cooldown reduction");
		this.setStatRequirement(StatType.INTELLIGENCE, 20);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer crit = passives.get(PassiveType.CooldownReduction);
		crit += 5;
		passives.put(PassiveType.CooldownReduction, crit);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer crit = passives.get(PassiveType.CooldownReduction);
		crit -= 5;
		passives.put(PassiveType.CooldownReduction, crit);
		
		cha.setPassives(passives);
	}

}
