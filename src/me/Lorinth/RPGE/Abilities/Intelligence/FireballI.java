package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.MagicType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.entity.Fireball;

public class FireballI extends ActiveSkill{

	public FireballI() {
		super(ChatColor.DARK_PURPLE + "Fireball I");
		this.setDescription("Deals damage to a non-friendly target for magic damage + 2 + (INT / 10)"
				+ "");
		this.setCooldown(6);
		this.setStatRequirement(StatType.INTELLIGENCE, 5);
		this.setVitalCost(Vital.MANA, 5);
		this.setMagicType(MagicType.Fire);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(Fireball.class);
		Action act1 = new Action(this, ActionType.Projectile, params);
		act1.setCustomFormula("<magic> + 2 + (<int> / 10)");
		this.addAction(act1);
	}

	
	
}
