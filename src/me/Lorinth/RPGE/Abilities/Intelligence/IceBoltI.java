package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.MagicType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.entity.Snowball;

public class IceBoltI extends ActiveSkill{

	public IceBoltI() {
		super(ChatColor.DARK_PURPLE + "Ice Bolt I");
		this.setDescription("Deals damage to, and slows, a non-friendly target for magic damage + (INT / 10)"
				+ "");
		this.setCooldown(15);
		this.setStatRequirement(StatType.INTELLIGENCE, 15);
		this.setVitalCost(Vital.MANA, 7);
		this.setMagicType(MagicType.Ice);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(Snowball.class);
		Action act1 = new Action(this, ActionType.Projectile, params);
		act1.setCustomFormula("<magic> + (<int> / 10)");
		this.addAction(act1);
	}
	
}
