package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class MagicAttackBoostI extends PassiveSkill{

	public MagicAttackBoostI() {
		super(ChatColor.DARK_PURPLE + "Magic Attack Boost I");
		this.setDescription("Adds 5 to your Magic Attack Modifier");
		this.setStatRequirement(StatType.INTELLIGENCE, 10);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer Attack = passives.get(PassiveType.Attack);
		Attack += 5;
		passives.put(PassiveType.MagicAttack, Attack);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer Attack = passives.get(PassiveType.Attack);
		Attack -= 5;
		passives.put(PassiveType.MagicAttack, Attack);
	}
	
}