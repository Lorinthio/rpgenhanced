package me.Lorinth.RPGE.Abilities.Intelligence;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class LightningStrikeI extends ActiveSkill{

	public LightningStrikeI(){
		super(ChatColor.DARK_PURPLE + "Lightning Strike I");
		this.setDescription("Deals lightning damage to your target");
		this.setCooldown(30);
		this.setStatRequirement(StatType.INTELLIGENCE, 20);
		this.setVitalCost(Vital.MANA, 10);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(20));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<magic> + <int> / 6");
		
		act1.addLightningEffect();
		
		this.addAction(act1);
	}
}
