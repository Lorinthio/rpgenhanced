package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class ManaFountII extends PassiveSkill{


	public ManaFountII() {
		super(ChatColor.BLUE + "Mana Fount II");
		this.setDescription("Adds 15 to maximum mana pool");
		this.setStatRequirement(StatType.WISDOM, 25);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mana = passives.get(PassiveType.ManaMax);
		mana += 15;
		passives.put(PassiveType.ManaMax, mana);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mana = passives.get(PassiveType.ManaMax);
		mana -= 15;
		passives.put(PassiveType.ManaMax, mana);
		
		cha.setPassives(passives);
	}
		
	
}
