package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class HealingCircleII extends ActiveSkill{

	public HealingCircleII() {
		super(ChatColor.BLUE + "Healing Circle II");
		this.setDescription("Heals nearby allies for 5 + <WIS> / 4"
				+ "");
		this.setCooldown(40);
		this.setStatRequirement(StatType.WISDOM, 45);
		this.setVitalCost(Vital.MANA, 15);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.AOE_FRIENDLY);
		params.add(Vital.HEALTH);
		params.add(new Distance(24));
		Action act1 = new Action(this, ActionType.Heal, params);
		act1.setCustomFormula("5 + (<wis> / 4)");
		act1.addVisualEffect(Effect.HEART, 1);
		
		params.clear();
		params.add(TargetType.SELF);
		Action act2 = new Action(this, ActionType.VisualEffect, params);
		act2.addVisualEffect(Effect.FLYING_GLYPH, 10);
		
		this.addAction(act1);
		this.addAction(act2);
	}
	
}
