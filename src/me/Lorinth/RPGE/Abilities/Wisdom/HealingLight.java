package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class HealingLight extends PassiveSkill{

	public HealingLight() {
		super(ChatColor.BLUE + "Healing Light");
		this.setDescription("Adds 5 to healing power");
		this.setStatRequirement(StatType.WISDOM, 40);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer defense = passives.get(PassiveType.HealingPotency);
		defense += 5;
		passives.put(PassiveType.HealingPotency, defense);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer defense = passives.get(PassiveType.HealingPotency);
		defense -= 5;
		passives.put(PassiveType.HealingPotency, defense);
		
		cha.setPassives(passives);
	}
	
}
