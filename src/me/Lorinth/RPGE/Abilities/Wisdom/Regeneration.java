package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class Regeneration extends ActiveSkill{

	public Regeneration() {
		super(ChatColor.BLUE + "Regeneration");
		this.setDescription("Heals a friendly target within 20 blocks for 5 and adds (WIS / 10)HP to the targets regeneration");
		
		this.setStatRequirement(StatType.WISDOM, 10);
		this.setVitalCost(Vital.MANA, 8);
		this.setCooldown(12);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		params.add(TargetType.SINGLE_FRIENDLY);
		params.add(Vital.HEALTH);
		params.add(new Distance(20));
		params.add((Integer) 5);
		
		Action act1 = new Action(this, ActionType.Heal, params);
		this.addAction(act1);
		
		
		params.clear();
		
		Buff regen = new Buff(PassiveType.HealthRegen, 2, 10, this);
		params.add(regen);
		params.add(TargetType.SINGLE_FRIENDLY);
		params.add(new Distance(20));
		
		Action act2 = new Action(this, ActionType.Buff, params);
		this.addAction(act2);
	}
	
}
