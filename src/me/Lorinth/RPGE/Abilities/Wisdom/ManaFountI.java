package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class ManaFountI extends PassiveSkill{


	public ManaFountI() {
		super(ChatColor.BLUE + "Mana Fount I");
		this.setDescription("Adds 10 to maximum mana pool");
		this.setStatRequirement(StatType.WISDOM, 10);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mana = passives.get(PassiveType.ManaMax);
		mana += 10;
		passives.put(PassiveType.ManaMax, mana);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mana = passives.get(PassiveType.ManaMax);
		mana -= 10;
		passives.put(PassiveType.ManaMax, mana);
		
		cha.setPassives(passives);
	}
		
	
}
