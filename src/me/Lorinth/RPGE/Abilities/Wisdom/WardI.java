package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class WardI extends ActiveSkill{

	public WardI() {
		super(ChatColor.BLUE + "Ward I");
		this.setDescription("Boosts a target allies magic defense (within 20m) by WIS / 3 for 5 minutes.");
		
		this.setStatRequirement(StatType.WISDOM, 35);
		this.setVitalCost(Vital.MANA, 5);
		this.setCooldown(20);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		Buff ward = new Buff(PassiveType.MagicDefense, 2, 300, this);
		ward.setCustomFormula("<wis> / 3");
		
		params.add(ward);
		params.add(TargetType.SINGLE_FRIENDLY);
		params.add(new Distance(20));
		
		Action act1 = new Action(this, ActionType.Buff, params);
		this.addAction(act1);
		
	}
	
}
