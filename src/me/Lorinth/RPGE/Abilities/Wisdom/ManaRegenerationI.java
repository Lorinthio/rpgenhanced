package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class ManaRegenerationI extends PassiveSkill{

	public ManaRegenerationI() {
		super(ChatColor.BLUE + "Mana Regeneration I");
		this.setDescription("Adds 1 to passive mana regeneration");
		this.setStatRequirement(StatType.WISDOM, 20);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mpRegen = passives.get(PassiveType.ManaRegen);
		mpRegen += 1;
		passives.put(PassiveType.ManaRegen, mpRegen);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mpRegen = passives.get(PassiveType.ManaRegen);
		mpRegen -= 1;
		passives.put(PassiveType.ManaRegen, mpRegen);
		
		cha.setPassives(passives);
	}
	
}
