package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class HealIII extends ActiveSkill{

	public HealIII() {
		super(ChatColor.BLUE + "Heal III");
		this.setDescription("Heals a friendly target within 20 blocks for 10 + (3 * magic damage) / 2");
		
		this.setStatRequirement(StatType.WISDOM, 40);
		this.setVitalCost(Vital.MANA, 10);
		this.setCooldown(15);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		params.add(TargetType.SINGLE_FRIENDLY);
		params.add(Vital.HEALTH);
		params.add(new Distance(20));
		
		Action act1 = new Action(this, ActionType.Heal, params);
		act1.setCustomFormula("10 + (3 * <magic>) / 2");
		act1.addVisualEffect(Effect.HEART, 1);
		
		this.addAction(act1);
	}
	
}
