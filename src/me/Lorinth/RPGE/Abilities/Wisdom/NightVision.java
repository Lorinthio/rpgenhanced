package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class NightVision extends ActiveSkill{

	public NightVision() {
		super(ChatColor.BLUE + "Night Vision");
		this.setDescription("Gives your party members night vision, within 20m");
		
		this.setStatRequirement(StatType.WISDOM, 30);
		this.setVitalCost(Vital.MANA, 10);
		this.setCooldown(60);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		Buff nightvis = new Buff(new PotionEffect(PotionEffectType.NIGHT_VISION, 6000, 1));
		
		params.add(nightvis);
		params.add(TargetType.AOE_FRIENDLY);
		params.add(new Distance(20));
		
		Action act1 = new Action(this, ActionType.Buff, params);
		this.addAction(act1);
		
	}
	
}
