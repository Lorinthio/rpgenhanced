package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class HealII extends ActiveSkill{

	public HealII() {
		super(ChatColor.BLUE + "Heal II");
		this.setDescription("Heals a friendly target within 20 blocks for 5 + magic damage");
		
		this.setStatRequirement(StatType.WISDOM, 25);
		this.setVitalCost(Vital.MANA, 7);
		this.setCooldown(12);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		params.add(TargetType.SINGLE_FRIENDLY);
		params.add(Vital.HEALTH);
		params.add(new Distance(20));
		
		Action act1 = new Action(this, ActionType.Heal, params);
		act1.setCustomFormula("7 + <magic>");
		act1.addVisualEffect(Effect.HEART, 1);
		
		this.addAction(act1);
	}
	
}
