package me.Lorinth.RPGE.Abilities.Wisdom;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class MagicDefenseI extends PassiveSkill{

	public MagicDefenseI() {
		super(ChatColor.GOLD + "Magic Defense I");
		this.setDescription("Adds 10 to magic defense");
		this.setStatRequirement(StatType.WISDOM, 40);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer defense = passives.get(PassiveType.Defense);
		defense += 5;
		passives.put(PassiveType.Defense, defense);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer defense = passives.get(PassiveType.Defense);
		defense -= 5;
		passives.put(PassiveType.Defense, defense);
		
		cha.setPassives(passives);
	}
	
}
