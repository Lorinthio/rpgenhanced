package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Sprint extends ActiveSkill{

	public Sprint() {
		super(ChatColor.GREEN + "Sprint");
		this.setDescription("Gives you a 5 second boost of speed");
		
		this.setStatRequirement(StatType.AGILITY, 30);
		this.setVitalCost(Vital.MANA, 10);
		this.setCooldown(60);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		Buff sprint = new Buff(new PotionEffect(PotionEffectType.SPEED, 100, 1));
		
		params.add(sprint);
		params.add(TargetType.SELF);
		params.add(new Distance(2));
		
		Action act1 = new Action(this, ActionType.Buff, params);
		this.addAction(act1);
		
	}
	
}
