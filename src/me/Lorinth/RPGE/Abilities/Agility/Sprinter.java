package me.Lorinth.RPGE.Abilities.Agility;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class Sprinter extends PassiveSkill{

	public Sprinter() {
		super(ChatColor.GREEN + "Sprinter");
		this.setDescription("Increases your movement speed by 15%");
		this.setStatRequirement(StatType.AGILITY, 10);
	}
	
	@Override
	public void apply(Character cha){
		cha.owner.setWalkSpeed((float) (cha.owner.getWalkSpeed() + 0.03));
	}
	
	@Override
	public void remove(Character cha){
		cha.owner.setWalkSpeed((float) (cha.owner.getWalkSpeed() - 0.03));
	}
}
