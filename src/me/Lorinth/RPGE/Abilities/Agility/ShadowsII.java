package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class ShadowsII extends ActiveSkill{

	public ShadowsII() {
		super(ChatColor.GREEN + "Shadows II");
		this.setDescription("Gives you 3 shadows that protect you from harm.");
		
		this.setStatRequirement(StatType.AGILITY, 45);
		this.setVitalCost(Vital.MANA, 7);
		this.setCooldown(120);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		Buff shadow = new Buff(PassiveType.Shadows, 3, 120, this);
		params.add(shadow);
		params.add(TargetType.SELF);
		
		Action act1 = new Action(this, ActionType.Buff, params);
		this.addAction(act1);
		
	}
	
}
