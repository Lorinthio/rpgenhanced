package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class ShadowsI extends ActiveSkill{

	public ShadowsI() {
		super(ChatColor.GREEN + "Shadows I");
		this.setDescription("Gives you 2 shadows that protect you from harm.");
		
		this.setStatRequirement(StatType.AGILITY, 25);
		this.setVitalCost(Vital.MANA, 5);
		this.setCooldown(60);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		Buff shadow = new Buff(PassiveType.Shadows, 2, 60, this);
		params.add(shadow);
		params.add(TargetType.SELF);
		
		Action act1 = new Action(this, ActionType.Buff, params);
		this.addAction(act1);
		
	}
	
}
