package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class DrainingBlade extends ActiveSkill{

	public DrainingBlade(){
		super(ChatColor.GREEN + "Draining Blade");
		this.setDescription("Deals damage to a non-friendly target for normal damage while healing yourself by AGI / 5"
				+ "");
		this.setCooldown(30);
		this.setStatRequirement(StatType.AGILITY, 40);
		this.setVitalCost(Vital.STAMINA, 10);
		
		//Deal damage to health (5 + (STR / 4)) to a hostile target
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(4));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<melee>");
		
		act1.addVisualEffect(Effect.CRIT, 10);
		
		params = new ArrayList<Object>();
		params.add(TargetType.SELF);
		params.add(Vital.HEALTH);
		Action act2 = new Action(this, ActionType.Heal, params);
		act2.setCustomFormula("<agi> / 5");
		
		this.addAction(act1);
		this.addAction(act2);
	}
	
}
