package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class StabII extends ActiveSkill{

	public StabII(){
		super(ChatColor.GREEN + "Stab II");
		this.setDescription("Deals damage to a non-friendly target for normal damage + 2 + (AGI / 5)"
				+ "");
		this.setCooldown(12);
		this.setStatRequirement(StatType.AGILITY, 35);
		this.setVitalCost(Vital.STAMINA, 7);
		
		//Deal damage to health (5 + (STR / 4)) to a hostile target
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(4));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<melee> + 2 + (<agi> / 5)");
		
		act1.addVisualEffect(Effect.CRIT, 10);
		
		this.addAction(act1);
	}
	
}
