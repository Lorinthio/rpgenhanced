package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class Cloak extends ActiveSkill{

	public Cloak(){
		super(ChatColor.GREEN + "Cloak");
		this.setDescription("Conceals yourself from all other players for a short duration. 10 seconds + (Agi / 5)"
				+ "");
		this.setCooldown(60);
		this.setStatRequirement(StatType.AGILITY, 50);
		this.setVitalCost(Vital.STAMINA, 5);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SELF);
		
		Action act1 = new Action(this, ActionType.Cloak, params);
		act1.setCustomFormula("10 + <agi> / 5");
		
		this.addAction(act1);
		
	}
	
}
