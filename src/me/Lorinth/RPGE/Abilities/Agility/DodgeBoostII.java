package me.Lorinth.RPGE.Abilities.Agility;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class DodgeBoostII extends PassiveSkill{

	public DodgeBoostII() {
		super(ChatColor.GREEN + "Dodge Boost II");
		this.setDescription("Adds 4 to passive dodge chance");
		this.setStatRequirement(StatType.AGILITY, 50);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer crit = passives.get(PassiveType.Dodge);
		crit += 4;
		passives.put(PassiveType.Dodge, crit);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer crit = passives.get(PassiveType.Dodge);
		crit -= 4;
		passives.put(PassiveType.Dodge, crit);
		
		cha.setPassives(passives);
	}
		
	
}
