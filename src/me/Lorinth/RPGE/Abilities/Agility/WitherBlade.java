package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class WitherBlade extends ActiveSkill{

	public WitherBlade(){
		super(ChatColor.GREEN + "Wither Blade");
		this.setDescription("Deals damage to a non-friendly target for half damage and withers them"
				+ "");
		this.setCooldown(16);
		this.setStatRequirement(StatType.AGILITY, 15);
		this.setVitalCost(Vital.STAMINA, 8);
		
		//Deal damage to health (5 + (STR / 4)) to a hostile target
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(4));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<melee> / 2");
		
		act1.addVisualEffect(Effect.CRIT, 15);
		
		//this.addAction(act1);
		
		PotionEffect bleedPE = new PotionEffect(PotionEffectType.WITHER, 2, 200);
		Buff bleed = new Buff(bleedPE);
		
		params.clear();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(bleed);
		params.add(new Distance(4));
		Action act2 = new Action(this, ActionType.Buff, params);
		
		this.addAction(act1);
		this.addAction(act2);
	}
	
}
