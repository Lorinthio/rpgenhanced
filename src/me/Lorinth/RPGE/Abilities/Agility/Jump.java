package me.Lorinth.RPGE.Abilities.Agility;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.util.Vector;

public class Jump extends ActiveSkill{

	public Jump(){
		super(ChatColor.GREEN + "Jump");
		this.setDescription("Jump higher than normal");
		this.setCooldown(20);
		this.setStatRequirement(StatType.AGILITY, 10);
		this.setVitalCost(Vital.STAMINA, 3);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		params.add(TargetType.SELF);
		Vector launch = new Vector(0, 1, 0);
		params.add(launch);
		
		Action act2 = new Action(this, ActionType.Launch, params);
		act2.addVisualEffect(Effect.SMOKE, 5);
		
		this.addAction(act2);
	}
	
}
