package me.Lorinth.RPGE.Abilities.Constitution;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class Stomp extends ActiveSkill{

	public Stomp() {
		super(ChatColor.GOLD + "Stomp");
		this.setDescription("Deals damage to non-friendly targets for (defense / 10) within 5 blocks, and generates threat"
				+ "");
		this.setCooldown(30);
		this.setStatRequirement(StatType.CONSTITUTION, 20);
		this.setVitalCost(Vital.STAMINA, 10);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.AOE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(5));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<PDefense> / 10");
		
		act1.addVisualEffect(Effect.CRIT, 3);
		
		params.clear();
		params.add(TargetType.SELF);
		Action act2 = new Action(this, ActionType.VisualEffect, params);
		act2.addVisualEffect(Effect.SMOKE, 1);
		
		params.clear();
		params.add(TargetType.AOE_HOSTILE);
		params.add(new Distance(5));
		Action act3 = new Action(this, ActionType.Taunt, params);
		act3.setCustomFormula("<con> / 5");
		
		this.addAction(act1);
		this.addAction(act2);
		this.addAction(act3);
	}
	
}
