package me.Lorinth.RPGE.Abilities.Constitution;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.Stance;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class Defender extends ActiveSkill{

	public Defender() {
		super(ChatColor.GOLD + "Defender");
		this.setDescription("Boosts your defense to 120% and lowers your attack to 80%.");
		
		this.setStatRequirement(StatType.CONSTITUTION, 25);
		this.setCooldown(1);
		
		//Buff Attack 20%
		ArrayList<Object> params = new ArrayList<Object>();
		
		Buff zerkAtt = new Buff(PassiveType.Defense, 2, 30, this);
		zerkAtt.setCustomFormula("<PDefense> / 5");
		
		Stance s = new Stance("Defender", getMain().sm);
		s.addType(PassiveType.Attack, "- (<Attack> / 5)");
		s.addType(PassiveType.Defense, "(<PDefender> / 5)");
		
		params.add(s);
		
		Action a1 = new Action(this, ActionType.Stance, params);
		
	}
	
}
