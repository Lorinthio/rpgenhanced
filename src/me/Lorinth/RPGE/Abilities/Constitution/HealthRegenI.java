package me.Lorinth.RPGE.Abilities.Constitution;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class HealthRegenI extends PassiveSkill{

	public HealthRegenI() {
		super(ChatColor.GOLD + "Health Regeneration I");
		this.setDescription("Adds 1 to passive health regeneration");
		this.setStatRequirement(StatType.CONSTITUTION, 20);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer hpRegen = passives.get(PassiveType.HealthRegen);
		hpRegen += 1;
		passives.put(PassiveType.HealthRegen, hpRegen);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer hpRegen = passives.get(PassiveType.HealthRegen);
		hpRegen -= 1;
		passives.put(PassiveType.HealthRegen, hpRegen);
		
		cha.setPassives(passives);
	}

}
