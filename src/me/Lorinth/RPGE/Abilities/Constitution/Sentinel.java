package me.Lorinth.RPGE.Abilities.Constitution;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class Sentinel extends ActiveSkill{

	public Sentinel() {
		super(ChatColor.GOLD + "Sentinel");
		this.setDescription("Boosts your allies defense (within 20m) by a fraction of your own for 15 seconds.");
		
		this.setStatRequirement(StatType.CONSTITUTION, 15);
		this.setVitalCost(Vital.MANA, 10);
		this.setCooldown(60);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		Buff sentinel = new Buff(PassiveType.Defense, 2, 10, this);
		sentinel.setCustomFormula("<PDefense> / 5");
		
		params.add(sentinel);
		params.add(TargetType.AOE_FRIENDLY);
		params.add(new Distance(20));
		
		Action act1 = new Action(this, ActionType.Buff, params);
		this.addAction(act1);
		
	}
	
}
