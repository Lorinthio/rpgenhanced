package me.Lorinth.RPGE.Abilities.Constitution;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class Taunt extends ActiveSkill{

	public Taunt() {
		super(ChatColor.GOLD + "Taunt");
		this.setDescription("Taunts a single target.");
		this.setCooldown(5);
		this.setStatRequirement(StatType.CONSTITUTION, 5);
		this.setVitalCost(Vital.STAMINA, 3);
		
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(new Distance(20));
		params.add((Integer) 100);
		Action act1 = new Action(this, ActionType.Taunt, params);
		
		this.addAction(act1);
	}

	
	
}
