package me.Lorinth.RPGE.Abilities.Constitution;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class DefenseBoostI extends PassiveSkill{

	public DefenseBoostI() {
		super(ChatColor.GOLD + "Defense Boost I");
		this.setDescription("Adds 5 to defense");
		this.setStatRequirement(StatType.CONSTITUTION, 10);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer defense = passives.get(PassiveType.Defense);
		defense += 5;
		passives.put(PassiveType.Defense, defense);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer defense = passives.get(PassiveType.Defense);
		defense -= 5;
		passives.put(PassiveType.Defense, defense);
		
		cha.setPassives(passives);
	}
	
}
