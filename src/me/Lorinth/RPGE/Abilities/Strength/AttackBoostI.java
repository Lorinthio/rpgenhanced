package me.Lorinth.RPGE.Abilities.Strength;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class AttackBoostI extends PassiveSkill{

	public AttackBoostI() {
		super(ChatColor.RED + "Attack Boost I");
		this.setDescription("Adds 5 to Attack Modifier");
		this.setStatRequirement(StatType.STRENGTH, 10);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer Attack = passives.get(PassiveType.Attack);
		Attack += 5;
		passives.put(PassiveType.Attack, Attack);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer Attack = passives.get(PassiveType.Attack);
		Attack -= 5;
		passives.put(PassiveType.Attack, Attack);
	}
	
}