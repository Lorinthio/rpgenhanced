package me.Lorinth.RPGE.Abilities.Strength;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.Stance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;

public class Berserker extends ActiveSkill{

	public Berserker() {
		super(ChatColor.RED + "Berserker");
		this.setDescription("Boosts your attack to 120% and lowers your defense to 80%.");
		
		this.setStatRequirement(StatType.STRENGTH, 20);
		this.setVitalCost(Vital.STAMINA, 1);
		this.setCooldown(1);
		
		ArrayList<Object> params = new ArrayList<Object>();
		
		Stance s = new Stance("Defender", getMain().sm);
		s.addType(PassiveType.Attack, "(<Attack> / 5)");
		s.addType(PassiveType.Defense, "- (<PDefender> / 5)");
		
		params.add(s);
		
		Action a1 = new Action(this, ActionType.Stance, params);
		
	}
	
}
