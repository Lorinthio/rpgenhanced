package me.Lorinth.RPGE.Abilities.Strength;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class Cleave extends ActiveSkill{
	
	public Cleave() {
		super(ChatColor.RED + "Cleave");
		this.setDescription("Deals damage to a group of enemies in front of you for your basic attack");
		this.setCooldown(16);
		this.setStatRequirement(StatType.STRENGTH, 15);
		this.setVitalCost(Vital.STAMINA, 12);
		
		//Deal damage to health (5 + (STR / 4)) to a hostile target
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.TARGET_AOE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(3, 3));
		
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<melee>");
		act1.addVisualEffect(Effect.CRIT, 10);
		
		this.addAction(act1);
	}
}
