package me.Lorinth.RPGE.Abilities.Strength;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class Whirlwind extends ActiveSkill{

	public Whirlwind() {
		super(ChatColor.RED + "Whirlwind");
		this.setDescription("Deals damage to non-friendly targets for normal damage within 5 blocks"
				+ "");
		this.setCooldown(24);
		this.setStatRequirement(StatType.STRENGTH, 25);
		this.setVitalCost(Vital.STAMINA, 12);
		
		//Deal damage to health (5 + (STR / 4)) to a hostile target
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.AOE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(5));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<melee>");
		
		act1.addVisualEffect(Effect.CRIT, 15);
		
		params.clear();
		params.add(TargetType.SELF);
		Action act2 = new Action(this, ActionType.VisualEffect, params);
		act2.addVisualEffect(Effect.SMOKE, 1);
		
		this.addAction(act1);
		this.addAction(act2);
	}
	
}
