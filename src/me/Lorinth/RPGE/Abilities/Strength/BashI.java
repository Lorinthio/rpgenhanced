package me.Lorinth.RPGE.Abilities.Strength;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;

public class BashI extends ActiveSkill{

	public BashI() {
		super(ChatColor.RED + "Bash I");
		this.setDescription("Deals damage to a non-friendly target for normal damage + 3 + (STR / 10)"
				+ "");
		this.setCooldown(12);
		this.setStatRequirement(StatType.STRENGTH, 5);
		this.setVitalCost(Vital.STAMINA, 5);
		
		//Deal damage to health (5 + (STR / 4)) to a hostile target
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(4));
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<melee> + 3 + (<str> / 10)");
		
		act1.addVisualEffect(Effect.CRIT, 10);
		
		this.addAction(act1);
	}

	
	
}
