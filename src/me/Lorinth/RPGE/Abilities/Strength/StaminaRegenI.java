package me.Lorinth.RPGE.Abilities.Strength;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class StaminaRegenI extends PassiveSkill{

	public StaminaRegenI() {
		super(ChatColor.RED + "Stamina Regeneration I");
		this.setDescription("Adds 1 to passive stamina regeneration");
		this.setStatRequirement(StatType.STRENGTH, 20);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer spRegen = passives.get(PassiveType.StaminaRegen);
		spRegen += 1;
		passives.put(PassiveType.StaminaRegen, spRegen);
		
		cha.setPassives(passives);
	}

}
