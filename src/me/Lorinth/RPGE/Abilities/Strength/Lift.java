package me.Lorinth.RPGE.Abilities.Strength;

import java.util.ArrayList;

import me.Lorinth.RPGE.SkillAPI.Action;
import me.Lorinth.RPGE.SkillAPI.ActionType;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.Distance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import me.Lorinth.RPGE.SkillAPI.TargetType;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.util.Vector;

public class Lift extends ActiveSkill{

	public Lift() {
		super(ChatColor.RED + "Lift");
		this.setDescription("Deals damage to a non-friendly target for normal damage and tosses them");
		this.setCooldown(12);
		this.setStatRequirement(StatType.STRENGTH, 10);
		this.setVitalCost(Vital.STAMINA, 5);
		
		//Deal damage to health (5 + (STR / 4)) to a hostile target
		ArrayList<Object> params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(Vital.HEALTH);
		params.add(new Distance(4));
		
		Action act1 = new Action(this, ActionType.Damage, params);
		act1.setCustomFormula("<melee>");
		act1.addVisualEffect(Effect.CRIT, 20);
		
		
		params = new ArrayList<Object>();
		params.add(TargetType.SINGLE_HOSTILE);
		params.add(new Distance(4));
		Vector launch = new Vector(0, 1.4, 0);
		params.add(launch);
		
		Action act2 = new Action(this, ActionType.Launch, params);
		
		
		this.addAction(act1);
		this.addAction(act2);
	}

	
	
}
