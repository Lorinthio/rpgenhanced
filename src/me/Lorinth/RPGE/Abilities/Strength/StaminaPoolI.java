package me.Lorinth.RPGE.Abilities.Strength;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.ChatColor;

public class StaminaPoolI extends PassiveSkill{

	public StaminaPoolI() {
		super(ChatColor.RED + "Stamina Pool I");
		this.setDescription("Adds 10 to maximum stamina pool");
		this.setStatRequirement(StatType.STRENGTH, 30);
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mana = passives.get(PassiveType.StaminaMax);
		mana += 10;
		passives.put(PassiveType.StaminaMax, mana);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer mana = passives.get(PassiveType.StaminaMax);
		mana -= 10;
		passives.put(PassiveType.StaminaMax, mana);
		
		cha.setPassives(passives);
	}
	
}
