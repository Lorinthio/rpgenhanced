package me.Lorinth.RPGE.Abilities.RacePassives;

import java.util.HashMap;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;
import me.Lorinth.RPGE.SkillAPI.PassiveType;

import org.bukkit.ChatColor;

public class HighElfPassive extends PassiveSkill{

	public HighElfPassive() {
		super(ChatColor.GRAY + "High Elf Knowledge");
		this.setDescription("Increases your mana regen by 1, and increases cooldown reduction by 10%.");
	}
	
	@Override
	public void apply(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer reduct = passives.get(PassiveType.CooldownReduction);
		reduct += 10;
		passives.put(PassiveType.CooldownReduction, reduct);
		
		cha.setPassives(passives);
	}
	
	@Override
	public void remove(Character cha){
		HashMap<PassiveType, Integer> passives = cha.getPassives();
		Integer reduct = passives.get(PassiveType.CooldownReduction);
		reduct -= 10;
		passives.put(PassiveType.CooldownReduction, reduct);
		
		cha.setPassives(passives);
	}
	
}
