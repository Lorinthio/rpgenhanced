package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class BloodOfTheNorth extends PassiveSkill{

	public BloodOfTheNorth() {
		super(ChatColor.GRAY + "Blood of the North");
		this.setDescription("Gain +3 DMG and +4 HP in cold regions, -1 DMG and -1hp in hot regions");
	}
	
}
