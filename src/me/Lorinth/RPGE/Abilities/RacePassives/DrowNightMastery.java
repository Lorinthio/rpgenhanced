package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class DrowNightMastery extends PassiveSkill{

	public DrowNightMastery() {
		super(ChatColor.GRAY + "Night Assassin");
		this.setDescription("Your damage(+2) and movement speed(17%) is increased at night.");
	}
	
}
