package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class HalfOrcMovementSpeed extends PassiveSkill{

	public HalfOrcMovementSpeed() {
		super(ChatColor.GRAY + "Clumsy");
		this.setDescription("Decreases your movement speed by 15%");
	}
	
	@Override
	public void apply(Character cha){
		cha.owner.setWalkSpeed((float) (cha.owner.getWalkSpeed() - 0.030));
	}
	
	@Override
	public void remove(Character cha){
		cha.owner.setWalkSpeed((float) (cha.owner.getWalkSpeed() + 0.030));
	}
	
}
