package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class PyriteMoltenSkin extends PassiveSkill{

	public PyriteMoltenSkin() {
		super(ChatColor.GRAY + "Molten Skin");
		this.setDescription("You don't burn or take damage in lava");
	}
	
}
