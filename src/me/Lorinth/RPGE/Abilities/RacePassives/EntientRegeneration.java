package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class EntientRegeneration extends PassiveSkill{

	public EntientRegeneration() {
		super(ChatColor.GRAY + "Entient Regeneration");
		this.setDescription("You gain extra regeneration while its raining.");
	}
	
}
