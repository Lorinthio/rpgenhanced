package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

public class Halflingpassive extends PassiveSkill{

	public Halflingpassive(){
		super("Sneaky Halflings");
		this.setDescription("Allows you to sneak at normal walking speed");
	}
	
}
