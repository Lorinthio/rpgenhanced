package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class DwarfMining extends PassiveSkill{

	public DwarfMining(){
		super(ChatColor.GRAY + "Dwarven Mining");
		
		this.setDescription("You gain haste when mining stone based blocks");
	}
	
}
