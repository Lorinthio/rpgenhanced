package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class AquosUnderwater extends PassiveSkill{

	public AquosUnderwater() {
		super(ChatColor.GRAY + "Underwater Mastery");
		this.setDescription("You can breath underwater");
	}
	
}
