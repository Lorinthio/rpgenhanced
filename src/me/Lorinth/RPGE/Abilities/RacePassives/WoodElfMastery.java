package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class WoodElfMastery extends PassiveSkill{

	public WoodElfMastery() {
		super(ChatColor.GRAY + "Elven Archery");
		this.setDescription("Increases your range, and speed with bows");
	}
	
}
