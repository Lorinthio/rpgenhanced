package me.Lorinth.RPGE.Abilities.RacePassives;

import me.Lorinth.RPGE.SkillAPI.PassiveSkill;

import org.bukkit.ChatColor;

public class AvianFlight extends PassiveSkill {

	public AvianFlight() {
		super(ChatColor.GRAY + "Avian Flight");
		this.setDescription("You can double tap jump to flap your wings! (Max 5)");
	}
	
}
