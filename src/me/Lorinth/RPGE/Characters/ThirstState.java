package me.Lorinth.RPGE.Characters;

public enum ThirstState {
	Sweating, DryMouth, LightHeaded, Dizzy, Dying, Hydrated;

}
