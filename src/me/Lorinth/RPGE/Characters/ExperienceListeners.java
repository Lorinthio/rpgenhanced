package me.Lorinth.RPGE.Characters;

import java.util.HashMap;
import java.util.Random;

import me.Lorinth.RPGE.Main.RpgEnhancedMain;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ExperienceListeners implements Listener {

	private RpgEnhancedMain mainPlugin;
	
	public HashMap<Player, SkillsProfile> playerSkills = new HashMap<Player, SkillsProfile>();
	
	public HashMap<Material, Integer> buildBlocks = new HashMap<Material, Integer>();
	public HashMap<Material, Integer> diggingBlocks = new HashMap<Material, Integer>();
	public HashMap<Material, Integer> miningBlocks = new HashMap<Material, Integer>();
	
	
	public Random random = new Random();
	
	public ExperienceListeners(RpgEnhancedMain Plugin){
		mainPlugin = Plugin;
		setupBlocks();
		
		loadAll();
	}
	
	@SuppressWarnings("deprecation")
	public void loadAll(){
		for(Player player : Bukkit.getOnlinePlayers()){
			SkillsProfile profile = new SkillsProfile(player, mainPlugin);
			playerSkills.put(player, profile);
		}
	}
	
	public void saveAll(){
		for(Player player : Bukkit.getOnlinePlayers()){
			this.getPlayerProfile(player).saveCharacter();
		}
		
		for(SkillsProfile profile : playerSkills.values()){
			profile.saveCharacter();
		}
	}
	
	@EventHandler
	public void onPlayerLogin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		SkillsProfile profile = new SkillsProfile(player, mainPlugin);
		playerSkills.put(player, profile);
	}
	
	@EventHandler
	public void onPlayerExit(PlayerQuitEvent event){
		try{
			Player player = event.getPlayer();
			playerSkills.get(player).saveCharacter();
			playerSkills.remove(player);
		}
		catch(NullPointerException e){
			
		}
	}
	
	@EventHandler
	public void onPlayerQuit(PlayerKickEvent event){
		try{
			Player player = event.getPlayer();
			playerSkills.get(player).saveCharacter();
			playerSkills.remove(player);
		}
		catch(NullPointerException e){
			
		}
	}
	
	public void setupBlocks(){
		setupBreakBlocks();
		setupBuildBlocks();
	}
	
	public void setupBreakBlocks(){
		diggingBlocks.put(Material.DIRT, 1);
		diggingBlocks.put(Material.SAND, 1);
		diggingBlocks.put(Material.GRAVEL, 1);
		diggingBlocks.put(Material.CLAY, 2);
		diggingBlocks.put(Material.GRASS, 1);
		
		miningBlocks.put(Material.STONE, 1);
		miningBlocks.put(Material.DIAMOND_ORE, 4);
		miningBlocks.put(Material.GOLD_ORE, 3);
		miningBlocks.put(Material.IRON_ORE, 2);
		miningBlocks.put(Material.COAL_ORE, 1);
	}
	
	public void setupBuildBlocks(){
		buildBlocks.put(Material.ACACIA_STAIRS, 2);     //duplicate
		buildBlocks.put(Material.BIRCH_WOOD_STAIRS, 2); //duplicate
		buildBlocks.put(Material.BOOKSHELF, 3);         //duplicate
		buildBlocks.put(Material.BRICK, 2);
		buildBlocks.put(Material.BRICK_STAIRS, 2);
		buildBlocks.put(Material.COBBLE_WALL, 1);
		buildBlocks.put(Material.COBBLESTONE, 1);
		buildBlocks.put(Material.DARK_OAK_STAIRS, 2);
		buildBlocks.put(Material.DIAMOND_BLOCK, 2);
		buildBlocks.put(Material.EMERALD_BLOCK, 2);
		buildBlocks.put(Material.FENCE, 1);
		buildBlocks.put(Material.FENCE_GATE, 1);
		buildBlocks.put(Material.GLASS, 1);
		buildBlocks.put(Material.GLOWSTONE, 1);
		buildBlocks.put(Material.IRON_FENCE, 1);
		buildBlocks.put(Material.JUNGLE_WOOD_STAIRS, 2);
		buildBlocks.put(Material.LADDER, 1);
		buildBlocks.put(Material.LOG, 1);
		buildBlocks.put(Material.MOSSY_COBBLESTONE, 1);
		buildBlocks.put(Material.NETHER_BRICK, 1);
		buildBlocks.put(Material.NETHER_BRICK_STAIRS, 1);
		buildBlocks.put(Material.NETHER_FENCE, 1);
		buildBlocks.put(Material.OBSIDIAN, 2);
		buildBlocks.put(Material.QUARTZ_BLOCK, 2);
		buildBlocks.put(Material.QUARTZ_STAIRS, 3);
		buildBlocks.put(Material.SANDSTONE, 1);
		buildBlocks.put(Material.SANDSTONE_STAIRS, 1);
		buildBlocks.put(Material.SMOOTH_BRICK, 2);
		buildBlocks.put(Material.SPRUCE_WOOD_STAIRS, 1);
		buildBlocks.put(Material.STONE, 1);
		buildBlocks.put(Material.THIN_GLASS, 1);
		buildBlocks.put(Material.WOOD, 2);
		buildBlocks.put(Material.WOOD_DOOR, 1);
		buildBlocks.put(Material.WOOD_STEP, 1);
	}

	public SkillsProfile getPlayerProfile(Player player){
		return playerSkills.get(player);
	}
	
	//AGILITY
	@EventHandler
	public void onPlayerFall(EntityDamageEvent event){
		if(event.isCancelled()){
			return;
		}
		if(event.getCause().equals(DamageCause.FALL)){
			Entity entity = event.getEntity();
			if(entity instanceof Player){
				SkillsProfile profile = playerSkills.get(entity);
				profile.addExp(RPGSkill.AGILITY, (int) event.getDamage());
				
				Integer reduction = profile.Agility / 10;
				event.setDamage(Math.max(event.getDamage() - reduction, 0));
			}
			
			
		}
	}
	
	//BUILDING
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event){
		Material block = event.getBlock().getType();
		Player player = event.getPlayer();
		if(player.getGameMode() == GameMode.CREATIVE){
			return;
		}
		SkillsProfile profile = playerSkills.get(player);
		if(buildBlocks.containsKey(block)){
			
			profile.addExp(RPGSkill.BUILDING, buildBlocks.get(block));
			//Chances to stop block place cost
			switch(block){
			
			//Ignored blocks go here, otherwise theres a chance to duplicate
			case DIAMOND_BLOCK: 
				break;
			case QUARTZ_BLOCK:
				break;
			case QUARTZ_STAIRS:
				break;
			default:
				profile.checkDuplicateBlock(block, random.nextInt(1000 + 1));
				break;
			}
		}
		
	}
	
	//DIGGING + MINING
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event){
		Material block = event.getBlock().getType();
		Player player = event.getPlayer();
		SkillsProfile profile = playerSkills.get(player);
		if(player.getItemInHand().getType().name().toLowerCase().contains("pick")){
			if(miningBlocks.containsKey(block)){
				Integer blockValue = miningBlocks.get(block);
				profile.addExp(RPGSkill.MINING, blockValue);
				profile.checkMiningBlock(block, random.nextInt(1000 + 1));
			}
		}
		else if(player.getItemInHand().getType().name().toLowerCase().contains("spade")){
			if(diggingBlocks.containsKey(block)){
				Integer value = diggingBlocks.get(block);
				profile.addExp(RPGSkill.DIGGING, value);
			}
		}
		else if(player.getItemInHand().getType().name().toLowerCase().contains("axe")){
			if(block.equals(Material.LOG)){
				profile.addExp(RPGSkill.WOODCUTTING, 1);
				//profile.checkTreeFeller();
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntityAttack(EntityDamageByEntityEvent event){
		if(!(event.getEntity() instanceof LivingEntity)){
			return;
		}
		LivingEntity entity = (LivingEntity) event.getEntity();
		if(entity.getNoDamageTicks() > (entity.getMaximumNoDamageTicks() / 2.0)){
			return;
		}
		Entity attacker = event.getDamager();
		if(attacker instanceof Player){
			if(((Player) attacker).getGameMode() == GameMode.CREATIVE){
				return;
			}
			if(entity instanceof Monster){
				SkillsProfile profile = this.playerSkills.get(attacker);
				
				Material item = ((Player) attacker).getItemInHand().getType();
				Integer level = mainPlugin.getLevel(entity);
				if(item.name().toLowerCase().contains("sword")){
					profile.addExp(RPGSkill.FENCING, level);
				}
				else if(item.equals(Material.AIR)){
					profile.addExp(RPGSkill.UNARMED, level);
				}
				else if(item.name().toLowerCase().contains("axe")){
					profile.addExp(RPGSkill.MARAUDING, level);
				}
				else if(item.name().toLowerCase().contains("spade")){
					profile.addExp(RPGSkill.BLUDGEONING, level);
				}
			}
		}
		if(attacker instanceof Arrow){
			if(((Arrow) attacker).getShooter() instanceof Player){
				Player player = (Player) ((Projectile) attacker).getShooter();
				SkillsProfile profile = this.playerSkills.get(player);
				Integer level = mainPlugin.getLevel(entity);
				profile.addExp(RPGSkill.MARKSMANSHIP, level);
			}
		}
		else if(attacker instanceof Projectile){
			Projectile pro = (Projectile) attacker;
			if(pro.hasMetadata("Skill")){
				if(pro.getMetadata("Skill").get(0).asString() == "Magic Missle"){
					Player player = Bukkit.getPlayer(pro.getMetadata("Player").get(0).asString());
					SkillsProfile profile = this.playerSkills.get(player);
					Integer level = mainPlugin.getLevel(entity);
					profile.addExp(RPGSkill.STAVES, level);
				}
			}
		}
	}
	
}
