package me.Lorinth.RPGE.Characters;

import java.io.File;
import java.io.IOException;

import me.Lorinth.RPGE.Main.RpgEnhancedMain;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SkillsProfile {

	private Player owner;
	
	public Integer SkillCap = 200;
	
	//Weapons
	public Integer Fencing = 1; //Sword Use        //Special Passive : Focused Strikes = Critical increase
	public Integer FencingExp = 0;
	public Integer FencingNext = 1000;
	
	public Integer Marauding = 1; //Axe Use        //Special Passive : Heavy Blows = Armor Penetration
	public Integer MaraudingExp = 0;
	public Integer MaraudingNext = 1000;
	
	public Integer Bludgeoning = 1;  //Shovel Use  //Special Passive : Blocking = Block chance
	public Integer BludgeoningExp = 0;
	public Integer BludgeoningNext = 1000;
	
	public Integer Marksmanship = 1; //Bow Use
	public Integer MarksmanshipExp = 0;
	public Integer MarksmanshipNext = 1000;
	
	public Integer Unarmed = 1; //Fists
	public Integer UnarmedExp = 0;
	public Integer UnarmedNext = 1000;
	
	public Integer Staves = 1;
	public Integer StavesExp = 0;
	public Integer StavesNext = 1000;
	
	
	//Terrain Changing
	public Integer Digging = 1;       //Special 
	public Integer DiggingExp = 0;
	public Integer DiggingNext = 1000;
	
	public Integer Mining = 1;        //Special Active : Super Breaker = Instant breaks blocks with silk touch;
	public Integer MiningExp = 0;
	public Integer MiningNext = 1000;
	
	public Integer Woodcutting = 1;   //Special Active : Tree Vaporizor = Tree destruction;
	public Integer WoodcuttingExp = 0;
	public Integer WoodcuttingNext = 1000;
	public Integer lastFeller = 0;
	
	public Integer Building = 1;      //Special Passive : Block Preservation = Chance to not use a block on placement
	public Integer BuildingExp = 0;
	public Integer BuildingNext = 1000;
	
	//Passive Defense
	public Integer Agility = 1;
	public Integer AgilityExp = 0;
	public Integer AgilityNext = 1000;
	public boolean chatty = false;
	
	public RpgEnhancedMain mainPlugin;
	
	public SkillsProfile(Player player, RpgEnhancedMain plugin){
		owner = player;
		mainPlugin = plugin;
		SkillCap = mainPlugin.levelCap + 10;
		loadCharacter(player);
	}

	public void loadCharacter(Player player){
		owner = player;
		File file = new File(mainPlugin.getDataFolder() + File.separator + "Players", player.getUniqueId().toString() + ".yml");
		if(file.exists()){
			YamlConfiguration fc = YamlConfiguration.loadConfiguration(file);
			
			Agility = fc.getInt("Agility");
			AgilityExp = fc.getInt("AgilityExp");
			AgilityNext = fc.getInt("AgilityNext");
			
			Bludgeoning = fc.getInt("Bludgeoning");
			BludgeoningExp = fc.getInt("BludgeoningExp");
			BludgeoningNext = fc.getInt("BludgeoningNext");
			
			Building = fc.getInt("Building");
			BuildingExp = fc.getInt("BuildingExp");
			BuildingNext = fc.getInt("BuildingNext");
			
			Digging = fc.getInt("Digging");
			DiggingExp = fc.getInt("DiggingExp");
			DiggingNext = fc.getInt("DiggingNext");
			
			Fencing = fc.getInt("Fencing");
			FencingExp = fc.getInt("FencingExp");
			FencingNext = fc.getInt("FencingNext");
			
			Marauding = fc.getInt("Marauding");
			MaraudingExp = fc.getInt("MaraudingExp");
			MaraudingNext = fc.getInt("MaraudingNext");
			
			Marksmanship = fc.getInt("Marksmanship");
			MarksmanshipExp = fc.getInt("MarksmanshipExp");
			MarksmanshipNext = fc.getInt("MarksmanshipNext");
			
			Mining = fc.getInt("Mining");
			MiningExp = fc.getInt("MiningExp");
			MiningNext = fc.getInt("MiningNext");
			
			Staves = fc.getInt("Staves");
			StavesExp = fc.getInt("StavesExp");
			StavesNext = fc.getInt("StavesNext");
			
			Unarmed = fc.getInt("Unarmed");
			UnarmedExp = fc.getInt("UnarmedExp");
			UnarmedNext = fc.getInt("UnarmedNext");
			
			Woodcutting = fc.getInt("Woodcutting");
			WoodcuttingExp = fc.getInt("WoodcuttingExp");
			WoodcuttingNext = fc.getInt("WoodcuttingNext");
		}
		else{
			saveCharacter();
			mainPlugin.console.sendMessage(ChatColor.GOLD + "[RpgEnhanced]: Creating new skills profile for " + player.getDisplayName());
		}
	}
	
	public int calculateExpForNextLevel(Integer level){
		Integer next_level = (int) ((int) (500 * Math.log(level)) + (5 *(level-1) * (level-10)) / 10 + 1000);
		
		
		return (next_level);
	}
	
	public void addExp(RPGSkill skill, Integer value){
		if(owner.getGameMode() == GameMode.CREATIVE){
			return;
		}
		Integer exp = 0;
		Integer level = 0;
		double percentage = 0;
		switch(skill){
			case AGILITY: // DONE
				exp = (value * 20) + (Agility / 2);
				AgilityExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Tumble]: "+ ChatColor.GOLD + AgilityExp + ChatColor.AQUA + " / " + ChatColor.GOLD + AgilityNext);
				}
				if(AgilityExp >= AgilityNext && Agility <= SkillCap){
					if(Agility == SkillCap){
						AgilityExp = AgilityNext;
					}
					else{
						AgilityExp -= AgilityNext;
						Agility += 1;
						AgilityNext = calculateExpForNextLevel(Agility);
						owner.sendMessage(ChatColor.AQUA + "[Tumble]: Has increased to " + ChatColor.GOLD + Agility);
					}
				}
				break;
			case BLUDGEONING:
				level = value;
				percentage = (float) (6 - (Bludgeoning - level)) / 6.0;
				exp = (int) (percentage * 50);
				if(exp < 10){
					exp = 10;
				}
				else if(exp > 100){
					exp = 100;
				}
				exp += Bludgeoning / 3;
				BludgeoningExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Bludgeoning]: "+ ChatColor.GOLD + BludgeoningExp + ChatColor.AQUA + " / " + ChatColor.GOLD + BludgeoningNext);
				}
				if(BludgeoningExp >= BludgeoningNext && Bludgeoning <= SkillCap){
					if(Bludgeoning == SkillCap){
						BludgeoningExp = BludgeoningNext;
					}
					else{
						BludgeoningExp -= BludgeoningNext;
						Bludgeoning += 1;
						BludgeoningNext = calculateExpForNextLevel(Bludgeoning);
						owner.sendMessage(ChatColor.AQUA + "[Bludgeoning]: Has increased to " + ChatColor.GOLD + Bludgeoning);
					}
				}
				break;
			case BUILDING: // DONE
				exp = (value * 10) + (Building / 4);
				BuildingExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Building]: "+ ChatColor.GOLD + BuildingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + BuildingNext);
				}
				if(BuildingExp >= BuildingNext && Building <= SkillCap){
					if(Building == SkillCap){
						BuildingExp = BuildingNext;
					}
					else{
						BuildingExp -= BuildingNext;
						Building += 1;
						BuildingNext = calculateExpForNextLevel(Building);
						owner.sendMessage(ChatColor.AQUA + "[Building]: Has increased to " + ChatColor.GOLD + Building);
					}
				}
				break;
			case DIGGING:
				exp = (value * 10) + (Digging / 4);
				DiggingExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Digging]: "+ ChatColor.GOLD + DiggingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + DiggingNext);
				}
				if(DiggingExp >= DiggingNext && Digging <= SkillCap){
					if(Digging == SkillCap){
						DiggingExp = DiggingNext;
					}
					else{
						DiggingExp -= DiggingNext;
						Digging += 1;
						DiggingNext = calculateExpForNextLevel(Digging);
						owner.sendMessage(ChatColor.AQUA + "[Digging]: Has increased to " + ChatColor.GOLD + Digging);
					}
				}
				break;
			case FENCING:
				level = value;
				percentage = (float) (6 - (this.Fencing - level)) / 6.0;
				exp = (int) (percentage * 50.0);
				if(exp < 10){
					exp = 10;
				}
				else if(exp > 100){
					exp = 100;
				}
				exp += Fencing / 3;
				FencingExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Fencing]: "+ ChatColor.GOLD + FencingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + FencingNext);
				}
				if(FencingExp >= FencingNext && Fencing <= SkillCap){
					if(Fencing == SkillCap){
						FencingExp = FencingNext;
					}
					else{
						FencingExp -= FencingNext;
						Fencing += 1;
						FencingNext = calculateExpForNextLevel(Fencing);
						owner.sendMessage(ChatColor.AQUA + "[Fencing]: Has increased to " + ChatColor.GOLD + Fencing);
					}
				}
				break;
			case MARAUDING:
				level = value;
				percentage = (float) (6 - (Marauding - level)) / 6.0;
				exp = (int) (percentage * 50);
				if(exp < 10){
					exp = 10;
				}
				else if(exp > 100){
					exp = 100;
				}
				exp += Marauding / 3;
				MaraudingExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Marauding]: "+ ChatColor.GOLD + MaraudingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + MaraudingNext);
				}
				if(MaraudingExp >= MaraudingNext && Marauding <= SkillCap){
					if(Marauding == SkillCap){
						MaraudingExp = MaraudingNext;
					}
					else{
						MaraudingExp -= MaraudingNext;
						Marauding += 1;
						MaraudingNext = calculateExpForNextLevel(Marauding);
						owner.sendMessage(ChatColor.AQUA + "[Marauding]: Has increased to " + ChatColor.GOLD + Marauding);
					}
				}
				break;
			case MARKSMANSHIP:
				level = value;
				percentage = (float) (6 - (Marksmanship - level)) / 6.0;
				exp = (int) (percentage * 50);
				if(exp < 10){
					exp = 10;
				}
				else if(exp > 100){
					exp = 100;
				}
				exp += Marksmanship / 3;
				MarksmanshipExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Marksmanship]: "+ ChatColor.GOLD + MarksmanshipExp + ChatColor.AQUA + " / " + ChatColor.GOLD + MarksmanshipNext);
				}
				if(MarksmanshipExp >= MarksmanshipNext && Marksmanship <= SkillCap){
					if(Marksmanship == SkillCap){
						MarksmanshipExp = MarksmanshipNext;
					}
					else{
						MarksmanshipExp -= MarksmanshipNext;
						Marksmanship += 1;
						MarksmanshipNext = calculateExpForNextLevel(Marksmanship);
						owner.sendMessage(ChatColor.AQUA + "[Marksmanship]: Has increased to " + ChatColor.GOLD + Marksmanship);
					}
				}
				break;
			case MINING:
				exp = (value * 10) + (Mining / 4);
				MiningExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Mining]: "+ ChatColor.GOLD + MiningExp + ChatColor.AQUA + " / " + ChatColor.GOLD + MiningNext);
				}
				if(MiningExp >= MiningNext && Mining <= SkillCap){
					if(Mining == SkillCap){
						MiningExp = MiningNext;
					}
					else{
						MiningExp -= MiningNext;
						Mining += 1;
						MiningNext = calculateExpForNextLevel(Mining);
						owner.sendMessage(ChatColor.AQUA + "[Mining]: Has increased to " + ChatColor.GOLD + Mining);
					}
				}
				break;
			case STAVES:
				level = value;
				percentage = (float) (6 - (this.Staves - level)) / 6.0;
				exp = (int) (percentage * 50.0);
				if(exp < 10){
					exp = 10;
				}
				else if(exp > 100){
					exp = 100;
				}
				exp += Staves / 3;
				StavesExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Staves]: "+ ChatColor.GOLD + StavesExp + ChatColor.AQUA + " / " + ChatColor.GOLD + StavesNext);
				}
				if(StavesExp >= StavesNext && Staves <= SkillCap){
					if(Staves == SkillCap){
						StavesExp = StavesNext;
					}
					else{
						StavesExp -= StavesNext;
						Staves += 1;
						StavesNext = calculateExpForNextLevel(Staves);
						owner.sendMessage(ChatColor.AQUA + "[Staves]: Has increased to " + ChatColor.GOLD + Staves);
					}
				}
				break;
			case UNARMED:
				level = value;
				percentage = (float) (6 - (Unarmed - level)) / 6.0;
				exp = (int) (percentage * 50);
				if(exp < 10){
					exp = 10;
				}
				else if(exp > 200){
					exp = 100;
				}
				exp += Unarmed / 3;
				UnarmedExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Unarmed]: " + ChatColor.GOLD + UnarmedExp + ChatColor.AQUA + " / " + ChatColor.GOLD + UnarmedNext);
				}
				if(UnarmedExp >= UnarmedNext && Unarmed <= SkillCap){
					if(Unarmed == SkillCap){
						UnarmedExp = UnarmedNext;
					}
					else{
						UnarmedExp -= UnarmedNext;
						Unarmed += 1;
						UnarmedNext = calculateExpForNextLevel(Unarmed);
						owner.sendMessage(ChatColor.AQUA + "[Unarmed]: Has increased to " + ChatColor.GOLD + Unarmed);
					}
				}
				break;
			case WOODCUTTING:
				exp = (value * 20) + (Woodcutting / 3);
				WoodcuttingExp += exp;
				if(chatty){
					owner.sendMessage(ChatColor.AQUA + "[Woodcutting]: "+ ChatColor.GOLD + WoodcuttingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + WoodcuttingNext);
				}
				if(WoodcuttingExp >= WoodcuttingNext && Woodcutting <= SkillCap){
					if(Woodcutting == SkillCap){
						WoodcuttingExp = WoodcuttingNext;
					}
					else{
						WoodcuttingExp -= WoodcuttingNext;
						Woodcutting += 1;
						WoodcuttingNext = calculateExpForNextLevel(Woodcutting);
						owner.sendMessage(ChatColor.AQUA + "[Woodcutting]: Has increased to " + ChatColor.GOLD + Woodcutting);
					}
				}
				break;
					
		}
	}
	
	public void saveCharacter(){
		File file = new File(mainPlugin.getDataFolder() + File.separator + "Players", owner.getUniqueId().toString() + ".yml");
		YamlConfiguration fc = YamlConfiguration.loadConfiguration(file);
		
		fc.set("Agility", Agility);
		fc.set("AgilityExp", AgilityExp);
		fc.set("AgilityNext", AgilityNext);
		
		fc.set("Bludgeoning", Bludgeoning);
		fc.set("BludgeoningExp", BludgeoningExp);
		fc.set("BludgeoningNext", BludgeoningNext);
		
		fc.set("Building", Building);
		fc.set("BuildingExp", BuildingExp);
		fc.set("BuildingNext", BuildingNext);
		
		fc.set("Digging", Digging);
		fc.set("DiggingExp", DiggingExp);
		fc.set("DiggingNext", DiggingNext);
		
		fc.set("Fencing", Fencing);
		fc.set("FencingExp", FencingExp);
		fc.set("FencingNext", FencingNext);
	
		fc.set("Marauding", Marauding);
		fc.set("MaraudingExp", MaraudingExp);
		fc.set("MaraudingNext", MaraudingNext);
		
		fc.set("Marksmanship", Marksmanship);
		fc.set("MarksmanshipExp", MarksmanshipExp);
		fc.set("MarksmanshipNext", MarksmanshipNext);
		
		fc.set("Mining", Mining);
		fc.set("MiningExp", MiningExp);
		fc.set("MiningNext", MiningNext);
		
		fc.set("Staves", Staves);
		fc.set("StavesExp", StavesExp);
		fc.set("StavesNext", StavesNext);
		
		fc.set("Unarmed", Unarmed);
		fc.set("UnarmedExp", UnarmedExp);
		fc.set("UnarmedNext", UnarmedNext);
		
		fc.set("Woodcutting", Woodcutting);
		fc.set("WoodcuttingExp", WoodcuttingExp);
		fc.set("WoodcuttingNext", WoodcuttingNext);
		
		System.out.println("Saved, " + owner.getName());
		
		try{
			fc.save(file);
		}
		catch(IOException error){
			error.printStackTrace();
			
		}
	}
	
	public void sendCharacterInfo(Player player){
		player.sendMessage(ChatColor.AQUA + "Player : " + ChatColor.GOLD + owner.getName());
		Integer powerlevel = Agility + Bludgeoning + Building + Digging + Fencing + Marauding + Marksmanship + Mining + Unarmed + Staves + Woodcutting;
		int average = (int) (powerlevel / 11.0);
		player.sendMessage(ChatColor.AQUA + "Power Level : " + ChatColor.GOLD + powerlevel + ChatColor.AQUA + "(" + average + ")");
		player.sendMessage("");
		
		player.sendMessage(ChatColor.AQUA + "Bludgeoning : " + ChatColor.GOLD + Bludgeoning);
		player.sendMessage(ChatColor.AQUA + "Building : " + ChatColor.GOLD + Building);
		player.sendMessage(ChatColor.AQUA + "Digging : " + ChatColor.GOLD + Digging);
		player.sendMessage(ChatColor.AQUA + "Fencing : " + ChatColor.GOLD + Fencing);
		player.sendMessage(ChatColor.AQUA + "Marauding : " + ChatColor.GOLD + Marauding);
		player.sendMessage(ChatColor.AQUA + "Marksmanship : " + ChatColor.GOLD + Marksmanship);
		player.sendMessage(ChatColor.AQUA + "Mining : " + ChatColor.GOLD + Mining);
		player.sendMessage(ChatColor.AQUA + "Staves : " + ChatColor.GOLD + Staves);
		player.sendMessage(ChatColor.AQUA + "Tumble : " + ChatColor.GOLD + Agility);
		player.sendMessage(ChatColor.AQUA + "Unarmed : " + ChatColor.GOLD + Unarmed);
		player.sendMessage(ChatColor.AQUA + "Woodcutting : " + ChatColor.GOLD + Woodcutting);
	}
	
	public void sendStats(){
		Integer powerlevel = Agility + Bludgeoning + Building + Digging + Fencing + Marauding + Marksmanship + Mining + Staves + Unarmed + Woodcutting;
		int average = (int) (powerlevel / 11.0);
		owner.sendMessage(ChatColor.AQUA + "Power Level : " + ChatColor.GOLD + powerlevel + ChatColor.AQUA + "(" + average + ")");
		owner.sendMessage("");
		owner.sendMessage(ChatColor.AQUA + "Bludgeoning : " + ChatColor.GOLD + Bludgeoning + ChatColor.AQUA + " (" + ChatColor.GOLD + BludgeoningExp + ChatColor.AQUA + " / " + ChatColor.GOLD + BludgeoningNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Building : " + ChatColor.GOLD + Building + ChatColor.AQUA + " (" + ChatColor.GOLD + BuildingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + BuildingNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Digging : " + ChatColor.GOLD + Digging + ChatColor.AQUA + " (" + ChatColor.GOLD + DiggingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + DiggingNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Fencing : " + ChatColor.GOLD + Fencing + ChatColor.AQUA + " (" + ChatColor.GOLD + FencingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + FencingNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Marauding : " + ChatColor.GOLD + Marauding + ChatColor.AQUA + " (" + ChatColor.GOLD + MaraudingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + MaraudingNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Marksmanship : " + ChatColor.GOLD + Marksmanship + ChatColor.AQUA + " (" + ChatColor.GOLD + MarksmanshipExp + ChatColor.AQUA + " / " + ChatColor.GOLD + MarksmanshipNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Mining : " + ChatColor.GOLD + Mining + ChatColor.AQUA + " (" + ChatColor.GOLD + MiningExp + ChatColor.AQUA + " / " + ChatColor.GOLD + MiningNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Staves : " + ChatColor.GOLD + Staves + ChatColor.AQUA + " (" + ChatColor.GOLD + StavesExp + ChatColor.AQUA + " / " + ChatColor.GOLD + StavesNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Tumble : " + ChatColor.GOLD + Agility + ChatColor.AQUA + " (" + ChatColor.GOLD + AgilityExp + ChatColor.AQUA + " / " + ChatColor.GOLD + AgilityNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Unarmed : " + ChatColor.GOLD + Unarmed + ChatColor.AQUA + " (" + ChatColor.GOLD + UnarmedExp + ChatColor.AQUA + " / " + ChatColor.GOLD + UnarmedNext + ChatColor.AQUA + ")");
		owner.sendMessage(ChatColor.AQUA + "Woodcutting : " + ChatColor.GOLD + Woodcutting + ChatColor.AQUA + " (" + ChatColor.GOLD + WoodcuttingExp + ChatColor.AQUA + " / " + ChatColor.GOLD + WoodcuttingNext + ChatColor.AQUA + ")");
	}
	
	//Specific Skill Methods
	public void checkDuplicateBlock(Material block, Integer random){
		double chance = Building / 20;
		if(random <= chance){
			ItemStack item = new ItemStack(block);
			owner.getInventory().addItem(item);
			owner.sendMessage("[Building] : Item duplicated!");
		}
	}
	
	public void checkMiningBlock(Material block, Integer random){
		double chance = Mining / 10;
		if(block != Material.STONE){
			if(random <= chance){
				ItemStack item = new ItemStack(block, 1);
				owner.getInventory().addItem(item);
				owner.sendMessage(ChatColor.GOLD + "[Mining] : " + block.name() + " dropped an extra!");
			}
		}
	}
	
	public boolean checkFencingCounter(){
		double chance = Fencing / 20.0;
		double roll = 100.0 * mainPlugin.getRandom().nextDouble();
		
		if(roll <= chance){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean checkBludgeoningBlock(){
		double chance = Bludgeoning / 20.0;
		double roll = 100.0 * mainPlugin.getRandom().nextDouble();
		
		if(roll <= chance){
			return true;
		}
		else{
			return false;
		}
	}
	
	public Integer getMaraudingDamage(){
		double chance = Marauding / 20.0;
		double roll = 100.0 * mainPlugin.getRandom().nextDouble();
		
		if(roll <= chance){
			return 0;
		}
		else{
			return (Marauding / 10) + 1;
		}
	}

//	public void checkTreeFeller(Block block) {
//		Integer time = (int) (System.currentTimeMillis() / 1000);
//		if(time >= this.lastFeller){
//			destroyTree();
//		}
//	}
	
}
