package me.Lorinth.RPGE.Characters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Items.WeaponType;
import me.Lorinth.RPGE.Managers.CharacterManager;
import me.Lorinth.RPGE.Parties.CharacterParty;
import me.Lorinth.RPGE.Races.Race;
import me.Lorinth.RPGE.SkillAPI.ActiveSkill;
import me.Lorinth.RPGE.SkillAPI.AttackType;
import me.Lorinth.RPGE.SkillAPI.Buff;
import me.Lorinth.RPGE.SkillAPI.PassiveType;
import me.Lorinth.RPGE.SkillAPI.Skill;
import me.Lorinth.RPGE.SkillAPI.Stance;
import me.Lorinth.RPGE.SkillAPI.StatType;
import net.elseland.xikage.MythicMobs.API.Mobs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Effect;
import org.bukkit.EntityEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;

public class Character {

	public ArrayList<Skill> active_skills = new ArrayList<Skill>();
	public ArrayList<Skill> passive_skills = new ArrayList<Skill>();
	public HashMap<Skill, Integer> cooldowns = new HashMap<Skill, Integer>();
	
	private HashMap<Integer, String> binds = new HashMap<Integer, String>();
	private HashMap<Buff, Integer> Buffs = new HashMap<Buff, Integer>();
	private HashMap<String, Integer> BuffValues = new HashMap<String, Integer>();
	//private HashMap<Skill, Integer> Cooldowns = new HashMap<Skill, Integer>();
	
	private Scoreboard sb;
	
	private Integer MMcooldown = 0;
	
	public Integer RaceToken = 2;
	public Integer ResetToken = 1;
	
	public Integer Health = 20;
	public Integer maxHealth = 20;
	public Integer Mana = 20;
	public Integer maxMana = 20;
	public Integer Stamina = 20;
	public Integer maxStamina = 20;
	
	//Stats
	public Integer Strength = 1;
	public Integer Constitution = 1;
	public Integer Dexterity = 1;
	public Integer Agility = 1;
	public Integer Wisdom = 1;
	public Integer Intelligence = 1;
	
	//CombatSpecific
	public Integer PhysicalAttack = 1;
	public Integer PhysicalDefense = 1;
	public Integer RangedAttack = 1;
	public Integer MagicAttack = 1;
	public Integer MagicDefense = 1;
	
	public Integer MeleeBonus = 1;
	public Integer RangedBonus = 1;
	public Integer MagicBonus = 1;
	
	public Integer WeaponCritical = 0;
	private int WeaponLS = 0;
	public Integer Critical = 1;
	public Integer Dodge = 1;
	
	//ExperienceSpecific
	public Integer LevelCap = 30;
	public Integer Level = 1; // Is also CP
	public Integer Experience = 0;
	public Integer ToNextLevel = 1500;
	
	public Integer TotalSkillPoints = 5;
	public Integer CurrentSkillPoints = 5;
	
	private HashMap<PassiveType, Integer> passives = new HashMap<PassiveType, Integer>();
	
	public long lastGearClaim = 0;
	
	public Stance stance = null;
	public HashMap<PassiveType, Integer> stancePassives = new HashMap<PassiveType, Integer>();
	
	private boolean isCloaked = false;
	private Integer cloakTask = 0;
	
	//private Integer raceChangeToken = 1;
	private Race race;
	
	public boolean Synced = false;
	public boolean forceSynced = false;
	private HashMap<String, Integer> unsyncedStats = new HashMap<String, Integer>();
	public Integer syncedLevel = 0;
	
	public CharacterParty currentParty = null;
	
	public SkillsProfile profile = null;
	
	public Player owner;
	
	public boolean isGod = false;
	
	public Inventory backpack = Bukkit.createInventory(null, 18, "Backpack");
	public Inventory equipment = Bukkit.createInventory(null, 9, "Equipment");
	
	private Random random = new Random();
	private CharacterManager cm;
	public int hydration = 2000;
	public ThirstState thirstState = ThirstState.Hydrated;
	
	public void addInBackpack(ItemStack item){
		backpack.addItem(item);
	}
	
	@SuppressWarnings("unchecked")
	public void loadBackpack(YamlConfiguration f){
		try{
			for(String slot : f.getConfigurationSection("Inventory").getKeys(false)){
				try{
					ItemStack item = new ItemStack(Material.AIR);
					if(f.getString("Inventory." + slot + ".type") != "AIR"){
						item = new ItemStack(Material.getMaterial(f.getString("Inventory." + slot + ".type")), f.getInt("Inventory." + slot + ".qty"));
						item.setDurability((short) f.getInt("Inventory." + slot + ".data"));
						if(!f.getString("Inventory." + slot + ".name").toLowerCase().trim().equals("none")){
							ItemMeta im = item.getItemMeta();
							im.setLore(f.getStringList("Inventory." + slot + ".lore"));
							im.setDisplayName(f.getString("Inventory." + slot + ".name"));
							item.setItemMeta(im);
						}
						backpack.setItem(Integer.parseInt(slot), item);
					}
				}
				catch(NullPointerException e){
					e.printStackTrace();
				}
			}
			for(String slot : f.getConfigurationSection("Equipment").getKeys(false)){
				try{
					ItemStack item = new ItemStack(Material.AIR);
					if(f.getString("Equipment." + slot + ".type") != "AIR"){
						item = new ItemStack(Material.getMaterial(f.getString("Equipment." + slot + ".type")), f.getInt("Equipment." + slot + ".qty"));
						item.setDurability((short) f.getInt("Equipment." + slot + ".data"));
						if(!f.getString("Equipment." + slot + ".name").toLowerCase().trim().equals("none")){
							ItemMeta im = item.getItemMeta();
							im.setLore(f.getStringList("Equipment." + slot + ".lore"));
							im.setDisplayName(f.getString("Equipment." + slot + ".name"));
							item.setItemMeta(im);
						}
						equipment.setItem(Integer.parseInt(slot), item);
					}
				}
				catch(NullPointerException e){
					e.printStackTrace();
				}
			}
		}
		catch(NullPointerException e2){
			
		}
	}
	
	public void saveBackpack(YamlConfiguration f){
		//Integer slot = 0;
		for(int i=0; i < 18; i++){
			ItemStack item = backpack.getItem(i);
			if(item != null){
				try{
					f.set("Inventory." + i + ".type", item.getType().name());
					f.set("Inventory." + i + ".qty", item.getAmount());
					f.set("Inventory." + i + ".data", item.getDurability());
					if(item.hasItemMeta()){
						try{
							ItemMeta im = item.getItemMeta();
							f.set("Inventory." + i + ".name", im.getDisplayName());				
							f.set("Inventory." + i + ".lore", im.getLore());
						}
						catch(NullPointerException e){
							f.set("Inventory." + i + ".name", "NONE");
							f.set("Inventory." + i + ".lore", null);
						}
					}
					else{
						f.set("Inventory." + i + ".name", "NONE");
						f.set("Inventory." + i + ".lore", null);
					}
				}
				catch(NullPointerException e){
					e.printStackTrace();
					f.set("Inventory." + i + ".type", "AIR");
					f.set("Inventory." + i + ".name", "NONE");
				}
			}
			else{
				f.set("Inventory." + i + ".type", "AIR");
				f.set("Inventory." + i + ".name", "NONE");
				f.set("Inventory." + i + ".qty", 1);
			}
		}
		for(int i=0; i < 9; i++){
			ItemStack item = equipment.getItem(i);
			if(item != null){
				try{
					f.set("Equipment." + i + ".type", item.getType().name());
					f.set("Equipment." + i + ".qty", item.getAmount());
					f.set("Equipment." + i + ".data", item.getDurability());
					if(item.hasItemMeta()){
						try{
							ItemMeta im = item.getItemMeta();
							f.set("Equipment." + i + ".name", im.getDisplayName());				
							f.set("Equipment." + i + ".lore", im.getLore());
						}
						catch(NullPointerException e){
							f.set("Equipment." + i + ".name", "NONE");
							f.set("Equipment." + i + ".lore", null);
						}
					}
					else{
						f.set("Equipment." + i + ".name", "NONE");
						f.set("Equipment." + i + ".lore", null);
					}
				}
				catch(NullPointerException e){
					e.printStackTrace();
					f.set("Equipment." + i + ".type", "AIR");
					f.set("Equipment." + i + ".name", "NONE");
				}
			}
			else{
				f.set("Equipment." + i + ".type", "AIR");
				f.set("Equipment." + i + ".name", "NONE");
				f.set("Equipment." + i + ".qty", 1);
			}
		}
	}
	
	public void equipItem(ItemStack item){
		String type = item.getType().name().toLowerCase();
		if(item.hasItemMeta()){
			Integer slot = null;
			String slotType = "";
			if(type.contains("sword") || type.contains("axe") || type.contains("spade")){
				slot = 0;
				slotType = "melee";
			}
			else if(type.contains("bow")){
				slot = 4;
				slotType = "ranged";
			}
			else if(type.contains("stick") || type.contains("rod")){
				slot = 8;
				slotType = "magic";
			}
			else{
				return;
			}
			
			equipment.setItem(slot, item);
			owner.sendMessage(ChatColor.GREEN + "You've equipped the, " + item.getItemMeta().getDisplayName() + ChatColor.GREEN + " in the " + slotType + " slot");
			
		}
	}
	
	public ArrayList<Character> getParty(){
		if(currentParty != null){
			currentParty.getCharacters().remove(this);
		}
		return null;
	}
	
	public ArrayList<Player> getPartyPlayers(){
		ArrayList<Player> players = new ArrayList<Player>();
		if(currentParty != null){
			for(Character cha : currentParty.getCharacters()){
				players.add(cha.owner);
			}
		}
		return players;
	}
	
	public void updateScoreBoard(Scoreboard s){
		this.sb = s;
//		try{
//			Objective o = sb.registerNewObjective("showhealth", "health");
//			o.setDisplaySlot(DisplaySlot.BELOW_NAME);
//			o.setDisplayName("/" + maxHealth);
//		}
//		catch(IllegalArgumentException e){
//			Objective o = sb.getObjective("showhealth");
//			o.setDisplaySlot(DisplaySlot.BELOW_NAME);
//			o.setDisplayName("/" + maxHealth);
//		}
		
		
		owner.setScoreboard(sb);
		//owner.setHealth(Health);
	}
	
	public Character(CharacterManager CM, Player player){
		owner = player;
		cm = CM;
		race = cm.getRaceManager().getRaceByName("Human");
		race.create(this);
		LevelCap = cm.getLevelCap();
		this.resetPassives();
		
		equipment = Bukkit.createInventory(null, 9, owner.getDisplayName() + "'s Equipment");
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				BuffDuration();
				
			}
			
		}, 20, 20);
	} 
	
	public final void setAttribute(StatType stat, Integer value){
		switch(stat){
			case STRENGTH:
				Strength = value;
			case CONSTITUTION:
				Constitution = value;
			case DEXTERITY:
				Dexterity = value;
			case AGILITY:
				Agility = value;
			case WISDOM:
				Wisdom = value;
			case INTELLIGENCE:
				Intelligence = value;
		}
	}
	
	public void resetCharacterPassives(){
		for(Skill skill : passive_skills){
			skill.remove(this);
		}
		
		owner.setWalkSpeed((float) 0.2);
	}
	
	public void update(boolean notify){
		resetCharacterPassives();
		checkSkills(notify);
		race.apply(this);
		
		//Strength && Constitution
		MeleeBonus = (Strength / 8) + passives.get(PassiveType.MeleeDamage);
		PhysicalAttack = Strength + passives.get(PassiveType.Attack);
		PhysicalDefense = (int) ((Constitution * 1.5) + passives.get(PassiveType.Defense));
		

		//Dexterity & Agility
		RangedBonus = (Dexterity / 8) + passives.get(PassiveType.RangedDamage);
		Dodge = (Agility / 10) + 1 + passives.get(PassiveType.Dodge);
		Critical = (Dexterity / 10) + 1 + passives.get(PassiveType.Critical);

		//Wisdom & Intelligence
		MagicBonus = (Intelligence / 8) + + passives.get(PassiveType.MagicDamage);
		MagicDefense = (int) ((Wisdom * 1.5) + passives.get(PassiveType.Defense));
		
		//Vitals
		maxHealth = (Strength / 6) + (Constitution / 3) + 16 + passives.get(PassiveType.HealthMax);
		maxMana = (Wisdom / 3) + (Intelligence / 3) + 10 + passives.get(PassiveType.ManaMax);
		maxStamina = (Strength / 3) + (Constitution / 3) + 10 + passives.get(PassiveType.StaminaMax);
		
		checkGear();
		updateHealth();
		
	}
	
	private void checkSkills(boolean notify){
		ArrayList<Skill> allskills = new ArrayList<Skill>();
		allskills.addAll(active_skills);
		allskills.addAll(passive_skills);
		
		active_skills.clear();
		passive_skills.clear();
		cm.getMain().sm.updateSkills(this, allskills, notify);
	}
	
	private void checkGear(){
		if(owner.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
//		
		for(ItemStack item : owner.getInventory().getArmorContents()){
			try{
				if(item.getType() == Material.SKULL_ITEM){
					//pass
				}
				else{
					if(item.hasItemMeta()){
						ItemMeta im = item.getItemMeta();
						List<String> lore = im.getLore();
						for(String line : lore){
							line = ChatColor.stripColor(line);
							if(line.contains("○") || line.contains("●")){
								
							}
							else if(line.contains("Level")){
								line = line.replace("Req", "");
								Integer level = Integer.parseInt(line.replace("Level : ", "").trim());
								if(Synced){
									if(this.syncedLevel < level){
										owner.sendMessage("You are not a high enough level to equip " + im.getDisplayName());
										return;
									}
								}
								else{
									if(this.Level < level){
										owner.sendMessage("You are not a high enough level to equip " + im.getDisplayName());
										return;
									}
								}
							}
							else if(line.contains("Health")){
								maxHealth += Integer.parseInt(line.replace("Health : ", "").trim());
							}
							else if(line.contains("Mana")){
								maxMana += Integer.parseInt(line.replace("Mana : ", "").trim());
							}
							else if(line.contains("Stamina")){
								maxStamina += Integer.parseInt(line.replace("Stamina : ", "").trim());
							}
						}
					}
				}

			}
			catch(NullPointerException e){
				
			}
		}
		
		if(Health > maxHealth){
			Health = maxHealth;
		}
		if(Mana > maxMana){
			Mana = maxMana;
		}
		if(Stamina > maxStamina){
			Stamina = maxStamina;
		}
	}
	
	private boolean checkDodge(){
		Integer dodgechance = (Integer) random.nextInt(100);
		Integer dod = Dodge + passives.get(PassiveType.Dodge);
		if(dodgechance <= dod){
			return true;
		}
		return false;
	}
	
	private boolean checkCritical(){
		Integer roll = (Integer) random.nextInt(100);
		Integer crit = Critical + WeaponCritical + passives.get(PassiveType.Critical);
		if(roll <= crit){
			return true;
		}
		return false;
	}
	
	public boolean damage(AttackType at, double d, Entity entity, boolean pureDamage){
		if(owner.getNoDamageTicks() > 10){
			return false;
		}
		int resultdamage = 0;
		Integer shadows = this.getPassive(PassiveType.Shadows);
		if(shadows > 0){
			shadows -= 1;
			
			this.setPassive(PassiveType.Shadows, shadows);
			owner.setNoDamageTicks(20);
			return false;
		}
		
		if(at.equals(AttackType.Physical)){
			if(checkDodge()){
				sendMessage(ChatColor.GREEN + "You dodged!");
				owner.setNoDamageTicks(20);
				return false;
			}
			else if(getWeaponType().equals(WeaponType.Sword) && profile.checkFencingCounter()){
				if(entity instanceof Projectile){
					//LivingEntity counter = ((Projectile)entity).getShooter();
					//counter.damage((double)1, owner);
					sendMessage(ChatColor.GREEN + "You countered!");
					owner.setNoDamageTicks(20);
					return false;
				}
				else{
					((LivingEntity) entity).damage((double)1, owner);
					sendMessage(ChatColor.GREEN + "You countered!");
					owner.setNoDamageTicks(20);
					return false;
				}
			}
			else if(getWeaponType().equals(WeaponType.Mace) && profile.checkBludgeoningBlock()){
				//((LivingEntity) entity).damage((double)1, owner);
				sendMessage(ChatColor.GREEN + "You blocked with your weapon!");
				owner.setNoDamageTicks(20);
				return false;
			}
			
		}
		if(pureDamage){
			resultdamage = (int) d;
		}
		else{
			float Attack = 0;
			if(entity instanceof Monster){
				if(Mobs.isMythicMob(entity.getUniqueId())){
					Attack = (float) ((Level * 2.0) + 30);
				}
				if(MobDifficulty.getPlugin().isDungeonMob(entity)){
					Attack = (float) ((cm.getLevel(entity) * 1.6) + 30);
				}
				else if(MobDifficulty.getPlugin().isElite(entity)){
					Attack = (float) ((cm.getLevel(entity) * 2.0) + 50);
				}
				else{
					if(cm.getLevel(entity) <= 10){
						Attack = (float) (cm.getLevel(entity)) * 2 + 10;
					}
					else{
						Attack = (float) (cm.getLevel(entity)) + 20;
					}
				}
			}
			else if(entity instanceof Player){
				Attack = (float) cm.getCharacter((Player) entity).getAttack();
			}
			
			float DReduct = getPhysicalDefense();
			if(at.equals(AttackType.Physical)){
				DReduct = getPhysicalDefense();
			}
			else if(at.equals(AttackType.Magic)){
				DReduct = getMagicDefense();
			}
			float DamageMod = (Attack + 10) / (DReduct + 10);
			if(DamageMod >= 2.5){
				DamageMod = (float) 2.5;
			}
			
			resultdamage = (int) (DamageMod * d) + 1;
			owner.damage((double)resultdamage);
		}
		
		//this.sendMessage("[Combat] : (" + Attack + "/" + DReduct + ") = " + DamageMod + " * " + d + " = " + resultdamage);
		//this.sendMessage("[Combat] : Health ( " + Health + "/" + maxHealth + " )");
		
		Health = (int) (Health - resultdamage);
		if(Health == 0){
			DeathCause cause;
			String name = "";
			if(entity instanceof Player){
				cause = DeathCause.PLAYER;
				name = ((Player) entity).getName();
			}
			else if(entity instanceof Monster){
				cause = DeathCause.MONSTER;
				name = ((Creature)entity).getCustomName();
				if(name == null){
					name = ((Creature)entity).getType().name();
				}
				//player.sendMessage(spell + ChatColor.GREEN + " dealt " + ChatColor.GOLD + damage + ChatColor.GREEN + " damage to the " + ChatColor.GOLD + name);
			}
			else{
				cause = DeathCause.UNKNOWN;
			}
			respawn(cause, name);
			return true;
		}
		
		this.normalizeVitals();
		
		return false;
		
	}
	
	public Integer getLevel(){
		if(Synced){
			return this.syncedLevel;
		}
		else{
			return Level;
		}
	}
	
	public void setPassive(PassiveType p, Integer v){
		passives.put(p, v);
	}
	
	public Integer getPassive(PassiveType p){
		return passives.get(p);
	}
	
    public double CalculateSkillPoints(double level){
    	Integer levelStart = 1;
    	
    	double Character1 = level;
    	
    	Integer sp = 5;
    	
    	for(Integer Level=levelStart; Level<Character1; Level++){
			if(Level % 10 == 0){
				sp += 5;
			}else if (Level % 5 == 0){
				sp += 3;
			}else{
				sp += 2;
			}
    	}
    	
    	//System.out.println("Character 1 Skill Points : " + sp);
    	
    	return sp;
    }
	
	public void sync(float level, boolean notify){
		if(level == 0){
			level = this.currentParty.getSyncLevel();
		}
		
		if(Synced){
			unsync("RESYNCING");
		}
		if(notify){
			sendMessage(ChatColor.DARK_AQUA + "[Party] You have synced to level " + (int) level);
		}
		
		double sp = CalculateSkillPoints(level);
		double ratio = sp / TotalSkillPoints;
		
		Synced = true;
		syncedLevel = (int) level;
		unsyncedStats.put("Strength", Strength);
		unsyncedStats.put("Constitution", Constitution);
		unsyncedStats.put("Dexterity", Dexterity);
		unsyncedStats.put("Agility", Agility);
		unsyncedStats.put("Wisdom", Wisdom);
		unsyncedStats.put("Intelligence", Intelligence);
		
		Integer Str = (int) ((Strength - 10) * ratio) + 10;
		Integer Con = (int) ((Constitution - 10) * ratio) + 10;
		Integer Dex = (int) ((Dexterity - 10) * ratio) + 10;
		Integer Agi = (int) ((Agility - 10) * ratio) + 10;
		Integer Wis = (int) ((Wisdom - 10) * ratio) + 10;
		Integer Int = (int) ((Intelligence - 10) * ratio) + 10;
		
		if(Str < Strength){
			Strength = Str;
		}
		if(Con < Constitution){
			Constitution = Con;
		}
		if(Dex < Dexterity){
			Dexterity = Dex;
		}
		if(Agi < Agility){
			Agility = Agi;
		}
		if(Wis < Wisdom){
			Wisdom = Wis;
		}
		if(Int < Intelligence){
			Intelligence = Int;
		}
		
		update(false);
	}
	
	public void unsync(String reason){
		//System.out.println(reason);
		if(reason == "SAVING"){
			this.sync(currentParty.getSyncLevel(), false);
		}
		if(Synced){
			Synced = false;
			Strength = unsyncedStats.get("Strength");
			Constitution = unsyncedStats.get("Constitution");
			Dexterity = unsyncedStats.get("Dexterity");
			Agility = unsyncedStats.get("Agility");
			Wisdom = unsyncedStats.get("Wisdom");
			Intelligence = unsyncedStats.get("Intelligence");
			update(false);
		}
	}
	
	public void god(){
		isGod = true;
		unsyncedStats.put("Strength", Strength);
		unsyncedStats.put("Constitution", Constitution);
		unsyncedStats.put("Dexterity", Dexterity);
		unsyncedStats.put("Agility", Agility);
		unsyncedStats.put("Wisdom", Wisdom);
		unsyncedStats.put("Intelligence", Intelligence);
		
		Strength = 999;
		Constitution = 999;
		Dexterity = 999;
		Agility = 999;
		Wisdom = 999;
		Intelligence = 999;
		update(false);
	}
	
	public void ungod(String reason){
		if(reason == "SAVING"){
			Strength = unsyncedStats.get("Strength");
			Constitution = unsyncedStats.get("Constitution");
			Dexterity = unsyncedStats.get("Dexterity");
			Agility = unsyncedStats.get("Agility");
			Wisdom = unsyncedStats.get("Wisdom");
			Intelligence = unsyncedStats.get("Intelligence");
		}
		else{
			isGod = false;
			
			Strength = unsyncedStats.get("Strength");
			Constitution = unsyncedStats.get("Constitution");
			Dexterity = unsyncedStats.get("Dexterity");
			Agility = unsyncedStats.get("Agility");
			Wisdom = unsyncedStats.get("Wisdom");
			Intelligence = unsyncedStats.get("Intelligence");
			update(false);
		}
	}
	
	public void sendMessage(String line){
		owner.sendMessage(line);
	}
	
	@SuppressWarnings("deprecation")
	public void updateHealth(){
		if(owner.getWorld().equals(Bukkit.getWorld("CODWarfare"))){
			return;
		}
		
		if(Health > maxHealth){
			Health = maxHealth;
		}
		else if(Health < 0){
			respawn(DeathCause.UNKNOWN, "");
		}
		owner.setMaxHealth((double)maxHealth);
		owner.setHealth((double)Health);
		
		if(currentParty != null){
			currentParty.healthUpdate(this);
		}
		
	}
	
	public void respawn(DeathCause cause, String name){
		Health = maxHealth;
		String message = "";
		hydration = 500;
		switch(cause){
		case MONSTER:
			message = "was overtaken by " + ChatColor.GOLD + name;
			break;
		case PLAYER:
			message = "was slain by " + ChatColor.GOLD + name;
			break;
		case UNKNOWN:
			message = "has been consumed by darkness...";
			break;
		default:
			message = "has been consumed by darkness...";
			break;
		}
		
		for(Player player : owner.getWorld().getPlayers()){
			player.sendMessage(ChatColor.GOLD + owner.getDisplayName() + ChatColor.DARK_RED + " " + message);
		}
		
		try{
			if(this.currentParty.inDungeon()){
				owner.teleport(this.currentParty.getDungeon().spawn);
				owner.sendMessage(ChatColor.RED + "***YOU HAVE DIED***");
			}
			else{
				owner.teleport(new Location(Bukkit.getWorld("world"), -0.5, 65, -0.5));
				owner.sendMessage(ChatColor.RED + "***YOU HAVE DIED***");
				owner.sendMessage(ChatColor.RED + "- You've lost 5% of your experience");
			}
		}
		catch(NullPointerException e){
			owner.teleport(new Location(Bukkit.getWorld("world"), -0.5, 65, -0.5));
			owner.sendMessage(ChatColor.RED + "***YOU HAVE DIED***");
			owner.sendMessage(ChatColor.RED + "- You've lost 5% of your experience");
		}
		
		
		
		Integer lostExp = (int)(this.ToNextLevel * 0.05);
		Experience -= lostExp;
		if(Experience < 0){
			Experience = 0;
		}
		
		updateHealth();
		
		EntityDeathEvent event = new EntityDeathEvent(owner, new ArrayList<ItemStack>());
		Bukkit.getPluginManager().callEvent(event);
	}
	
	public void giveExp(Integer exp){
		Experience += exp;
		owner.sendMessage(ChatColor.GREEN + "You've gained " + exp + " experience!");
		while(Experience >= ToNextLevel){
			if(Level + 1 > LevelCap){
				Experience = ToNextLevel;
				return;
			}
			Level += 1;
			if(currentParty != null){
				currentParty.checkPartySync(this);
			}
			Experience -= ToNextLevel;
			
			Integer sp = 0;
			if(Level % 10 == 0){
				CurrentSkillPoints += 5;
				TotalSkillPoints += 5;
				sp = 5;
			}else if (Level % 5 == 0){
				CurrentSkillPoints += 3;
				TotalSkillPoints += 3;
				sp = 3;
			}else{
				CurrentSkillPoints += 2;
				TotalSkillPoints += 2;
				sp = 2;
			}
			
			owner.sendMessage(ChatColor.GOLD + "====" + ChatColor.GREEN + "LEVEL UP" + ChatColor.GOLD + "====");
			owner.sendMessage(ChatColor.GREEN + "* You have gained " + ChatColor.GOLD + sp + ChatColor.GREEN + " skill points!");
			owner.sendMessage(ChatColor.GREEN + "* To spend your points, use " + ChatColor.GOLD + "/addstat <stat");
			owner.sendMessage(ChatColor.GOLD + "================");
			owner.playEffect(EntityEffect.WITCH_MAGIC);
			
			ToNextLevel = exp = (int) ((int) (1000 * Math.log(Level)) + (20 *(Level-1) * (Level-10)) / 10 + 1500);
			if(Level > 30){
				ToNextLevel += -3000 + (100 * Level);
			}
			
			for(Entity ent : owner.getNearbyEntities(45, 45, 45)){
				if(ent instanceof Player){
					((Player) ent).sendMessage(ChatColor.GOLD + owner.getDisplayName() + ChatColor.GREEN +  " has leveled up to " + ChatColor.GOLD +  Level + ChatColor.GREEN + "!");
				}
			}
		}
	}

	public void giveGold(double value, boolean tripleDrop){
		
		if(tripleDrop){
			sendMessage(ChatColor.GOLD + "[TRIPLE DROP]" + ChatColor.BLUE + "You have recieved " + ChatColor.GOLD + value + ChatColor.BLUE + " gold!");
		}
		else{
			sendMessage(ChatColor.GOLD + "[Reward]" + ChatColor.BLUE + "You have recieved " + ChatColor.GOLD + value + ChatColor.BLUE + " gold!");
		}
		cm.getMain().addMoney(owner, value);
	}
	
	public void regen(){
		Integer hRegen = 1 + passives.get(PassiveType.HealthRegen);
		Integer mRegen = 1 + passives.get(PassiveType.ManaRegen);
		Integer sRegen = 1 + passives.get(PassiveType.StaminaRegen);
		
		
		for(PotionEffect pe : owner.getActivePotionEffects()){
			if(pe.getType() == PotionEffectType.REGENERATION){
				hRegen += pe.getAmplifier();
				mRegen += pe.getAmplifier();
				sRegen += pe.getAmplifier();
			}
			else if(pe.getType() == PotionEffectType.POISON){
				hRegen = pe.getAmplifier();
			}
			else if(pe.getType() == PotionEffectType.WITHER){
				mRegen = pe.getAmplifier();
				sRegen = pe.getAmplifier();
			}
		}
		
		Health += hRegen;
		Mana += mRegen;
		Stamina += sRegen;
		
		normalizeVitals();
		updateHealth();
	}
	
	public void ShootMagicMissle(){
		if(owner.getGameMode() == GameMode.CREATIVE){
			return;
		}
		
		Integer curtime = (int) (System.currentTimeMillis() / 1000);
		if(curtime < MMcooldown){
			return;	
		}
		
		cm.getMain().ch.checkWeaponDurability(owner);
		
		final Snowball arrow = owner.launchProjectile(Snowball.class, owner.getLocation().getDirection());
		arrow.setVelocity(owner.getLocation().getDirection().multiply(2));
		
		arrow.setMetadata("Skill", new FixedMetadataValue(cm.getMain(), "Magic Missle"));
		arrow.setMetadata("Damage", new FixedMetadataValue(cm.getMain(), getSpellMagicDamage()));
		arrow.setMetadata("Player", new FixedMetadataValue(cm.getMain(), owner.getName()));
		
		MMcooldown = curtime + 1;
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 1);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 1);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 1);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 1);
			
				}
			}			
		}, 2);

		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 4);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 6);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 8);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 10);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 12);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 14);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 16);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 18);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(cm.getMain(), new Runnable(){

			@Override
			public void run() {
				if(arrow.isValid()){
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
					arrow.getWorld().playEffect(arrow.getLocation(), Effect.WITCH_MAGIC, 10);
			
				}
			}
			
		}, 20);
		
		
		
	}
	
	public HashMap<PassiveType, Integer> getPassives(){
		return passives;
	}
	
	public void cast(String[] args){
		if(owner.getGameMode() == GameMode.CREATIVE){
			owner.sendMessage(ChatColor.RED + "You cannot cast spells while in creative");
			return;
		}
		
		String skillName = "";
		for(String word : args){
			skillName += word + " ";
		}
		skillName = skillName.trim();
		
		cast(skillName);
	}

	public void cast(String name){
		if(owner.getGameMode() == GameMode.CREATIVE){
			owner.sendMessage(ChatColor.RED + "You cannot cast spells while in creative");
			return;
		}
		
		Skill skill = cm.getMain().sm.getSkillByName(ChatColor.stripColor(name));
		if(skill instanceof ActiveSkill){
			if(this.active_skills.contains(skill)){
				if(cooldowns.containsKey(skill)){
					Integer curtime = (int) (System.currentTimeMillis() / 1000);
					Integer lastCast = cooldowns.get(skill);
					Integer remainder = lastCast - curtime;
					if(curtime < lastCast){
						sendMessage(skill.getName() + ChatColor.RED + " is not ready to cast (" + remainder + " seconds remaining)");		
					}
					else{
						skill.cast(owner);
					}
				}
				else{
					skill.cast(owner);
				}
			}
		}
	}
	
	public Integer getCooldown(Skill skill){
		return cooldowns.get(skill);
	}
	
	public void addCooldown(Skill skill){
		Integer curtime = (int) (System.currentTimeMillis() / 1000);
		Integer cd = skill.getCooldown();
		
		double cdReduction = passives.get(PassiveType.CooldownReduction);
		cdReduction = (100 - cdReduction) / 100.00;
		
		cd = (int) (cd * cdReduction);
		
		cooldowns.put(skill, curtime + cd);
	}
	
	public void sendSpells(){
		owner.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Active Spells");
		for(Skill skill : active_skills){
			owner.sendMessage(skill.getName() + skill.getDescription());
		}
		owner.sendMessage(ChatColor.GOLD + "" + ChatColor.UNDERLINE + "Passive Spells");
		for(Skill skill : passive_skills){
			owner.sendMessage(skill.getName() + skill.getDescription());
		}
		
	}
	
	public void sendStats(Player player){
		Integer Exp = Experience;
		Integer Exptnl = ToNextLevel;
		Integer SkillP = CurrentSkillPoints;
		Integer Str = Strength;
		Integer Con = Constitution;
		Integer Dex = Dexterity;
		Integer Agi = Agility;
		Integer Wis = Wisdom;
		Integer Int = Intelligence;
		
		if(player != owner){
			player.sendMessage(ChatColor.GREEN + "Profile for " + ChatColor.GOLD + owner.getName());
		}
		player.sendMessage("Level " + Level + " " + race.getName() + " (" + LevelCap + ")");
		if(Synced){
			player.sendMessage("Synced : " + syncedLevel);
		}
		//player.sendMessage("Race : " + race.getName());
		player.sendMessage("Experience : " + Exp);
		player.sendMessage("Exptnl : " + Exptnl);
		player.sendMessage(ChatColor.RED + "Str : " + Str + "   " + ChatColor.GOLD + "Con : " + Con);
		player.sendMessage(ChatColor.DARK_GREEN + "Dex : " + Dex + "   " + ChatColor.GREEN + "Agi : " + Agi);
		player.sendMessage(ChatColor.BLUE + "Wis : " + Wis + "   " + ChatColor.DARK_PURPLE + "Int : " + Int);
		if(player == owner){
			player.sendMessage("Available Stat Points : " + SkillP);
		}
	}
	
	public void sendBattleStats(Player player){
		//update(false);
		player.sendMessage(ChatColor.GREEN + "Health" + ChatColor.RESET +" : " + Health + "/" + maxHealth);
		player.sendMessage(ChatColor.BLUE + "Mana" + ChatColor.RESET + " : " + Mana + "/" + maxMana);
		player.sendMessage(ChatColor.YELLOW + "Stamina" + ChatColor.RESET + " : " + Stamina + "/" + maxStamina);
		player.sendMessage(ChatColor.UNDERLINE + "Damage Bonuses");
		player.sendMessage(ChatColor.RED + "Melee :  " + MeleeBonus);
		player.sendMessage(ChatColor.DARK_GREEN + "Ranged : " + RangedBonus);
		player.sendMessage(ChatColor.DARK_PURPLE + "Magic :  " + MagicBonus);
		player.sendMessage(ChatColor.UNDERLINE + "Combat Ratings");
		player.sendMessage(ChatColor.RED + "Melee-Atk : " + (int) getAttack());
		player.sendMessage(ChatColor.DARK_GREEN + "Ranged-Atk : " + (int) getRangedAttack());
		player.sendMessage(ChatColor.GOLD + "Physical-Def : " + (int) getPhysicalDefense());
		player.sendMessage(ChatColor.DARK_PURPLE + "Magic-Atk : " + (int) getMagicAttack());
		player.sendMessage(ChatColor.BLUE + "Magic-Def : " + (int) getMagicDefense());
		player.sendMessage(ChatColor.UNDERLINE + "Misc stats");
		player.sendMessage("Critical : " + Critical + "%");
		player.sendMessage("Dodge : " + Dodge + "%");
	}
	
	public void addStat(StatType st){
		if(Synced){
			owner.sendMessage(ChatColor.RED + "Unable to add stat points while synced.");
			return;
		}
		
		Integer statmax = Level + 15;
		if (!(CurrentSkillPoints >= 1)){
			owner.sendMessage("Not enough skill points to spend");
			return;
		}
		switch(st){
		case STRENGTH:
			if (Strength + 1 > statmax){
				owner.sendMessage("This stat is maxed out!");
			}
			else{
				Strength += 1;
				owner.sendMessage("Increased strength by 1");
				CurrentSkillPoints -= 1;
			}
			break;
		case CONSTITUTION:
			if (Constitution + 1 > statmax){
				owner.sendMessage("This stat is maxed out!");
			}
			else{
				Constitution += 1;
				owner.sendMessage("Increased constitution by 1");
				CurrentSkillPoints -= 1;
			}
			break;
		case DEXTERITY:
			if (Dexterity + 1 > statmax){
				owner.sendMessage("This stat is maxed out!");
			}
			else{
				Dexterity += 1;
				owner.sendMessage("Increased dexterity by 1");
				CurrentSkillPoints -= 1;
			}
			break;
		case AGILITY:
			if (Agility + 1 > statmax){
				owner.sendMessage("This stat is maxed out!");
			}
			else{
				Agility += 1;
				owner.sendMessage("Increased agility by 1");
				CurrentSkillPoints -= 1;
			}
			break;
		case WISDOM:
			if (Wisdom + 1 > statmax){
				owner.sendMessage("This stat is maxed out!");
			}
			else{
				Wisdom += 1;
				owner.sendMessage("Increased wisdom by 1");
				CurrentSkillPoints -= 1;
			}
			break;
		case INTELLIGENCE:
			if (Intelligence + 1 > statmax){
				owner.sendMessage("This stat is maxed out!");
			}
			else{
				Intelligence += 1;
				owner.sendMessage("Increased intelligence by 1");
				CurrentSkillPoints -= 1;
			}
			break;
		}
		update(true);
	}
	
	public void resetStats(){
		if(this.ResetToken >= 1){
			race.create(this);
			
			this.CurrentSkillPoints = 5;
			this.TotalSkillPoints = 5;
			this.Experience = 0;
			this.ToNextLevel = 1500;
			this.Level = 1;
			this.resetPassives();
			
			update(false);
			
			owner.sendMessage(ChatColor.BLUE + "Your stats have been reset");
		}
		else{
			owner.sendMessage(ChatColor.RED + "You don't have enough reset tokens for this.");
		}
	}
	
	public void normalizeVitals(){
		if(Health > maxHealth){
			Health = maxHealth;
		}
		if(Mana > maxMana){
			Mana = maxMana;
		}
		if(Stamina > maxStamina){
			Stamina = maxStamina;
		}
		updateHealth();
	}
	
	public float getPhysicalDefense() {
		float Totaldefense = 0;
		ItemStack chest = owner.getEquipment().getChestplate();
		ItemStack legs = owner.getEquipment().getLeggings();
		ItemStack boots = owner.getEquipment().getBoots();
		ItemStack helm = owner.getEquipment().getHelmet();
		Integer armor = 0;
		if(chest != null){
			if(chest.hasItemMeta()){
				boolean valid = true;
				for(String line : chest.getItemMeta().getLore()){
					line = ChatColor.stripColor(line);
					if(line.contains("Level")){
						Integer level = Integer.parseInt(line.replace("Level : ", ""));
						if(this.Level < level){
							valid = false;
							owner.sendMessage("You are not a high enough level to equip " + chest.getItemMeta().getDisplayName());
						}
					}
					else if(line.contains("Armor") && !line.contains("Magic") && valid){
						line = line.replace("Armor : ", "");
						armor += Integer.parseInt(line.trim());
					}
				}
			}
		}
		if(legs != null){
			if(legs.hasItemMeta()){
				boolean valid = true;
				for(String line : legs.getItemMeta().getLore()){
					line = ChatColor.stripColor(line);
					if(line.contains("Level")){
						Integer level = Integer.parseInt(line.replace("Level : ", ""));
						if(this.Level < level){
							valid = false;
							owner.sendMessage("You are not a high enough level to equip " + chest.getItemMeta().getDisplayName());
						}
					}
					else if(line.contains("Armor") && !line.contains("Magic") && valid){
						line = line.replace("Armor : ", "");
						armor += Integer.parseInt(ChatColor.stripColor(line.trim()));
					}
				}
			}
		}
		if(boots != null){
			if(boots.hasItemMeta()){
				boolean valid = true;
				for(String line : boots.getItemMeta().getLore()){
					line = ChatColor.stripColor(line);
					if(line.contains("Level")){
						Integer level = Integer.parseInt(line.replace("Level : ", ""));
						if(this.Level < level){
							valid = false;
							owner.sendMessage("You are not a high enough level to equip " + chest.getItemMeta().getDisplayName());
						}
					}
					else if(line.contains("Armor") && !line.contains("Magic") && valid){
						line = line.replace("Armor : ", "");
						armor += Integer.parseInt(ChatColor.stripColor(line.trim()));
					}
				}
			}
		}
		if(helm != null){
			if(helm.hasItemMeta()){
				boolean valid = true;
				for(String line : helm.getItemMeta().getLore()){
					line = ChatColor.stripColor(line);
					if(line.contains("Level")){
						Integer level = Integer.parseInt(line.replace("Level : ", ""));
						if(this.Level < level){
							valid = false;
							owner.sendMessage("You are not a high enough level to equip " + chest.getItemMeta().getDisplayName());
						}
					}
					if(line.contains("Armor") && !line.contains("Magic") && valid){
						line = line.replace("Armor : ", "");
						armor += Integer.parseInt(ChatColor.stripColor(line.trim()));
					}
				}
			}
		}
		Totaldefense = PhysicalDefense + armor;
		Totaldefense += passives.get(PassiveType.Defense);
		if(owner.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)){
			Totaldefense += (Level / 5) + 10;
		}
		return Totaldefense;
	}
	
	public double getAttackDamage(){
		double Damage = 1;
		
		Damage += MeleeBonus;
		
		if(owner.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)){
			Damage += (Level / 10) + 3;
		}
		
		Damage += checkWeapon(AttackType.Physical);
		
		Damage += passives.get(PassiveType.PureDamage);
		
		if(getWeaponType().equals(WeaponType.Axe)){
			Damage += profile.getMaraudingDamage();
		}
		if(getWeaponType().equals(WeaponType.Bow)){
			Damage /= 3;
		}
		
		if(checkCritical()){
			Damage = (Damage * 1.5);
			owner.sendMessage(ChatColor.GREEN + "Critical Strike!");
		}
		
		if(WeaponLS > 0){
			float ratio = (float) ((float)WeaponLS / 100.0);
			Integer lifestolen = (int) (ratio * Damage);
			heal(lifestolen);
		}
		
		return Damage;
	}
	
	public double getSpellMeleeDamage(){
		double Damage = 1;
		
		Damage += MeleeBonus;
		
		if(owner.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)){
			Damage += (Level / 10) + 3;
		}
		
		Damage += checkSpellWeapon(WeaponType.Melee);
		
		Damage += passives.get(PassiveType.PureDamage);
		
		if(getWeaponType().equals(WeaponType.Axe)){
			Damage += profile.getMaraudingDamage();
		}
		
		if(checkCritical()){
			Damage = (Damage * 1.5);
			owner.sendMessage(ChatColor.GREEN + "Critical Strike!");
		}
		
		if(WeaponLS > 0){
			float ratio = (float) ((float)WeaponLS / 100.0);
			Integer lifestolen = (int) (ratio * Damage);
			heal(lifestolen);
		}
		
		return Damage;
	}
	
	public double getSpellMagicDamage(){
		double Damage = 1;
		
		Damage += MagicBonus;
		
		if(owner.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)){
			Damage += (Level / 10) + 3;
		}
		
		Damage += checkSpellWeapon(WeaponType.Magic);
		Damage += passives.get(PassiveType.PureDamage);
		
		if(checkCritical()){
			Damage = (Damage * 1.5);
			owner.sendMessage(ChatColor.GREEN + "Critical Strike!");
		}
		
		if(WeaponLS > 0){
			float ratio = (float) ((float)WeaponLS / 100.0);
			Integer lifestolen = (int) (ratio * Damage);
			heal(lifestolen);
		}
		
		return Damage;
	}
	
	public double getSpellRangedDamage(){
		double Damage = 1;
		
		Damage += RangedBonus;
		
		if(owner.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)){
			Damage += (Level / 10) + 3;
		}
		
		Damage += checkSpellWeapon(WeaponType.Magic);
		Damage += passives.get(PassiveType.PureDamage);
		
		if(checkCritical()){
			Damage = (Damage * 1.5);
			owner.sendMessage(ChatColor.GREEN + "Critical Strike!");
		}
		
		if(WeaponLS > 0){
			float ratio = (float) ((float)WeaponLS / 100.0);
			Integer lifestolen = (int) (ratio * Damage);
			heal(lifestolen);
		}
		
		return Damage;
	}
	
	public double getMagicDamage(){
		double Damage = 1;
		
		Damage += MagicBonus;
		
		if(owner.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)){
			Damage += (Level / 10) + 3;
		}
		
		Damage += checkWeapon(AttackType.Magic);
		Damage += passives.get(PassiveType.PureDamage);
		
		if(getWeaponType().equals(WeaponType.Axe)){
			Damage += profile.getMaraudingDamage();
		}
		
		if(checkCritical()){
			Damage = (Damage * 1.5);
			owner.sendMessage(ChatColor.GREEN + "Critical Strike!");
		}
		
		if(WeaponLS > 0){
			float ratio = (float) ((float)WeaponLS / 100.0);
			Integer lifestolen = (int) (ratio * Damage);
			heal(lifestolen);
		}
		
		return Damage;
	}
	
	public void heal(Integer health){
		Health += health;
		if(Health >= maxHealth){
			Health = maxHealth;
			updateHealth();
		}
	}
	
	public Integer checkSpellWeapon(WeaponType wType){
		Integer damage = 0;
		boolean critFound = false;
		
		if(profile == null){
			profile = cm.getSkills(owner);
		}
		
		Integer skillLevel = 0;
		
		ItemStack item = null;
		
		if(wType == WeaponType.Melee){
			item = equipment.getItem(0);
		}
		else if(wType == WeaponType.Ranged){
			item = equipment.getItem(4);
		}
		else if(wType == WeaponType.Magic){
			item = equipment.getItem(8);
		}
		
		try{
			Material type = item.getType();
	
			if(type.name().toLowerCase().contains("sword")){
				skillLevel = profile.Fencing;
				damage += skillLevel / 15;
			}
			else if(type.equals(Material.AIR)){
				skillLevel = profile.Unarmed;
				damage += 3 + (int)(skillLevel / 10.0);
			}
			else if(type.name().toLowerCase().contains("axe")){
				skillLevel = profile.Marauding;
				damage += skillLevel / 15;
			}
			else if(type.name().toLowerCase().contains("spade")){
				skillLevel = profile.Bludgeoning;
				damage += skillLevel / 15;
			}
			else if(type.equals(Material.BOW)){
				skillLevel = profile.Marksmanship;
				damage += skillLevel / 15;
			}
			else if(type.equals(Material.STICK)){
				skillLevel = profile.Staves;
				damage += skillLevel / 15;
			}
			else if(type.equals(Material.BLAZE_ROD)){
				skillLevel = profile.Staves;
				damage += skillLevel / 15;
			}
			
			if(item.hasItemMeta()){
				ItemMeta im = item.getItemMeta();
				List<String> lore = im.getLore();
				try{
					if(lore.isEmpty()){
						
					}
					else{
						for(String line : lore){
							line = ChatColor.stripColor(line);
							if(line.contains("Critical")){
								WeaponCritical = Integer.parseInt(line.replace("Critical : ", "").trim());
								critFound = true;
							}
							else if(line.contains("Level")){
								line = line.replace("Req", "").trim();
								Integer level = Integer.parseInt(line.replace("Level : ", ""));
								if(Synced){
									if(this.syncedLevel < level){
										owner.sendMessage("You are not a high enough level to equip " + im.getDisplayName());
										return 0;
									}
								}
								else{
									if(this.Level < level){
										owner.sendMessage("You are not a high enough level to equip " + im.getDisplayName());
										return 0;
									}
								}
							}
							else if(line.contains("Skill")){	
								Integer skill = Integer.parseInt(line.replace("Skill : ", ""));
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Bludgeoning")){
								Integer skill = Integer.parseInt(line.replace("Req Bludgeoning : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Fencing")){
								Integer skill = Integer.parseInt(line.replace("Req Fencing : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Marksmanship")){
								Integer skill = Integer.parseInt(line.replace("Req Marksmanship : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Marauding")){
								Integer skill = Integer.parseInt(line.replace("Req Marauding : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Staves")){
								Integer skill = Integer.parseInt(line.replace("Req Staves : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.startsWith("Damage")){
								if(wType == WeaponType.Melee || wType == WeaponType.Ranged){
									damage += Integer.parseInt(line.replace("Damage : ", "").trim());
								}
							}
							else if(line.startsWith("Magic Damage")){
								if(wType == WeaponType.Magic){
									damage += Integer.parseInt(line.replace("Magic Damage : ", "").trim());
								}
							}
							else if(line.contains("Lifesteal")){
								WeaponLS  = Integer.parseInt(line.replace("Lifesteal : ", "").trim());
							}
						}
					}
				}
				catch(NullPointerException e){
					//
				}
			}
			
			if(!critFound){
				WeaponCritical = 0;
			}
		}
		catch(NullPointerException e){
			return 1;
		}
		
		return damage;
	}
	
	public Integer checkWeapon(AttackType Atype){
		Integer damage = 0;
		boolean critFound = false;
		
		if(profile == null){
			profile = cm.getSkills(owner);
		}
		
		Integer skillLevel = 0;
		
		ItemStack item = owner.getItemInHand();
		Material type = item.getType();
		if(type.name().toLowerCase().contains("sword")){
			skillLevel = profile.Fencing;
			damage += skillLevel / 15;
		}
		else if(type.equals(Material.AIR)){
			skillLevel = profile.Unarmed;
			damage += 3 + (int)(skillLevel / 10.0);
		}
		else if(type.name().toLowerCase().contains("axe")){
			skillLevel = profile.Marauding;
			damage += skillLevel / 15;
		}
		else if(type.name().toLowerCase().contains("spade")){
			skillLevel = profile.Bludgeoning;
			damage += skillLevel / 15;
		}
		else if(type.equals(Material.BOW)){
			skillLevel = profile.Marksmanship;
			damage += skillLevel / 15;
		}
		else if(type.equals(Material.STICK)){
			skillLevel = profile.Staves;
			damage += skillLevel / 15;
		}
		else if(type.equals(Material.BLAZE_ROD)){
			skillLevel = profile.Staves;
			damage += skillLevel / 15;
		}
		
		if(item.hasItemMeta()){
			ItemMeta im = item.getItemMeta();
			List<String> lore = im.getLore();
			try{
				if(lore.isEmpty()){
					
				}
				else{
					for(String line : lore){
						try{
							line = ChatColor.stripColor(line);
							if(line.contains("Critical")){
								WeaponCritical = Integer.parseInt(line.replace("Critical : ", "").trim());
								critFound = true;
							}
							else if(line.contains("Level")){
								line = line.replace("Req", "").trim();
								Integer level = Integer.parseInt(line.replace("Level : ", ""));
								if(Synced){
									if(this.syncedLevel < level){
										owner.sendMessage("You are not a high enough level to equip " + im.getDisplayName());
										return 0;
									}
								}
								else{
									if(this.Level < level){
										owner.sendMessage("You are not a high enough level to equip " + im.getDisplayName());
										return 0;
									}
								}
							}
							else if(line.contains("Skill")){	
								Integer skill = Integer.parseInt(line.replace("Skill : ", ""));
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Bludgeoning")){
								Integer skill = Integer.parseInt(line.replace("Req Bludgeoning : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Fencing")){
								Integer skill = Integer.parseInt(line.replace("Req Fencing : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Marksmanship")){
								Integer skill = Integer.parseInt(line.replace("Req Marksmanship : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Marauding")){
								Integer skill = Integer.parseInt(line.replace("Req Marauding : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.contains("Staves")){
								Integer skill = Integer.parseInt(line.replace("Req Staves : ", "").trim());
								if(skill > skillLevel){
									owner.sendMessage(ChatColor.RED + "You aren't skilled enough for this item..." + im.getDisplayName());
									return 0;
								}
							}
							else if(line.startsWith("Damage")){
								if(Atype.equals(AttackType.Physical)){
									damage += Integer.parseInt(line.replace("Damage : ", "").trim());
								}
							}
							else if(line.startsWith("Magic Damage")){
								if(Atype.equals(AttackType.Magic)){
									damage += Integer.parseInt(line.replace("Magic Damage : ", "").trim());
								}
							}
							else if(line.contains("Lifesteal")){
								WeaponLS  = Integer.parseInt(line.replace("Lifesteal : ", "").trim());
							}
						}
						catch(NumberFormatException e){
							//PASS
						}
					}
				}
			}
			catch(NullPointerException e){
				//
			}
		}
		
		if(!critFound){
			WeaponCritical = 0;
		}
		
		return damage;
	}
	
	public double GetRangedAttackDamage(){
		double Damage = 1;
		
		Damage += RangedBonus;
		
		if(owner.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)){
			Damage += (Level / 10) + 3;
		}
		
		Damage += checkWeapon(AttackType.Physical);
		
		Damage += passives.get(PassiveType.PureDamage);
		
		if(checkCritical()){
			Damage = (Damage * 1.5);
			owner.sendMessage(ChatColor.GREEN + "Critical Strike!");
		}
		
		if(WeaponLS > 0){
			float ratio = (float) ((float)WeaponLS / 100.0);
			Integer lifestolen = (int) (ratio * Damage);
			heal(lifestolen);
		}
		
		return Damage;
	}
	
	public float getAttack(){
		float attack = Strength;
		attack += passives.get(PassiveType.Attack);
		return attack;
	}
	public float getMagicAttack(){
		float attack = Intelligence;
		attack += passives.get(PassiveType.MagicAttack);
		return attack;
	}

	public void resetPassives(){
		for(PassiveType p : PassiveType.values()){
			passives.put(p, 0);
		}
	}

	public boolean inParty(Character cha){
		if(currentParty == null){
			return false;
		}
		else{
			if(currentParty.getCharacters().contains(cha)){
				return true;
			}
			else{
				return false;
			}
		}
	}
	
	public boolean playerInParty(Player player){
		if(currentParty == null){
			return false;
		}
		else{
			Character cha = cm.getCharacter(player);
			if(currentParty.getCharacters().contains(cha)){
				return true;
			}
			else{
				return false;
			}
		}
	}
	
	public float getRangedAttack(){
		float attack = Dexterity;
		attack += passives.get(PassiveType.Attack);
		return attack;
	}
	
	public float getMagicDefense(){
		float Totaldefense = 0;
		ItemStack chest = owner.getEquipment().getChestplate();
		ItemStack legs = owner.getEquipment().getLeggings();
		ItemStack boots = owner.getEquipment().getBoots();
		ItemStack helm = owner.getEquipment().getHelmet();
		Integer armor = 0;
		if(chest != null){
			if(chest.hasItemMeta()){
				for(String line : chest.getItemMeta().getLore()){
					if(line.contains("Magic Armor")){
						line = line.replace("Magic Armor : ", "");
						armor += Integer.parseInt(ChatColor.stripColor(line.trim()));
					}
				}
			}
		}
		if(legs != null){
			if(legs.hasItemMeta()){
				for(String line : legs.getItemMeta().getLore()){
					if(line.contains("Magic Armor")){
						line = line.replace("Magic Armor : ", "");
						armor += Integer.parseInt(ChatColor.stripColor(line.trim()));
					}
				}
			}
		}
		if(boots != null){
			if(boots.hasItemMeta()){
				for(String line : boots.getItemMeta().getLore()){
					if(line.contains("Magic Armor")){
						line = line.replace("Magic Armor : ", "");
						armor += Integer.parseInt(ChatColor.stripColor(line.trim()));
					}
				}
			}
		}
		if(helm != null){
			if(helm.hasItemMeta()){
				for(String line : helm.getItemMeta().getLore()){
					if(line.contains("Magic Armor")){
						line = line.replace("Magic Armor : ", "");
						armor += Integer.parseInt(ChatColor.stripColor(line.trim()));
					}
				}
			}
		}
		float Fulldefense = (float) armor;
		Fulldefense += passives.get(PassiveType.MagicDefense);
		Totaldefense = MagicDefense + Fulldefense;
		return Totaldefense;
	}
		
	public WeaponType getWeaponType(){
		ItemStack held = owner.getItemInHand();
		if(held.hasItemMeta()){
			String type = held.getType().name().toLowerCase();
			if(type.contains("sword")){
				return WeaponType.Sword;
			}
			else if(type.contains("axe")){
				return WeaponType.Axe;
			}
			else if(type.contains("spade")){
				return WeaponType.Mace;
			}
			else if(type.contains("bow")){
				return WeaponType.Bow;
			}
			else if(held.getType() == Material.STICK){
				return WeaponType.Wand;
			}
			else if(held.getType() == Material.BLAZE_ROD){
				return WeaponType.Staff;
			}
		}
		else if(held.getType() == Material.AIR){
			return WeaponType.Unarmed;
		}
		return WeaponType.Misc;
	}

	public void setPassives(HashMap<PassiveType, Integer> passives) {
		this.passives = passives;
		//update();
	}
	
	public void addBuff(Buff buff, Integer value){
		if(this.Buffs.containsKey(buff)){
			this.removeBuff(buff);
		}
		
		Buffs.put(buff, buff.getDuration());
		this.BuffValues.put(buff.getName(), value);
		sendMessage("You have gained the effect " + buff.getName());
		
		
	}
	
	public boolean hasBuff(Buff buff){
		for(String b : this.BuffValues.keySet()){
			if(b.equals(buff.getName())){
				return true;
			}
		}
		return false;
	}
	
	public void removeBuff(Buff buff){
		Integer value = this.BuffValues.get(buff.getName());
		PassiveType type = buff.getType();
		Integer base = this.passives.get(type);
		
		this.BuffValues.remove(buff.getName());
		
		base -= value;
		this.passives.put(type, base);
	}
	
	public void BuffDuration(){
		ArrayList<Buff> expired = new ArrayList<Buff>();
		for(Buff buff : Buffs.keySet()){
			Integer duration = Buffs.get(buff);
			duration -= 1;
			if(duration <= 0){
				expired.add(buff);
			}
			else{
				if(duration == 5){
					sendMessage(buff.getName() + ChatColor.GREEN + " is going to wear off soon...");
				}
				Buffs.put(buff, duration);
			}
		}
		for(Buff buff : expired){
			Buffs.remove(buff);
			this.removeBuff(buff);
			sendMessage(buff.getName() + ChatColor.RED + " has wore off...");
		}
	}
	
	public void setRace(Race race, boolean deduct){
		if(deduct){
			if(RaceToken >= 1){
				try{
					if(race != null){
						this.race = race;
						race.create(this);
						this.CurrentSkillPoints = this.TotalSkillPoints;
						
						RaceToken -= 1;
						
						owner.sendMessage(ChatColor.GREEN + "Your race has been set to " + ChatColor.GOLD + race.getName());
					}
					else{
						owner.sendMessage(ChatColor.RED + "There was an error setting your race...");
					}
				}
				catch(NullPointerException e){
					owner.sendMessage(ChatColor.RED + "There was an error setting your race...");
				}
			}
		}
		else{
			this.race = race;
		}
		
	}
	
	public Race getRace(){
		return race;
	}

	public void checkCast(){
		if(binds.get(owner.getInventory().getHeldItemSlot()) == null){
			return;
		}
		if(binds.get(owner.getInventory().getHeldItemSlot()) != ""){
			
			String skill = binds.get(owner.getInventory().getHeldItemSlot());
			cast(skill);
		}
	}
	
	public void addBind(int heldItemSlot, String[] args, boolean silent) {
		// TODO Auto-generated method stub
		String skillname = "";
		for(String word : args){
			skillname += word + " ";
		}
		skillname = skillname.trim();
		
		Skill skill = cm.getMain().sm.getSkillByName(skillname);
		if(active_skills.contains(skill)){
			binds.put((Integer)heldItemSlot, skillname);
			if(!silent){
				owner.sendMessage(ChatColor.GREEN + "You have bound " + skillname + " to Slot " + (heldItemSlot + 1));
			}
		}
		else{
			if(skillname == ""){
				binds.remove((Integer)heldItemSlot);
				owner.sendMessage(ChatColor.RED + "You removed the bind in slot, " + (heldItemSlot + 1));
			}
			else{
				owner.sendMessage(ChatColor.RED + "You don't have access to the skill, " + skillname);
			}
			
			
		}
	}

	public void showBinds(){
		Inventory bindInv = Bukkit.createInventory(null, 9, ChatColor.YELLOW + "Skill Binds");
		for(Integer bind : this.binds.keySet()){
			String name = binds.get(bind);
			try{
				Skill skill = cm.getMain().sm.getSkillByName(name);
				if(skill != null){
					StatType type = skill.getMainStat();
					Wool wool = new Wool();
					if(type == StatType.STRENGTH){
						wool.setColor(DyeColor.RED);
					}
					else if(type == StatType.CONSTITUTION){
						wool.setColor(DyeColor.ORANGE);
					}
					else if(type == StatType.DEXTERITY){
						wool.setColor(DyeColor.GREEN);
					}
					else if(type == StatType.AGILITY){
						wool.setColor(DyeColor.LIME);
					}
					else if(type == StatType.WISDOM){
						wool.setColor(DyeColor.BLUE);
					}
					else if(type == StatType.INTELLIGENCE){
						wool.setColor(DyeColor.PURPLE);
					}
					ItemStack item = wool.toItemStack(1);
					ItemMeta meta = item.getItemMeta();
					meta.setDisplayName(skill.getName());
					//meta.setLore(skill.getDescription());
					item.setItemMeta(meta);
					bindInv.setItem(bind, item);
				}
			}
			catch(NullPointerException e){
				bindInv.setItem(bind, new ItemStack(Material.GLASS, 1));
			}
		}
		owner.openInventory(bindInv);
	}
	
	public HashMap<Integer, String> getBindings() {
		// TODO Auto-generated method stub
		return binds;
	}

	public void putBindings(HashMap<Integer, String> bindings) {
		binds = bindings;
		
	}
	
	public void removeBind(int heldItemSlot) {
		binds.remove((Integer)heldItemSlot);
		owner.sendMessage(ChatColor.RED + "You removed the bind in slot, " + (heldItemSlot + 1));
	}

	public void sendSpellBook() {
		ArrayList<Skill> strASkills = new ArrayList<Skill>();
		ArrayList<Skill> conASkills = new ArrayList<Skill>();
		ArrayList<Skill> dexASkills = new ArrayList<Skill>();
		ArrayList<Skill> agiASkills = new ArrayList<Skill>();
		ArrayList<Skill> wisASkills = new ArrayList<Skill>();
		ArrayList<Skill> intASkills = new ArrayList<Skill>();
		
		ArrayList<Skill> strPSkills = new ArrayList<Skill>();
		ArrayList<Skill> conPSkills = new ArrayList<Skill>();
		ArrayList<Skill> dexPSkills = new ArrayList<Skill>();
		ArrayList<Skill> agiPSkills = new ArrayList<Skill>();
		ArrayList<Skill> wisPSkills = new ArrayList<Skill>();
		ArrayList<Skill> intPSkills = new ArrayList<Skill>();
		
		ArrayList<Skill> racial = new ArrayList<Skill>();
		
		for(Skill s : this.active_skills){
			if(s.getMainStat() == StatType.STRENGTH){
				if(s.getName().contains(ChatColor.GRAY + "")){
					racial.add(s);
				}
				else{
					strASkills.add(s);
				}
			}
			else if(s.getMainStat() == StatType.CONSTITUTION){
				conASkills.add(s);
			}
			else if(s.getMainStat() == StatType.DEXTERITY){
				dexASkills.add(s);
			}
			else if(s.getMainStat() == StatType.AGILITY){
				agiASkills.add(s);
			}
			else if(s.getMainStat() == StatType.WISDOM){
				wisASkills.add(s);
			}
			else if(s.getMainStat() == StatType.INTELLIGENCE){
				intASkills.add(s);
			}
		}
		
		for(Skill s : this.passive_skills){
			if(s.getMainStat() == StatType.STRENGTH){
				//System.out.println(ChatColor.getLastColors(s.getName()) + " vs " + ChatColor.GRAY.toString());
				if(s.getName().contains(ChatColor.GRAY + "")){
					racial.add(s);
				}
				else{
					strPSkills.add(s);
				}
			}
			else if(s.getMainStat() == StatType.CONSTITUTION){
				conPSkills.add(s);
			}
			else if(s.getMainStat() == StatType.DEXTERITY){
				dexPSkills.add(s);
			}
			else if(s.getMainStat() == StatType.AGILITY){
				agiPSkills.add(s);
			}
			else if(s.getMainStat() == StatType.WISDOM){
				wisPSkills.add(s);
			}
			else if(s.getMainStat() == StatType.INTELLIGENCE){
				intPSkills.add(s);
			}
		}
		
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK, 1);
		BookMeta bm = (BookMeta) book.getItemMeta();
		bm.setDisplayName(ChatColor.GOLD + "Spell Book");
		
		if(!racial.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.GRAY + "Racial Bonuses");
			for(Skill s : racial){
				bm.addPage(s.getName() + ChatColor.RESET + " \n  \n" + s.getDescription());
			}
		}
		
		if(!strASkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.RED + "Strength  \n  Actives");
			for(Skill s : strASkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n  \n" + s.getDescription());
			}
		}
		if(!strPSkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.RED + "Strength  \n  Passives");
			for(Skill s : strPSkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		
		if(!conASkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.GOLD + "Constitution  \n  Actives");
			for(Skill s : conASkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		if(!conPSkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.GOLD + "Constitution  \n  Passives");
			for(Skill s : conPSkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		
		if(!dexASkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.DARK_GREEN + "Dexterity  \n  Actives");
			for(Skill s : dexASkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		if(!dexPSkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.DARK_GREEN + "Dexterity  \n  Passives");
			for(Skill s : dexPSkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		
		if(!agiASkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.GREEN + "Agility  \n  Actives");
			for(Skill s : agiASkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		if(!agiPSkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.GREEN + "Agility  \n  Passives");
			for(Skill s : agiPSkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		
		if(!wisASkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.BLUE + "Wisdom  \n  Actives");
			for(Skill s : wisASkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		if(!wisPSkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.BLUE + "Wisdom  \n  Passives");
			for(Skill s : wisPSkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		
		if(!intASkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.DARK_PURPLE + "Intelligence  \n  Actives");
			for(Skill s : intASkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		if(!intPSkills.isEmpty()){
			bm.addPage("\n \n \n " + ChatColor.DARK_PURPLE + "Intelligence  \n  Passives");
			for(Skill s : intPSkills){
				bm.addPage(s.getName() + ChatColor.RESET + " \n \n" + s.getDescription());
			}
		}
		book.setItemMeta(bm);
		
		owner.getInventory().addItem(book);
		owner.updateInventory();
	}

	public void cloak(Integer value) {
		if(this.isCloaked){
			Bukkit.getScheduler().cancelTask(cloakTask);
		}
		else{
			for(Player p : Bukkit.getOnlinePlayers()){
				p.hidePlayer(owner);
			}
			owner.sendMessage(ChatColor.GRAY + "You have been concealed...");
		}
		
		isCloaked = true;
		cloakTask = Bukkit.getScheduler().scheduleSyncDelayedTask(this.cm.getMain(), new Runnable(){

			@Override
			public void run() {
				removeCloak();
			}
			
		}, (long)value * 20);
		
	}
	
	public void removeCloak(){
		if(isCloaked){
			Bukkit.getScheduler().cancelTask(cloakTask);
			for(Player p : Bukkit.getOnlinePlayers()){
				p.showPlayer(owner);
			}
			isCloaked = false;
			owner.sendMessage(ChatColor.GRAY + "You are now revealed...");
		}
	}
	
	public boolean isCloaked(){
		return isCloaked;
	}
	
}
