package me.Lorinth.RPGE.Characters;


import java.util.ArrayList;

import me.Lorinth.RPGE.Main.RpgEnhancedMain;
import me.Lorinth.RPGE.SkillAPI.StatType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

public class PlayerMenuPage implements Listener{

	private Inventory inv;
	private ItemStack str, con, dex, agi, inte, wis, skillp;
	private Character cha;

	public PlayerMenuPage(Character character){
		cha = character;

		String name = character.owner.getName();
		inv = Bukkit.getServer().createInventory(null, 18, name + "'s Core Stats");

		ItemStack str = createItem("Strength", character);
		ItemStack con = createItem("Constitution", character);
		ItemStack dex = createItem("Dexterity", character);
		ItemStack agi = createItem("Agility", character);
		ItemStack wis = createItem("Wisdom", character);
		ItemStack inte = createItem("Intelligence", character);
		ItemStack skillp = createItem("SkillP", character);

		ItemStack raceChanges = new ItemStack(Material.DIAMOND, character.RaceToken);
		ItemMeta im = raceChanges.getItemMeta();
		im.setDisplayName(ChatColor.AQUA + "Race Changes");
		raceChanges.setItemMeta(im);		
		
		inv.setItem(0, str);
		inv.setItem(1, con);
		inv.setItem(2, dex);
		inv.setItem(3, agi);
		inv.setItem(4, wis);
		inv.setItem(5, inte);
		inv.setItem(8, skillp);
		
		inv.setItem(9, raceChanges);
	}

	public void handleEvent(ItemStack item, RpgEnhancedMain main){
		Material mat = item.getType();
		if(mat == null){
			return;
		}
		
		switch(mat){
		case WOOL:
			boolean change = false;
			ItemMeta im = item.getItemMeta();
			if(im.getDisplayName().contains("Strength")){
				if(cha.CurrentSkillPoints >= 1){
					cha.addStat(StatType.STRENGTH);
					str = createItem("Strength", cha);
					inv.setItem(0, str);

					change = true;
				}
			}
			else if(im.getDisplayName().contains("Constitution")){
				if(cha.CurrentSkillPoints >= 1){
					cha.addStat(StatType.CONSTITUTION);
					con = createItem("Constitution", cha);
					inv.setItem(1, con);
					change = true;
				}
			}
			else if(im.getDisplayName().contains("Dexterity")){
				if(cha.CurrentSkillPoints >= 1){
					cha.addStat(StatType.DEXTERITY);
					dex = createItem("Dexterity", cha);
					inv.setItem(2, dex);
					change = true;
				}
			}
			else if(im.getDisplayName().contains("Agility")){
				if(cha.CurrentSkillPoints >= 1){
					cha.addStat(StatType.AGILITY);
					agi = createItem("Agility", cha);
					inv.setItem(3, agi);
					change = true;
				}
			}
			else if(im.getDisplayName().contains("Wisdom")){
				if(cha.CurrentSkillPoints >= 1){
					cha.addStat(StatType.WISDOM);
					wis = createItem("Wisdom", cha);
					inv.setItem(4, wis);
					change = true;
				}
			}
			else if(im.getDisplayName().contains("Intelligence")){
				if(cha.CurrentSkillPoints >= 1){
					cha.addStat(StatType.INTELLIGENCE);
					inte = createItem("Intelligence", cha);
					inv.setItem(5, inte);
					change = true;
				}
			}

			if(change){
				skillp = createItem("SkillP", cha);
				inv.setItem(8, skillp);
			}

			break;
		case DIAMOND:
			main.cm.getRaceManager().openRaceWindow(cha.owner);
			break;
		default:
			break;
		}
	}

	private ItemStack createItem(String name, Character cha)
	{
		ItemStack i = new ItemStack(Material.WOOL, 1);
		ItemMeta im = i.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();

		Wool wool;

		switch(name){
		case "Strength":
			wool = new Wool();
			wool.setColor(DyeColor.RED);
			i = wool.toItemStack(cha.Strength);

			im.setDisplayName(ChatColor.RED + name + " : " + cha.Strength);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.RED + "Melee Bonus Damage : " + cha.MeleeBonus);
			lore.add(ChatColor.RED + "Health Bonus : " + cha.Strength.intValue() / 10);
			lore.add(ChatColor.RED + "Stamina Bonus : " + cha.Strength);
			break;
		case "Constitution":
			wool = new Wool();
			wool.setColor(DyeColor.ORANGE);
			i = wool.toItemStack(cha.Constitution);

			im.setDisplayName(ChatColor.GOLD + name + " : " + cha.Constitution);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			//Integer defense = Integer.valueOf(cha.Constitution.intValue() / 2 + cha.Constitution.intValue() / 5 + 10);
			lore.add(ChatColor.GOLD + "Defense : " + cha.PhysicalDefense);
			lore.add(ChatColor.GOLD + "Health Bonus : " + cha.Constitution.intValue() / 4);
			lore.add(ChatColor.GOLD + "Stamina Bonus : " + cha.Constitution.intValue() / 2);
			break;
		case "Dexterity":
			wool = new Wool();
			wool.setColor(DyeColor.GREEN);
			i = wool.toItemStack(cha.Dexterity);

			im.setDisplayName(ChatColor.DARK_GREEN + name + " : " + cha.Dexterity);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.DARK_GREEN + "Ranged Bonus Damage : " + cha.RangedBonus);
			lore.add(ChatColor.DARK_GREEN + "Critical : " + (cha.Dexterity.intValue() / 8 + 1));
			break;
		case "Agility":
			wool = new Wool();
			wool.setColor(DyeColor.LIME);
			i = wool.toItemStack(cha.Agility);

			im.setDisplayName(ChatColor.GREEN + name + " : " + cha.Agility);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.GREEN + "Critical : " + cha.Agility.intValue() / 12);
			lore.add(ChatColor.GREEN + "Dodge : " + cha.Dodge);
			break;
		case "Wisdom":
			wool = new Wool();
			wool.setColor(DyeColor.BLUE);
			i = wool.toItemStack(cha.Wisdom);

			im.setDisplayName(ChatColor.BLUE + name + " : " + cha.Wisdom);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.BLUE + "Magic Defense : " + cha.MagicDefense);
			lore.add(ChatColor.BLUE + "Mana Bonus : " + cha.Wisdom);
			break;
		case "Intelligence":
			wool = new Wool();
			wool.setColor(DyeColor.PURPLE);
			i = wool.toItemStack(cha.Intelligence);

			im.setDisplayName(ChatColor.DARK_PURPLE + name + " = " + cha.Intelligence);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.DARK_PURPLE + "Magic Bonus Damage : " + cha.MagicBonus);
			lore.add(ChatColor.DARK_PURPLE + "Mana : " + cha.Intelligence);
			break;
		case "SkillP":
			i.setType(Material.NETHER_STAR);
			im.setDisplayName(ChatColor.WHITE + "Skill Points");
			lore.add("Available Skill Points : " + cha.CurrentSkillPoints);
			i.setAmount(cha.CurrentSkillPoints);
			break;
		case "ResetPoint":
			i.setType(Material.EMERALD);
			im.setDisplayName(ChatColor.YELLOW + "Reset Points");

			lore.add(ChatColor.WHITE + "This will return your stat points back to you!");
			break;
		}
		im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}

	public void show(Player player){
		player.openInventory(inv);
	}
}
