package me.Lorinth.RPGE.Characters;


import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

public class SkillsPage implements Listener{

	private Inventory inv;
	public SkillsPage(Character character){
		String name = character.owner.getName();
		inv = Bukkit.getServer().createInventory(null, 18, name + "'s Core Stats");

		ItemStack str = createItem("Strength", character);
		ItemStack con = createItem("Constitution", character);
		ItemStack dex = createItem("Dexterity", character);
		ItemStack agi = createItem("Agility", character);
		ItemStack wis = createItem("Wisdom", character);
		ItemStack inte = createItem("Intelligence", character);
		ItemStack skillp = createItem("SkillP", character);

		ItemStack raceChanges = new ItemStack(Material.DIAMOND, character.RaceToken);
		ItemMeta im = raceChanges.getItemMeta();
		im.setDisplayName(ChatColor.AQUA + "Race Changes");
		raceChanges.setItemMeta(im);
		
		inv.setItem(0, str);
		inv.setItem(1, con);
		inv.setItem(2, dex);
		inv.setItem(3, agi);
		inv.setItem(4, wis);
		inv.setItem(5, inte);
		inv.setItem(8, skillp);
		
		inv.setItem(9, raceChanges);
	}

	private ItemStack createItem(String name, Character cha)
	{
		ItemStack i = new ItemStack(Material.WOOL, 1);
		ItemMeta im = i.getItemMeta();
		ArrayList<String> lore = new ArrayList<String>();

		Wool wool;

		switch(name){
		case "Strength":
			wool = new Wool();
			wool.setColor(DyeColor.RED);
			i = wool.toItemStack(1);

			im.setDisplayName(ChatColor.RED + name + " : " + cha.Strength);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.RED + "Melee Bonus Damage : " + cha.MeleeBonus);
			lore.add(ChatColor.RED + "Health Bonus : " + cha.Strength.intValue() / 10);
			lore.add(ChatColor.RED + "Stamina Bonus : " + cha.Strength);
		case "Constitution":
			wool = new Wool();
			wool.setColor(DyeColor.ORANGE);
			i = wool.toItemStack(1);

			im.setDisplayName(ChatColor.GOLD + name + " : " + cha.Constitution);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			//Integer defense = Integer.valueOf(cha.Constitution.intValue() / 2 + cha.Constitution.intValue() / 5 + 10);
			lore.add(ChatColor.GOLD + "Defense : " + cha.PhysicalDefense);
			lore.add(ChatColor.GOLD + "Health Bonus : " + cha.Constitution.intValue() / 4);
			lore.add(ChatColor.GOLD + "Stamina Bonus : " + cha.Constitution.intValue() / 2);
		case "Dexterity":
			wool = new Wool();
			wool.setColor(DyeColor.GREEN);
			i = wool.toItemStack(1);

			im.setDisplayName(ChatColor.DARK_GREEN + name + " : " + cha.Dexterity);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.DARK_GREEN + "Ranged Bonus Damage : " + cha.RangedBonus);
			lore.add(ChatColor.DARK_GREEN + "Critical : " + (cha.Dexterity.intValue() / 8 + 1));
		case "Agility":
			wool = new Wool();
			wool.setColor(DyeColor.LIME);
			i = wool.toItemStack(1);

			im.setDisplayName(ChatColor.GREEN + name + " : " + cha.Agility);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.GREEN + "Critical : " + cha.Agility.intValue() / 12);
			lore.add(ChatColor.GREEN + "Dodge : " + cha.Dodge);
		case "Wisdom":
			wool = new Wool();
			wool.setColor(DyeColor.BLUE);
			i = wool.toItemStack(1);

			im.setDisplayName(ChatColor.BLUE + name + " : " + cha.Wisdom);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.BLUE + "Magic Defense : " + cha.MagicDefense);
			lore.add(ChatColor.BLUE + "Mana Bonus : " + cha.Wisdom);
		case "Intelligence":
			wool = new Wool();
			wool.setColor(DyeColor.PURPLE);
			i = wool.toItemStack(1);

			im.setDisplayName(ChatColor.DARK_PURPLE + name + " = " + cha.Intelligence);

			lore.add(ChatColor.UNDERLINE + "Bonuses");
			lore.add(ChatColor.DARK_PURPLE + "Magic Bonus Damage : " + cha.MagicBonus);
			lore.add(ChatColor.DARK_PURPLE + "Mana : " + cha.Intelligence);
		case "SkillP":
			i.setType(Material.NETHER_STAR);
			im.setDisplayName(ChatColor.WHITE + "Skill Points");
			lore.add("Available Skill Points : " + cha.CurrentSkillPoints);
		case "ResetPoint":
			i.setType(Material.EMERALD);
			im.setDisplayName(ChatColor.YELLOW + "Reset Points");

			lore.add(ChatColor.WHITE + "This will return your stat points back to you!");
		}
		im.setLore(lore);
		i.setItemMeta(im);
		return i;
	}

	public void show(Player player){
		player.openInventory(inv);
	}

	public void onInventoryClick(InventoryClickEvent e){
		if(!e.getInventory().equals(inv)) return;
		if(e.getCurrentItem().getItemMeta().getDisplayName().contains("Strength")){
//
		}
	}
}
