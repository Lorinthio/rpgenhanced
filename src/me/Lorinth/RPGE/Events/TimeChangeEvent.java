package me.Lorinth.RPGE.Events;

import org.bukkit.World;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class TimeChangeEvent extends Event{

	private World world;
	private DayTime daytime;
	
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();
	
	public TimeChangeEvent(World world, DayTime daytime) {
        this.world = world;
        this.daytime = daytime;
    }
 
	public World getWorld(){
		return world;
	}
	
	public DayTime getDayTime(){
		return daytime;
	}
	
    public boolean isCancelled() {
        return cancelled;
    }
 
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
	
}
