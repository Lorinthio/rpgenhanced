package me.Lorinth.RPGE.Events;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.Skill;
import me.Lorinth.RPGE.SkillAPI.Vital;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class HealingEvent extends Event{

	private Character caster;
	private Character target;
	private Integer amount;
	private Vital vital;
	private Skill skill;
	
	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();
	
	public HealingEvent(Character character, Character cha, Vital vital, Integer amount, Skill skill) {
        this.caster = character;
        this.target = cha;
        this.vital = vital;
        this.amount = amount;
        this.skill = skill;
    }
 
    public Character getCaster() {
        return caster;
    }
    
    public Vital getVitalType(){
    	return vital;
    }
    
    public Character getTarget(){
    	return target;
    }
    
    public Integer getAmount(){
    	return amount;
    }
    
    public Skill getSkill(){
    	return skill;
    }
 
    public boolean isCancelled() {
        return cancelled;
    }
 
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
	
}
