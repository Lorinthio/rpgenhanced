package me.Lorinth.RPGE.Events;

import java.util.ArrayList;

import me.Lorinth.RPGE.Characters.Character;
import me.Lorinth.RPGE.SkillAPI.Skill;

import org.bukkit.entity.Entity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SpellCastEvent extends Event{

private static final HandlerList handlers = new HandlerList();
	
	private Character character;
	private Skill skill;
	private ArrayList<Entity> targets;

	private boolean cancelled;
	
	public SpellCastEvent(Character cha, Skill skill, ArrayList<Entity> targets) {
        this.character = cha;
        this.targets = targets;
        this.skill = skill;
    }
 
    public Character getCharacter(){
    	return character;
    }
    
    public ArrayList<Entity> getTargets(){
    	return targets;
    }
 
    public Skill getSkill(){
    	return skill;
    }
    
    public boolean isCancelled() {
        return cancelled;
    }
 
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
	
}
