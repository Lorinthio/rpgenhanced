package me.Lorinth.RPGE.Events;

import me.Lorinth.RPGE.Characters.Character;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class RewardEvent extends Event{

private static final HandlerList handlers = new HandlerList();
	
	private Character character;
	private Integer Gold;
	private Integer Exp;

	private boolean cancelled;
	
	public RewardEvent(Character cha, Integer gold, Integer exp) {
        this.character = cha;
        Gold = gold;
        Exp = exp;
    }
 
    public Character getCharacter(){
    	return character;
    }
    
    public Integer getGold(){
    	return Gold;
    }
 
    public Integer getExp(){
    	return Exp;
    }
    
    public boolean isCancelled() {
        return cancelled;
    }
 
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
	
}
