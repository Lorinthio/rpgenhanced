package me.Lorinth.RPGE.Items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.avaje.ebeaninternal.server.persist.dml.InsertMeta;

public class Weapon extends Gear{

	private Material material;
	public Integer level;
	private ItemGenerator IG;
	public String name;
	public Integer damage;
	public Integer durability;
	
	public Weapon(String Name, Integer Level, Integer Damage, Material item, Integer dura, ItemGenerator ig) {
		material = item;
		level = Level;
		damage = Damage;
		name = Name;
		durability = dura;
		IG = ig;
	}

	public Material getMaterial(){
		return material;
	}
	
	public ItemStack spawnItem(Object loc, String rarity, ArrayList<Title> titles) {
		ItemStack newItem = new ItemStack(material);
		ItemMeta meta = newItem.getItemMeta();
		
		Integer physDamage = damage;
		Integer magicDamage = damage;
		
		Integer skillL = level;
		Integer copyDura = durability;
		
		Integer value = 2 * level;
		
		//Sockets
		Integer minS = 0;
		Integer maxS = 1;
		
		String rarityLine = "";
		ChatColor rareColor = null;
		
		//Color and name the item
		switch(rarity){
			case("Common"):
				rareColor = ChatColor.GRAY;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Common";
				break;
			case("Uncommon"):
				rareColor = ChatColor.GREEN;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Uncommon";
				physDamage += 1;
				magicDamage += 1;
				skillL = level + 1;
				
				maxS = 2;
				
				copyDura += 75;
				value += 5 + (level / 4);
				
				break;
			case("Rare"):
				rareColor = ChatColor.AQUA;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Rare";
				physDamage += 2;
				magicDamage += 2;
				skillL = level + 2;
				
				maxS = 2;
				
				copyDura += 150;
				value += 10 + (level / 2);
				
				break;
			case("Epic"):
				rareColor = ChatColor.DARK_RED;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Epic";
				physDamage += 3;
				magicDamage += 3;
				skillL = level + 3;
				
				minS = 1;
				maxS = 2;
					
				
				copyDura += 300;
				value += 20 + level;
				
				break;
			case("Legendary"):
				rareColor = ChatColor.DARK_PURPLE;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Legendary";
				physDamage += 4;
				magicDamage += 4;
				skillL = level + 4;
				
				minS = 1;
				maxS = 3;
				
				copyDura += 600;
				value += (int) (30 + (level * 1.5));
				
				break;
			case("Godly"):
				rareColor = ChatColor.GOLD;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Godly";
				physDamage += 6;
				magicDamage += 6;
				skillL = level + 6;
				copyDura += 750;
				
				minS = 1;
				maxS = 4;
				
				value += (int) (45 + (level * 2));
				
				break;
		}
		
		String copiedName = "";
		
		Integer Level = (int)(level * 0.8);
		if(Level < 1){
			Level = 1;
		}
		
		ArrayList<String> lore = new ArrayList<String>();
		
		lore.add(ChatColor.GOLD +      "Req Level : " + Level);
		String skillname = "";
		if(this.material.name().toLowerCase().contains("sword")){
			skillname = "Fencing";
		}
		else if(this.material.name().toLowerCase().contains("axe")){
			skillname = "Marauding";
		}
		else if(this.material.name().toLowerCase().contains("bow")){
			skillname = "Marksmanship";
		}
		else if(this.material.name().toLowerCase().contains("spade")){
			skillname = "Bludgeoning";
		}
		else if(this.material.name().toLowerCase().contains("stick")){
			skillname = "Staves";
		}
		else if(this.material.getId() == 369){
			skillname = "Staves";
		}
		
		lore.add(ChatColor.DARK_AQUA + "Req " + skillname + " : " + skillL);
		lore.add(rarityLine); 
		lore.add(ChatColor.GRAY + "Durability : " + copyDura + "/" + copyDura);
		
		
		
		if(!titles.isEmpty()){
			lore.add("");
			for(Title title : titles){
				if(!copiedName.contains(title.name)){
					copiedName += title.name + " ";
				}
				HashMap<String, Integer> Terms = title.terms;
				//Iterate through the different terms in the title
				for(String key : Terms.keySet()){
					//Make sure its not already in the item, if it is add to the value
					if(key.equals("Damage")){
						physDamage += Terms.get(key);
					}
					else if(key.equals("Magic Damage")){
						magicDamage += Terms.get(key); 

					}
					else{
						boolean Found = false;
						for(String line : lore){
							List<String> words = Arrays.asList(line.split(" "));
							//If the term is the same...
							if(words.get(0).contains(key)){
								//parse to int and add to it...
								Integer number = Integer.parseInt(words.get(words.size() - 1));
								number += Terms.get(key);
								Found = true;
							}
						}
						if(!Found){
							String line = rareColor + key + " : " + Terms.get(key);
							lore.add(line);
						}
					}
				}
			}
		}
		
		if(material.equals(Material.STICK)){
			physDamage /= 3;
		}
		else if(material.equals(Material.BLAZE_ROD)){
			physDamage /= 3;
		}
		else{
			magicDamage = damage / 2;
		}
		
		lore.add(0, ChatColor.DARK_PURPLE + "Magic Damage : " + magicDamage);
		lore.add(0, ChatColor.DARK_RED + "Damage : " + physDamage);
		
		meta.setDisplayName(rareColor + name);
		
		lore = IG.addSockets(minS, maxS, lore);
		
		lore.add("");
		lore.add(ChatColor.GRAY + "Value : " + ChatColor.GOLD + value);
		
		meta.setLore(lore);
		
		newItem.setItemMeta(meta);
		
		if(loc instanceof Location){
			((Location) loc).getWorld().dropItem((Location) loc, newItem);
		}
		else if(loc instanceof Player){
			((Player) loc).getInventory().addItem(newItem);
		}
		
		return newItem;
	}

	
	
}
