package me.Lorinth.RPGE.Items;

import java.util.HashMap;

import org.bukkit.ChatColor;


public class Title {

	public String name;
	public Integer level;
	public HashMap<String, Integer> terms;
	
	public Title(String Name, Integer Level, HashMap<String, Integer> Terms){
		name = Name;
		terms = Terms;
		level = Level;
	}
	
}
