package me.Lorinth.RPGE.Items;

public enum WeaponType {

	Bow, Sword, Axe, Mace, Unarmed, Wand, Staff, Melee, Ranged, Magic, Misc;
	
}
