package me.Lorinth.RPGE.Items;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import me.Lorinth.MobDifficulty.MobDifficulty;
import me.Lorinth.RPGE.Main.RpgEnhancedMain;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemGenerator {

	Random random;
	RpgEnhancedMain plugin;
	ArrayList<Weapon> weapons = new ArrayList<Weapon>();
	ArrayList<Material> weaponTypes = new ArrayList<Material>();
	ArrayList<Material> armorTypes = new ArrayList<Material>();
	ArrayList<Armor> armors = new ArrayList<Armor>();
	ArrayList<Title> weapontitles = new ArrayList<Title>();
	ArrayList<Title> armortitles = new ArrayList<Title>();
	
	ArrayList<SocketGem> gems = new ArrayList<SocketGem>();
	
	public ItemGenerator(RpgEnhancedMain plugin){
		this.plugin = plugin;
		random = new Random();
		makeBasicItems();
	}

	//On entity death this will check if an item is dropped, and what it is
	public void checkEntityDeath(Entity entity){
		Integer level = plugin.getLevel(entity);
		if(entity instanceof Monster){
			Integer dropCheck = random.nextInt((1000) + 1);
			Integer dropChance = 200;
			boolean inDungeon = plugin.inDungeon(entity);
			if(MobDifficulty.getPlugin().isElite(entity)){
				generateItem(level, entity.getLocation(), inDungeon);
				spawnSocketGem(level, entity.getLocation(), null);
			}
			else{
				if(inDungeon){
					dropChance *= 2;
				}
				if(dropCheck <= dropChance){
					generateItem(level, entity.getLocation(), inDungeon);
				}
				dropCheck = random.nextInt((1000) + 1);
				if(dropCheck <= 30){ /// FIX THIS
					spawnSocketGem(level, entity.getLocation(), null);
				}
			}
		}
	}

	public void makeBasicItems(){
		makeWeapons();
		makeArmors();
		makeTitles();
		makeSocketGems();
	}

	@SuppressWarnings("serial")
	public void makeSocketGems(){
		// Offensive
		gems.add(new SocketGem(ChatColor.RED + "Sharpen I", 1, new HashMap<String, Integer>(){{
			put("Damage", 1);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.RED + "Sharpen II", 20, new HashMap<String, Integer>(){{
			put("Damage", 2);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.RED + "Sharpen III", 50, new HashMap<String, Integer>(){{
			put("Damage", 3);
		}}, socket.Weapon));
		
		gems.add(new SocketGem(ChatColor.DARK_GREEN + "Focus I", 1, new HashMap<String, Integer>(){{
			put("Critical", 1);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.DARK_GREEN + "Focus II", 20, new HashMap<String, Integer>(){{
			put("Critical", 2);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.DARK_GREEN + "Focus III", 50, new HashMap<String, Integer>(){{
			put("Critical", 3);
		}}, socket.Weapon));
		
		gems.add(new SocketGem(ChatColor.DARK_PURPLE + "Runic I", 1, new HashMap<String, Integer>(){{
			put("Magic Damage", 1);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.DARK_PURPLE + "Runic II", 20, new HashMap<String, Integer>(){{
			put("Magic Damage", 2);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.DARK_PURPLE + "Runic III", 50, new HashMap<String, Integer>(){{
			put("Magic Damage", 3);
		}}, socket.Weapon));
		
//		gems.add(new SocketGem(ChatColor.DARK_AQUA + "Agility I", 20, new HashMap<String, Integer>(){{
//			put("AttSpeed", 2);
//		}}, socket.Weapon));
//		gems.add(new SocketGem(ChatColor.DARK_AQUA + "Agility II", 50, new HashMap<String, Integer>(){{
//			put("AttSpeed", 3);
//		}}, socket.Weapon));
//		
//		gems.add(new SocketGem(ChatColor.DARK_AQUA + "Drawback I", 20, new HashMap<String, Integer>(){{
//			put("Velocity", 1);
//		}}, socket.Weapon));
//		gems.add(new SocketGem(ChatColor.DARK_AQUA + "Drawback II", 50, new HashMap<String, Integer>(){{
//			put("Velocity", 2);
//		}}, socket.Weapon));
		
		gems.add(new SocketGem(ChatColor.DARK_AQUA + "Drain I", 1, new HashMap<String, Integer>(){{
			put("Lifesteal", 1);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.DARK_AQUA + "Drain II", 20, new HashMap<String, Integer>(){{
			put("Lifesteal", 2);
		}}, socket.Weapon));
		gems.add(new SocketGem(ChatColor.DARK_AQUA + "Drain III", 50, new HashMap<String, Integer>(){{
			put("Lifesteal", 3);
		}}, socket.Weapon));
		
		//Defensive
		gems.add(new SocketGem(ChatColor.GREEN + "Health I", 1, new HashMap<String, Integer>(){{
			put("Health", 1);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.GREEN + "Health II", 20, new HashMap<String, Integer>(){{
			put("Health", 2);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.GREEN + "Health III", 50, new HashMap<String, Integer>(){{
			put("Health", 3);
		}}, socket.Armor));
		
		gems.add(new SocketGem(ChatColor.BLUE + "Mana I", 1, new HashMap<String, Integer>(){{
			put("Mana", 1);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.BLUE + "Mana II", 20, new HashMap<String, Integer>(){{
			put("Mana", 2);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.BLUE + "Mana III", 50, new HashMap<String, Integer>(){{
			put("Mana", 3);
		}}, socket.Armor));
		
		gems.add(new SocketGem(ChatColor.YELLOW + "Stamina I", 1, new HashMap<String, Integer>(){{
			put("Stamina", 1);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.YELLOW + "Stamina II", 20, new HashMap<String, Integer>(){{
			put("Stamina", 2);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.YELLOW + "Stamina III", 50, new HashMap<String, Integer>(){{
			put("Stamina", 3);
		}}, socket.Armor));
		
		gems.add(new SocketGem(ChatColor.YELLOW + "Reinforce I", 1, new HashMap<String, Integer>(){{
			put("Armor", 1);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.YELLOW + "Reinforce II", 20, new HashMap<String, Integer>(){{
			put("Armor", 2);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.YELLOW + "Reinforce III", 50, new HashMap<String, Integer>(){{
			put("Armor", 3);
		}}, socket.Armor));
		
		gems.add(new SocketGem(ChatColor.YELLOW + "Warded I", 1, new HashMap<String, Integer>(){{
			put("Armor", 1);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.YELLOW + "Warded II", 20, new HashMap<String, Integer>(){{
			put("Armor", 2);
		}}, socket.Armor));
		gems.add(new SocketGem(ChatColor.YELLOW + "Warded III", 50, new HashMap<String, Integer>(){{
			put("Armor", 3);
		}}, socket.Armor));
	}
	
	@SuppressWarnings("serial")
	public void makeTitles(){
		//weapons
		weapontitles.add(new Title("Sharpened", 1, new HashMap<String, Integer>(){{
			put("Damage", 1);
		}}));
		weapontitles.add(new Title("Focused", 1, new HashMap<String, Integer>(){{
			put("Magic Damage", 1);
		}}));
		weapontitles.add(new Title("Honed", 1, new HashMap<String, Integer>(){{
			put("Critical", 1);
		}}));
		weapontitles.add(new Title("Draining", 1, new HashMap<String, Integer>(){{
			put("Lifesteal", 5);
		}}));
		
		
		
		weapontitles.add(new Title("Sharpened", 10, new HashMap<String, Integer>(){{
			put("Damage", 3);
		}}));
		weapontitles.add(new Title("Focused", 10, new HashMap<String, Integer>(){{
			put("Magic Damage", 3);
		}}));
		weapontitles.add(new Title("Honed", 10, new HashMap<String, Integer>(){{
			put("Critical", 3);
		}}));
		weapontitles.add(new Title("Draining", 10, new HashMap<String, Integer>(){{
			put("Lifesteal", 8);
		}}));
		weapontitles.add(new Title("Refined", 10, new HashMap<String, Integer>(){{
			put("Damage", 2);
			put("Critical", 2);
		}}));
		weapontitles.add(new Title("Plagued", 10, new HashMap<String, Integer>(){{
			put("Lifesteal", 5);
			put("Critical", 2);
		}}));
		weapontitles.add(new Title("Undead Forged", 10, new HashMap<String, Integer>(){{
			put("Damage", 2);
			put("Lifesteal", 5);
		}}));
		weapontitles.add(new Title("Spellsheen", 10, new HashMap<String, Integer>(){{
			put("Damage", 2);
			put("Magic Damage", 2);
		}}));
		
		weapontitles.add(new Title("Sharpened", 20, new HashMap<String, Integer>(){{
			put("Damage", 4);
		}}));
		weapontitles.add(new Title("Focused", 20, new HashMap<String, Integer>(){{
			put("Magic Damage", 4);
		}}));
		weapontitles.add(new Title("Honed", 20, new HashMap<String, Integer>(){{
			put("Critical", 4);
		}}));
		weapontitles.add(new Title("Draining", 20, new HashMap<String, Integer>(){{
			put("Lifesteal", 10);
		}}));
		weapontitles.add(new Title("Refined", 20, new HashMap<String, Integer>(){{
			put("Damage", 3);
			put("Critical", 2);
		}}));
		weapontitles.add(new Title("Plagued", 20, new HashMap<String, Integer>(){{
			put("Lifesteal", 8);
			put("Critical", 3);
		}}));
		weapontitles.add(new Title("Undead Forged", 20, new HashMap<String, Integer>(){{
			put("Damage", 3);
			put("Lifesteal", 7);
		}}));
		weapontitles.add(new Title("Spellsheen", 20, new HashMap<String, Integer>(){{
			put("Damage", 3);
			put("Magic Damage", 3);
		}}));
		weapontitles.add(new Title("Recast", 20, new HashMap<String, Integer>(){{
			put("Cooldown Reduction", 10);
		}}));

		//armor
		armortitles.add(new Title("Hardy", 1, new HashMap<String, Integer>(){{
			put("Health", 3);
		}}));
		armortitles.add(new Title("Fount", 1, new HashMap<String, Integer>(){{
			put("Mana", 3);
		}}));
		armortitles.add(new Title("Endearing", 1, new HashMap<String, Integer>(){{
			put("Stamina", 3);
		}}));
		armortitles.add(new Title("Sturdy", 1, new HashMap<String, Integer>(){{
			put("Armor", 3);
		}}));
		armortitles.add(new Title("Warded", 1, new HashMap<String, Integer>(){{
			put("Magic Armor", 3);
		}}));
		
		//Level 10
		armortitles.add(new Title("Hardy", 10, new HashMap<String, Integer>(){{
			put("Health", 4);
		}}));
		armortitles.add(new Title("Fount", 10, new HashMap<String, Integer>(){{
			put("Mana", 4);
		}}));
		armortitles.add(new Title("Endearing", 10, new HashMap<String, Integer>(){{
			put("Stamina", 4);
		}}));
		armortitles.add(new Title("Sturdy", 10, new HashMap<String, Integer>(){{
			put("Armor", 7);
		}}));
		armortitles.add(new Title("Warded", 10, new HashMap<String, Integer>(){{
			put("Magic Armor", 7);
		}}));
		armortitles.add(new Title("Mage", 10, new HashMap<String, Integer>(){{
			put("Health", 3);
			put("Mana", 3);
		}}));
		armortitles.add(new Title("Warrior", 10, new HashMap<String, Integer>(){{
			put("Health", 3);
			put("Stamina", 3);
		}}));
		armortitles.add(new Title("Resource", 10, new HashMap<String, Integer>(){{
			put("Mana", 3);
			put("Stamina", 3);
		}}));
		armortitles.add(new Title("Vital", 10, new HashMap<String, Integer>(){{
			put("Health", 2);
			put("Mana", 2);
			put("Stamina", 2);
		}}));
		
		//Level 20
		armortitles.add(new Title("Hardy", 20, new HashMap<String, Integer>(){{
			put("Health", 5);
		}}));
		armortitles.add(new Title("Fount", 20, new HashMap<String, Integer>(){{
			put("Mana", 5);
		}}));
		armortitles.add(new Title("Endearing", 20, new HashMap<String, Integer>(){{
			put("Stamina", 5);
		}}));
		armortitles.add(new Title("Sturdy", 20, new HashMap<String, Integer>(){{
			put("Armor", 8);
		}}));
		armortitles.add(new Title("Warded", 20, new HashMap<String, Integer>(){{
			put("Magic Armor", 8);
		}}));
		armortitles.add(new Title("Mage", 20, new HashMap<String, Integer>(){{
			put("Health", 4);
			put("Mana", 5);
		}}));
		armortitles.add(new Title("Warrior", 20, new HashMap<String, Integer>(){{
			put("Health", 4);
			put("Stamina", 5);
		}}));
		armortitles.add(new Title("Resource", 20, new HashMap<String, Integer>(){{
			put("Mana", 6);
			put("Stamina", 6);
		}}));
		armortitles.add(new Title("Vital", 20, new HashMap<String, Integer>(){{
			put("Health", 3);
			put("Mana", 3);
			put("Stamina", 3);
		}}));

		
		//Level 35
		armortitles.add(new Title("Hardy", 35, new HashMap<String, Integer>(){{
			put("Health", 6);
		}}));
		armortitles.add(new Title("Fount", 35, new HashMap<String, Integer>(){{
			put("Mana", 6);
		}}));
		armortitles.add(new Title("Endearing", 35, new HashMap<String, Integer>(){{
			put("Stamina", 6);
		}}));
		armortitles.add(new Title("Sturdy", 35, new HashMap<String, Integer>(){{
			put("Armor", 9);
		}}));
		armortitles.add(new Title("Warded", 35, new HashMap<String, Integer>(){{
			put("Magic Armor", 9);
		}}));
		armortitles.add(new Title("Mage", 35, new HashMap<String, Integer>(){{
			put("Health", 5);
			put("Mana", 7);
		}}));
		armortitles.add(new Title("Warrior", 35, new HashMap<String, Integer>(){{
			put("Health", 5);
			put("Stamina", 7);
		}}));
		armortitles.add(new Title("Resource", 35, new HashMap<String, Integer>(){{
			put("Mana", 7);
			put("Stamina", 7);
		}}));
		armortitles.add(new Title("Vital", 35, new HashMap<String, Integer>(){{
			put("Health", 4);
			put("Mana", 4);
			put("Stamina", 4);
		}}));
		
		
		//50
		armortitles.add(new Title("Hardy", 50, new HashMap<String, Integer>(){{
			put("Health", 8);
		}}));
		armortitles.add(new Title("Fount", 50, new HashMap<String, Integer>(){{
			put("Mana", 8);
		}}));
		armortitles.add(new Title("Endearing", 50, new HashMap<String, Integer>(){{
			put("Stamina", 8);
		}}));
		armortitles.add(new Title("Sturdy", 50, new HashMap<String, Integer>(){{
			put("Armor", 10);
		}}));
		armortitles.add(new Title("Warded", 50, new HashMap<String, Integer>(){{
			put("Magic Armor", 10);
		}}));
		armortitles.add(new Title("Mage", 50, new HashMap<String, Integer>(){{
			put("Health", 6);
			put("Mana", 9);
		}}));
		armortitles.add(new Title("Warrior", 50, new HashMap<String, Integer>(){{
			put("Health", 6);
			put("Stamina", 9);
		}}));
		armortitles.add(new Title("Resource", 50, new HashMap<String, Integer>(){{
			put("Mana", 8);
			put("Stamina", 8);
		}}));
		armortitles.add(new Title("Vital", 50, new HashMap<String, Integer>(){{
			put("Health", 6);
			put("Mana", 6);
			put("Stamina", 6);
		}}));
	}

	@SuppressWarnings("unused")
	public void makeWeapons(){
		//Weapon(this, name, level, damage, itemstack)
		//Different Types = Swords, Axes, Bows
		Material woodSword = Material.WOOD_SWORD;
		Material stoneSword = Material.STONE_SWORD;
		Material ironSword = Material.IRON_SWORD;
		Material goldSword = Material.GOLD_SWORD;
		Material diamSword = Material.DIAMOND_SWORD;

		Material woodAxe = Material.WOOD_AXE;
		Material stoneAxe = Material.STONE_AXE;
		Material ironAxe = Material.IRON_AXE;
		Material goldAxe = Material.GOLD_AXE;
		Material diamAxe = Material.DIAMOND_AXE;

		Material woodMace = Material.WOOD_SPADE;
		Material stoneMace = Material.STONE_SPADE;
		Material ironMace = Material.IRON_SPADE;
		Material goldMace = Material.GOLD_SPADE;
		Material diamMace = Material.DIAMOND_SPADE;

		Material wand = Material.STICK;
		Material staff = Material.BLAZE_ROD;

		Material bow = Material.BOW;
		
		weaponTypes.add(woodSword);
		weaponTypes.add(stoneSword);
		weaponTypes.add(ironSword);
		weaponTypes.add(goldSword);
		weaponTypes.add(diamSword);
		
		weaponTypes.add(woodAxe);
		weaponTypes.add(stoneAxe);
		weaponTypes.add(ironAxe);
		weaponTypes.add(goldAxe);
		weaponTypes.add(diamAxe);

		weaponTypes.add(woodMace);
		weaponTypes.add(stoneMace);
		weaponTypes.add(ironMace);
		weaponTypes.add(goldMace);
		weaponTypes.add(diamMace);
		
		weaponTypes.add(wand);
		weaponTypes.add(staff);
		weaponTypes.add(bow);
		
		//Swords
		weapons.add(new Weapon( "Oaken Sword", 1, 3, woodSword, 200, this));
		weapons.add(new Weapon( "Dark Oaken Sword", 5, 4, woodSword, 220, this));
		weapons.add(new Weapon( "Golden Leaf Sword", 9, 5, woodSword, 240, this));
		weapons.add(new Weapon( "Stone Blade", 13, 5, stoneSword, 260, this));
		weapons.add(new Weapon( "Jagged Stone Blade", 17, 6, stoneSword, 280, this));
		weapons.add(new Weapon( "Dark Stone Blade", 21, 7, stoneSword, 300, this));
		weapons.add(new Weapon( "Bone Blade", 25, 8, stoneSword, 325, this));
		weapons.add(new Weapon( "Jagged Bone Blade", 29, 9, stoneSword, 350, this));
		weapons.add(new Weapon( "Dark Bone Blade", 33, 10, stoneSword, 375, this));
		weapons.add(new Weapon( "Rusted Blade", 37, 10, ironSword, 400, this));
		weapons.add(new Weapon( "Steel Blade", 41, 11, ironSword, 430, this));
		weapons.add(new Weapon( "Dark Steel Blade", 45, 12, ironSword, 460, this));
		weapons.add(new Weapon( "Rusted Mithril Blade", 49, 13, ironSword, 490, this));
		weapons.add(new Weapon( "Dark Mithril Blade", 53, 14, ironSword, 520, this));
		weapons.add(new Weapon( "Gilded Mithril Blade", 57, 15, ironSword, 550, this));
//		weapons.add(new Weapon( "Ebon Forged Mithril Blade", 61, 16, ironSword, 580, this));
//		weapons.add(new Weapon( "Gilden Blade", 65, 16, goldSword, 600));
//		weapons.add(new Weapon( "Reinforced Gilden Blade", 69, 17, goldSword, 620));

		//Axes
		weapons.add(new Weapon( "Oaken Axe", 1, 3, woodAxe, 200, this));
		weapons.add(new Weapon( "Dark Oaken Axe", 5, 4, woodAxe, 220, this));
		weapons.add(new Weapon( "Golden Leaf Axe", 9, 5, woodAxe, 240, this));
		weapons.add(new Weapon( "Stone Axe", 13, 5, stoneAxe, 260, this));
		weapons.add(new Weapon( "Jagged Stone Axe", 17, 6, stoneAxe, 280, this));
		weapons.add(new Weapon( "Dark Stone Axe", 21, 7, stoneAxe, 300, this));
		weapons.add(new Weapon( "Bone Axe", 25, 8, stoneAxe, 325, this));
		weapons.add(new Weapon( "Jagged Bone Axe", 29, 9, stoneAxe, 350, this));
		weapons.add(new Weapon( "Dark Bone Axe", 33, 10, stoneAxe, 375, this));
		weapons.add(new Weapon( "Rusted Axe", 37, 10, ironAxe, 400, this));
		weapons.add(new Weapon( "Steel Axe", 41, 11, ironAxe, 430, this));
		weapons.add(new Weapon( "Dark Steel Axe", 45, 12, ironAxe, 460, this));
		weapons.add(new Weapon( "Rusted Mithril Axe", 49, 13, ironAxe, 490, this));
		weapons.add(new Weapon( "Dark Mithril Axe", 53, 14, ironAxe, 520, this));
		weapons.add(new Weapon( "Gilded Mithril Axe", 57, 15, ironAxe, 550, this));
//		weapons.add(new Weapon( "Ebon Forged Mithril Axe", 61, 16, ironAxe, 580, this));
//		weapons.add(new Weapon( "Gilden Axe", 65, 16, goldAxe, 600));
//		weapons.add(new Weapon( "Reinforced Gilden Axe", 69, 17, goldAxe, 620));

		//Mace
		weapons.add(new Weapon( "Oaken Mace", 1, 3, woodMace, 200, this));
		weapons.add(new Weapon( "Dark Oaken Mace", 5, 4, woodMace, 220, this));
		weapons.add(new Weapon( "Golden Leaf Mace", 9, 5, woodMace, 240, this));
		weapons.add(new Weapon( "Stone Mace", 13, 5, stoneMace, 260, this));
		weapons.add(new Weapon( "Jagged Stone Mace", 17, 6, stoneMace, 280, this));
		weapons.add(new Weapon( "Dark Stone Mace", 21, 7, stoneMace, 300, this));
		weapons.add(new Weapon( "Bone Mace", 25, 8, stoneMace, 325, this));
		weapons.add(new Weapon( "Jagged Bone Mace", 29, 9, stoneMace, 350, this));
		weapons.add(new Weapon( "Dark Bone Mace", 33, 10, stoneMace, 375, this));
		weapons.add(new Weapon( "Rusted Mace", 37, 10, ironMace, 400, this));
		weapons.add(new Weapon( "Steel Mace", 41, 11, ironMace, 430, this));
		weapons.add(new Weapon( "Dark Steel Mace", 45, 12, ironMace, 460, this));
		weapons.add(new Weapon( "Rusted Mithril Mace", 49, 13, ironMace, 490, this));
		weapons.add(new Weapon( "Dark Mithril Mace", 53, 14, ironMace, 520, this));
		weapons.add(new Weapon( "Gilded Mithril Mace", 57, 15, ironMace, 550, this));
//		weapons.add(new Weapon( "Ebon Forged Mithril Mace", 61, 16, ironMace, 580, this));
//		weapons.add(new Weapon( "Gilden Mace", 65, 16, goldMace, 600));
//		weapons.add(new Weapon( "Reinforced Gilden Mace", 69, 17, goldMace, 620));

		//Bows
		weapons.add(new Weapon( "Balsa Short Bow", 1, 4, bow, 200, this));
		weapons.add(new Weapon( "Balsa Long Bow", 7, 6, bow, 220, this));
		weapons.add(new Weapon( "Balsa Composite Long Bow", 13, 7, bow, 240, this));
		weapons.add(new Weapon( "Oaken Short Bow", 19, 8, bow, 260, this));
		weapons.add(new Weapon( "Oaken Long Bow", 25, 9, bow, 325, this));
		weapons.add(new Weapon( "Oaken Composite Long Bow", 31, 10, bow, 375, this));
		weapons.add(new Weapon( "Dark Oak Long Bow", 38, 11, bow, 425, this));
		weapons.add(new Weapon( "Dark Oak Composite Long Bow", 45, 12, bow, 475, this));
		weapons.add(new Weapon( "Mahogany Long Bow", 52, 13, bow, 525, this));
		weapons.add(new Weapon( "Serpent Mahogany Long Bow", 59, 15, bow, 575, this));
//		weapons.add(new Weapon( "Cursed Mahogany Composite Long Bow", 66, 17, bow, 625));
	
		//Staves
		weapons.add(new Weapon( "Balsa Short Wand", 1, 3, wand, 200, this));
		weapons.add(new Weapon( "Balsa Long Wand", 7, 4, wand, 240, this));
		weapons.add(new Weapon( "Balsa Staff", 13, 6, staff, 260, this));
		weapons.add(new Weapon( "Oaken Wand", 19, 6, wand, 325, this));
		weapons.add(new Weapon( "Oaken Staff", 25, 8, staff, 375, this));
		weapons.add(new Weapon( "Oaken Composite Wand", 31, 8, wand, 425, this));
		weapons.add(new Weapon( "Dark Oak Wand", 38, 9, wand, 475, this));
		weapons.add(new Weapon( "Dark Oak Staff", 45, 11, staff, 525, this));
		weapons.add(new Weapon( "Mahogany Wand", 52, 11, wand, 575, this));
		weapons.add(new Weapon( "Serpent Mahogany Wand", 59, 12, wand, 625, this));
//		weapons.add(new Weapon( "Cursed Mahogany Wand", 66, 13, wand, 675));
	}

	@SuppressWarnings("unused")
	public void makeArmors(){
		//Leather Set
		Material lHat = Material.LEATHER_HELMET;
		Material lChest = Material.LEATHER_CHESTPLATE;
		Material lLegs = Material.LEATHER_LEGGINGS;
		Material lBoots = Material.LEATHER_BOOTS;

		//Chainmail Set
		Material cHat = Material.CHAINMAIL_HELMET;
		Material cChest = Material.CHAINMAIL_CHESTPLATE;
		Material cLegs = Material.CHAINMAIL_LEGGINGS;
		Material cBoots = Material.CHAINMAIL_BOOTS;

		//Gold Set
		Material gHat = Material.GOLD_HELMET;
		Material gChest = Material.GOLD_CHESTPLATE;
		Material gLegs = Material.GOLD_LEGGINGS;
		Material gBoots = Material.GOLD_BOOTS;
		
		//Iron Set
		Material iHat = Material.IRON_HELMET;
		Material iChest = Material.IRON_CHESTPLATE;
		Material iLegs = Material.IRON_LEGGINGS;
		Material iBoots = Material.IRON_BOOTS;

		//Diamond Set
		Material dHat = Material.DIAMOND_HELMET;
		Material dChest = Material.DIAMOND_CHESTPLATE;
		Material dLegs = Material.DIAMOND_LEGGINGS;
		Material dBoots = Material.DIAMOND_BOOTS;

		armorTypes.add(lHat);
		armorTypes.add(lChest);
		armorTypes.add(lLegs);
		armorTypes.add(lBoots);
		
		armorTypes.add(cHat);
		armorTypes.add(cChest);
		armorTypes.add(cLegs);
		armorTypes.add(cBoots);
		
		armorTypes.add(gHat);
		armorTypes.add(gChest);
		armorTypes.add(gLegs);
		armorTypes.add(gBoots);
		
		armorTypes.add(iHat);
		armorTypes.add(iChest);
		armorTypes.add(iLegs);
		armorTypes.add(iBoots);
		
		armorTypes.add(dHat);
		armorTypes.add(dChest);
		armorTypes.add(dLegs);
		armorTypes.add(dBoots);
		
		//Light
		armors.add(new Armor("Beaded Helmet", 1, 1, lHat, 200, this, ArmorType.Light));
		armors.add(new Armor("Beaded Chestplate", 1, 2, lChest, 300, this, ArmorType.Light));
		armors.add(new Armor("Beaded Leggings", 1, 1, lLegs, 280, this, ArmorType.Light));
		armors.add(new Armor("Beaded Boots", 1, 1, lBoots, 240, this, ArmorType.Light));
		armors.add(new Armor("Disciple's Helmet", 7, 1, lHat, 225, this, ArmorType.Light));
		armors.add(new Armor("Disciple's Chestplate", 7, 2, lChest, 325, this, ArmorType.Light));
		armors.add(new Armor("Disciple's Leggings", 7, 2, lLegs, 305, this, ArmorType.Light));
		armors.add(new Armor("Disciple's Boots", 7, 1, lBoots, 265, this, ArmorType.Light));
		armors.add(new Armor("Native Helmet", 13, 2, lHat, 250, this, ArmorType.Light));
		armors.add(new Armor("Native Skin Chest", 13, 3, lChest, 350, this, ArmorType.Light));
		armors.add(new Armor("Native Skin Legs", 13, 3, lLegs, 330, this, ArmorType.Light));
		armors.add(new Armor("Native Skin Boots", 13, 3, lBoots, 290, this, ArmorType.Light));
		armors.add(new Armor("Willow Helmet", 19, 3, lHat, 275, this, ArmorType.Light));
		armors.add(new Armor("Willow Chestplate", 19, 4, lChest, 375, this, ArmorType.Light));
		armors.add(new Armor("Willow Leggings", 19, 4, lLegs, 355, this, ArmorType.Light));
		armors.add(new Armor("Willow Boots", 19, 3, lBoots, 315, this, ArmorType.Light));
		armors.add(new Armor("Aboriginal Helmet", 25, 3, lHat, 300, this, ArmorType.Light));
		armors.add(new Armor("Aboriginal Chestplate", 25, 5, lChest, 400, this, ArmorType.Light));
		armors.add(new Armor("Aboriginal Leggings", 25, 4, lLegs, 380, this, ArmorType.Light));
		armors.add(new Armor("Aboriginal Boots", 25, 4, lBoots, 340, this, ArmorType.Light));
		armors.add(new Armor("Ritual Helmet", 31, 4, cHat, 330, this, ArmorType.Light));
		armors.add(new Armor("Ritual Chestplate", 31, 5, cChest, 430, this, ArmorType.Light));
		armors.add(new Armor("Ritual Leggings", 31, 5, cLegs, 410, this, ArmorType.Light));
		armors.add(new Armor("Ritual Boots", 31, 5, cBoots, 370, this, ArmorType.Light));
		armors.add(new Armor("Shimmering Helmet", 37, 4, cHat, 360, this, ArmorType.Light));
		armors.add(new Armor("Shimmering Chestplate", 37, 6, cChest, 460, this, ArmorType.Light));
		armors.add(new Armor("Shimmering Leggings", 37, 6, cLegs, 440, this, ArmorType.Light));
		armors.add(new Armor("Shimmering Boots", 37, 5, cBoots, 400, this, ArmorType.Light));
		armors.add(new Armor("Pagan Helmet", 43, 5, cHat, 390, this, ArmorType.Light));
		armors.add(new Armor("Pagan Chestplate", 43, 7, cChest, 490, this, ArmorType.Light));
		armors.add(new Armor("Pagan Leggings", 43, 7, cLegs, 470, this, ArmorType.Light));
		armors.add(new Armor("Pagan Boots", 43, 5, cBoots, 430, this, ArmorType.Light));
		armors.add(new Armor("Watcher's Helmet", 49, 5, cHat, 420, this, ArmorType.Light));
		armors.add(new Armor("Watcher's Chestplate", 49, 8, cChest, 520, this, ArmorType.Light));
		armors.add(new Armor("Watcher's Leggings", 49, 7, cLegs, 500, this, ArmorType.Light));
		armors.add(new Armor("Watcher's Boots", 49, 6, cBoots, 460, this, ArmorType.Light));

		
		//Medium
		armors.add(new Armor("Primal Helmet", 1, 1, cHat, 200, this, ArmorType.Medium));
		armors.add(new Armor("Primal Chestplate", 1, 2, cChest, 300, this, ArmorType.Medium));
		armors.add(new Armor("Primal Leggings", 1, 1, cLegs, 280, this, ArmorType.Medium));
		armors.add(new Armor("Primal Boots", 1, 1, cBoots, 240, this, ArmorType.Medium));
		armors.add(new Armor("Pioneer Helmet", 7, 1, cHat, 225, this, ArmorType.Medium));
		armors.add(new Armor("Pioneer Chestplate", 7, 2, cChest, 325, this, ArmorType.Medium));
		armors.add(new Armor("Pionner Leggings", 7, 2, cLegs, 305, this, ArmorType.Medium));
		armors.add(new Armor("Pionner Boots", 7, 1, cBoots, 265, this, ArmorType.Medium));
		armors.add(new Armor("Grizzly Helmet", 13, 2, cHat, 250, this, ArmorType.Medium));
		armors.add(new Armor("Grizzly Skin Chest", 13, 3, cChest, 350, this, ArmorType.Medium));
		armors.add(new Armor("Grizzly Skin Legs", 13, 3, cLegs, 330, this, ArmorType.Medium));
		armors.add(new Armor("Grizzly Skin Boots", 13, 3, cBoots, 290, this, ArmorType.Medium));
		armors.add(new Armor("Lupine Helmet", 19, 3, cHat, 275, this, ArmorType.Medium));
		armors.add(new Armor("Lupine Chestplate", 19, 4, cChest, 375, this, ArmorType.Medium));
		armors.add(new Armor("Lupine Leggings", 19, 4, cLegs, 355, this, ArmorType.Medium));
		armors.add(new Armor("Lupine Boots", 19, 3, cBoots, 315, this, ArmorType.Medium));
		armors.add(new Armor("Bandit Helmet", 25, 3, cHat, 300, this, ArmorType.Medium));
		armors.add(new Armor("Bandit Chestplate", 25, 5, cChest, 400, this, ArmorType.Medium));
		armors.add(new Armor("Bandit Leggings", 25, 4, cLegs, 380, this, ArmorType.Medium));
		armors.add(new Armor("Bandit Boots", 25, 4, cBoots, 340, this, ArmorType.Medium));
		armors.add(new Armor("Feral Helmet", 31, 4, gHat, 330, this, ArmorType.Medium));
		armors.add(new Armor("Feral Chestplate", 31, 5, gChest, 430, this, ArmorType.Medium));
		armors.add(new Armor("Feral Leggings", 31, 5, gLegs, 410, this, ArmorType.Medium));
		armors.add(new Armor("Feral Boots", 31, 5, gBoots, 370, this, ArmorType.Medium));
		armors.add(new Armor("Scouting Helmet", 37, 4, gHat, 360, this, ArmorType.Medium));
		armors.add(new Armor("Scouting Chestplate", 37, 6, gChest, 460, this, ArmorType.Medium));
		armors.add(new Armor("Scouting Leggings", 37, 6, gLegs, 440, this, ArmorType.Medium));
		armors.add(new Armor("Scouting Boots", 37, 5, gBoots, 400, this, ArmorType.Medium));
		armors.add(new Armor("Wrangler's Helmet", 43, 5, gHat, 390, this, ArmorType.Medium));
		armors.add(new Armor("Wrangler's Chestplate", 43, 7, gChest, 490, this, ArmorType.Medium));
		armors.add(new Armor("Wrangler's Leggings", 43, 7, gLegs, 470, this, ArmorType.Medium));
		armors.add(new Armor("Wrangler's Boots", 43, 5, gBoots, 430, this, ArmorType.Medium));
		armors.add(new Armor("Dervish Helmet", 49, 5, gHat, 420, this, ArmorType.Medium));
		armors.add(new Armor("Dervish Chestplate", 49, 8, gChest, 520, this, ArmorType.Medium));
		armors.add(new Armor("Dervish Leggings", 49, 7, gLegs, 500, this, ArmorType.Medium));
		armors.add(new Armor("Dervish Boots", 49, 6, gBoots, 460, this, ArmorType.Medium));
		
		
		//Heavy
		armors.add(new Armor("Infantry Helmet", 1, 1, gHat, 200, this, ArmorType.Heavy));
		armors.add(new Armor("Infantry Chestplate", 1, 2, gChest, 300, this, ArmorType.Heavy));
		armors.add(new Armor("Infantry Leggings", 1, 1, gLegs, 280, this, ArmorType.Heavy));
		armors.add(new Armor("Infantry Boots", 1, 1, gBoots, 240, this, ArmorType.Heavy));
		armors.add(new Armor("Cadets Helmet", 7, 1, gHat, 225, this, ArmorType.Heavy));
		armors.add(new Armor("Cadets Chestplate", 7, 2, gChest, 325, this, ArmorType.Heavy));
		armors.add(new Armor("Cadets Leggings", 7, 2, gLegs, 305, this, ArmorType.Heavy));
		armors.add(new Armor("Cadets Boots", 7, 1, gBoots, 265, this, ArmorType.Heavy));
		armors.add(new Armor("Soldier's Helmet", 13, 2, gHat, 250, this, ArmorType.Heavy));
		armors.add(new Armor("Soldier's Skin Chest", 13, 3, gChest, 350, this, ArmorType.Heavy));
		armors.add(new Armor("Soldier's Skin Legs", 13, 3, gLegs, 330, this, ArmorType.Heavy));
		armors.add(new Armor("Soldier's Skin Boots", 13, 3, gBoots, 290, this, ArmorType.Heavy));
		armors.add(new Armor("Bloodspattered Helmet", 19, 3, gHat, 275, this, ArmorType.Heavy));
		armors.add(new Armor("Bloodspattered Chestplate", 19, 4, gChest, 375, this, ArmorType.Heavy));
		armors.add(new Armor("Bloodspattered Leggings", 19, 4, gLegs, 355, this, ArmorType.Heavy));
		armors.add(new Armor("Bloodspattered Boots", 19, 3, gBoots, 315, this, ArmorType.Heavy));
		armors.add(new Armor("Raiders Helmet", 25, 3, gHat, 300, this, ArmorType.Heavy));
		armors.add(new Armor("Raiders Chestplate", 25, 5, gChest, 400, this, ArmorType.Heavy));
		armors.add(new Armor("Raiders Leggings", 25, 4, gLegs, 380, this, ArmorType.Heavy));
		armors.add(new Armor("Raiders Boots", 25, 4, gBoots, 340, this, ArmorType.Heavy));
		armors.add(new Armor("Defenders Helmet", 31, 4, iHat, 330, this, ArmorType.Heavy));
		armors.add(new Armor("Defenders Chestplate", 31, 5, iChest, 430, this, ArmorType.Heavy));
		armors.add(new Armor("Defenders Leggings", 31, 5, iLegs, 410, this, ArmorType.Heavy));
		armors.add(new Armor("Defenders Boots", 31, 5, iBoots, 370, this, ArmorType.Heavy));
		armors.add(new Armor("Fortified Helmet", 37, 4, iHat, 360, this, ArmorType.Heavy));
		armors.add(new Armor("Fortified Chestplate", 37, 6, iChest, 460, this, ArmorType.Heavy));
		armors.add(new Armor("Fortified Leggings", 37, 6, iLegs, 440, this, ArmorType.Heavy));
		armors.add(new Armor("Fortified Boots", 37, 5, iBoots, 400, this, ArmorType.Heavy));
		armors.add(new Armor("Sentry's Helmet", 43, 5, iHat, 390, this, ArmorType.Heavy));
		armors.add(new Armor("Sentry's Chestplate", 43, 7, iChest, 490, this, ArmorType.Heavy));
		armors.add(new Armor("Sentry's Leggings", 43, 7, iLegs, 470, this, ArmorType.Heavy));
		armors.add(new Armor("Sentry's Boots", 43, 5, iBoots, 430, this, ArmorType.Heavy));
		armors.add(new Armor("Battleforge Helmet", 49, 5, iHat, 420, this, ArmorType.Heavy));
		armors.add(new Armor("Battleforge Chestplate", 49, 8, iChest, 520, this, ArmorType.Heavy));
		armors.add(new Armor("Battleforge Leggings", 49, 7, iLegs, 500, this, ArmorType.Heavy));
		armors.add(new Armor("Battleforge Boots", 49, 6, iBoots, 460, this, ArmorType.Heavy));


	}

	public String calculateRarity(Integer Level, boolean inDungeon){
		Integer roll = random.nextInt(1000);

		//COMMON (Grey)
		//62.5% at Level 1
		//41.5% at Level 100
		//20.5% at Level 200
		Integer rarityoffset = 0;
		if(!inDungeon){
			rarityoffset += 625 - 21 * (Level / 10);
		}
		if(roll <= rarityoffset){
			return "Common";
		}

		//UNCOMMON (Light Green)
		//20% chance at Level 1
		//30% chance at Level 100
		//40% chance at Level 200
		if(!inDungeon){
			rarityoffset += 200 + 10 * (Level / 10);
		}
		if(roll <= rarityoffset){
			return "Uncommon";

		}

		//RARE (Light Blue)
		//10% chance at Level 1
		//15% chance at Level 100
		//20% chance at Level 200
		if(!inDungeon){
			rarityoffset += 100 + 5 * (Level / 10);
		}
		if(roll <= rarityoffset){
			return "Rare";
		}

		//EPIC (Red)
		//4.0% chance at Level 1
		//7.0% chance at Level 100
		//10.0% chance at Level 200
		if(!inDungeon){
			rarityoffset += 40 + 3 * (Level / 10); 
		}
		if(roll <= rarityoffset){
			return "Epic";
		}

		//LEGENDARY (Gold)
		//2.5% chance at Level 1
		//4.5% chance at Level 100
		//6.5% chance at Level 200
		if(!inDungeon){
			rarityoffset += 25 + 2 * (Level / 10);
		}
		if(roll <= rarityoffset){
			return "Legendary";
		}

		//GODLY (Purple)
		//1% chance at Level 1
		//2% chance at Level 100
		//3% chance at Level 200
		if(!inDungeon){
			rarityoffset += 10 + Level / 10;
		}
		if(roll <= rarityoffset){
			return "Godly";
		}

		return "Common";
	}

	public void generateItem(Integer Level, Location loc, boolean inDungeon){
		String rare = calculateRarity(Level, inDungeon);
		
		Integer check = random.nextInt(13);
		if(check <= 8){
			spawnArmor(Level, loc, rare, null, null);
		}
		else{
			spawnWeapon(Level, loc, rare, null, null);
		}
		
	}
	
	public void spawnSocketGem(Integer level, Location loc, Player player){
		ArrayList<SocketGem> availableSockets = new ArrayList<SocketGem>();
		for(SocketGem s : this.gems){
			if(s.level <= level){
				availableSockets.add(s);
			}
		}

		if(availableSockets.size() < 1){
			return; //Cancel if no items in level range
		}
		
		int choice = random.nextInt(availableSockets.size());
		SocketGem sg = availableSockets.get(choice);
		try{
			if(player != null){
				ItemStack socket = sg.create();
				player.getInventory().addItem(socket);
				
			}
			else{
				sg.drop(loc);
			}
		}
		catch(NullPointerException e){
			sg.drop(loc);
		}
	}
	
	public void spawnWeapon(Integer level, Location loc, String rarity, String type, Player player){
		ArrayList<Material> materials = new ArrayList<Material>();
		if(type != null){
			switch(type.toLowerCase()){
			case "sword":
				materials.add(Material.WOOD_SWORD);
				materials.add(Material.STONE_SWORD);
				materials.add(Material.IRON_SWORD);
				materials.add(Material.GOLD_SWORD);
				materials.add(Material.DIAMOND_SWORD);
				break;
			case "axe":
				materials.add(Material.WOOD_AXE);
				materials.add(Material.STONE_AXE);
				materials.add(Material.IRON_AXE);
				materials.add(Material.GOLD_AXE);
				materials.add(Material.DIAMOND_AXE);
				break;
			case "mace":
				materials.add(Material.WOOD_SPADE);
				materials.add(Material.STONE_SPADE);
				materials.add(Material.IRON_SPADE);
				materials.add(Material.GOLD_SPADE);
				materials.add(Material.DIAMOND_SPADE);
				break;
			case "bow":
				materials.add(Material.BOW);
				break;
			case "staff":
				materials.add(Material.STICK);
				materials.add(Material.BLAZE_ROD);
			}
		}
		
		
		ArrayList<Weapon> availableWeapons = new ArrayList<Weapon>();
		for(Weapon weapon : weapons){
			if((level - 8) <= weapon.level && weapon.level <= (level + 8)){
				if(type != null){
					if(materials.contains(weapon.getMaterial())){
						availableWeapons.add(weapon);
					}
				}
				else{
					availableWeapons.add(weapon);
				}
			}
		}

		if(availableWeapons.size() < 1){
			for(Weapon weapon : weapons){
				if((plugin.levelCap - 5) <= weapon.level){
					if(type != null){
						if(materials.contains(weapon.getMaterial())){
							availableWeapons.add(weapon);
						}
					}
					else{
						availableWeapons.add(weapon);
					}
				}
			}
		}
		
		int choice = random.nextInt(availableWeapons.size());
		Weapon weapon = availableWeapons.get(choice);

		ArrayList<Title> titles = new ArrayList<Title>();

		if(rarity != "Common"){
			Integer titleCount = 0;
			switch(rarity){
			case("Uncommon"):
				titleCount = 1;
			break;
			case("Rare"):
				titleCount = 1;
			break;
			case("Epic"):
				titleCount = 2;
			break;
			case("Legendary"):
				titleCount = 2;
			break;
			case("Godly"):
				titleCount = 3;
			break;
			}

			if(titleCount >= 1){
				ArrayList<Title> available = new ArrayList<Title>();
				for(Title title : weapontitles){
					if((weapon.level - 30) <= title.level && title.level <= weapon.level){
						available.add(title);
					}
				}
				for(int i=0;i<titleCount;i++){
					choice = random.nextInt(available.size());
					Title test = available.get(choice);
					if(!titles.contains(test)){
						titles.add(test);
					}
					else{
						i--;
					}
				}
			}
		}

		if(player == null){
			weapon.spawnItem(loc, rarity, titles);
		}
		else{
			weapon.spawnItem(player, rarity, titles);
		}
	}

	public ArrayList<Title> getArmorTitles(){
		return armortitles;
	}
	
	public ArrayList<String> addSockets(Integer min, Integer max, ArrayList<String> lore){
		Integer slotCount = random.nextInt(max - min) + min;
		for(int i = 0; i < slotCount; i++){
			lore.add(ChatColor.GRAY + "○ - <empty>");
		}
		return lore;
	}
	
	public ArrayList<Title> getWeaponTitles(){
		return weapontitles;
	}
	
	public void spawnArmor(Integer level, Location loc, String rarity, String type, Player player){
		ArrayList<Material> materials = new ArrayList<Material>();
		if(type != null){
			switch(type.toLowerCase()){
			case "chest":
				materials.add(Material.LEATHER_CHESTPLATE);
				materials.add(Material.CHAINMAIL_CHESTPLATE);
				materials.add(Material.IRON_CHESTPLATE);
				materials.add(Material.GOLD_CHESTPLATE);
				materials.add(Material.DIAMOND_CHESTPLATE);
				break;
			case "legs":
				materials.add(Material.LEATHER_LEGGINGS);
				materials.add(Material.CHAINMAIL_LEGGINGS);
				materials.add(Material.IRON_LEGGINGS);
				materials.add(Material.GOLD_LEGGINGS);
				materials.add(Material.DIAMOND_LEGGINGS);
				break;
			case "boots":
				materials.add(Material.LEATHER_BOOTS);
				materials.add(Material.CHAINMAIL_BOOTS);
				materials.add(Material.IRON_BOOTS);
				materials.add(Material.GOLD_BOOTS);
				materials.add(Material.DIAMOND_BOOTS);
				break;
			case "helm":
				materials.add(Material.LEATHER_HELMET);
				materials.add(Material.CHAINMAIL_HELMET);
				materials.add(Material.IRON_HELMET);
				materials.add(Material.GOLD_HELMET);
				materials.add(Material.DIAMOND_HELMET);
				break;
				
			}
		}
		
		ArrayList<Armor> availableArmors = new ArrayList<Armor>();
		for(Armor armor : armors){
			if((level - 8) <= armor.level && armor.level <= (level + 8)){
				if(type != null){
					if(materials.contains(armor.getMaterial())){
						availableArmors.add(armor);
					}
				}
				else{
					availableArmors.add(armor);
				}
			}
		}

		if(availableArmors.size() < 1){
			for(Armor armor : armors){
				if((plugin.levelCap - 5) <= armor.level){
					if(type != null){
						if(materials.contains(armor.getMaterial())){
							availableArmors.add(armor);
						}
					}
					else{
						availableArmors.add(armor);
					}
				}
			}
		}
		
		int choice = 0;
		
		try{
			choice = random.nextInt(availableArmors.size());
		}
		catch(IllegalArgumentException e){
			return;
		}
		Armor armor = availableArmors.get(choice);

		ArrayList<Title> titles = new ArrayList<Title>();

		if(rarity != "Common"){
			Integer titleCount = 0;
			switch(rarity){
			case("Uncommon"):
				titleCount = 1;
			break;
			case("Rare"):
				titleCount = 1;
			break;
			case("Epic"):
				titleCount = 2;
			break;
			case("Legendary"):
				titleCount = 2;
			break;
			case("Godly"):
				titleCount = 3;
			break;
			}

			if(titleCount >= 1){
				ArrayList<Title> available = new ArrayList<Title>();
				for(Title title : armortitles){
					if((armor.level - 30) <= title.level && title.level <= armor.level){
						available.add(title);
					}
				}
				for(int i=0;i<titleCount;i++){
					choice = random.nextInt(available.size());
					Title test = available.get(choice);
					if(!titles.contains(test)){
						titles.add(test);
					}
					else{
						i--;
					}
				}
			}
		}

		if(player == null){
			armor.spawnItem(loc, rarity, titles);
		}
		else{
			armor.spawnItem(player, rarity, titles);
		}
	}

	
	public boolean isGear(ItemStack item) {
		try{
			if(item.hasItemMeta()){
				if(weaponTypes.contains(item.getType()) || armorTypes.contains(item.getType())){
					java.util.List<String> lore = item.getItemMeta().getLore();
					for(String line : lore){
						if(line.contains("Rarity")){
							return true;
						}
					}
				}
			}
			return false;
		}
		catch(NullPointerException e){
			return false;
		}
	}
	
	public boolean isSocketable(ItemStack item){
		if(isGear(item)){
			java.util.List<String> lore = item.getItemMeta().getLore();
			for(String line : lore){
				if(line.contains("○ - <empty>")){
					return true;
				}
			}
		}
		return false;
	}
	

	public boolean isSocketableWeapon(ItemStack i) {
		if(isSocketable(i) && weaponTypes.contains(i.getType())){
			return true;
		}
		return false;
	}
	
	public boolean isSocketableArmor(ItemStack i) {
		if(isSocketable(i) && armorTypes.contains(i.getType())){
			return true;
		}
		return false;
	}
}
