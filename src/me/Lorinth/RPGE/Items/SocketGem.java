package me.Lorinth.RPGE.Items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SocketGem {

	public String name;
	public HashMap<String, Integer> terms;
	public socket type;
	public Integer level;
	
	public SocketGem(String Name, Integer Level, HashMap<String, Integer> Terms, socket type){
		name = Name;
		terms = Terms;
		this.type = type;
		level = Level;
	}
	
	public void drop(Location loc){
		ItemStack is = null;
		if(type == socket.Weapon){
			is = new ItemStack(Material.MAGMA_CREAM);
		}
		else if(type == socket.Armor){
			is = new ItemStack(Material.SLIME_BALL);
		}
		else{
			return;
		}
		
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		
		ArrayList<String> lore = new ArrayList<String>();
		
		if(type == socket.Weapon){
			lore.add(ChatColor.RED + "Weapon Socket");
		}
		else if(type == socket.Armor){
			lore.add(ChatColor.GOLD + "Armor Socket");
		}
		lore.add(ChatColor.GRAY + "Hold in hand and right click");
		lore.add(ChatColor.GRAY + "to socket into equipment");
		lore.add("");
		
		//Iterate through the different terms in the title
		for(String key : terms.keySet()){
			//Make sure its not already in the item, if it is add to the value
			boolean Found = false;
			for(String line : lore){
				List<String> words = Arrays.asList(line.split(" "));
				//If the term is the same...
				if(words.get(0).contains(key)){
					//parse to int and add to it...
					try{
						Integer number = Integer.parseInt(words.get(words.size() - 1));
						number += terms.get(key);
						Found = true;
					}
					catch(NumberFormatException e){
						
					}
				}
			}
			if(!Found){
				String line = key + " + " + terms.get(key);
				lore.add(line);
			}
		}
		
		im.setLore(lore);
		
		is.setItemMeta(im);
		loc.getWorld().dropItem(loc, is);
	}

	public ItemStack create() {
		ItemStack is = null;
		if(type == socket.Weapon){
			is = new ItemStack(Material.SLIME_BALL);
		}
		else if(type == socket.Armor){
			is = new ItemStack(Material.SLIME_BALL);
		}
		else{
			return null;
		}
		
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		
		ArrayList<String> lore = new ArrayList<String>();
		
		if(type == socket.Weapon){
			lore.add(ChatColor.RED + "Weapon Socket");
		}
		else if(type == socket.Armor){
			lore.add(ChatColor.GOLD + "Armor Socket");
		}
		
		lore.add("");
		
		//Iterate through the different terms in the title
		for(String key : terms.keySet()){
			//Make sure its not already in the item, if it is add to the value
			boolean Found = false;
			for(String line : lore){
				List<String> words = Arrays.asList(line.split(" "));
				//If the term is the same...
				if(words.get(0).contains(key)){
					//parse to int and add to it...
					Integer number = Integer.parseInt(words.get(words.size() - 1));
					number += terms.get(key);
					Found = true;
				}
			}
			if(!Found){
				String line = key + " : " + terms.get(key);
				lore.add(line);
			}
		}
		
		im.setLore(lore);
		
		is.setItemMeta(im);
		return is;
	}
	
}
