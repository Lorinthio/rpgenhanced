package me.Lorinth.RPGE.Items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.avaje.ebeaninternal.server.persist.dml.InsertMeta;

public class DungeonWeapon extends Gear{

	private Material material;
	public Integer level;
	private ItemGenerator IG;
	public String name;
	public Integer damage;
	public Integer durability;
	
	public ArrayList<String> description = new ArrayList<String>();
	
	public DungeonWeapon(String Name, Integer Level, Integer Damage, Material item, Integer dura) {
		material = item;
		level = Level;
		damage = Damage;
		name = Name;
		durability = dura;
	}

	public void setDescription(List<String> lore){
		for(String line : lore){
			String newline = convertColors(line);
			this.description.add(newline);
		}
	}

	public String convertColors(String line){
		line = line.replace("&0", ChatColor.BLACK + "");
		line = line.replace("&1", ChatColor.DARK_BLUE + "");
		line = line.replace("&2", ChatColor.DARK_GREEN + "");
		line = line.replace("&3", ChatColor.DARK_AQUA + "");
		line = line.replace("&4", ChatColor.DARK_RED + "");
		line = line.replace("&5", ChatColor.DARK_PURPLE + "");
		line = line.replace("&6", ChatColor.GOLD + "");
		line = line.replace("&7", ChatColor.GRAY + "");
		line = line.replace("&8", ChatColor.DARK_GRAY + "");
		line = line.replace("&9", ChatColor.BLUE + "");
		line = line.replace("&a", ChatColor.GREEN + "");
		line = line.replace("&b", ChatColor.AQUA + "");
		line = line.replace("&c", ChatColor.RED + "");
		line = line.replace("&d", ChatColor.LIGHT_PURPLE + "");
		line = line.replace("&e", ChatColor.YELLOW + "");
		line = line.replace("&f", ChatColor.WHITE + "");
		return line;
	}
	
	public Material getMaterial(){
		return material;
	}
	
	@Override
	public ItemStack spawnItem(String rarity, ArrayList<Title> titles) {
		ItemStack newItem = new ItemStack(material);
		ItemMeta meta = newItem.getItemMeta();
		
		Integer physDamage = damage;
		Integer magicDamage = damage;
		
		Integer skillL = level;
		Integer copyDura = durability;
		
		Integer value = 2 * level;
		
		Integer minS = 0;
		Integer maxS = 1;
		
		String rarityLine = "";
		ChatColor rareColor = null;
		
		//Color and name the item
		switch(rarity){
			case("Common"):
				rareColor = ChatColor.GRAY;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Common";
				break;
			case("Uncommon"):
				rareColor = ChatColor.GREEN;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Uncommon";
				physDamage += 1;
				magicDamage += 1;
				skillL = level + 1;
				
				maxS = 2;
				
				copyDura += 75;
				value += 5 + (level / 4);
				
				break;
			case("Rare"):
				rareColor = ChatColor.AQUA;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Rare";
				physDamage += 2;
				magicDamage += 2;
				skillL = level + 2;
				
				maxS = 2;
				
				copyDura += 150;
				value += 10 + (level / 2);
				
				break;
			case("Epic"):
				rareColor = ChatColor.DARK_RED;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Epic";
				physDamage += 3;
				magicDamage += 3;
				skillL = level + 3;
				
				minS = 1;
				maxS = 2;
					
				
				copyDura += 300;
				value += 20 + level;
				
				break;
			case("Legendary"):
				rareColor = ChatColor.DARK_PURPLE;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Legendary";
				physDamage += 4;
				magicDamage += 4;
				skillL = level + 4;
				
				minS = 1;
				maxS = 3;
				
				copyDura += 600;
				value += (int) (30 + (level * 1.5));
				
				break;
			case("Godly"):
				rareColor = ChatColor.GOLD;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Godly";
				physDamage += 6;
				magicDamage += 6;
				skillL = level + 6;
				copyDura += 750;
				
				minS = 1;
				maxS = 4;
				
				value += (int) (45 + (level * 2));
				
				break;
		}
		
		String copiedName = "";
		
		Integer Level = level;
		if(Level < 1){
			Level = 1;
		}
		
		ArrayList<String> lore = new ArrayList<String>();
		
		lore.add(ChatColor.GOLD +      "Req Level : " + Level);
		String skillname = "";
		if(this.material.name().toLowerCase().contains("sword")){
			skillname = "Fencing";
		}
		else if(this.material.name().toLowerCase().contains("axe")){
			skillname = "Marauding";
		}
		else if(this.material.name().toLowerCase().contains("bow")){
			skillname = "Marksmanship";
		}
		else if(this.material.name().toLowerCase().contains("spade")){
			skillname = "Bludgeoning";
		}
		else if(this.material.name().toLowerCase().contains("stick")){
			skillname = "Staves";
		}
		else if(this.material.getId() == 369){
			skillname = "Staves";
		}
		
		lore.add(ChatColor.DARK_AQUA + "Req " + skillname + " : " + skillL);
		lore.add(rarityLine); 
		lore.add(ChatColor.GRAY + "Durability : " + copyDura + "/" + copyDura);
		
		
		
		if(!titles.isEmpty()){
			lore.add("");
			for(Title title : titles){
				if(!copiedName.contains(title.name)){
					copiedName += title.name + " ";
				}
				HashMap<String, Integer> Terms = title.terms;
				//Iterate through the different terms in the title
				for(String key : Terms.keySet()){
					//Make sure its not already in the item, if it is add to the value
					if(key.equals("Damage")){
						physDamage += Terms.get(key);
					}
					else if(key.equals("Magic Damage")){
						magicDamage += Terms.get(key); 

					}
					else{
						boolean Found = false;
						for(String line : lore){
							List<String> words = Arrays.asList(line.split(" "));
							//If the term is the same...
							if(words.get(0).contains(key)){
								//parse to int and add to it...
								Integer number = Integer.parseInt(words.get(words.size() - 1));
								number += Terms.get(key);
								Found = true;
							}
						}
						if(!Found){
							String line = rareColor + key + " : " + Terms.get(key);
							lore.add(line);
						}
					}
				}
			}
		}
		
		if(material.equals(Material.STICK)){
			physDamage /= 3;
		}
		else if(material.equals(Material.BLAZE_ROD)){
			physDamage /= 3;
		}
		else{
			magicDamage = damage / 2;
		}
		
		lore.add(0, ChatColor.DARK_PURPLE + "Magic Damage : " + magicDamage);
		lore.add(0, ChatColor.DARK_RED + "Damage : " + physDamage);
		
		meta.setDisplayName(rareColor + name);
		
		lore.add("");
		lore = addSockets(minS, maxS, lore);
		lore.add(ChatColor.GRAY + "Value : " + ChatColor.GOLD + value);
		
		if(!this.description.isEmpty()){
			lore.add(ChatColor.GRAY + "Description : " + description.get(0));
			try{
				lore.add(ChatColor.GRAY + description.get(1));
				lore.add(ChatColor.GRAY + description.get(2));
			}
			catch(IndexOutOfBoundsException e){
				//no more lore;
			}
		}
		
		meta.setLore(lore);
		
		newItem.setItemMeta(meta);
		
		return newItem;
	}
	
	public ArrayList<String> addSockets(Integer min, Integer max, ArrayList<String> lore){
		Integer slotCount = new Random().nextInt(max - min) + min;
		for(int i = 0; i < slotCount; i++){
			lore.add(ChatColor.GRAY + "○ - <empty>");
		}
		return lore;
	}

}
