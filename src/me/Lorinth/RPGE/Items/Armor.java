package me.Lorinth.RPGE.Items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Armor extends Gear{

	private Material material;
	public Integer level;
	private ItemGenerator IG;
	public String name;
	public Integer armor;
	public Integer durability;
	private ArmorType type;

	public Armor(String Name, Integer Level, Integer Armor, Material item, Integer dura, ItemGenerator ig, ArmorType t) {
		material = item;
		level = Level;
		armor = Armor;
		//this.type = type;
		name = Name;
		durability = dura;
		IG = ig;
		type = t;
	}

	public Material getMaterial(){
		return material;
	}
	
	public ItemStack spawnItem(Object loc, String rarity, ArrayList<Title> titles) {
		ItemStack newItem = new ItemStack(material);
		ItemMeta meta = newItem.getItemMeta();
		
		Integer copyArmor = armor;
		Integer copyDura = durability;

		//Sockets
		Integer minS = 0;
		Integer maxS = 1;
		
		Integer value = 2 * level;
		
		ChatColor rareColor = null;
		String rarityLine = "";
		
		//Color and name the item
		switch(rarity){
			case("Common"):
				rareColor = ChatColor.GRAY;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Common";
				break;
			case("Uncommon"):
				rareColor = ChatColor.GREEN;
				copyArmor += 1;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Uncommon";
				
				value += 10 + (level / 4);
				copyDura += 75;
				
				maxS = 2;
				
				break;
			case("Rare"):
				rareColor = ChatColor.AQUA;
				copyArmor += 2;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Rare";
				
				value += 20 + (level / 2);
				copyDura += 150;
				
				maxS = 2;
				
				break;
			case("Epic"):
				rareColor = ChatColor.DARK_RED;
				copyArmor += 4;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Epic";
				
				value += 30 + level;
				copyDura += 300;
				
				minS = 1;
				maxS = 2;
				
				break;
			case("Legendary"):
				rareColor = ChatColor.DARK_PURPLE;
				copyArmor += 7;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Legendary";
				
				value += (int) (50 + level * 1.5);
				copyDura += 600;
				
				minS = 1;
				maxS = 3;
				
				break;
			case("Godly"):
				rareColor = ChatColor.GOLD;
				copyArmor += 10;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Godly";
				copyDura += 750;
				value += (int) (75 + level * 2.5);
				
				minS = 1;
				maxS = 4;
				break;
		}
		
		String copiedName = "";
		
		Integer parmor = 0;
		Integer marmor = 0;
		
		
		if(type == ArmorType.Heavy){
			parmor = copyArmor;
			marmor = copyArmor/2;
		}
		else if(type == ArmorType.Medium){
			parmor = (3 * copyArmor) / 4;
			marmor = parmor;
		}
		else if(type == ArmorType.Light){
			parmor = copyArmor / 2;
			marmor = copyArmor;
		}
		
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_RED +      "Armor : " + parmor);
		lore.add(ChatColor.LIGHT_PURPLE + "Magic Armor : " + marmor);
		Integer Level = (int)(level * 0.8);
		if(Level < 1){
			Level = 1;
		}
		lore.add(ChatColor.GOLD +      "Level : " + Level);
		lore.add(rarityLine);
		lore.add(ChatColor.GRAY + "Type : " + this.type.toString());
		lore.add(ChatColor.GRAY + "Durability : " + copyDura + "/" + copyDura);
		
		if(!titles.isEmpty()){
			lore.add("");
			for(Title title : titles){
				//copiedName += title.name + " ";
				HashMap<String, Integer> Terms = title.terms;
				//Iterate through the different terms in the title
				for(String key : Terms.keySet()){
					//Make sure its not already in the item, if it is add to the value
					if(key.equals("Armor")){
						parmor += Terms.get(key);
						String line = ChatColor.DARK_RED + "Armor : " + parmor; 
						lore.set(0, line);
					}
					if(key.equals("Magic Armor")){
						marmor += Terms.get(key);
						String line = ChatColor.LIGHT_PURPLE + "Magic Armor : " + marmor;
						lore.set(1, line);
					}
					else{
						boolean Found = false;
						for(String line : lore){
							List<String> words = Arrays.asList(line.split(" "));
							//If the term is the same...
							if(words.get(0).contains(key)){
								//parse to int and add to it...
								Integer number = Integer.parseInt(words.get(words.size() - 1));
								number += Terms.get(key);
								Found = true;
							}
						}
						if(!Found){
							String line = rareColor + key + " : " + Terms.get(key);
							lore.add(line);
						}
					}
				}
			}
		}
		
		meta.setDisplayName(rareColor + name);
		
		lore = IG.addSockets(minS, maxS, lore);
		
		lore.add("");
		
		lore.add(ChatColor.GRAY + "Value : " + ChatColor.GOLD + value);
		
		meta.setLore(lore);
		
		newItem.setItemMeta(meta);
		
		if(loc instanceof Location){
			((Location) loc).getWorld().dropItem((Location) loc, newItem);
		}
		else if(loc instanceof Player){
			((Player) loc).getInventory().addItem(newItem);
		}
		
		return newItem;
	}
	
}
