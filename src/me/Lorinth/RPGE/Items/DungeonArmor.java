package me.Lorinth.RPGE.Items;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DungeonArmor extends Gear{

	private Material material;
	public Integer level;
	public String name;
	public Integer armor;
	public Integer durability;
	public String type;
	
	private ArrayList<String> description = new ArrayList<String>();

	public DungeonArmor(String Name, Integer Level, Integer Armor, Material item, Integer dura, String type) {
		material = item;
		level = Level;
		armor = Armor;
		name = Name;
		durability = dura;
		this.type = type;
	}
	
	public void setDescription(List<String> lore){
		for(String line : lore){
			String newline = convertColors(line);
			this.description.add(newline);
		}
	}

	public String convertColors(String line){
		line = line.replace("&0", ChatColor.BLACK + "");
		line = line.replace("&1", ChatColor.DARK_BLUE + "");
		line = line.replace("&2", ChatColor.DARK_GREEN + "");
		line = line.replace("&3", ChatColor.DARK_AQUA + "");
		line = line.replace("&4", ChatColor.DARK_RED + "");
		line = line.replace("&5", ChatColor.DARK_PURPLE + "");
		line = line.replace("&6", ChatColor.GOLD + "");
		line = line.replace("&7", ChatColor.GRAY + "");
		line = line.replace("&8", ChatColor.DARK_GRAY + "");
		line = line.replace("&9", ChatColor.BLUE + "");
		line = line.replace("&a", ChatColor.GREEN + "");
		line = line.replace("&b", ChatColor.AQUA + "");
		line = line.replace("&c", ChatColor.RED + "");
		line = line.replace("&d", ChatColor.LIGHT_PURPLE + "");
		line = line.replace("&e", ChatColor.YELLOW + "");
		line = line.replace("&f", ChatColor.WHITE + "");
		return line;
	}
	
	public Material getMaterial(){
		return material;
	}
	
	@Override
	public ItemStack spawnItem(String rarity, ArrayList<Title> titles) {
		try{
			if(material.equals(null)){
				return null;
			}
		}
		catch(NullPointerException e){
			return null;
		}
		ItemStack newItem = new ItemStack(material);
		ItemMeta meta = newItem.getItemMeta();
		
		Integer copyArmor = armor;
		Integer copyDura = durability;

		Integer value = 2 * level;
		
		Integer minS = 0;
		Integer maxS = 1;
		
		ChatColor rareColor = null;
		String rarityLine = "";
		
		//Color and name the item
		switch(rarity){
			case("Common"):
				rareColor = ChatColor.GRAY;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Common";
				break;
			case("Uncommon"):
				rareColor = ChatColor.GREEN;
				copyArmor += 1;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Uncommon";
				
				value += 10 + (level / 4);
				copyDura += 75;
				
				maxS = 2;
				
				break;
			case("Rare"):
				rareColor = ChatColor.AQUA;
				copyArmor += 2;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Rare";
				
				value += 20 + (level / 2);
				copyDura += 150;
				
				maxS = 2;
				
				break;
			case("Epic"):
				rareColor = ChatColor.DARK_RED;
				copyArmor += 4;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Epic";
				
				value += 30 + level;
				copyDura += 300;
				
				minS = 1;
				maxS = 2;
				
				break;
			case("Legendary"):
				rareColor = ChatColor.DARK_PURPLE;
				copyArmor += 7;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Legendary";
				
				value += (int) (50 + level * 1.5);
				copyDura += 600;
				
				minS = 1;
				maxS = 3;
				
				break;
			case("Godly"):
				rareColor = ChatColor.GOLD;
				copyArmor += 10;
				rarityLine = ChatColor.GRAY + "Rarity : " + rareColor + "Godly";
				copyDura += 750;
				value += (int) (75 + level * 2.5);
				
				minS = 1;
				maxS = 4;
				break;
		}
		
		String copiedName = "";
	
		Integer parmor = 0;
		Integer marmor = 0;
		
		if(type.equals("Heavy")){
			parmor = copyArmor;
			marmor = copyArmor/2;
		}
		else if(type.equals("Medium")){
			parmor = (3 * copyArmor) / 4;
			marmor = parmor;
		}
		else if(type.equals("Light")){
			parmor = copyArmor / 2;
			marmor = copyArmor;
		}
		
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.DARK_RED +      "Armor : " + parmor);
		lore.add(ChatColor.LIGHT_PURPLE + "Magic Armor : " + marmor);
		Integer Level = level;
		if(Level < 1){
			Level = 1;
		}
		lore.add(ChatColor.GOLD +      "Level : " + Level);
		lore.add(rarityLine); 
		lore.add(ChatColor.GRAY + "Type : " + type);
		lore.add(ChatColor.GRAY + "Durability : " + copyDura + "/" + copyDura);
		
		if(!titles.isEmpty()){
			lore.add("");
			for(Title title : titles){
				copiedName += title.name + " ";
				HashMap<String, Integer> Terms = title.terms;
				//Iterate through the different terms in the title
				for(String key : Terms.keySet()){
					//Make sure its not already in the item, if it is add to the value
					if(key.equals("Armor")){
						copyArmor += Terms.get(key);
						String line = ChatColor.DARK_RED + "Armor : " + copyArmor; 
						lore.set(0, line);
					}
					else{
						boolean Found = false;
						for(String line : lore){
							List<String> words = Arrays.asList(line.split(" "));
							//If the term is the same...
							if(words.get(0).contains(key)){
								//parse to int and add to it...
								Integer number = Integer.parseInt(words.get(words.size() - 1));
								number += Terms.get(key);
								Found = true;
							}
						}
						if(!Found){
							String line = rareColor + key + " : " + Terms.get(key);
							lore.add(line);
						}
					}
				}
			}
		}
		
		meta.setDisplayName(rareColor + name);
		lore.add("");
		
		lore = addSockets(minS, maxS, lore);
		
		lore.add(ChatColor.GRAY + "Value : " + ChatColor.GOLD + value);
		
		if(!this.description.isEmpty()){
			lore.add(ChatColor.GRAY + "Description : " + description.get(0));
			try{
				lore.add(ChatColor.GRAY + description.get(1));
				lore.add(ChatColor.GRAY + description.get(2));
			}
			catch(IndexOutOfBoundsException e){
				//no more lore;
			}
		}
		
		meta.setLore(lore);
		
		newItem.setItemMeta(meta);
		
		return newItem;
	}
	
	public ArrayList<String> addSockets(Integer min, Integer max, ArrayList<String> lore){
		Integer slotCount = new Random().nextInt(max - min) + min;
		for(int i = 0; i < slotCount; i++){
			lore.add(ChatColor.GRAY + "○ - <empty>");
		}
		return lore;
	}
	
}
