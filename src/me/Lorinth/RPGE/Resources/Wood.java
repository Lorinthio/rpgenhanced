package me.Lorinth.RPGE.Resources;

import java.util.ArrayList;

import me.Lorinth.RPGE.Characters.RPGSkill;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Wood extends Resource{

	public Wood(String Name, Material Type, Integer Level){
		super(Name, Type, Level, RPGSkill.WOODCUTTING);
	}
	
	public void spawnWood(Location loc){
		ItemStack wood = new ItemStack(this.getType());
		ItemMeta meta = wood.getItemMeta();
		
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GREEN + "Woodcutting Material");
		lore.add(ChatColor.GOLD + "Woodcutting : " + this.getLevel());
		
		Integer value = 10 + this.getLevel() * 2;
		
		lore.add(ChatColor.GRAY + "Value : " + ChatColor.GOLD + value);
		
		meta.setDisplayName(ChatColor.GREEN + this.getName());
		meta.setLore(lore);
		
		wood.setItemMeta(meta);
		
		loc.getWorld().dropItem(loc, wood);
	}
	
}
