package me.Lorinth.RPGE.Resources;

import java.util.ArrayList;

import me.Lorinth.RPGE.Characters.RPGSkill;
import net.md_5.bungee.api.ChatColor;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Ore extends Resource{

	//This will cover... stone/cobblestone, iron, gold, diamond
	
	public Ore(String Name, Material Type, Integer Level){
		super(Name, Type, Level, RPGSkill.MINING);
	}
	
	public void spawnOre(Location loc){
		ItemStack ore = new ItemStack(this.getType());
		ItemMeta meta = ore.getItemMeta();
		
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GREEN + "Mining Material");
		lore.add(ChatColor.GOLD + "Mining : " + this.getLevel());
		
		Integer value = 10 + this.getLevel() * 2;
		
		lore.add(ChatColor.GRAY + "Value : " + ChatColor.GOLD + value);
		
		meta.setDisplayName(ChatColor.GREEN + this.getName());
		meta.setLore(lore);
		
		ore.setItemMeta(meta);
		
		loc.getWorld().dropItem(loc, ore);
	}
	
}
