package me.Lorinth.RPGE.Resources;

import me.Lorinth.RPGE.Characters.RPGSkill;
import org.bukkit.Material;

public abstract class Resource {

	private String name;
	private Material type;
	private Integer minLevel;
	private RPGSkill skill;
	
	public Resource(String name, Material appear, Integer level, RPGSkill st){
		this.name = name;
		this.type = appear;
		minLevel = level;
		skill = st;
	}
	
	public Material getType(){
		return type;
	}
	
	public String getName(){
		return name;
	}
	
	public Integer getLevel(){
		return minLevel;
	}
	
	public Integer getTargetLevel(){
		return minLevel + 5;
	}
}
